<?php
	$this->set_css($this->default_theme_path.'/no-flexigrid-ftk-productivity/css/flexigrid.css');
	$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
	$this->set_js_lib($this->default_javascript_path.'/common/lazyload-min.js');

	if (!$this->is_IE7()) {
		$this->set_js_lib($this->default_javascript_path.'/common/list.js');
	}

	$this->set_js($this->default_theme_path.'/no-flexigrid-ftk-productivity/js/cookies.js');
	$this->set_js($this->default_theme_path.'/no-flexigrid-ftk-productivity/js/flexigrid.js');
	$this->set_js($this->default_theme_path.'/no-flexigrid-ftk-productivity/js/jquery.form.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.numeric.min.js');
	$this->set_js($this->default_theme_path.'/no-flexigrid-ftk-productivity/js/jquery.printElement.min.js');

	/** Fancybox */
	$this->set_css($this->default_css_path.'/jquery_plugins/fancybox/jquery.fancybox.css');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.fancybox-1.3.4.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.easing-1.3.pack.js');

	/** Jquery UI */
	$this->load_js_jqueryui();

    $crud = new folder_ftk_productivity();


?>
<script type='text/javascript'>
	var base_url = '<?php echo base_url();?>';

	var subject = '<?php echo $subject?>';
	var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
	var unique_hash = '<?php echo $unique_hash; ?>';
	var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";

</script>

    

<div class="col-md-3">          
    <div class="box box-primary">
        <form action="" method="get">
            <div class="box-body">
                <div class="form-group">
                    <label>{{ language:Saved Period }}</label>
                    <select data-live-search="true" style="width: 100%;" class="selectpicker form-control show-tick" data-show-subtext="true" data-container="body"  name="period" id="period">
                        <?php echo $crud->select_option_period();?>
                    </select>
                </div>

                <div class="form-group">
                    <label>{{ language:Business Area }}</label>
                    <select data-live-search="true" style="width: 100%;" class="selectpicker form-control show-tick" data-show-subtext="true" data-container="body" name="business" id="business">                      
                        <?php echo $crud->select_option_business_unit();?>
                    </select>
                </div>
                <button type="submit" id="search-btn" class="btn btn-primary btn-block margin-bottom btn-flat"><i class="fa fa-search"></i> {{ language:Show }}</button>
            </div>
        </form>
    </div>          
</div>
<div class="col-md-9">        
    <div id="non-printable" class="no-flexigrid box box-default" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
	   <div id="hidden-operations" class="hidden-operations"></div> 

        <div class="box-header with-border">
            <div class="pull-right box-tools">                
                <a class="btn btn-default btn-sm btn-flat pull-right" id="btnExcel" title="<?php echo $this->l('list_export');?>" data-url="<?php echo $export_url; ?>" target="_blank" style="margin-right: 5px;">
                    <div class="fbutton">
                        <div>
                            <span class="export"><i class="fa fa-file-excel-o fa-lg"></i></span>
                        </div>
                    </div>
                </a>               
            </div>
            <h3 class="box-title"><?php echo $subject?></h3>
        </div>

	   <div id='main-table-box' class="main-table-box box-body">
        	<div id='ajax_list' class="ajax_list">
        		<?php echo $list_view?>
        	</div>    	
    	</div>
    </div>
</div>

<script type="text/javascript">

    $('.selectpicker').selectpicker({
      style: 'btn-default',size: "auto"
    });

</script>