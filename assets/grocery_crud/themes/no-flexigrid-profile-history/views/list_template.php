<?php
	$this->set_css($this->default_theme_path.'/no-flexigrid-profile-history/css/flexigrid.css');
	$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
	$this->set_js_lib($this->default_javascript_path.'/common/lazyload-min.js');

	if (!$this->is_IE7()) {
		$this->set_js_lib($this->default_javascript_path.'/common/list.js');
	}

	$this->set_js($this->default_theme_path.'/no-flexigrid-profile-history/js/cookies.js');
	$this->set_js($this->default_theme_path.'/no-flexigrid-profile-history/js/flexigrid.js');
	$this->set_js($this->default_theme_path.'/no-flexigrid-profile-history/js/jquery.form.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.numeric.min.js');
	$this->set_js($this->default_theme_path.'/no-flexigrid-profile-history/js/jquery.printElement.min.js');

	/** Fancybox */
	$this->set_css($this->default_css_path.'/jquery_plugins/fancybox/jquery.fancybox.css');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.fancybox-1.3.4.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.easing-1.3.pack.js');

	/** Jquery UI */
	$this->load_js_jqueryui();

    $crud = new raw_data_management();


?>
<script type='text/javascript'>
	var base_url = '<?php echo base_url();?>';

	var subject = '<?php echo $subject?>';
	var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
	var unique_hash = '<?php echo $unique_hash; ?>';

	var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";

</script>
<div id='list-report-error' class='report-div error alert alert-danger'></div>
<div id='list-report-success' class='report-div success report-list alert alert-success ' <?php if($success_message !== null){?>style="display:block"<?php }?>><?php
if($success_message !== null){?>
	<p><?php echo $success_message; ?></p>
<?php }
?></div>
<div class="flexigrid box box-default" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
	<div id="hidden-operations" class="hidden-operations"></div>
	<div class="">
		<div class="ftitle">
			<div class='box-header with-border'>
                <h3 class="box-title"><?php echo $subject?></h3>

                <div class="pull-right box-tools">
                
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" title="Menu">
                    <i class="fa fa-bars"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li class="<?php echo $crud->navigation_name('');?>"><a href="{{ MODULE_SITE_URL }}raw_data_management"><i class="fa fa-user"></i>{{ language:Profile }}</a></li>
                    <li class="<?php echo $crud->navigation_name('position_history');?>"><a href="{{ MODULE_SITE_URL }}raw_data_management/position_history"><i class="fa fa-industry"></i>{{ language:Position History }}</a></li>
                    <li class="<?php echo $crud->navigation_name('training_history');?>"><a href="{{ MODULE_SITE_URL }}raw_data_management/training_history"><i class="fa fa-briefcase"></i>{{ language:Training History }}</a></li>
                    <li class="<?php echo $crud->navigation_name('education_history');?>"><a href="{{ MODULE_SITE_URL }}raw_data_management/education_history"><i class="fa fa-graduation-cap"></i>{{ language:Education History }}</a></li>
                    <li class="<?php echo $crud->navigation_name('talent_history');?>"><a href="{{ MODULE_SITE_URL }}raw_data_management/talent_history"><i class="fa fa-trophy"></i>{{ language:Talent History }}</a></li>
                    <li class="<?php echo $crud->navigation_name('profession_history');?>"><a href="{{ MODULE_SITE_URL }}raw_data_management/profession_history"><i class="fa fa-black-tie"></i>{{ language:Profession History }}</a></li>
                  </ul>
                </div>

                <?php if(!$unset_add){?>
        
                    <a href='<?php echo $add_url?>' title='<?php echo $this->l('list_add'); ?>' class='add-anchor add_button btn btn-default btn-flat'>
                    <div class="fbutton">
                        <div>
                            <i class="fa fa-plus-circle"></i>
                        </div>
                    </div>
                    </a>           
               
                <?php }?>


                <?php if(!$unset_export) { ?>
                    <a class="export-anchor btn btn-default btn-flat" data-url="<?php echo $export_url; ?>" target="_blank" title="{{ language:Export }}">
                    <div class="fbutton">
                        <div>
                            <span class="export"><i class="fa fa-file-excel-o"></i></span>
                        </div>
                    </div>
                </a>
                <?php } ?>                
            
              </div>

            </div>
		</div>
	</div>
	<div id='main-table-box' class="main-table-box box-body scrollit">

	<?php if(!$unset_add || !$unset_export || !$unset_print){?>
	<div class="tDiv">

        <div class="tDiv2">            
            <form action="<?php echo site_url('talent_pool/raw_data_management/do_upload_'.$crud->cms_nav_name());?>" method="post" accept-charset="utf-8" class="form-inline" enctype="multipart/form-data">
                <input type="file" id="file_upload" name="userfile" size="20" />&nbsp;
                <div class="btnseparator"></div>
                <button type="submit" name="search" id="search-btn" class="btn btn-primary btn-flat"><i class="fa fa-upload"></i> <?php echo $this->l('upload_data');?> (csv)</button>&nbsp;&nbsp;
                <a href="#" data-toggle="tooltip" title="{{ language:Please make sure your data before upload }}" data-trigger="hover" data-html="true"><i class="fa fa-question-circle"></i></a>
            </form>
            <div class="btnseparator"></div>
        </div>

		
		<div class="tDiv3">

        <!--
        <div class="btn-group">
            <a href="{{ MODULE_SITE_URL }}raw_data_management" class="btn btn-default <?php echo $crud->navigation_name('');?>" title="{{ language:Profile }}"><i class="fa fa-user"></i></a>

            <a href="{{ MODULE_SITE_URL }}raw_data_management/position_history" class="btn btn-default <?php echo $crud->navigation_name('position_history');?>" title="{{ language:Position History }}"><i class="fa fa-industry"></i></a>

            <a href="{{ MODULE_SITE_URL }}raw_data_management/training_history" class="btn btn-default <?php echo $crud->navigation_name('training_history');?>" title="{{ language:Training History }}"><i class="fa fa-briefcase"></i></a>

            <a href="{{ MODULE_SITE_URL }}raw_data_management/education_history" class="btn btn-default <?php echo $crud->navigation_name('education_history');?>" title="{{ language:Education History }}"><i class="fa fa-graduation-cap"></i></a>

            <a href="{{ MODULE_SITE_URL }}raw_data_management/talent_history" class="btn btn-default <?php echo $crud->navigation_name('talent_history');?>" title="{{ language:Talent History }}"><i class="fa fa-trophy"></i></a>

            <a href="{{ MODULE_SITE_URL }}raw_data_management/profession_history" class="btn btn-default <?php echo $crud->navigation_name('profession_history');?>" title="{{ language:Profession History }}"><i class="fa fa-black-tie"></i></a>
        </div>
        -->
        &nbsp;
        &nbsp;

        
		
        </div>
	</div>
	<?php }?>

	<div id='ajax_list' class="ajax_list">
		<?php echo $list_view?>
	</div>
	<?php echo form_open( $ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="'.$ajax_list_info_url.'"'); ?>
	<div class="sDiv quickSearchBox form-inline" id='quickSearchBox'>
	    <div class="form-group">
            <input type="text" class="qsbsearch_fieldox search_text form-control" name="search_text" id="search_text" placeholder="<?php echo $this->l('list_search');?>">
        </div>
        <div class="form-group">
            <select name="search_field" id="search_field" class="form-control">
                <option value=""><?php echo $this->l('list_search_all');?></option>
                <?php foreach($columns as $column){?>
                <option value="<?php echo $column->field_name?>"><?php echo $column->display_as?>&nbsp;&nbsp;</option>
                <?php }?>
            </select>
        </div>
        <div class="form-group">
            <input type="button" value="<?php echo $this->l('list_search');?>" class="crud_search btn btn-primary btn-flat form-control" id='crud_search'>
        </div>
        <div class="form-group">
            <input type="button" value="<?php echo $this->l('list_clear_filtering');?>" id='search_clear' class="search_clear btn btn-primary btn-flat form-control">
        </div>
	</div>
	<div class="pDiv">
		<div class="pDiv2">
            <div class="pGroup">
                <div class="pSearch pButton quickSearchButton" id='quickSearchButton' title="<?php echo $this->l('list_search');?>">
                    <span><i class="glyphicon glyphicon-search"></i></span>
                </div>
            </div>
            <div class="btnseparator">
            </div>
            <div class="pGroup">
                <select name="per_page" id='per_page' class="per_page">
                    <?php foreach($paging_options as $option){?>
                        <option value="<?php echo $option; ?>" <?php if($option == $default_per_page){?>selected="selected"<?php }?>><?php echo $option; ?>&nbsp;&nbsp;</option>
                    <?php }?>
                </select>
                <input type='hidden' name='order_by[0]' id='hidden-sorting' class='hidden-sorting' value='<?php if(!empty($order_by[0])){?><?php echo $order_by[0]?><?php }?>' />
                <input type='hidden' name='order_by[1]' id='hidden-ordering' class='hidden-ordering'  value='<?php if(!empty($order_by[1])){?><?php echo $order_by[1]?><?php }?>'/>
            </div>
            <div class="btnseparator">
            </div>
            <div class="pGroup">
                <div class="pFirst pButton first-button">
                    <span>&nbsp;<i class="glyphicon glyphicon-fast-backward"></i></span>
                </div>
                <div class="pPrev pButton prev-button">
                    <span>&nbsp;<i class="glyphicon glyphicon-backward"></i>&nbsp;</span>
                </div>
            </div>
            <div class="btnseparator">
            </div>
            <div class="pGroup form-inline">
                <span class="pcontrol form-group">
                    <label><?php echo $this->l('list_page'); ?></label>
                    <input name='page' type="text" value="1" size="4" id='crud_page' class="crud_page">
                    <label><?php echo $this->l('list_paging_of'); ?></label>
                    <label id='last-page-number' class="last-page-number"><?php echo ceil($total_results / $default_per_page)?></label>
                </span>
            </div>
            <div class="btnseparator">
            </div>
            <div class="pGroup">
                <div class="pNext pButton next-button" >
                    <span>&nbsp;<i class="glyphicon glyphicon-forward"></i>&nbsp;</span>
                </div>
                <div class="pLast pButton last-button">
                    <span><i class="glyphicon glyphicon-fast-forward"></i>&nbsp;</span>
                </div>
            </div>
            <div class="btnseparator">
            </div>
            <div class="pGroup">
                <div class="pReload pButton ajax_refresh_and_loading" id='ajax_refresh_and_loading'>
                    <span><i class="glyphicon glyphicon-refresh"></i></span>
                </div>
            </div>
            <div class="btnseparator">
            </div>
            <div class="pGroup">
                <span class="pPageStat">
                    <?php $paging_starts_from = "<span id='page-starts-from' class='page-starts-from'>1</span>"; ?>
                    <?php $paging_ends_to = "<span id='page-ends-to' class='page-ends-to'>". ($total_results < $default_per_page ? $total_results : $default_per_page) ."</span>"; ?>
                    <?php $paging_total_results = "<span id='total_items' class='total_items'>$total_results</span>"?>
                    <?php echo str_replace( array('{start}','{end}','{results}'),
                                            array($paging_starts_from, $paging_ends_to, $paging_total_results),
                                            $this->l('list_displaying')
                                           ); ?>
                </span>
            </div>
        </div>
		<div style="clear: both;">
		</div>
	</div>
	<?php echo form_close(); ?>
	</div>
</div>

<style type="text/css">
    .scrollit {
        overflow-y:scroll;
        background:#ffffff;
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){
        var height_my_device = screen.height;
        var height_my_header = $('.navbar-static-top:visible').height();
        var height_my_footer = $('.main-footer:visible').height();
        var height_my_nav    = $('.content-header:visible').height();
        var height_my_tool   = $('#nav_tools:visible').height();
        var height_my_diagram = (height_my_device-(height_my_header+height_my_footer+height_my_nav+height_my_tool+200));
        $(".scrollit").css("height", height_my_diagram+"px");         
    })
</script>
