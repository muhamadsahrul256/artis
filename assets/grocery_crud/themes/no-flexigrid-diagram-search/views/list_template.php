<?php
	$this->set_css($this->default_theme_path.'/no-flexigrid-diagram-search/css/flexigrid.css');
	$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
	$this->set_js_lib($this->default_javascript_path.'/common/lazyload-min.js');

	if (!$this->is_IE7()) {
		$this->set_js_lib($this->default_javascript_path.'/common/list.js');
	}

	$this->set_js($this->default_theme_path.'/no-flexigrid-diagram-search/js/cookies.js');
	$this->set_js($this->default_theme_path.'/no-flexigrid-diagram-search/js/flexigrid.js');
	$this->set_js($this->default_theme_path.'/no-flexigrid-diagram-search/js/jquery.form.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.numeric.min.js');
	$this->set_js($this->default_theme_path.'/no-flexigrid-diagram-search/js/jquery.printElement.min.js');

	/** Fancybox */
	$this->set_css($this->default_css_path.'/jquery_plugins/fancybox/jquery.fancybox.css');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.fancybox-1.3.4.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.easing-1.3.pack.js');

	/** Jquery UI */
	$this->load_js_jqueryui();

?>
<script type='text/javascript'>
	var base_url = '<?php echo base_url();?>';

	var subject = '<?php echo $subject?>';
	var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
	var unique_hash = '<?php echo $unique_hash; ?>';

	var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";

</script>
<div id='list-report-error' class='report-div error alert alert-danger'></div>
<div id='list-report-success' class='report-div success report-list alert alert-success ' <?php if($success_message !== null){?>style="display:block"<?php }?>><?php
if($success_message !== null){?>
	<p><?php echo $success_message; ?></p>
<?php }
?></div>
<div class="flexigrid box box-default" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
	<div id="hidden-operations" class="hidden-operations"></div>
	<div class="mDiv">
		<div class="ftitle">
			<div class='box-header with-border'>
                <h3 class="box-title"><?php echo $subject?></h3>
            </div>
		</div>
	</div>
	<div id='main-table-box' class="main-table-box box-body">

	<div class="tDiv">       
        
		<div class="tDiv3">

            <a href="{{ base_url }}organization_mapping/diagram_frame" class="btn btn-default btn-flat">
                <div class="fbutton">
                    <div>
                        <span class="export"><i class="fa fa-sitemap"></i>&nbsp;{{ language:Back to chart }}</span>
                    </div>
                </div>
            </a>&nbsp;
            <div class="btnseparator"></div>

            <a href="javascript:void(0);" class="btn btn-default btn-flat launch-search" onclick="edit_person('<?php echo $primary_key;?>')" data-id="<?php echo $primary_key;?>">
                <div class="fbutton">
                    <div>
                        <span class="export"><i class="fa fa-search"></i>&nbsp;{{ language:Search Again }}</span>
                    </div>
                </div>
            </a>&nbsp;
     


            <div class="btnseparator"></div>
            <!--
            <a class="btn btn-default btn-flat" href="#">
                <i class="fa fa-file-pdf-o"></i> {{ language:Print Selected }}
            </a>
            -->
            <!--
            <a class="btn btn-default btn-flat" id="btnExcel" target="_blank">
                <i class="fa fa-file-excel-o"></i> {{ language:Export Selected }}
            </a>
            -->


            <a class="print_all_button btn btn-default btn-flat" href="#">
                <i class="fa fa-file-excel-o"></i> {{ language:Export Selected }}
            </a>

            
		</div>
		<div class='clear'></div>
	</div>


	<div id='ajax_list' class="ajax_list">
		<?php echo $list_view?>
	</div>



	</div>
</div>

<!--
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});
</script>
-->