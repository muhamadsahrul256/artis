<?php
    
    $this->set_css($this->default_theme_path.'/no-flexigrid-hcr-ocr/css/myflex.css');
    $this->set_css($this->default_theme_path.'/no-flexigrid-hcr-ocr/css/flexigrid.css');
    $this->set_js_lib($this->default_theme_path.'/no-flexigrid-hcr-ocr/js/jquery.form.js');
    $this->set_js_config($this->default_theme_path.'/no-flexigrid-hcr-ocr/js/flexigrid-edit.js');

    $this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
    $this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<script type="text/javascript">
$(document).ready(function(){

    $('input[type=text]').addClass('form-control');
    $('textarea').addClass('form-control');

    $(".datepicker-input").datepicker({showOn: 'focus', dateFormat: 'dd/mm/yy', width:'100%',showButtonPanel: false});

});
</script>
<div class="crud-form box box-warning" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <div class="mDiv">
        <div class="ftitle">
            <div class='box-header with-border'>
                <h3 class="box-title"><?php echo $this->l('form_edit'); ?> <?php echo $subject?></h3>
            </div>
            <div class='clear'></div>
        </div>
    </div>
<div id='main-table-box' class="box-body">
    <?php echo form_open( $update_url, 'method="post" id="crudForm" autocomplete="off" class="form-horizontal" enctype="multipart/form-data"'); ?>

    <?php if(!empty($hidden_fields)){?>
        <?php
            foreach($hidden_fields as $hidden_field){
                echo $hidden_field->input;
            }
        ?>
    <?php }?>



    <div class='row'>       
  
      <!-- left column -->
      <!--
      <div class="col-md-3">
        <div class="text-center">
          <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
          <h6>Upload a different photo...</h6>
          
          <?php echo $input_fields['Photos']->input; ?>
        </div>
      </div>
    -->


    <div class="col-md-3">
        <div class="text-center">          
          <?php echo $input_fields['Photos']->input; ?>          
        </div>
    </div>

<!--
      <div class='avatar img-circle' id="Photos_input_box">
                            <span class="fileinput-button qq-upload-button" id="upload-button-219980838" style="display:none;">
      <span>Upload a file</span>
      <input type="file" name="s5daaf223" class="gc-file-upload" rel="http://astelapps.unias.com/hris2/karyawan/frmProfile/index/upload_file/Photos" id="219980838">
      <input class="hidden-upload-input" type="hidden" name="Photos" value="e8acd-dompak.jpg" rel="s5daaf223" />
    </span>
    <div id='uploader_219980838' rel='219980838' class='grocery-crud-uploader avatar img-circle' style='display:none;'></div>
    <div id='success_219980838' class='upload-success-url' style=' padding-top:7px;'>
      <a href='http://astelapps.unias.com/hris2/assets/uploads/files/e8acd-dompak.jpg' id='file_219980838' class='avatar img-circle'>
        <img src='http://astelapps.unias.com/hris2/assets/uploads/files/e8acd-dompak.jpg' height='100px' class='avatar img-circle'></a>
        <a href='javascript:void(0)' id='delete_219980838' class='delete-anchor'>delete</a>
    </div>
        <div style='clear:both'></div>
        <div id='loading-219980838' style='display:none'>
          <span id='upload-state-message-219980838'></span>
          <span class='qq-upload-spinner'></span>
          <span id='progress-219980838'></span></div>
          <div style='display:none'>
            <a href='http://astelapps.unias.com/hris2/karyawan/frmProfile/index/upload_file/Photos' id='url_219980838'></a></div>
            <div style='display:none'>
              <a href='http://astelapps.unias.com/hris2/karyawan/frmProfile/index/delete_file/Photos' id='delete_url_219980838' rel='e8acd-dompak.jpg' ></a></div>                        </div>
                       

                    
-->


      



      
      <!-- edit form column -->
      <div class="col-md-9 personal-info">
        
        <h3>Personal info</h3>       
          <div class="form-group">
            <label class="col-lg-3 control-label"><?php echo $input_fields['Pers_No']->display_as; ?><?php echo ($input_fields['Pers_No']->required)? "<span class='required'> *</span> " : ""?></label>
            <div class="col-lg-8">
              <?php echo $input_fields['Pers_No']->input; ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label"><?php echo $input_fields['Full_Name']->display_as; ?><?php echo ($input_fields['Full_Name']->required)? "<span class='required'> *</span> " : ""?></label>
            <div class="col-lg-8">
              <?php echo $input_fields['Full_Name']->input; ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label"><?php echo $input_fields['Position_ID']->display_as; ?><?php echo ($input_fields['Position_ID']->required)? "<span class='required'> *</span> " : ""?></label>
            <div class="col-lg-8">
              <?php echo $input_fields['Position_ID']->input; ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label"><?php echo $input_fields['Unit_ID']->display_as; ?><?php echo ($input_fields['Unit_ID']->required)? "<span class='required'> *</span> " : ""?></label>
            <div class="col-lg-8">
              <?php echo $input_fields['Unit_ID']->input; ?>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-md-3 control-label"><?php echo $input_fields['Date_IN']->display_as; ?><?php echo ($input_fields['Date_IN']->required)? "<span class='required'> *</span> " : ""?></label>
            <div class="col-md-8">
              <?php echo $input_fields['Date_IN']->input; ?>
            </div>
          </div>
          

          <div class="form-group">
            <label class="col-md-3 control-label"><?php echo $input_fields['evidence']->display_as; ?><?php echo ($input_fields['evidence']->required)? "<span class='required'> *</span> " : ""?></label>
            <div class="col-md-8">
              <?php echo $input_fields['evidence']->input; ?>
            </div>
          </div>

          
      </div>
 
    </div>       

        
        <?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>
        <div id='report-error' class='report-div error alert alert-danger'></div>
        <div id='report-success' class='report-div success alert alert-success'></div>
    

    <div class="pDiv box-footer">
            <div class='form-button-box'>
                <input id="form-button-save" type='submit' value='<?php echo $this->l('form_save'); ?>'  class="btn btn-primary btn-large"/>
            </div>
<?php     if(!$this->unset_back_to_list) { ?>
            <div class='form-button-box'>
                <input type='button' value='<?php echo $this->l('form_save_and_go_back'); ?>' id="save-and-go-back-button"  class="btn btn-default btn-large"/>
            </div>
            <div class='form-button-box'>
                <input type='button' value='<?php echo $this->l('form_cancel'); ?>' class="btn btn-default btn-large" id="cancel-button" />
            </div>
<?php     } ?>
            <div class='form-button-box'>
                <div class='small-loading' id='FormLoading'><?php echo $this->l('form_insert_loading'); ?></div>
            </div>
            <div class='clear'></div>
        </div>    
    
    <?php echo form_close(); ?>
</div>
</div>
<script>
    var validation_url = '<?php echo $validation_url?>';
    var list_url = '<?php echo $list_url?>';

    var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
    var message_update_error = "<?php echo $this->l('update_error')?>";
</script>