<?php
	$today     = date('Y-m-d_H:i:s');
	
	$file_name = 'Export_'.$today;

	//$this->set_css($this->default_theme_path.'/datatables-default/css/demo_table_jui.css');
	$this->set_css($this->default_css_path.'/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS);
	//$this->set_css($this->default_theme_path.'/datatables-default/css/datatables.css');
	$this->set_css($this->default_theme_path.'/datatables-talent-pool/datatables/dataTables.bootstrap.css');
	//$this->set_css($this->default_theme_path.'/datatables-default/css/jquery.dataTables.css');
	//$this->set_css($this->default_theme_path.'/datatables-default/extras/TableTools/media/css/TableTools.css');
	$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
	$this->set_js_lib($this->default_javascript_path.'/common/lazyload-min.js');

	if (!$this->is_IE7()) {
		$this->set_js_lib($this->default_javascript_path.'/common/list.js');
	}

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);
	
	//$this->set_js_lib($this->default_theme_path.'/datatables-default/js/jquery.dataTables.min.js');
	//$this->set_js($this->default_theme_path.'/datatables-default/js/datatables-extras.js');
	$this->set_js($this->default_theme_path.'/datatables-talent-pool/js/datatables.js');
	//$this->set_js($this->default_theme_path.'/datatables-default/extras/TableTools/media/js/ZeroClipboard.js');
	//$this->set_js($this->default_theme_path.'/datatables-default/extras/TableTools/media/js/TableTools.min.js');
	
	$this->set_js($this->default_javascript_path.'/ex_excel/dist/jquery.table2excel.js');

	$this->set_js_lib($this->default_theme_path.'/datatables-talent-pool/datatables/jquery.dataTables.min.js');
	$this->set_js_lib($this->default_theme_path.'/datatables-talent-pool/datatables/dataTables.bootstrap.min.js');

	/** Fancybox */
	$this->set_css($this->default_css_path.'/jquery_plugins/fancybox/jquery.fancybox.css');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.fancybox-1.3.4.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.easing-1.3.pack.js');
?>
<style type="text/css">
	table.dataTable tbody td {
	  vertical-align: middle;
	  font-size: 12px;
	}
</style>
<script type='text/javascript'>
	var base_url = '<?php echo base_url();?>';
	var subject = '<?php echo $subject?>';

	var unique_hash = '<?php echo $unique_hash; ?>';

	var displaying_paging_string = "<?php echo str_replace( array('{start}','{end}','{results}'),
		array('_START_', '_END_', '_TOTAL_'),
		$this->l('list_displaying')
	   ); ?>";
	var filtered_from_string 	= "<?php echo str_replace('{total_results}','_MAX_',$this->l('list_filtered_from') ); ?>";
	var show_entries_string 	= "<?php echo str_replace('{paging}','_MENU_',$this->l('list_show_entries') ); ?>";
	var search_string 			= "<?php echo $this->l('list_search'); ?>";
	var list_no_items 			= "<?php echo $this->l('list_no_items'); ?>";
	var list_zero_entries 			= "<?php echo $this->l('list_zero_entries'); ?>";

	var list_loading 			= "<?php echo $this->l('list_loading'); ?>";

	var paging_first 	= "<?php echo $this->l('list_paging_first'); ?>";
	var paging_previous = "<?php echo $this->l('list_paging_previous'); ?>";
	var paging_next 	= "<?php echo $this->l('list_paging_next'); ?>";
	var paging_last 	= "<?php echo $this->l('list_paging_last'); ?>";

	var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";

	var default_per_page = <?php echo $default_per_page;?>;

	var unset_export = <?php echo ($unset_export ? 'true' : 'false'); ?>;
	var unset_print = <?php echo ($unset_print ? 'true' : 'false'); ?>;

	var export_text = '<?php echo $this->l('list_export');?>';
	var print_text = '<?php echo $this->l('list_print');?>';

	<?php
	//A work around for method order_by that doesn't work correctly on datatables theme
	//@todo remove PHP logic from the view to the basic library
	$ordering = 0;
	$sorting = 'asc';
	if(!empty($order_by))
	{
		foreach($columns as $num => $column) {
			if($column->field_name == $order_by[0]) {
				$ordering = $num;
				$sorting = isset($order_by[1]) && $order_by[1] == 'asc' || $order_by[1] == 'desc' ? $order_by[1] : $sorting ;
			}
		}
	}
	?>

	var datatables_aaSorting = [[ <?php echo $ordering; ?>, "<?php echo $sorting;?>" ]];

</script>
<script>
  $(function () {
    $("#example2").DataTable();
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "order": [[ 0, "desc" ],[ 1, "asc" ],[ 2, "asc" ]],
      "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
    });
  });
</script>

<?php
	if(!empty($actions)){
?>
	<style type="text/css">
		<?php foreach($actions as $action_unique_id => $action){?>
			<?php if(!empty($action->image_url)){ ?>
				.<?php echo $action_unique_id; ?>{
					background: url('<?php echo $action->image_url; ?>') !important;
				}
			<?php }?>
		<?php }?>
	</style>
<?php
	}
?>
<?php if($unset_export && $unset_print){?>
<style type="text/css">
	.datatables-add-button
	{
		position: static !important;
	}
</style>
<?php }?>
<div id='list-report-error' class='report-div error report-list'></div>
<div id='list-report-success' class='report-div success report-list' <?php if($success_message !== null){?>style="display:block"<?php }?>><?php
 if($success_message !== null){?>
	<p><?php echo $success_message; ?></p>
<?php }
?></div>
<?php if(!$unset_add){?>
<div class="datatables-add-button btn-group">

	<form action="<?php echo site_url('talent_pool/import_raw_data/do_upload');?>" method="post" accept-charset="utf-8" class="form-inline" enctype="multipart/form-data">
                <input type="file" id="file_upload" name="userfile" size="20" />&nbsp;
                <div class="btnseparator"></div>
                <button type="submit" name="search" id="search-btn" class="btn btn-primary"><i class="fa fa-upload"></i> Upload Data (xls,xlsx,csv)
            </form>

</div>
<?php }?>
<div style='height:10px;'></div>

<div class="box">
    <div class="box-header">
    	<div class="form-group">
            <h3 class="box-title">Data <?php echo $subject?></h3>

            <?php if(!$unset_export) { ?>

            <a class="btn btn-default btn-flat pull-right" id="btnExcel" data-url="<?php echo $export_url; ?>" target="_blank">
				<div class="fbutton">
					<div>
						<span class="fa fa-file-excel-o"></span> <?php echo $this->l('list_export');?>
					</div>
				</div>
            </a>                     
            <?php } ?>
            <?php if(!$unset_print) { ?>
        	<a class="btn btn-default btn-flat pull-right" id="btnPrint" data-url="<?php echo $print_url; ?>">
				<div class="fbutton">
					<div>
						<span class="fa fa-print"> <?php echo $this->l('list_print');?></span>
					</div>
				</div>
            </a>
			
			<?php }?>

        </div>        
 	</div>	

<div class="box-body">
	<?php echo $list_view?>
</div>
</div>

<script type="text/javascript">
$("#btnExcel").click(function(){
  $("#example1").table2excel({
    // exclude CSS class
    exclude: ".noExl",
    name: "Worksheet Name",
    filename: "<?php echo $file_name ?>",
    fileext: ".xls",
    exclude_img: true,
    exclude_links: true,
    exclude_inputs: true
  }); 
});
</script>

<script type="text/javascript">

$(function () {
    // $('#myModal').modal('hide');
});

document.getElementById("btnPrint").onclick = function () {
    printElement(document.getElementById("example1"));

    /**var modThis = document.querySelector("#printSection .modifyMe");
    modThis.appendChild(document.createTextNode(" new"));**/

}

function printElement(elem) {
    var domClone = elem.cloneNode(true);

    var $printSection = document.getElementById("printSection");

    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }   
    $printSection.innerHTML = "";

    $printSection.appendChild(domClone);

    document.title = '<?php echo $file_name; ?>';

    window.print();
}

</script>