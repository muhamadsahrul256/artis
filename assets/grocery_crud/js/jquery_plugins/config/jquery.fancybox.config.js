$(function(){
	var screen_width = $(window).width();
	var screen_height = $(window).height();

	if (screen_width >= 767){
		var dialog_height = $(window).height() - 100;
		var dialog_width = 850;
	}
	else{
		var dialog_height = $(window).height() - 200;
		var dialog_width = 'auto';
	}	

	$('.image-thumbnail').fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	false
	});

	$('.ui-dialog-content').dialog({
		'width'		: dialog_width,
		'height' 	: dialog_height,
	});	

	$('.ui-dialog-content').dialog().parent().position({
		
		/*my 	: 'center top',
		at 	: 'center top',
		of 	: $('.content-wrapper'),*/
		
	});
	

});	