<?php
$lang['language_alias'] = 'id';

$lang['Complete Menu'] = 'Menu Lengkap';
$lang['Home'] = 'Beranda';
$lang['Language'] = 'Bahasa';
$lang['Third Party Authentication'] = 'Autentikasi Pihak Ke Tiga';
$lang['Login'] = 'Masuk';
$lang['Logout'] = 'Keluar';
$lang['Forgot Password'] = 'Lupa Kata sandi';
$lang['Register'] = 'Daftar';
$lang['User Guide'] = 'Panduan Penggunaan';
$lang['Change Profile'] = 'Ubah Profil';
$lang['CMS Management'] = 'Manajemen CMS';
$lang['Group Management'] = 'Manajemen Grup';
$lang['User Management'] = 'Manajemen Pengguna';
$lang['Privilege Management'] = 'Manajemen Hak Akses';
$lang['Navigation Management'] = 'Manajemen Navigasi (Halaman)';
$lang['Widget Management'] = 'Manajemen Widget';
$lang['Module Management'] = 'Manajemen Modul';
$lang['Change Theme'] = 'Ubah Tema';
$lang['Quick Link Management'] = 'Manajemen Penghubung Cepat';
$lang['Configuration Management'] = 'Manajemen Konfigurasi';

$lang['User Info'] = 'Informasi Pengguna';
$lang['Share This Page !!'] = 'Bagikan Halaman Ini !!';

$lang['Welcome'] = 'Selamat Datang';

$lang['Username already exists'] = 'Username sudah ada';
$lang['Username is empty'] = 'Username kosong';