-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: artis
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog_article`
--

DROP TABLE IF EXISTS `blog_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_article` (
  `article_id` int(20) NOT NULL AUTO_INCREMENT,
  `article_title` varchar(100) DEFAULT NULL,
  `article_url` varchar(100) DEFAULT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `description` text,
  `date` timestamp NULL DEFAULT NULL,
  `author_user_id` int(20) DEFAULT NULL,
  `content` text,
  `allow_comment` int(20) DEFAULT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_article`
--

LOCK TABLES `blog_article` WRITE;
/*!40000 ALTER TABLE `blog_article` DISABLE KEYS */;
INSERT INTO `blog_article` VALUES (1,'ARTIS','-','-','-','2017-05-18 06:58:04',1,NULL,1);
/*!40000 ALTER TABLE `blog_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_category`
--

DROP TABLE IF EXISTS `blog_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_category` (
  `category_id` int(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category`
--

LOCK TABLES `blog_category` WRITE;
/*!40000 ALTER TABLE `blog_category` DISABLE KEYS */;
INSERT INTO `blog_category` VALUES (1,'News',NULL),(2,'Fun',NULL);
/*!40000 ALTER TABLE `blog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_category_article`
--

DROP TABLE IF EXISTS `blog_category_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_category_article` (
  `category_article_id` int(20) NOT NULL AUTO_INCREMENT,
  `category_id` int(20) DEFAULT NULL,
  `article_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`category_article_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category_article`
--

LOCK TABLES `blog_category_article` WRITE;
/*!40000 ALTER TABLE `blog_category_article` DISABLE KEYS */;
INSERT INTO `blog_category_article` VALUES (1,2,1);
/*!40000 ALTER TABLE `blog_category_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_comment`
--

DROP TABLE IF EXISTS `blog_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_comment` (
  `comment_id` int(20) NOT NULL AUTO_INCREMENT,
  `article_id` int(20) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `author_user_id` int(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `content` text,
  `parent_comment_id` int(20) DEFAULT NULL,
  `read` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_comment`
--

LOCK TABLES `blog_comment` WRITE;
/*!40000 ALTER TABLE `blog_comment` DISABLE KEYS */;
INSERT INTO `blog_comment` VALUES (1,1,'2013-03-25 02:53:16',1,NULL,NULL,NULL,'Great comment for great article',NULL,0);
/*!40000 ALTER TABLE `blog_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_photo`
--

DROP TABLE IF EXISTS `blog_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_photo` (
  `photo_id` int(20) NOT NULL AUTO_INCREMENT,
  `article_id` int(10) NOT NULL,
  `url` varchar(50) NOT NULL,
  PRIMARY KEY (`photo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_photo`
--

LOCK TABLES `blog_photo` WRITE;
/*!40000 ALTER TABLE `blog_photo` DISABLE KEYS */;
INSERT INTO `blog_photo` VALUES (1,1,'main_01.jpg'),(2,1,'main_02.jpg'),(3,1,'main_03.jpg'),(4,1,'main_04.jpg'),(5,1,'main_05.jpg');
/*!40000 ALTER TABLE `blog_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(50) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(20) NOT NULL,
  `user_data` text,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('458c146d83447b678e00f76221a86908','::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0',1497446735,'a:9:{s:8:\"cms_lang\";s:10:\"indonesian\";s:11:\"cms_old_url\";s:10:\"main/login\";s:21:\"__cms_navigation_name\";s:29:\"master_mapping_upload_history\";s:13:\"cms_user_name\";s:8:\"sysadmin\";s:11:\"cms_user_id\";s:1:\"3\";s:18:\"cms_user_real_name\";s:12:\"System Admin\";s:14:\"cms_user_email\";s:16:\"dompak@gmail.com\";s:15:\"cms_user_layout\";s:5:\"fixed\";s:16:\"cms_user_sidebar\";s:12:\"sidebar-mini\";}'),('5d9720f09b06deb301f46e58ab819e1b','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',1495095587,'a:1:{s:8:\"cms_lang\";s:10:\"indonesian\";}'),('a8dac90b36b8583e04ce7b26a07108fa','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495103192,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";s:11:\"cms_old_url\";s:0:\"\";}'),('3487ef895ee451e6af9e03c620329823','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495104586,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";s:11:\"cms_old_url\";s:0:\"\";}'),('65ac961dc67508aa5270f42c9468238f','172.17.8.3','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/',1495112577,'a:8:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:34:\"organization_mapping_diagram_frame\";s:13:\"cms_user_name\";s:8:\"sysadmin\";s:11:\"cms_user_id\";s:1:\"3\";s:18:\"cms_user_real_name\";s:12:\"System Admin\";s:14:\"cms_user_email\";s:16:\"dompak@gmail.com\";s:15:\"cms_user_layout\";s:5:\"fixed\";s:16:\"cms_user_sidebar\";s:12:\"sidebar-mini\";}'),('d1d31c7e896ab69ab93d9ffbc57aaae6','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495104707,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";s:11:\"cms_old_url\";s:0:\"\";}'),('4da5f5e874f1ad4b909f559e9ad4ea74','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495106036,'a:8:{s:8:\"cms_lang\";s:10:\"indonesian\";s:13:\"cms_user_name\";s:8:\"sysadmin\";s:11:\"cms_user_id\";s:1:\"3\";s:18:\"cms_user_real_name\";s:12:\"System Admin\";s:14:\"cms_user_email\";s:16:\"dompak@gmail.com\";s:15:\"cms_user_layout\";s:5:\"fixed\";s:16:\"cms_user_sidebar\";s:12:\"sidebar-mini\";s:21:\"__cms_navigation_name\";s:34:\"organization_mapping_diagram_frame\";}'),('6de05b932c5ec1585ddea28cd0eea4c0','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495106062,'a:2:{s:8:\"cms_lang\";s:10:\"indonesian\";s:11:\"cms_old_url\";s:31:\"hcr_ocr/folder_ftk_productivity\";}'),('07aaafac4e2e1a913dee48370b2845a2','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495108310,'a:10:{s:8:\"cms_lang\";s:10:\"indonesian\";s:11:\"cms_old_url\";N;s:21:\"__cms_navigation_name\";s:31:\"talent_pool_raw_data_management\";s:13:\"cms_user_name\";s:8:\"sysadmin\";s:11:\"cms_user_id\";s:1:\"3\";s:18:\"cms_user_real_name\";s:12:\"System Admin\";s:14:\"cms_user_email\";s:16:\"dompak@gmail.com\";s:15:\"cms_user_layout\";s:5:\"fixed\";s:16:\"cms_user_sidebar\";s:12:\"sidebar-mini\";s:19:\"flash:old:msg_excel\";s:22:\"Data berhasil disimpan\";}'),('86f6b9535129325411028c2ab53af49e','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495112706,'a:9:{s:8:\"cms_lang\";s:10:\"indonesian\";s:11:\"cms_old_url\";s:10:\"main/login\";s:21:\"__cms_navigation_name\";s:34:\"organization_mapping_diagram_frame\";s:13:\"cms_user_name\";s:8:\"sysadmin\";s:11:\"cms_user_id\";s:1:\"3\";s:18:\"cms_user_real_name\";s:12:\"System Admin\";s:14:\"cms_user_email\";s:16:\"dompak@gmail.com\";s:15:\"cms_user_layout\";s:5:\"fixed\";s:16:\"cms_user_sidebar\";s:12:\"sidebar-mini\";}'),('9bcd74c104c5397817ec30594db49c05','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495079402,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:11:\"cms_old_url\";s:0:\"\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";}'),('64f080100cb300f887f0324c3c25d685','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495078750,'a:2:{s:8:\"cms_lang\";s:10:\"indonesian\";s:11:\"cms_old_url\";s:0:\"\";}'),('775cba7fe669162fd64627603821b2b2','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495078750,'a:2:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";}'),('aa1584c803e840d352e56304768aa14a','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495080220,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:11:\"main_forgot\";s:11:\"cms_old_url\";s:0:\"\";}'),('88f8aeae975cfb3927c8013966872907','172.17.8.3','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36',1495093298,'a:2:{s:8:\"cms_lang\";s:10:\"indonesian\";s:11:\"cms_old_url\";s:0:\"\";}'),('b17a4b269aa5c665c5f6dd35fe286121','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495080258,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";s:11:\"cms_old_url\";s:0:\"\";}'),('be34d07752d63223efa6c0f3adfbdbac','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495081249,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";s:11:\"cms_old_url\";s:0:\"\";}'),('045405880550031f7192728ec9efa727','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',1495097570,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:11:\"cms_old_url\";s:0:\"\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";}'),('ae57b63f3a3d1f5243da778476a1c4f5','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495081704,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";s:11:\"cms_old_url\";s:0:\"\";}'),('a044eb7683b1dabf28ee7e3a73ce9de9','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495101868,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";s:11:\"cms_old_url\";s:0:\"\";}'),('a33d9c807487d5107846edd4d188bda2','172.17.8.3','Mozilla/5.0 (Android 4.4.2; Mobile; rv:50.0) Gecko/50.0 Firefox/50.0',1495085423,'a:2:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:11:\"main_forgot\";}'),('a20dda4f108a0ebea65ff22f206b928b','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495094769,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";s:11:\"cms_old_url\";s:0:\"\";}'),('a31e07836463bb3c2de784ed357476a8','172.17.2.111','Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; Media Center PC 6.0; .',1495097622,'a:2:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";}'),('fc2733479bf0e056440235f2eae225e6','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495102171,'a:3:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";s:11:\"cms_old_url\";s:0:\"\";}'),('d814db7ec232977b5f9ca7d22da983c4','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495081866,'a:2:{s:8:\"cms_lang\";s:10:\"indonesian\";s:11:\"cms_old_url\";s:0:\"\";}'),('0d763e4702546e3baed044805b586f64','172.17.2.111','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',1495081867,'a:2:{s:8:\"cms_lang\";s:10:\"indonesian\";s:21:\"__cms_navigation_name\";s:10:\"main_login\";}');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ChapterNumber` int(3) NOT NULL DEFAULT '0',
  `Title` varchar(50) DEFAULT NULL,
  `Text1` text,
  `Text2` text,
  `DateInserted` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`ChapterNumber`),
  UNIQUE KEY `ID` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (7,5,'asd','asd','asd','2016-11-22 15:06:36','2016-11-22 15:32:32'),(8,6,'asd','asd','asd','2016-11-22 15:06:36',NULL);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_all`
--

DROP TABLE IF EXISTS `content_all`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_all` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ChapterNumber` int(3) DEFAULT '0',
  `Title` varchar(50) DEFAULT NULL,
  `Text1` text,
  `Text2` text,
  `DateInserted` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_all`
--

LOCK TABLES `content_all` WRITE;
/*!40000 ALTER TABLE `content_all` DISABLE KEYS */;
INSERT INTO `content_all` VALUES (1,5,'asd','asd','asd','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,6,'asd','asd','asd','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,0,'Positive','Negative Format 1','Negative Format 2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,1234,'123456789.12345','-123456789.12345','-123456789.12345','0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,1234,'123456789.12345','-123456789.12345','-123456789.12345','0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,1234,'123456789.12345','-123456789.12345','-123456789.12345','00