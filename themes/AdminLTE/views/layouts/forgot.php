<!DOCTYPE html>
<html lang="{{ language:language_alias }}">
  <head>
        <meta charset="utf-8">
        <title><?php echo $template['title'];?></title>
        <?php echo $template['metadata'];?>
        <link rel="icon" href="{{ site_favicon }}">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <?php
            $asset = new CMS_Asset();       
            $asset->add_cms_css('bootstrap/css/bootstrap.min.css');
            $asset->add_themes_css('bootstrap.min.css', '{{ used_theme }}', 'bootstrap/css');
            $asset->add_themes_css('AdminLTE.min.css', '{{ used_theme }}', 'dist/css');
            $asset->add_themes_css('_all-skins.min.css', '{{ used_theme }}', 'dist/css/skins');
            $asset->add_themes_css('blue.css', '{{ used_theme }}', 'plugins/iCheck/flat');
            $asset->add_themes_css('morris.css', '{{ used_theme }}', 'plugins/morris');
            $asset->add_themes_css('jquery-jvectormap-1.2.2.css', '{{ used_theme }}', 'plugins/jvectormap');
            $asset->add_themes_css('datepicker3.css', '{{ used_theme }}', 'plugins/datepicker');
            $asset->add_themes_css('daterangepicker.css', '{{ used_theme }}', 'plugins/daterangepicker');
            $asset->add_themes_css('bootstrap3-wysihtml5.min.css', '{{ used_theme }}', 'plugins/bootstrap-wysihtml5');
            $asset->add_themes_css('style.css', '{{ used_theme }}', 'default');
            echo $asset->compile_css();
        ?>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-green login-page">
<div class="login-box">
  <div class="login-logo">
    <!--<a href="{{ base_url }}"><img src ="{{ site_logo_login }}" style="max-height:80px; max-width:300px;" /></a>-->
  
    <a href="{{ base_url }}"><img src ="{{ site_logo }}" style="max-height:80px; max-width:200px;" /></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><strong>{{ language:Please fill your data }}</strong></p>

   <?php echo $template['body'];?>
  </div>

</div>
<footer>{{ widget_name:section_bottom }}</footer>
<?php
            $asset->add_cms_js("bootstrap/js/bootstrap.min.js");
            $asset->add_themes_js('jquery-2.2.3.min.js', '{{ used_theme }}', 'plugins/jQuery');
            $asset->add_themes_js('bootstrap.min.js', '{{ used_theme }}', 'bootstrap/js');
            $asset->add_themes_js('icheck.min.js', '{{ used_theme }}', 'plugins/iCheck');
            
            echo $asset->compile_js();
        ?>


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
