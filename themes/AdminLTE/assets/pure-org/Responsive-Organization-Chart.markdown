Responsive Organization Chart
-----------------------------
A pure HTML/ CSS responsive organization chart with departments and sub-sections

A [Pen](http://codepen.io/siiron/pen/aLkdE) by [Ronny Siikaluoma](http://codepen.io/siiron) on [CodePen](http://codepen.io/).

[License](http://codepen.io/siiron/pen/aLkdE/license).