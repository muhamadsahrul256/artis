<!DOCTYPE html>
<html lang="{{ language:language_alias }}">
    <head>
        <meta charset="utf-8">
        <title><?php echo $template['title'];?></title>
        <?php echo $template['metadata'];?>
        <link rel="icon" href="{{ site_favicon }}">
        <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <?php
            $asset = new CMS_Asset();       
            //$asset->add_cms_css('bootstrap/css/bootstrap.min.css');
            $asset->add_themes_css('bootstrap.min.css', '{{ used_theme }}', 'bootstrap/css');
            $asset->add_cms_css('bootstrap-select-1.9.4/css/bootstrap-select.min.css');
            $asset->add_themes_css('morris.css', '{{ used_theme }}', 'plugins/morris');            
            $asset->add_themes_css('select2.min.css', '{{ used_theme }}', 'plugins/select2');
            $asset->add_themes_css('AdminLTE.min.css', '{{ used_theme }}', 'dist/css');
            $asset->add_themes_css('_all-skins.min.css', '{{ used_theme }}', 'dist/css/skins');
            $asset->add_themes_css('blue.css', '{{ used_theme }}', 'plugins/iCheck/flat');
            $asset->add_themes_css('all.css', '{{ used_theme }}', 'plugins/iCheck');            
            $asset->add_themes_css('jquery-jvectormap-1.2.2.css', '{{ used_theme }}', 'plugins/jvectormap');
            $asset->add_themes_css('datepicker3.css', '{{ used_theme }}', 'plugins/datepicker');
            $asset->add_themes_css('daterangepicker.css', '{{ used_theme }}', 'plugins/daterangepicker');
            $asset->add_themes_css('bootstrap-colorpicker.min.css', '{{ used_theme }}', 'plugins/colorpicker');
            $asset->add_themes_css('bootstrap-timepicker.min.css', '{{ used_theme }}', 'plugins/colorpicker');
            $asset->add_themes_css('bootstrap3-wysihtml5.min.css', '{{ used_theme }}', 'plugins/bootstrap-wysihtml5');
            $asset->add_themes_css('style.css', '{{ used_theme }}', 'default');
            $asset->add_themes_css('font-awesome.min.css', '{{ used_theme }}', 'plugins/font-awesome-4.6.3/css');
            $asset->add_themes_css('ionicons.min.css', '{{ used_theme }}', 'plugins/ionicons-2.0.1/css');
            echo $asset->compile_css();
        ?>

  
 
  <!-- Theme style -->
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link rel="shortcut icon" href="{{ site_favicon }}">
       
</head>
<body class="hold-transition skin-blue fixed sidebar-mini" onload="noBack()">

  <?php
            //$asset->add_cms_js("bootstrap/js/bootstrap.min.js");
            //$asset->add_themes_js('script.js', '{{ used_theme }}', 'default');
            echo $asset->compile_js();
        ?>

<div class="wrapper">

  {{ widget_name:section_top_fix }}
  <!-- Left side column. contains the logo and sidebar -->
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      {{ navigation_path }}<br/>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      
      <!--
      <div class="row">
        <div id="__section-content" class="col-md-12"><?php echo $template['body'];?></div> 
      </div>
      -->

      {{ widget_name:dashboard_greeting }}

      {{ widget_name:dashboard_widget }}

      {{ widget_name:dashboard_setting }}

      {{ widget_name:dashboard_chart }}
      <!-- /.row (main row) -->
             
     <!--
      <div class="row">
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form Pengajuan Kasbon (Graph 1)</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="revenue-chart" style="height: 300px;"></div>
            </div>
            
          </div>
          
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Donut Chart</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
            </div>           
          </div>     

        </div>
        
        <div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Line Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="line-chart" style="height: 300px;"></div>
            </div>
            
          </div>         

          
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Bar Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 300px;"></div>
            </div>
            
          </div>      

       
       
      </div> 
        
      </div>-->

<!--
      <div class="row">
          <div class="col-md-6">
          <div class="box box-default collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Chatting</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              
            </div>
            
            <div class="box-body">

              <div class="box box-warning direct-chat direct-chat-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Direct Chat</h3>

                  <div class="box-tools pull-right">
                    <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                      <i class="fa fa-comments"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                  </div>
                </div>
                
                <div class="box-body">
                
                  <div class="direct-chat-messages">
                   
                    <div class="direct-chat-msg">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                        <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                      </div>
                      
                      <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                      <div class="direct-chat-text">
                        Is this template really for free? That's unbelievable!
                      </div>
                      
                    </div>
                    
                    <div class="direct-chat-msg right">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right">Sarah Bullock</span>
                        <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                      </div>
                      
                      <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
                      <div class="direct-chat-text">
                        You better believe it!
                      </div>
                      
                    </div>
                    
                    <div class="direct-chat-msg">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                        <span class="direct-chat-timestamp pull-right">23 Jan 5:37 pm</span>
                      </div>
                      
                      <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                      <div class="direct-chat-text">
                        Working with AdminLTE on a great new app! Wanna join?
                      </div>
                      
                    </div>
                    
                    <div class="direct-chat-msg right">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right">Sarah Bullock</span>
                        <span class="direct-chat-timestamp pull-left">23 Jan 6:10 pm</span>
                      </div>
                      
                      <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
                      <div class="direct-chat-text">
                        I would love to.
                      </div>
                      
                    </div>
                   

                  </div>
                  
                  <div class="direct-chat-contacts">
                    <ul class="contacts-list">
                      <li>
                        <a href="#">
                          <img class="contacts-list-img" src="dist/img/user1-128x128.jpg" alt="User Image">

                          <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                  Count Dracula
                                  <small class="contacts-list-date pull-right">2/28/2015</small>
                                </span>
                            <span class="contacts-list-msg">How have you been? I was...</span>
                          </div>
                          
                        </a>
                      </li>
                     
                      <li>
                        <a href="#">
                          <img class="contacts-list-img" src="dist/img/user7-128x128.jpg" alt="User Image">

                          <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                  Sarah Doe
                                  <small class="contacts-list-date pull-right">2/23/2015</small>
                                </span>
                            <span class="contacts-list-msg">I will be waiting for...</span>
                          </div>
                          
                        </a>
                      </li>
                     
                      <li>
                        <a href="#">
                          <img class="contacts-list-img" src="dist/img/user3-128x128.jpg" alt="User Image">

                          <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                  Nadia Jolie
                                  <small class="contacts-list-date pull-right">2/20/2015</small>
                                </span>
                            <span class="contacts-list-msg">I'll call you back at...</span>
                          </div>
                          
                        </a>
                      </li>
                      
                      <li>
                        <a href="#">
                          <img class="contacts-list-img" src="dist/img/user5-128x128.jpg" alt="User Image">

                          <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                  Nora S. Vans
                                  <small class="contacts-list-date pull-right">2/10/2015</small>
                                </span>
                            <span class="contacts-list-msg">Where is your new...</span>
                          </div>
                          
                        </a>
                      </li>
                      
                      <li>
                        <a href="#">
                          <img class="contacts-list-img" src="dist/img/user6-128x128.jpg" alt="User Image">

                          <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                  John K.
                                  <small class="contacts-list-date pull-right">1/27/2015</small>
                                </span>
                            <span class="contacts-list-msg">Can I take a look at...</span>
                          </div>
                          
                        </a>
                      </li>
                      
                      <li>
                        <a href="#">
                          <img class="contacts-list-img" src="dist/img/user8-128x128.jpg" alt="User Image">

                          <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                  Kenneth M.
                                  <small class="contacts-list-date pull-right">1/4/2015</small>
                                </span>
                            <span class="contacts-list-msg">Never mind I found...</span>
                          </div>
                         
                        </a>
                      </li>
                    
                    </ul>
                   
                  </div>
                 
                </div>
                
                <div class="box-footer">
                  <form action="#" method="post">
                    <div class="input-group">
                      <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-warning btn-flat">Send</button>
                          </span>
                    </div>
                  </form>
                </div>
                
              </div>          

            </div>            
          </div>
         
        </div>
        </div>-->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  
    <footer class="main-footer">
      <div class="pull-right hidden-xs"><b>Version</b> {{ site_version }}</div>{{ widget_name:section_bottom }}
    </footer>
    
{{ widget_name:section_control_sidebar }}
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
        <?php
            //$asset->add_cms_js("bootstrap/js/bootstrap.min.js");
            $asset->add_themes_js('jquery-2.2.3.min.js', '{{ used_theme }}', 'plugins/jQuery');
            $asset->add_themes_js('bootstrap.min.js', '{{ used_theme }}', 'bootstrap/js');
            
            $asset->add_themes_js('raphael-min.js', '{{ used_theme }}', 'plugins/raphael');
            $asset->add_themes_js('jquery-jvectormap-world-mill-en.js', '{{ used_theme }}', 'plugins/jvectormap');
            $asset->add_themes_js('jquery.knob.js', '{{ used_theme }}', 'plugins/knob');
            $asset->add_themes_js('daterangepicker.js', '{{ used_theme }}', 'plugins/daterangepicker');
            $asset->add_themes_js('bootstrap-datepicker.js', '{{ used_theme }}', 'plugins/datepicker');
            $asset->add_themes_js('bootstrap3-wysihtml5.all.min.js', '{{ used_theme }}', 'plugins/bootstrap-wysihtml5');
            $asset->add_themes_js('jquery.slimscroll.min.js', '{{ used_theme }}', 'plugins/slimScroll');
            $asset->add_themes_js('morris.min.js', '{{ used_theme }}', 'plugins/morris');
            $asset->add_themes_js('fastclick.js', '{{ used_theme }}', 'plugins/fastclick');
            $asset->add_themes_js('app.min.js', '{{ used_theme }}', 'dist/js');
            $asset->add_themes_js('icheck.min.js', '{{ used_theme }}', 'plugins/iCheck');

            $asset->add_themes_js('select2.full.min.js', '{{ used_theme }}', 'plugins/select2');

            $asset->add_themes_js('jquery.inputmask.js', '{{ used_theme }}', 'plugins/input-mask');
            $asset->add_themes_js('jquery.inputmask.date.extensions.js', '{{ used_theme }}', 'plugins/input-mask');
            $asset->add_themes_js('jquery.inputmask.extensions.js', '{{ used_theme }}', 'plugins/input-mask');

            $asset->add_themes_js('bootstrap-colorpicker.min.js', '{{ used_theme }}', 'plugins/colorpicker');
            $asset->add_themes_js('bootstrap-timepicker.min.js', '{{ used_theme }}', 'plugins/timepicker');
            $asset->add_themes_js('jquery.slimscroll.min.js', '{{ used_theme }}', 'plugins/slimScroll');

            $asset->add_themes_js('highcharts.js', '{{ used_theme }}', 'highcharts/js');
            //$asset->add_themes_js('data.js', '{{ used_theme }}', 'highcharts/js');
            //$asset->add_themes_js('drilldown.js', '{{ used_theme }}', 'highcharts/js');

            //$asset->add_themes_js('exporting.js', '{{ used_theme }}', 'highcharts/js');
            //$asset->add_themes_js('dashboard.js', '{{ used_theme }}', 'dist/js/pages');
            $asset->add_themes_js('demo.js', '{{ used_theme }}', 'dist/js');
            $asset->add_themes_js('jquery.sparkline.min.js', '{{ used_theme }}', 'plugins/sparkline');
            //$asset->add_themes_js('jquery-ui.min.js', '{{ used_theme }}', 'plugins/jquery-ui-1.11.4');
            $asset->add_themes_js('raphael-min.js', '{{ used_theme }}', 'plugins/raphael');
            $asset->add_themes_js('moment.min.js', '{{ used_theme }}', 'plugins/moment');
            echo $asset->compile_js();
        ?>

        <?php
            $asset = new CMS_Asset();            
            $asset->add_cms_css('bootstrap-select-1.9.4/css/bootstrap-select.min.css');          
            echo $asset->compile_css();
                 
            $asset->add_cms_js("bootstrap-select-1.9.4/js/bootstrap-select.min.js");
            echo $asset->compile_js();
        ?>

        <script type="text/javascript">
          $(document).ready(function(){
              $("#dashboard_btn").click(function(){
                  $("#toggleDemo").collapse('toggle');
              });

              $('#start1').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
              });
              $('#finish1').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
              });

              $('#start2').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
              });

              $('#finish2').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
              });

              $(".select2").select2();
              
              $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
              });
          });
        </script>

</body>
</html>