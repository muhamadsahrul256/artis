<!DOCTYPE html>
<html lang="{{ language:language_alias }}">
    <head>
        <meta charset="utf-8">
        <title><?php echo $template['title'];?></title>
        <?php echo $template['metadata'];?>
        <link rel="icon" href="{{ site_favicon }}">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <?php
            $asset = new CMS_Asset();       
           
            $asset->add_themes_css('font-googleapis.css', '{{ used_theme }}', 'css');
            $asset->add_themes_css('bootstrap.min.css', '{{ used_theme }}', 'bootstrap/css');
            $asset->add_themes_css('font-awesome.min.css', '{{ used_theme }}', 'font-awesome/css');
            $asset->add_themes_css('form-elements.css', '{{ used_theme }}', 'css');
            $asset->add_themes_css('style.css', '{{ used_theme }}', 'css');

            echo $asset->compile_css();
        ?>

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ base_url }}/themes/{{ used_theme }}/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ base_url }}/themes/{{ used_theme }}/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ base_url }}/themes/{{ used_theme }}/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="{{ base_url }}/themes/{{ used_theme }}/assets/ico/apple-touch-icon-57-precomposed.png">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
<body>

    <div class="top-content"> 
        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <a href="{{ base_url }}"><img src ="{{ site_logo }}" style="max-height:80px; max-width:200px;" /></a>        
                    <div class="description"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4 form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                              <h3>{{ language:Please Login }}</h3>
                            </div>
                            <div class="form-top-right">
                              <i class="fa fa-lock"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <?php echo $template['body'];?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


  <footer>{{ widget_name:section_bottom }}</footer>
    <?php          

        $asset->add_themes_js('jquery-1.11.1.min.js', '{{ used_theme }}', 'js');
        $asset->add_themes_js('bootstrap.min.js', '{{ used_theme }}', 'bootstrap/js');
        $asset->add_themes_js('jquery.backstretch.min.js', '{{ used_theme }}', 'js');
        $asset->add_themes_js('scripts.js', '{{ used_theme }}', 'js');
            
        echo $asset->compile_js();
    ?>

        <!--[if lt IE 10]>
            <script src="{{ base_url }}themes/{{ used_theme }}/assets/js/placeholder.js"></script>
        <![endif]-->

   
        {{ widget_name:static_login_slideshow }}

</body>
</html>
