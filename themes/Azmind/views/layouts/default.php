<!DOCTYPE html>
<html lang="{{ language:language_alias }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $template['title'];?></title>
        <?php echo $template['metadata'];?>
        <link rel="icon" href="{{ site_favicon }}">
        <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

 

        <?php
            $asset = new CMS_Asset();       
            //$asset->add_cms_css('bootstrap/css/bootstrap.min.css');
            $asset->add_themes_css('bootstrap.min.css', '{{ used_theme }}', 'bootstrap/css');
            $asset->add_cms_css('bootstrap-select-1.9.4/css/bootstrap-select.min.css');
            $asset->add_themes_css('AdminLTE.min.css', '{{ used_theme }}', 'dist/css');
            $asset->add_themes_css('_all-skins.min.css', '{{ used_theme }}', 'dist/css/skins');
            $asset->add_themes_css('blue.css', '{{ used_theme }}', 'plugins/iCheck/flat');
            //$asset->add_themes_css('morris.css', '{{ used_theme }}', 'plugins/morris');
            $asset->add_themes_css('jquery-jvectormap-1.2.2.css', '{{ used_theme }}', 'plugins/jvectormap');
            $asset->add_themes_css('datepicker3.css', '{{ used_theme }}', 'plugins/datepicker');
            $asset->add_themes_css('daterangepicker.css', '{{ used_theme }}', 'plugins/daterangepicker');
            $asset->add_themes_css('bootstrap3-wysihtml5.min.css', '{{ used_theme }}', 'plugins/bootstrap-wysihtml5');
            $asset->add_themes_css('style.css', '{{ used_theme }}', 'default');
            $asset->add_themes_css('font-awesome.min.css', '{{ used_theme }}', 'plugins/font-awesome-4.6.3/css');
            $asset->add_themes_css('ionicons.min.css', '{{ used_theme }}', 'plugins/ionicons-2.0.1/css');

            $asset->add_themes_css('all.css', '{{ used_theme }}', 'plugins/iCheck');

            $asset->add_themes_css('orgchart.css', '{{ used_theme }}', 'orgchart');

            $asset->add_themes_css('jquerysctipttop.css', '{{ used_theme }}', 'jquery-orgchart');
            $asset->add_themes_css('jquery.orgchart.css', '{{ used_theme }}', 'jquery-orgchart');

            $asset->add_themes_css('jquery.orgchart.css', '{{ used_theme }}', 'jquery-orgchart');

            //$asset->add_themes_css('bootstrap-switch.css', '{{ used_theme }}', 'dist/css/bootstarp3');



            //$asset->add_themes_css('bs.sm.css', '{{ used_theme }}', 'dist');

            //$asset->add_themes_css('bootstrap.dynamicModal.css', '{{ used_theme }}', 'css');
            //$asset->add_themes_css('bootstrap-datetimepicker.min.css', '{{ used_theme }}', 'css');

            echo $asset->compile_css();
        ?>

        <link rel="shortcut icon" href="{{ site_favicon }}">
        {{ widget_name:section_custom_script }}

  
  <!-- Theme style -->
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


        
</head>
<body class="hold-transition skin-blue-light fixed sidebar-mini" onload="noBack()">

  <?php
            //$asset->add_cms_js("bootstrap/js/bootstrap.min.js");
            $asset->add_themes_js('script.js', '{{ used_theme }}', 'default');
            echo $asset->compile_js();
        ?>

<div class="wrapper">

  {{ widget_name:section_top_fix }}
  <!-- Left side column. contains the logo and sidebar -->
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      {{ navigation_path }}<br/>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div id="__section-content" class="col-md-12"><?php echo $template['body'];?></div>
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
    <footer class="main-footer">
      <div class="pull-right hidden-xs"><b>Version</b> {{ site_version }}</div>{{ widget_name:section_bottom }}
    </footer>
    <script type="text/javascript">
            $(document).ready(function(){
                // if section-banner is empty, remove it
                if($.trim($('__section-banner').html()) == ''){
                    $('__section-banner').remove();
                }            
            });
    </script>

<!--{{ widget_name:section_control_sidebar }} -->
  <!-- Control Sidebar -->
  <!-- Control Sidebar -->

  <!--
  <aside class="control-sidebar control-sidebar-dark">   
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>     
    </ul>    
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">My Profile</h4>
                <p>Last update on 27-Apr-2016</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">My CV</h4>
                <p>Last update on 27-May-2016</p>
              </div>
            </a>
          </li>
          <li>
            <a href="main/change_profile">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Change Profile Login</h4>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Substitution</h4>
                <span class="label label-success pull-right">ON</span>
              </div>
            </a>
          </li>
        </ul>

      </div>     
    </div>
  </aside>

-->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
        <?php
            $asset->add_cms_js("grocery_crud/js/ex_excel/dist/jquery.table2excel.js");
            //$asset->add_themes_js('jquery-2.2.3.min.js', '{{ used_theme }}', 'plugins/jQuery');
            $asset->add_themes_js('bootstrap.min.js', '{{ used_theme }}', 'bootstrap/js');
            $asset->add_themes_js('morris.min.js', '{{ used_theme }}', 'plugins/morris');
            $asset->add_themes_js('jquery.sparkline.min.js', '{{ used_theme }}', 'plugins/sparkline');

            $asset->add_themes_js('jquery-jvectormap-world-mill-en.js', '{{ used_theme }}', 'plugins/jvectormap');
            $asset->add_themes_js('jquery.knob.js', '{{ used_theme }}', 'plugins/knob');
            $asset->add_themes_js('daterangepicker.js', '{{ used_theme }}', 'plugins/daterangepicker');
            $asset->add_themes_js('bootstrap-datepicker.js', '{{ used_theme }}', 'plugins/datepicker');
            $asset->add_themes_js('bootstrap3-wysihtml5.all.min.js', '{{ used_theme }}', 'plugins/bootstrap-wysihtml5');
            $asset->add_themes_js('jquery.slimscroll.min.js', '{{ used_theme }}', 'plugins/slimScroll');
            $asset->add_themes_js('fastclick.js', '{{ used_theme }}', 'plugins/fastclick');
            $asset->add_themes_js('app.min.js', '{{ used_theme }}', 'dist/js');
            $asset->add_themes_js('dashboard.js', '{{ used_theme }}', 'dist/js/pages');
            $asset->add_themes_js('demo.js', '{{ used_theme }}', 'dist/js');
            $asset->add_themes_js('jquery.sparkline.min.js', '{{ used_theme }}', 'plugins/sparkline');

            $asset->add_themes_js('jquery-ui.min.js', '{{ used_theme }}', 'plugins/jquery-ui-1.11.4');
            $asset->add_themes_js('raphael-min.js', '{{ used_theme }}', 'plugins/raphael');
            $asset->add_themes_js('moment.min.js', '{{ used_theme }}', 'plugins/moment');

            $asset->add_themes_js('icheck.min.js', '{{ used_theme }}', 'plugins/iCheck');
            $asset->add_themes_js('bootstrap.dynamicModal.js', '{{ used_theme }}', 'js');
            //$asset->add_themes_js('bootstrap-switch.js', '{{ used_theme }}', 'dist/js');
            //$asset->add_themes_js('jquery.min.js', '{{ used_theme }}', 'docs/js');
            //$asset->add_themes_js('bootstrap-datetimepicker.min.js', '{{ used_theme }}', 'js');
            $asset->add_themes_js('orgchart.js', '{{ used_theme }}', 'orgchart');

            //$asset->add_themes_js('jquery-1.11.1.min.js', '{{ used_theme }}', 'jquery-orgchart');
            $asset->add_themes_js('jquery.orgchart.js', '{{ used_theme }}', 'jquery-orgchart');

            echo $asset->compile_js();
        ?>

        <?php
            $asset = new CMS_Asset();            
            $asset->add_cms_css('bootstrap-select-1.9.4/css/bootstrap-select.min.css');          
            echo $asset->compile_css();
                 
            $asset->add_cms_js("bootstrap-select-1.9.4/js/bootstrap-select.min.js");
            echo $asset->compile_js();
        ?> 

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
  
</script>
<!-- Bootstrap 3.3.6 -->



</body>
</html>
