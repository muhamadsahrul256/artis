<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class grade_system extends CMS_Priv_Strict_Controller {

    protected $URL_MAP = array();

    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    private function make_crud(){
       
        $crud = $this->new_crud();
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        //$crud->unset_add();
        // $crud->unset_edit();
        // $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        // $crud->unset_export();

        $crud->unset_texteditor('Karir_Fungsional_Name');

        $crud->set_language($this->cms_language());
        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->set_theme('no-flexigrid-default');
        }
        else{
            $crud->set_theme('no-flexigrid-default');
            
        }

    
        $crud->set_table('mst_grade_system');
        $crud->order_by('Level_Kompetensi_Id','ASC');

        $crud->set_primary_key('Level_Kompetensi_Id');

        $crud->set_subject($this->cms_lang('Grading System'));

        $crud->columns('Level_Kompetensi_Id','Level_Kompetensi_Name','Karir_Fungsional_Id','Group_Level_Id','Jenjang_Jabatan_Id');
        $crud->edit_fields('Level_Kompetensi_Id','Level_Kompetensi_Name','Karir_Fungsional_Id','Group_Level_Id','Jenjang_Jabatan_Id');
        $crud->add_fields('Level_Kompetensi_Id','Level_Kompetensi_Name','Karir_Fungsional_Id','Group_Level_Id','Jenjang_Jabatan_Id');

        $crud->display_as('Level_Kompetensi_Id', $this->cms_lang('ID'));
        $crud->display_as('Level_Kompetensi_Name', $this->cms_lang('Name'));
        $crud->display_as('Karir_Fungsional_Id', $this->cms_lang('Kode Fungsional'));
        $crud->display_as('Group_Level_Id', $this->cms_lang('Group'));
        $crud->display_as('Jenjang_Jabatan_Id', $this->cms_lang('Jenjang Jabatan'));
     
        $crud->required_fields('Level_Kompetensi_Id','Level_Kompetensi_Name');
        $crud->unique_fields('Level_Kompetensi_Id');

        $crud->field_type('Group_Level_Id','multiselect', array(1=>'Group I', 2=>'Group II',3=>'Group III',4=>'Group IV', 5=>'Group V', 6=>'Group VI'));
        $crud->field_type('Jenjang_Jabatan_Id','multiselect', array(1=>'Manajemen Atas', 2=>'Manajemen Menengah',3=>'Manajemen Dasar',4=>'Supervisori Atas', 5=>'Supervisori Dasar'));        
        $crud->field_type('Karir_Fungsional_Id','multiselect', $this->_callback_Karir_Fungsional_Id());

        //$crud->set_relation('business_area_id', 'mst_business_area', 'description');      
     
        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        $this->crud = $crud;
        return $crud;
    }

    public function index(){
        $crud = $this->make_crud();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view($this->cms_module_path().'/grade_system_view', $output,
            $this->cms_complete_navigation_name('grade_system'));
    }

    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        return $success;
    }

    public function _before_delete($primary_key){
        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){

        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }

    public function _callback_Karir_Fungsional_Id(){

        $query = $this->db->select('iFungsionalID, cFungsionalName')
               ->from('mst_fungsional_matrik')
               ->order_by('iFungsionalID','ASC')
               ->get();

        $results = array();

        foreach ($query->result() as $key => $value) {
            $results[$value->iFungsionalID] = $value->cFungsionalName;
        }
        return $results;
        
    }

}