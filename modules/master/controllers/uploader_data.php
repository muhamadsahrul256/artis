<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class uploader_data extends CMS_Priv_Strict_Controller {

    protected $URL_MAP = array();

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->helper('cookie');
    }

    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    private function make_crud(){
       
        $crud = $this->new_crud();
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        //$crud->unset_add();
        // $crud->unset_edit();
        // $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        // $crud->unset_export();

        $crud->unset_texteditor('description');

        $crud->set_language($this->cms_language());
        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->set_theme('no-flexigrid-uploader_data');
        }
        else{
            $crud->set_theme('no-flexigrid-uploader_data');
        }

    
        $crud->set_table(cms_module_config($this->cms_module_path(), 'upload_table'));

        $crud->set_primary_key(cms_module_config($this->cms_module_path(), 'primary_key'));

        $crud->set_subject(cms_module_config($this->cms_module_path(), 'upload_table'));

        $crud->columns(cms_module_config($this->cms_module_path(), 'list_column'));
        $crud->edit_fields(cms_module_config($this->cms_module_path(), 'list_column'));
        $crud->add_fields(cms_module_config($this->cms_module_path(), 'list_column'));

        /*
        $crud->display_as('unit_id', $this->cms_lang('Unit ID'));
        $crud->display_as('description', $this->cms_lang('Description'));
        $crud->display_as('business_area_id', $this->cms_lang('Bus Area'));
        */
     
        //$crud->required_fields('unit_id','description','business_area_id');
        //$crud->unique_fields('unit_id');

        //$crud->set_relation('business_area_id', 'mst_business_area', 'description');      
     
        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        $this->crud = $crud;
        return $crud;
    }

    public function index(){
        $crud = $this->make_crud();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////      

        $output = $crud->render();
        $this->view($this->cms_module_path().'/uploader_data_view', $output,
            $this->cms_complete_navigation_name('uploader_data'));
    }

    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        return $success;
    }

    public function _before_delete($primary_key){
        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){

        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }


    public function do_upload(){
        $today = date('Ymd_His');

        $fileName = $today.'_'.$_FILES['userfile']['name'];

        if (is_null($_FILES['userfile']['name']) || empty($_FILES['userfile']['name'])){
            $this->session->set_flashdata('empty_excel', $this->cms_lang('Please select data first'));
        }
        else{        
         
        $config['upload_path'] = './assets/files/upload/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000000;
         
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
         
        if($this->upload->do_upload('userfile') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('userfile');
        $inputFileName = './assets/files/upload/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));

            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            if ($highestColumn != cms_module_config($this->cms_module_path(), 'highest_column')){

                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            
            }
            else{           
             
                for ($row = 2; $row <= $highestRow; $row++){                
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);
                    
                    $primary_key = $rowData[0][cms_module_config($this->cms_module_path(), 'primary_key_column')];

                    $query = $this->db->select(cms_module_config($this->cms_module_path(), 'primary_key'))
                                      ->from(cms_module_config($this->cms_module_path(), 'upload_table'))
                                      ->where(cms_module_config($this->cms_module_path(), 'primary_key'), $primary_key)
                                      ->get();
                                      

                    if($query->num_rows() > 0){                        

                        $data = array();
                        foreach (cms_module_config($this->cms_module_path(), 'update_column') as $key => $value) {

                            $data[$key] = $rowData[0][$value];
    
                        }

                        //$this->db->update(cms_module_config($this->cms_module_path(), 'upload_table'), $data, array(cms_module_config($this->cms_module_path(), 'primary_key') => $primary_key));  
                        

                        /*

                        $i = 1;
                        $data = array();
                        foreach (cms_module_config($this->cms_module_path(), 'update_column') as $key => $value) {

                            $data[$i] = $rowData[0][$i];
    
                        $i++;
                        }   

                        /*

                        $data = array(                            
                            'tNamaPanjangOrg'=> $rowData[0][2],
                            'business_area_id' => $rowData[0][1],
                            );                     
                        

                        $this->db->update(cms_module_config($this->cms_module_path(), 'upload_table'), $data, array(cms_module_config($this->cms_module_path(), 'primary_key') => $primary_key));
                        */

                    }
                    else{                        

                        $data = array();
                        foreach (cms_module_config($this->cms_module_path(), 'insert_column') as $key => $value) {

                            $data[$key] = $rowData[0][$value];
    
                        }                                       
                    
                        $this->db->insert(cms_module_config($this->cms_module_path(), 'upload_table'), $data);
                        
                    }           
                         
                }

                $this->session->set_flashdata('msg_excel', $this->cms_lang('Inserting data success'));
            }

        }           

        redirect($this->cms_module_path().'/uploader_data/');
    }
}