<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class import_data extends CMS_Priv_Strict_Controller {

    protected $URL_MAP = array();

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('phpexcel');
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));

        $this->load->model('Querypage');
        $this->load->helper(array('form', 'url', 'inflector'));
        $this->load->library('form_validation');
    }

    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    private function make_crud(){
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // initialize groceryCRUD
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $crud = $this->new_crud();
        // this is just for code completion
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        // $crud->unset_add();
        $crud->unset_edit();
        // $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        // $crud->unset_export();


        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->set_theme('no-flexigrid-import');
        }
        else{
            $crud->set_theme('no-flexigrid-import');
        }

        $crud->set_language($this->cms_language());

        // table name
        $crud->set_table($this->cms_complete_table_name('content'));
        // primary key
        $crud->set_primary_key('id');

        // set subject
        $crud->set_subject('Data');

        // displayed columns on list
        $crud->columns('ChapterNumber','Title','Text1','Text2','DateInserted','DateUpdated');
        // displayed columns on edit operation
        $crud->edit_fields('data_id','description','last_update');
        // displayed columns on add operation
        $crud->add_fields('data_id','description','last_update');   

        // caption of each columns
        $crud->display_as('ChapterNumber','Number');
        $crud->display_as('DateInserted','Inserted');
        $crud->display_as('DateUpdated','Updated');
        $crud->display_as('commodity','Commodity');
        $crud->display_as('citizen','Citizen');       

       
        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        $this->crud = $crud;
        return $crud;
    }

    public function index(){

       
        $crud = $this->make_crud();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view($this->cms_module_path().'/import_data_view', $output,
            $this->cms_complete_navigation_name('import_data'));
    }

    public function delete_selection(){
        $crud = $this->make_crud();
        if(!$crud->unset_delete){
            $id_list = json_decode($this->input->post('data'));
            foreach($id_list as $id){
                if($this->_before_delete($id)){
                    $this->db->delete($this->cms_complete_table_name('content'),array('id'=>$id));
                    $this->_after_delete($id);
                }
            }
        }
    }

    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_delete($primary_key){
        // delete corresponding citizen
        
        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){
        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }

    public function __upload(){


        if ($this->input->post('save')) {
        $fileName = $_FILES['import']['name'];

            $config['upload_path'] = './assets/files/';
            $config['file_name'] = $fileName;
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']     = 10000;

            $this->load->library('upload');
            $this->upload->initialize($config);

            if(! $this->upload->do_upload('import') )
                $this->upload->display_errors();

            $media = $this->upload->data('import');
            $inputFileName = './assets/files/'.$media['file_name'];

            //  Read your Excel workbook
            try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            //  Loop through each row of the worksheet in turn
            for ($row = 2; $row <= $highestRow; $row++){                //  Read a row of data into an array                
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                //  Insert row data array into your database of choice here
                $data = array(
                            "idimport"=> $rowData[0][0],
                            "nama"=> $rowData[0][1],
                            "alamat"=> $rowData[0][2],
                            "kontak"=> $rowData[0][3],
                        );

                $this->db->insert("eimport",$data);
            }
                        echo "Import Success";
                }
                
                //$this->load->view('import_data_view');

                redirect('master/import_data/');

    }


    public function upload(){
        $fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = './assets/files/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
         
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
         
        if($this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        $inputFileName = './assets/files/'.$media['file_name'];
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    "idimport"=> $rowData[0][0],
                    "nama"=> $rowData[0][1],
                    "alamat"=> $rowData[0][2],
                    "kontak"=> $rowData[0][3]
                );
                 
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("eimport",$data);
                delete_files($media['file_path']);
                     
            }
        redirect('master/import_data/');
    }

    public function do_upload(){

        $today = date('Ymd_His');

        $fileName = $today.'_'.$_FILES['userfile']['name'];

        $config['upload_path'] = './assets/files/';
        $config['allowed_types'] = 'xls';
        $config['file_name'] = $fileName;
                    
        $this->load->library('upload', $config);

         if (!$this->upload->do_upload())
         {
            $data = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('msg_excel', 'Insert failed. Please check your file, only .xls file allowed.');
         }
         else{
                $data = array('error' => false);
                $upload_data = $this->upload->data();

                $this->load->library('excel_reader');
                $this->excel_reader->setOutputEncoding('CP1251');

                $file =  $upload_data['full_path'];
                $this->excel_reader->read($file);
                error_reporting(E_ALL ^ E_NOTICE);

                // Sheet 1
                $data = $this->excel_reader->sheets[0] ;
                $dataexcel = Array();
                for ($i = 1; $i <= $data['numRows']; $i++) {
                   if($data['cells'][$i][1] == '') break;
                   $dataexcel[$i-1]['chapternumber'] = $data['cells'][$i][1];
                   $dataexcel[$i-1]['title'] = $data['cells'][$i][2];
                   $dataexcel[$i-1]['text1'] = $data['cells'][$i][3];
                 $dataexcel[$i-1]['text2'] = $data['cells'][$i][4];
                }
        //cek data
        $check= $this->Querypage->search_chapter($dataexcel);
        if (count($check) > 0)
        {
          $this->Querypage->update_chapter($dataexcel);
          // set pesan
          $this->session->set_flashdata('msg_excel', 'update data success');
      }else{
          $this->Querypage->insert_chapter($dataexcel);
          // set pesan
          $this->session->set_flashdata('msg_excel', 'inserting data success');
      }
      }
      redirect('master/import_data/');
      }

}