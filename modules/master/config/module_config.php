<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['module_table_prefix']  = '';
$config['module_prefix']        = 'master';

/* Untuk konfigurasi upload data ria*/

$config['primary_key']         	= 'user_id'; // nama kolom primary key table db
$config['upload_table']         = 'main_user'; // nama table db
$config['list_column'] 			= array('user_id','user_name','real_name'); // nama kolom db yang ditampilkan dalam halaman

$config['primary_key_column']   = 0; // nomor index primary key dalam excel
$config['highest_column']   	= 'D'; // maksimal kolom excel yang akan di unggal, jika lebih atau kurang akan ada error
$config['insert_column'] 		= array('user_id'=>0, 'user_name'=>1, 'email'=>1, 'password'=>3,'real_name'=>2); // nama kolom yang untuk insert dan nomor index kolom di excelnya
$config['update_column'] 		= array('user_id'=>0, 'email'=>1, 'password'=>3,'real_name'=>2); // nama kolom yang untuk update dan nomor index kolom di excelnya
