<?php

$lang['Statistic Type'] = 'Statistic Type';
$lang['Period'] = 'Period';
$lang['Organizational Unit'] = 'Organizational Unit';
$lang['Position Ladder'] = 'Position Ladder';
$lang['Business Area'] = 'Business Area';
$lang['Organization chart'] = 'Organization chart';
$lang['Business Area ID'] = 'Business Area ID';
$lang['Education ID'] = 'Education ID';
$lang['Long Position Name'] = 'Long Position Name';