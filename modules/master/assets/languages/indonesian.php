<?php

$lang['Statistic Type'] = 'Tipe Statistik';
$lang['Period'] = 'Periode';
$lang['Organizational Unit'] = 'Satuan Organisasi';
$lang['Position Ladder'] = 'Jenjang Jabatan';
$lang['Business Area'] = 'Lokasi Bisnis';
$lang['Organization chart'] = 'Struktur Organisasi';
$lang['Business Area ID'] = 'ID Lokasi Bisnis';
$lang['Education ID'] = 'ID Pendidikan';
$lang['Long Position Name'] = 'Nama Panjang Posisi';
$lang['Long Position Name'] = 'Nama Panjang Posisi';
$lang['Talent Period'] = 'Periode Talenta';
$lang['Profession Tree'] = 'Pohon Profesi';
$lang['Talents'] = 'Talenta';
$lang['Mapping Upload History'] = 'Riwayat Mengunggah Pemetaan';
$lang['Document Item'] = 'Item Dokumen';
$lang['Long Name'] = 'Nama Panjang';
$lang['Bussiness Area'] = 'Area Bisnis';
$lang['Selected Candidate'] = 'Kandidat yang dipilih';
$lang['Grading System'] = 'Sistem Grading';
