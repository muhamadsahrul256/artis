<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$today     = date('Y-m-d_H:i:s');
$file_name = 'Talent_Pool_'.$today;


$asset = new CMS_Asset();
foreach($css_files as $file){
    $asset->add_css($file);
}
echo $asset->compile_css();

foreach($js_files as $file){
    $asset->add_js($file);
}

echo $asset->compile_js();
/*
if ($state == 'edit'){
    echo $asset->compile_js();
}
*/

// For every content of option tag, this will replace '&nbsp;' with ' '
function __ommit_nbsp($matches){
    return $matches[1].str_replace('&nbsp;', ' ', $matches[2]).$matches[3];
}

echo preg_replace_callback('/(<option[^<>]*>)(.*?)(<\/option>)/si', '__ommit_nbsp', $output);

?>

<script type="text/javascript">
$(document).ready(function(){

	$('.selectpicker').selectpicker({
		style: 'btn-default',size: "auto"
	});

    $('.launch-search').click(function(){
        $('#myModal').modal({
            backdrop: 'static'
        });
    }); 

});
</script>
<style type="text/css">
    .bs-example{
        margin: 20px;
    }
</style>



<script type="text/javascript">

function querystring(key) {
   var re=new RegExp('(?:\\?|&)'+key+'=(.*?)(?=&|$)','gi');
   var r=[], m;
   while ((m=re.exec(document.location.search)) != null) r[r.length]=m[1];
   return r;
}
    
    // CHECK ALL
    $('.checkall').live('click', function(){
        $(this).parents('table:eq(0)').find(':checkbox').attr('checked', this.checked);
    });
    // DELETE ALL
    $('.print_all_button').live('click', function(event){
        event.preventDefault();
        var list = new Array();
        $('input[type=checkbox]').each(function() {
            if (this.checked) {                
                list.push(this.value);
            }
        });
        //send data to delete
        $.post('{{ MODULE_SITE_URL }}diagram_frame/set_candidate_value', { data: JSON.stringify(list) }, function(data) {            
            var primary_key = querystring("id");
            var win = window.open('{{ MODULE_SITE_URL }}diagram_frame/export_to_excel/?id='+primary_key+'&json='+JSON.stringify(list), '_blank');
            win.focus();           

        });
    });


    $(document).ajaxComplete(function(){
        
    });

    $(document).ready(function(){
        $('#flex1 tr .prt-check').prepend('<input type="checkbox" class="checkall" />');
            $('#flex1 tr .chd-check').each(function(){
                $(this).prepend('<div align="center"><input type="checkbox" value="'+$(this).attr('rowId')+'" /></div>');
            });    
    })
</script>



<style type="text/css">

.border-all{
    border-left: solid 1px;
    border-right: solid 1px;
    border-top: solid 1px;
    border-bottom: solid 1px; 
}
.chd-check{
    border-left: solid 1px;
    border-right: solid 1px;
    border-top: solid 1px;
    border-bottom: solid 1px; 
}
.tebalin{
    font-weight: bold;
}

table td {
    word-wrap:break-word;
}

input[type=checkbox]
{
  -webkit-appearance:checkbox;
}



</style>

<script type="text/javascript">

var save_method;
var table;
var primary_key;

function edit_person(id){
    save_method = 'update';
    primary_key = '<?php echo $primary_key ?>';
    //$('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('organization_mapping/diagram_frame/ajax_edit/')?>/" + <?php echo $primary_key ?>,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            var status = data.Functional;

            if (status == 0){
                var text_status = 'Struktural';
            }
            else{
                var text_status = 'Fungsional';
            }        

            var remarks = '<div class="box-body remarks">'+
              '<dl>'+
                '<dt>{{ language:Full Name Position }} :</dt>'+
                '<dd>'+data.long_name_position+'</dd>'+
                '<dt>{{ language:Organization Unit }} :</dt>'+
                '<dd>'+data.UnitID+'</dd>'+
                '<dt>{{ language:Mapping Code }} :</dt>'+
                '<dd>'+data.Full_Mapping_Code+'</dd>'+
                '<dt>{{ language:Officer }} :</dt>'+
                '<dd>'+data.EmployeeName+'</dd>'+
                '<dt>{{ language:Grade (maximum) }} :</dt>'+
                '<dd>'+data.GradeCode+'</dd>'+
              '</dl>'+
            '</div>';


            $('[name="title_txt"]').attr("data-content", remarks);
            $('[name="title_txt"]').text(data.Description);
            $('[name="COID"]').val(data.COID);

            var trHTML = '<select data-live-search="true" class="selectpicker form-control" multiple data-show-subtext="true" data-container="body" data-width="100%" name="PositionID[]" id="PositionID" data-header="{{ language:Select }} {{ language:Position History }}">';
            var no = 1;
            $.each(data.option_position, function (key,value) {
                trHTML +='<option value="'+value.position_name+'" data-subtext="">'+value.position_name +'</option>';
               no++;     
            });
            trHTML += '</select>';

            $('#position_field_struktural').html(trHTML);
            $('#position_field_fungsional').html(trHTML);

            $('.selectpicker').selectpicker({style: 'btn-default',size: 'auto'});
            $('.selectpicker').selectpicker('refresh');


            if (status == 1){
                $('[name="Grade"]').text(data.search_grade);
                $('[name="GradeID"]').val(data.search_grade);
                
                $('#modal_form_fungsional').modal({backdrop: 'static'});
                $('#modal_form_fungsional').modal('show');
            }
            else{
                $('[name="Grade"]').text(data.search_grade);
                $('[name="GradeID"]').val(data.search_grade);

                $('#modal_form').modal({backdrop: 'static'});
                $('#modal_form').modal('show');
            }

            //$('.modal-title').text('{{ language:Searching Talent }} '+text_status);
            $('.modal-title').html('{{ language:Searching Talent }} '+text_status+' #'+data.COID+' <i class="fa fa-pencil call-edit" title="edit" onclick="open_diagram_edit()"></i>');

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}


function save(){

    $('#btnSave').text('saving...'); 
    $('#btnSave').attr('disabled',true); 
    var url;

    if(save_method == 'add') {
        url = "<?php echo site_url('organization_mapping/diagram_frame/chart_update')?>";
    } else {
        url = "<?php echo site_url('organization_mapping/diagram_frame/chart_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) 
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('save');
            $('#btnSave').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save');
            $('#btnSave').attr('disabled',false);
        }
    });
}

function open_diagram_edit(){
    var url_edit = "<?php echo site_url('organization_mapping/diagram_frame/table_frame/edit/')?>/"+primary_key;
    location.href = url_edit;
}

</script>


<script type="text/javascript">

    $(".modal-dialog").draggable({
        handle: ".modal-header"
    });

    $(".modal-dialog").draggable({
        handle: ".modal-header"
    });

    $('#TrainingID').change(function(){
        if($(this).attr('checked')){
            $(this).val(1);
        }else{
            $(this).val(0);
        }
    });

</script>

<!-- Modal untuk pencarian struktural -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" onclick="open_diagram_edit()">{{ language:Searching }}</h4>
            </div>
            <div class="modal-body form">
                <form action="{{ base_url }}organization_mapping/diagram_frame/chart_update" method="post" class="form-horizontal">
                    <input type="hidden" value="" name="COID"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-12 text-center" data-toggle="popover" title="{{ language:Information }}" data-content="" name="title_txt"></label>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Level Grade }}</label>
                            <div class="col-md-9">
                                <label for="Grade" class="control-label" name="Grade"></label>
                                <input type="hidden" value="" name="GradeID"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Position History }}</label>
                            <div class="col-md-9" id="position_field_struktural">
                                  
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Profession }}</label>
                            <div class="col-md-9">
                                <select data-live-search="true" class="selectpicker form-control" multiple data-max-options="2" data-show-subtext="true" data-container="body" data-width="100%" name="ProfessionID[]" id="ProfessionID" data-header="{{ language:Select }} {{ language:Profession }}">
                                <?php
                                $SQL    = "SELECT profession_code,profession_name FROM mst_profession ORDER BY profession_code ASC";
                                $QUERY  = $this->db->query($SQL);    

                                foreach($QUERY->result() as $row): ?>                                           
                                    <option value="<?php echo $row->profession_code?>" data-subtext="<?php echo $row->profession_code?>"><?php echo $row->profession_name?></option>            
                                <?php 
                                endforeach;
                                ?>
                                </select>    
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Education }}</label>
                            <div class="col-md-9">
                                <select data-live-search="true" class="selectpicker form-control" multiple data-show-subtext="true" data-container="body" data-width="100%" name="EducationID[]" id="EducationID" data-header="{{ language:Select }} {{ language:Education }}">
                                <?php
                                $SQL    = "SELECT Branch_of_study FROM tp_profile_education WHERE Branch_of_study != '' AND (Branch_of_study NOT LIKE 'SD%' AND Branch_of_study NOT LIKE 'SMP%' AND Branch_of_study NOT LIKE 'SLTP%' AND Branch_of_study NOT LIKE 'Sekolah Dasar%') GROUP BY Branch_of_study ORDER BY Branch_of_study ASC";
                                $QUERY  = $this->db->query($SQL);    

                                foreach($QUERY->result() as $row): ?>                                           
                                    <option value="<?php echo $row->Branch_of_study?>" data-subtext=""><?php echo $row->Branch_of_study?></option>            
                                <?php 
                                endforeach;
                                ?>
                                </select>    
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-9">
                                <label><input type="checkbox" name="TrainingID" id="TrainingID" value="1" checked> {{ language:Show Training Data }}</label>
                            </div>
                        </div>
                                                                     
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-flat pull-left"><i class="fa fa-search"></i> {{ language:Search }}</button>
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal"><i class="fa fa-close"></i> {{ language:Close }}</button>
            </div>
            </form>
        </div>
    </div>
</div>



<!-- Modal untuk pencarian fungsional -->
<div class="modal fade" id="modal_form_fungsional" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" onclick="open_diagram_edit()">{{ language:Searching }}</h4>
            </div>
            <div class="modal-body form">
                <form action="{{ base_url }}organization_mapping/diagram_frame/chart_update" method="post" class="form-horizontal">
                    <input type="hidden" value="" name="COID"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-12 text-center" data-toggle="popover" title="{{ language:Information }}" data-content="" name="title_txt"></label>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Level Grade }}</label>
                            <div class="col-md-9">
                                <label for="Grade" class="control-label" name="Grade"></label>
                                <input type="hidden" value="" name="GradeID"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Position History }}</label>
                            <div class="col-md-9" id="position_field_fungsional">
                                    
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Profession }}</label>
                            <div class="col-md-9">
                                <select data-live-search="true" class="selectpicker form-control" multiple data-max-options="2" data-show-subtext="true" data-container="body" data-width="100%" name="ProfessionID[]" id="ProfessionID" data-header="{{ language:Select }} {{ language:Profession }}">
                                <?php
                                $SQL    = "SELECT profession_code,profession_name FROM mst_profession ORDER BY profession_code ASC";
                                $QUERY  = $this->db->query($SQL);    

                                foreach($QUERY->result() as $row): ?>                                           
                                    <option value="<?php echo $row->profession_code?>" data-subtext="<?php echo $row->profession_code?>"><?php echo $row->profession_name?></option>            
                                <?php 
                                endforeach;
                                ?>
                                </select>    
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Education }}</label>
                            <div class="col-md-9">
                                <select data-live-search="true" class="selectpicker form-control" multiple data-show-subtext="true" data-container="body" data-width="100%" name="EducationID[]" id="EducationID" data-header="{{ language:Select }} {{ language:Education }}">
                                <?php
                                $SQL    = "SELECT Branch_of_study FROM tp_profile_education WHERE Branch_of_study != '' AND (Branch_of_study NOT LIKE 'SD%' AND Branch_of_study NOT LIKE 'SMP%' AND Branch_of_study NOT LIKE 'SLTP%' AND Branch_of_study NOT LIKE 'Sekolah Dasar%') GROUP BY Branch_of_study ORDER BY Branch_of_study ASC";
                                $QUERY  = $this->db->query($SQL);    

                                foreach($QUERY->result() as $row): ?>                                           
                                    <option value="<?php echo $row->Branch_of_study?>" data-subtext=""><?php echo $row->Branch_of_study?></option>            
                                <?php 
                                endforeach;
                                ?>
                                </select>    
                            </div>
                        </div>                        
                        <input type="hidden" name="TrainingID" id="TrainingID" value="0" />                       
                                                                     
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-flat pull-left"><i class="fa fa-search"></i> {{ language:Search }}</button>
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal"><i class="fa fa-close"></i> {{ language:Close }}</button>
            </div>
            </form>
        </div>
    </div>
</div>

<style type="text/css">
/*
.modal-title{
    cursor: pointer !important;
}
*/
.call-edit{
    cursor: pointer !important;
}
.result:hover {
    background-color: #ffff99;
}
.erows{
    background-color: #F4F4F4;
}

.remarks{
    font-size: 10px !important;
}
dt{
    text-decoration: underline;
}
label[name="title_txt"]{
    cursor: pointer !important;
}

</style>

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'bottom',
        trigger : 'hover',
        html: true,
    });
});
</script>

<script type="text/javascript">
$(document).ready(function(){   
    $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});
});
</script>