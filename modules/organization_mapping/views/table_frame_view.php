<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$asset = new CMS_Asset();
foreach($css_files as $file){
    $asset->add_css($file);
}
echo $asset->compile_css();

foreach($js_files as $file){
    $asset->add_js($file);
}
echo $asset->compile_js();

// For every content of option tag, this will replace '&nbsp;' with ' '
function __ommit_nbsp($matches){
    return $matches[1].str_replace('&nbsp;', ' ', $matches[2]).$matches[3];
}

if(isset($dropdown_setup)){
  $this->load->view('dependent_dropdown', $dropdown_setup);
}


echo preg_replace_callback('/(<option[^<>]*>)(.*?)(<\/option>)/si', '__ommit_nbsp', $output);
?>

<style type="text/css">
	a.no-link{
		color: #333;
	}
</style>

<script type="text/javascript">
$(document).ready(function(){

	var Functional = $('input[name="Functional"]:checked', '#crudForm').val();

	if (Functional == 1){		
		$('#md_table_citizen_container').show();
		$('#Detail_field_box').show();
		$('#EmployeeID_field_box').hide();
		$('#GradeID_field_box').hide();
	}
	else{
		$('#md_table_citizen_container').hide();
		$('#Detail_field_box').hide();
		$('#EmployeeID_field_box').show();
		$('#GradeID_field_box').show();
		
	}

    $('[name="Functional"]').change(function(){

    	if ($(this).val() == 1){		
			$('#md_table_citizen_container').show();
			$('#Detail_field_box').show();
			$('#EmployeeID_field_box').hide();
			$('#GradeID_field_box').hide();			
		}
		else{
			$('#md_table_citizen_container').hide();
			$('#Detail_field_box').hide();
			$('#EmployeeID_field_box').show();
			$('#GradeID_field_box').show();
		}        
    });
});
</script>