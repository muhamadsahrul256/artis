<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$today     = date('Y-m-d_H:i:s');

$file_name = 'Organization_Mapping_'.$today;

function __ommit_nbsp($matches){
    return $matches[1].str_replace('&nbsp;', ' ', $matches[2]).$matches[3];
}

$this->load->model('orgchart_model');

$crud = new organization_chart();

if(isset($_POST['COID'])){
    echo $crud->open_form_chart($COID=$_POST['COID']);
}

elseif(isset($_POST['COID_TP'])){
    echo $crud->open_form_chart_search($COID=$_POST['COID_TP']);
}
else{
    echo $output;
}

?>


<style type="text/css">
    /*Now the CSS*/
    * {
        margin: 0;
        padding: 0;
    }
    .tree {
        width: auto;
        margin-left: auto;
        margin-right: auto;
    }
    .tree ul {
        padding-top: 20px;
        position: relative;
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
    }
    .tree li {
        float: left;
        text-align: center;
        list-style-type: none;
        position: relative;
        padding: 20px 5px 0 5px;
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
    }
    /*We will use ::before and ::after to draw the connectors*/
    .tree li::before, .tree li::after {
        content:'';
        position: absolute;
        top: 0;
        right: 50%;
        border-top: 1px solid black;
        width: 50%;
        height: 20px;
    }
    .tree li:after {
        right: auto;
        left: 50%;
        border-left: 1px solid black;
    }
    /*We need to remove left-right connectors from elements without 
any siblings*/
    .tree li:only-child::after, .tree li:only-child::before {
        display: none;
    }
    /*Remove space from the top of single children*/
    .tree li:only-child {
        padding-top: 0;
    }
    /*Remove left connector from first child and 
right connector from last child*/
    .tree li:first-child::before, .tree li:last-child::after {
        border: 0 none;
    }
    /*Adding back the vertical connector to the last nodes*/
    .tree li:last-child::before {
        border-right: 1px solid black;
        border-radius: 0 5px 0 0;
        -webkit-border-radius: 0 5px 0 0;
        -moz-border-radius: 0 5px 0 0;
    }
    .tree li:first-child::after {
        border-radius: 5px 0 0 0;
        -webkit-border-radius: 5px 0 0 0;
        -moz-border-radius: 5px 0 0 0;
    }
    /*Time to add downward connectors from parents*/
    .tree ul ul::before {
        content:'';
        position: absolute;
        top: 0;
        left: 50%;
        border-left: 1px solid black;
        width: 0;
        height: 20px;
        margin-left: -1px;
    }
    .tree li a {
        border: 1px solid black;
        padding: 5px 10px;
        text-decoration: none;
        color: black;
        font-family: arial, verdana, tahoma;
        font-size: 11px;
        display: inline-block;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
    }
    /*Time for some hover effects*/
    /*We will apply the hover effect the the lineage of the element also*/
    .tree li a:hover, .tree li a:hover+ul li a {
        background: #c8e4f8;
        color: #000;
        border: 1px solid #94a0b4;
    }
    /*Connector styles on hover*/
    .tree li a:hover+ul li::after, .tree li a:hover+ul li::before, .tree li a:hover+ul::before, .tree li a:hover+ul ul::before {
        border-color: #94a0b4;

    }
    li a.just-line {
        display: none;
    }
    a.just-line + ul {
        padding-top: 74px;
    }
    a.just-line + ul:before {
        height: 74px;
    }

    .garis-atas{
        border-bottom: 1px solid black;
        display: block;        
        width: 100%;
    }
    .judul{
        font-weight: bold;
    }
    .btn-toolbar .btn{
        margin-right: 5px;
    }
    .btn-toolbar .btn:last-child{
        margin-right: 0;
    }
    .jabatan-kosong{
        background: #FFFF99;
    }




    
</style>

<script type="text/javascript">
$(document).ready(function(){

    $('.launch-modal').click(function(){
        $('#myModal').modal({
            backdrop: 'static'
        });
    });

    $('.launch-search').click(function(){
        $('#myModal').modal({
            backdrop: 'static'
        });
    }); 

});
</script>
<style type="text/css">
    .bs-example{
        margin: 20px;
    }
</style>

<div class="bs-example">
    <div id="myModal" class="modal fade">
        <form action="{{ base_url }}organization_mapping/organization_chart/chart_update" method="post" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">{{ language:Control }}</h4>
                </div>
                <div class="modal-body">
                  

                </div>               


            </div>
        </div>
        </form>
    </div>
</div>

<script type="text/javascript">
var base_url = '<?php echo base_url()?>';
        $(function(){
            $(document).on('click','.launch-modal',function(e){  
                
                $("#myModal").modal('show');             

                $.post(base_url+'organization_mapping/organization_chart/',
                    {COID:$(this).attr('data-id')},
                    function(html){                                              
                                 
                        $(".modal-body").html(html);
                        $('.selectpicker').selectpicker({style: 'btn-default',size: "auto"});                         

                    }   
                );
            });

            

        });
</script>


<script type="text/javascript">
var base_url = '<?php echo base_url()?>';
        $(function(){
            $(document).on('click','.launch-search',function(e){  

                $("#myModal").modal('show');             

                $.post(base_url+'organization_mapping/organization_chart/',
                    {COID_TP:$(this).attr('data-id')},
                    function(html){                        
                        $(".modal-body").html(html);
                        $('.selectpicker').selectpicker({style: 'btn-default',size: "auto"}); 

                    }   
                );
            });

            

        });
</script>

<script type="text/javascript">

$(".modal-dialog").draggable({
    handle: ".modal-header"
});

    $('#TrainingID').change(function(){
        if($(this).attr('checked')){
            $(this).val(1);
        }else{
            $(this).val(0);
        }
    });
</script>

<script type="text/javascript">

$(function () {
    // $('#myModal').modal('hide');
});

document.getElementById("btnPrint").onclick = function () {
    printElement(document.getElementById("clonetext"));

    /**var modThis = document.querySelector("#printSection .modifyMe");
    modThis.appendChild(document.createTextNode(" new"));**/

}

function printElement(elem) {
    var domClone = elem.cloneNode(true);

    var $printSection = document.getElementById("printSection");

    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }   
    $printSection.innerHTML = "";

    $printSection.appendChild(domClone);

    document.title = '<?php echo $file_name; ?>';

    window.print();
}

</script>

<style type="text/css">
@media screen {
    #printSection {
        display: none;
    }
}
@media print {
    body * {
        visibility:hidden;        
        font-family: Arial;
    }
    table{
        font-size: 8px;
    }
    #non-printable { display: none; }
    
    #printSection, #printSection * {
        visibility:visible;
    }
    #printSection {
        position:absolute;
        left:0;
        top:0;
    }
}

</style>

<script type="text/javascript">
$("#btnExcel").click(function(){
  $("#clonetext").table2excel({
    // exclude CSS class
    exclude: ".noExl",
    name: "Worksheet Name",
    filename: "<?php echo $file_name ?>",
    fileext: ".xls",
    exclude_img: true,
    exclude_links: true,
    exclude_inputs: true
  }); 
});
</script>