<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$today     = date('Y-m-d_H:i:s');
$file_name = 'Talent_Pool_'.$today;


$asset = new CMS_Asset();
foreach($css_files as $file){
    $asset->add_css($file);
}
echo $asset->compile_css();

foreach($js_files as $file){
    $asset->add_js($file);
}


if ($state == 'edit'){
    echo $asset->compile_js();
}


$crud = new diagram_frame();

// For every content of option tag, this will replace '&nbsp;' with ' '
function __ommit_nbsp($matches){
    return $matches[1].str_replace('&nbsp;', ' ', $matches[2]).$matches[3];
}

if(isset($_POST['COID_TP'])){
    echo $crud->open_form_chart_search($COID=$_POST['COID_TP']);
}
else{
	echo preg_replace_callback('/(<option[^<>]*>)(.*?)(<\/option>)/si', '__ommit_nbsp', $output);
}
?>

<script type="text/javascript">
$(document).ready(function(){

	$('.selectpicker').selectpicker({
		style: 'btn-default',size: "auto"
	});

    $('.launch-search').click(function(){
        $('#myModal').modal({
            backdrop: 'static'
        });
    }); 

});
</script>
<style type="text/css">
    .bs-example{
        margin: 20px;
    }
</style>

<div class="bs-example">
    <div id="myModal" class="modal fade">
        <form action="{{ base_url }}organization_mapping/diagram_frame/chart_update" method="post" class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">{{ language:Searching }}</h4>
                </div>
                <div class="modal-body">                  

                </div>
            </div>
        </div>
        </form>
    </div>
</div>

<script type="text/javascript">
var base_url = '<?php echo base_url()?>';
        $(function(){
            $(document).on('click','.launch-search',function(e){                
                $("#myModal").modal('show');
                $.post(base_url+'organization_mapping/diagram_frame/form_searching/',
                    {COID_TP:'<?php echo $primary_key;?>'},
                    function(html){                               
                                 
                        $(".modal-body").html(html);
                        $('.selectpicker').selectpicker({style: 'btn-default',size: "auto"});                         

                    }   
                );
            });

            

        });
</script>

<script type="text/javascript">

    $(".modal-dialog").draggable({
        handle: ".modal-header"
    });

    $('#TrainingID').change(function(){
        if($(this).attr('checked')){
            $(this).val(1);
        }else{
            $(this).val(0);
        }
    });
</script>


<script type="text/javascript">

function querystring(key) {
   var re=new RegExp('(?:\\?|&)'+key+'=(.*?)(?=&|$)','gi');
   var r=[], m;
   while ((m=re.exec(document.location.search)) != null) r[r.length]=m[1];
   return r;
}

    // CHECK ALL
    $('.checkall').live('click', function(){
        $(this).parents('table:eq(0)').find(':checkbox').attr('checked', this.checked);
    });
    // DELETE ALL
    $('.print_all_button').live('click', function(event){
        event.preventDefault();
        var list = new Array();
        $('#flex1 input[type=checkbox]').each(function() {
            if (this.checked) {
                //create list of values that will be parsed to controller
                list.push(this.value);
            }
        });
        //send data to delete
        $.post('{{ MODULE_SITE_URL }}diagram_frame/set_candidate_value', { data: JSON.stringify(list) }, function(data) {
            /*
            var primary_key = querystring("id");
            var win = window.open('{{ MODULE_SITE_URL }}diagram_frame/export_to_excel/?id='+primary_key+'&json='+JSON.stringify(list), '_blank');
            win.focus();
            */

            for(i=0; i<list.length; i++){
                //remove selection rows
                $('#flex1 tr[canId="' + list[i] + '"]').remove();
            }

            alert(list);
        });
    });


    $(document).ajaxComplete(function(){
        // TODO: Put your custom code here
    });

    $(document).ready(function(){       
        
            $('#flex1 tr .prt-check').prepend('<input type="checkbox" class="checkall" />');
            $('#flex1 tr .chd-check').each(function(){
                $(this).prepend('<input type="checkbox" value="'+$(this).attr('canId')+'"/>');
            });
       

    })
</script>

<script type="text/javascript">

$(function () {
    // $('#myModal').modal('hide');
});

document.getElementById("btnPrint").onclick = function () {
    printElement(document.getElementById("clonetext"));

    window.open('Content-Type: application/vnd.ms-excel,' + $('#clonetext').html());
    e.preventDefault();

    /**var modThis = document.querySelector("#printSection .modifyMe");
    modThis.appendChild(document.createTextNode(" new"));**/

}

function printElement(elem){
    var domClone = elem.cloneNode(true);

    var $printSection = document.getElementById("printSection");

    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }   
    $printSection.innerHTML = "";

    $printSection.appendChild(domClone);
    window.print();
}

</script>

<script type="text/javascript">
$("#btnExcel").click(function(){
  $("#clonetext").table2excel({
    // exclude CSS class
    exclude: ".noExl",
    name: "Worksheet Name",
    filename: "<?php echo $file_name ?>",
    fileext: ".xls",
    exclude_img: true,
    exclude_links: true,
    exclude_inputs: true
  }); 
});
</script>

<style type="text/css">
@media screen {
    #printSection {
        display: none;
    }
}
@media print {
    body * {
        visibility:hidden;        
        font-family: Arial;
    }
    table{
        font-size: 8px;
    }
    .non-printable { display: none; }
    
    #printSection, #printSection * {
        visibility:visible;
    }
    #printSection {
        position:absolute;
        left:0;
        top:0;
    }
}

.border-all{
    border-left: solid 1px;
    border-right: solid 1px;
    border-top: solid 1px;
    border-bottom: solid 1px; 
}
.chd-check{
    border-left: solid 1px;
    border-right: solid 1px;
    border-top: solid 1px;
    border-bottom: solid 1px; 
}
.tebalin{
    font-weight: bold;
}

table td {
    word-wrap:break-word;
}

</style>