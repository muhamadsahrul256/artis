<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>


<link rel="stylesheet" href="{{ base_url }}assets/plugins/OrgChart-master/dist/css/jquery.orgchart.css">
<link rel="stylesheet" href="{{ base_url }}assets/plugins/OrgChart-master/examples/css/style.css">

<script src="{{ base_url }}assets/plugins/Panning-Zooming/dist/jquery.panzoom.js"></script>
<script src="{{ base_url }}assets/plugins/Panning-Zooming/test/libs/jquery.mousewheel.js"></script>


<script type="text/javascript" src="{{ base_url }}themes/AdminLTE/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="{{ base_url }}assets/plugins/OrgChart-master/dist/js/jquery.orgchart.js"></script>


<script type="text/javascript" src="{{ base_url }}assets/plugins/OrgChart-master/dist/js/es6-promise.auto.min.js"></script>
<script type="text/javascript" src="{{ base_url }}assets/plugins/OrgChart-master/dist/js/html2canvas.min.js"></script>
<script type="text/javascript" src="{{ base_url }}assets/plugins/OrgChart-master/dist/js/jspdf.debug.js"></script>


<nav class="navbar navbar-default navbar-fixed" id="nav_tools">
    <div class="container-fluid">    
        <ul class="nav navbar-nav">
            <li><a href="{{ base_url }}organization_mapping/diagram_frame/table_frame"><i class="fa fa-th-large"></i> {{ language:Table View }}</a></li>
            <li><a href="javascript:void(0);" class="show-all-tree"><i class="fa fa-plus"></i> {{ language:Show All Tree }}</a></li>
            <!--<li><a href="javascript:void(0);" class="hide-all-tree"><i class="fa fa-minus"></i> {{ language:Hide All Tree }}</a></li>-->
            
            <li class="dropdown">
                <?php echo $period_option; ?>
            </li>

            <?php if ($unmapping_qty > 0){ ?>
            <li><a href="javascript:void(0);" class="" onclick="unmapping_person('<?php echo $period_id;?>')">{{ language:Unmapping }} <span class="label label-danger"><?php echo $unmapping_qty;?></span></a></li>
            <?php } ?>
            
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="#Kantor_Induk_TJBB"><i class="fa fa-sitemap"></i> TJBB</a></li>
            <li><a href="#APP_Cawang"><i class="fa fa-sitemap"></i> Cawang</a></li>
            <li><a href="#APP_Pulogadung"><i class="fa fa-sitemap"></i> Pulogadung</a></li>
            <li><a href="#APP_Durikosambi"><i class="fa fa-sitemap"></i> Durikosambi</a></li>
            <li><a href="#APP_Cilegon"><i class="fa fa-sitemap"></i> Cilegon</a></li>            
        </ul>

        <!--     
        <div class="input-group col-sm-3 pull-right">
            <input type="text" class="form-control" placeholder="{{ language:Search something, see the green text... }}" name="search_val" id="search_val" value="">         
        </div>
        -->
        
        

    </div>    
</nav>

<div class="scrollit">

        <section id="y-only">     
        <style>
            #set .parent { text-align: center; }
            #set .parent .panzoom { border: 3px solid; }
            #set .parent > div { display: inline-block; width: 30%; }
            #set .parent img { width: 100%; }
            
            
        </style>

        <div class="parent clearfix">
            <div class="panzoom">               
                <div class="box-header">
                    <i class="fa fa-sitemap"></i><h3 class="box-title"><?php echo $subject; ?></h3>       
                </div>
                
                <div class="content-chart" id="content">
                    <section id="Kantor_Induk_TJBB">
                        <div class="charts" id="chart-container-3401"></div>
                    </section>
                    <section id="APP_Cawang">
                        <div class="charts" id="chart-container-3411"></div>
                    </section>
                    <section id="APP_Pulogadung">
                        <div class="charts" id="chart-container-3412"></div>
                    </section>
                    <section id="APP_Durikosambi">
                        <div class="charts" id="chart-container-3413"></div>
                    </section>
                    <section id="APP_Cilegon">
                        <div class="charts" id="chart-container-3414"></div>
                    </section>            
                </div>

            </div>
        </div>
    

        <script>
            (function() {
              var $section = $('#y-only');
              var $panzoom = $section.find('.panzoom').panzoom();
             
            })();
        </script>

        </section>
    
</div>


<?php echo $Kantor_Induk_TJBB; ?>  
<?php echo $APP_Cawang; ?>
<?php echo $APP_Pulogadung; ?>
<?php echo $APP_Durikosambi; ?>
<?php echo $APP_Cilegon; ?>


<script type="text/javascript">


$(document).ready(function(){

    
    /*
    $("tr").removeClass("hidden");
    $("ul").removeClass("hidden");
    $("div").removeClass("slide-up");
    $(".lines").css("visibility", "visible");
    $("ul").removeClass("slide");

    $('.orgchart').find('.toggleBtn').removeClass('fa-plus-square').addClass('fa-minus-square');
    */

    

    //$(".slide").removeClass("toggleBtn fa fa-plus-square").addClass( "toggleBtn fa fa-minus-square" );

});


$('a.show-all-tree').click(function() {
   
    $(".orgchart").find("tr").removeClass("hidden");
    $(".orgchart").find("ul").removeClass("hidden");
    $(".orgchart").find("div").removeClass("slide-up");
    $(".orgchart").find(".lines").css("visibility", "visible");
    $(".orgchart").find("ul").removeClass("slide");
    $(".orgchart").find(".toggleBtn").removeClass("fa-plus-square").addClass("fa-minus-square");

});

$('a.hide-all-tree').click(function() {
    //$(".orgchart").find("tr").addClass("hidden");
    /*
    $(".orgchart").find("ul").addClass("hidden");
    $(".orgchart").find("div [id >=1]").addClass("slide-up");
    $(".orgchart").find(".lines").css("visibility", "hidden");
    $(".orgchart").find("ul").addClass("slide");
    $(".orgchart").find(".toggleBtn").removeClass("fa-minus-square").addClass("fa-plus-square");
    */
});


</script>


<script type="text/javascript">

var save_method;
var table;
var primary_key;

function edit_person(id){
    save_method = 'update';
    primary_key = id;
    //$('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('organization_mapping/diagram_frame/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            var status = data.Functional;

            var url_edit = "<?php echo site_url('organization_mapping/diagram_frame/table_frame/edit/')?>/"+id;

            if (status == 0){
                var text_status = 'Struktural';
            }
            else{
                var text_status = 'Fungsional';
            }

            var remarks = '<div class="box-body remarks">'+
              '<dl>'+
                '<dt>{{ language:Full Name Position }} :</dt>'+
                '<dd>'+data.long_name_position+'</dd>'+
                '<dt>{{ language:Organization Unit }} :</dt>'+
                '<dd>'+data.UnitID+'</dd>'+
                '<dt>{{ language:Mapping Code }} :</dt>'+
                '<dd>'+data.Full_Mapping_Code+'</dd>'+
                '<dt>{{ language:Officer }} :</dt>'+
                '<dd>'+data.EmployeeName+'</dd>'+
                '<dt>{{ language:Grade (maximum) }} :</dt>'+
                '<dd>'+data.GradeCode+'</dd>'+
              '</dl>'+
            '</div>';


            $('[name="title_txt"]').attr("data-content", remarks);
            $('[name="title_txt"]').text(data.Description);
            $('[name="COID"]').val(data.COID);


            var trHTML = '<select data-live-search="true" class="selectpicker form-control" multiple data-show-subtext="true" data-container="body" data-width="100%" name="PositionID[]" id="PositionID" data-header="{{ language:Select }} {{ language:Position History }}">';
            var no = 1;
            $.each(data.option_position, function (key,value) {
                trHTML +='<option value="'+value.position_name+'" data-subtext="">'+value.position_name +'</option>';
               no++;     
            });
            trHTML += '</select>';

            $('#position_field_struktural').html(trHTML);
            $('#position_field_fungsional').html(trHTML);

            $('.selectpicker').selectpicker({style: 'btn-default',size: 'auto'});
            $('.selectpicker').selectpicker('refresh');

            if (status == 1){

                $('[name="Grade"]').text(data.search_grade);
                $('[name="GradeID"]').val(data.search_grade);

                $('#modal_form_fungsional').modal({backdrop: 'static'});
                $('#modal_form_fungsional').modal('show');
                //$('#modal_form_fungsional').draggable({handle: ".modal-header"});
            }
            else{

                $('[name="Grade"]').text(data.search_grade);
                $('[name="GradeID"]').val(data.search_grade);
                
                $('#modal_form').modal({backdrop: 'static'});
                $('#modal_form').modal('show');
                //$('#modal_form').draggable({handle: ".modal-header"});
            }

            //$('.modal-title').text('{{ language:Searching Talent }} #'+data.COID);
            $('.modal-title').html('{{ language:Searching Talent }} '+text_status+' #'+data.COID+' <i class="fa fa-pencil call-edit" title="edit" onclick="open_diagram_edit()"></i>');

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function unmapping_person(id){
    save_method = 'update';
    primary_key = id;
    //$('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('organization_mapping/diagram_frame/ajax_unmapping/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            var trHTML = '';
            var no = 1;
            $.each(data, function (key,value) {
                trHTML += 
                '<tr>'+
                '<td class="text-center">'+no+'</td>'+
                '<td class="text-center">'+value.EmployeeID+'</td>'+
                '<td class="text-left">'+value.Full_Name +'</td>'+
                '<td class="text-left">'+value.new_position +'</td>'+
                '<td class="text-left">'+value.unit_id +'</td>'+
                '</tr>';
               no++;     
            });

            $('#body_unmapping').html(trHTML);

            $('#modal_form_unmapping').modal({backdrop: 'static'});
            $('#modal_form_unmapping').modal('show');           

            $('.modal-title').text('{{ language:Unmapping }}');

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}


function save(){

    $('#btnSave').text('saving...'); 
    $('#btnSave').attr('disabled',true); 
    var url;

    if(save_method == 'add') {
        url = "<?php echo site_url('organization_mapping/diagram_frame/chart_update')?>";
    } else {
        url = "<?php echo site_url('organization_mapping/diagram_frame/chart_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status)
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('save');
            $('#btnSave').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); 
            $('#btnSave').attr('disabled',false); 

        }
    });
}

function open_diagram_edit(){
    var url_edit = "<?php echo site_url('organization_mapping/diagram_frame/table_frame/edit/')?>/"+primary_key;
    location.href = url_edit;
}


</script>


<script type="text/javascript">

    $(".modal-dialog").draggable({
        handle: ".modal-header"
    });

    $(".modal-dialog").draggable({
        handle: ".modal-header"
    });

    $('#TrainingID').change(function(){
        if($(this).attr('checked')){
            $(this).val(1);
        }else{
            $(this).val(0);
        }
    });

</script>

<!-- Modal untuk pencarian struktural -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ language:Searching }} </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ base_url }}organization_mapping/diagram_frame/chart_update" method="post" class="form-horizontal">
                    <input type="hidden" value="" name="COID"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-12 text-center" data-toggle="popover" title="{{ language:Information }}" data-content="" name="title_txt"></label>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Level Grade }}</label>
                            <div class="col-md-9">
                                <label for="Grade" class="control-label" name="Grade"></label>
                                <input type="hidden" value="" name="GradeID"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Position History }}</label>
                            <div class="col-md-9" id="position_field_struktural">

                                <!--
                                <select data-live-search="true" class="selectpicker form-control" multiple data-show-subtext="true" data-container="body" data-width="100%" name="PositionID[]" id="PositionID" data-header="{{ language:Select }} {{ language:Position History }}">
                                <?php
                                $SQL    = "SELECT position_id, position_name FROM mst_position_ladder GROUP BY position_name ORDER BY position_name ASC";
                                $QUERY  = $this->db->query($SQL);    

                                foreach($QUERY->result() as $row): ?>                                           
                                    <option value="<?php echo $row->position_name?>" data-subtext="<?php echo $row->position_id?>"><?php echo $row->position_name?></option>            
                                <?php 
                                endforeach;
                                ?>
                                </select>
                                -->

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Profession }}</label>
                            <div class="col-md-9">
                                <select data-live-search="true" class="selectpicker form-control" multiple data-max-options="2" data-show-subtext="true" data-container="body" data-width="100%" name="ProfessionID[]" id="ProfessionID" data-header="{{ language:Select }} {{ language:Profession }}">
                                <?php
                                $SQL    = "SELECT profession_code,profession_name FROM mst_profession ORDER BY profession_code ASC";
                                $QUERY  = $this->db->query($SQL);    

                                foreach($QUERY->result() as $row): ?>                                           
                                    <option value="<?php echo $row->profession_code?>" data-subtext="<?php echo $row->profession_code?>"><?php echo $row->profession_name?></option>            
                                <?php 
                                endforeach;
                                ?>
                                </select>    
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Education }}</label>
                            <div class="col-md-9">
                                <select data-live-search="true" class="selectpicker form-control" multiple data-show-subtext="true" data-container="body" data-width="100%" name="EducationID[]" id="EducationID" data-header="{{ language:Select }} {{ language:Education }}">
                                <?php
                                $SQL    = "SELECT Branch_of_study FROM tp_profile_education WHERE Branch_of_study != '' AND (Branch_of_study NOT LIKE 'SD%' AND Branch_of_study NOT LIKE 'SMP%' AND Branch_of_study NOT LIKE 'SLTP%' AND Branch_of_study NOT LIKE 'Sekolah Dasar%') GROUP BY Branch_of_study ORDER BY Branch_of_study ASC";
                                $QUERY  = $this->db->query($SQL);    

                                foreach($QUERY->result() as $row): ?>                                           
                                    <option value="<?php echo $row->Branch_of_study?>" data-subtext=""><?php echo $row->Branch_of_study?></option>            
                                <?php 
                                endforeach;
                                ?>
                                </select>    
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-9">
                                <label><input type="checkbox" name="TrainingID" id="TrainingID" value="1" checked> {{ language:Show Training Data }}</label>
                            </div>
                        </div>
                                                                     
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-flat pull-left"><i class="fa fa-search"></i> {{ language:Search }}</button>
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal"><i class="fa fa-close"></i> {{ language:Close }}</button>
            </div>
            </form>
        </div>
    </div>
</div>



<!-- Modal untuk pencarian fungsional -->
<div class="modal fade" id="modal_form_fungsional" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ language:Searching }}</h4>
            </div>
            <div class="modal-body form">
                <form action="{{ base_url }}organization_mapping/diagram_frame/chart_update" method="post" class="form-horizontal">
                    <input type="hidden" value="" name="COID"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-12 text-center" data-toggle="popover" title="{{ language:Information }}" data-content="" name="title_txt"></label>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Level Grade }}</label>
                            <div class="col-md-9">
                                <label for="Grade" class="control-label" name="Grade"></label>
                                <input type="hidden" value="" name="GradeID"/>                    
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Position History }}</label>
                            <div class="col-md-9" id="position_field_fungsional">

                              
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Profession }}</label>
                            <div class="col-md-9">
                                <select data-live-search="true" class="selectpicker form-control" multiple data-max-options="2" data-show-subtext="true" data-container="body" data-width="100%" name="ProfessionID[]" id="ProfessionID" data-header="{{ language:Select }} {{ language:Profession }}">
                                <?php
                                $SQL    = "SELECT profession_code,profession_name FROM mst_profession ORDER BY profession_code ASC";
                                $QUERY  = $this->db->query($SQL);    

                                foreach($QUERY->result() as $row): ?>                                           
                                    <option value="<?php echo $row->profession_code?>" data-subtext="<?php echo $row->profession_code?>"><?php echo $row->profession_name?></option>            
                                <?php 
                                endforeach;
                                ?>
                                </select>    
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">{{ language:Education }}</label>
                            <div class="col-md-9">
                                <select data-live-search="true" class="selectpicker form-control" multiple data-show-subtext="true" data-container="body" data-width="100%" name="EducationID[]" id="EducationID" data-header="{{ language:Select }} {{ language:Education }}">
                                <?php
                                $SQL    = "SELECT Branch_of_study FROM tp_profile_education WHERE Branch_of_study != '' AND (Branch_of_study NOT LIKE 'SD%' AND Branch_of_study NOT LIKE 'SMP%' AND Branch_of_study NOT LIKE 'SLTP%' AND Branch_of_study NOT LIKE 'Sekolah Dasar%') GROUP BY Branch_of_study ORDER BY Branch_of_study ASC";
                                $QUERY  = $this->db->query($SQL);    

                                foreach($QUERY->result() as $row): ?>                                           
                                    <option value="<?php echo $row->Branch_of_study?>" data-subtext=""><?php echo $row->Branch_of_study?></option>            
                                <?php 
                                endforeach;
                                ?>
                                </select>    
                            </div>
                        </div>                        
                        <input type="hidden" name="TrainingID" id="TrainingID" value="0" />                       
                                                                     
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-flat pull-left"><i class="fa fa-search"></i> {{ language:Search }}</button>
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal"><i class="fa fa-close"></i> {{ language:Close }}</button>
            </div>
            </form>
        </div>
    </div>
</div>



<!-- Modal untuk Unmapping -->
<div class="modal fade" id="modal_form_unmapping" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ language:Unmapping }}</h4>
            </div>
            <div class="modal-body form">
                    <input type="hidden" value="" name="periode_id"/> 
                    <div class="form-body">

                        <div class="table-responsive" style="margin-top:10px;">
                            <table id="table1" class="table table-vcenter table-responsive table-condensed table-bordered table-striped table-hover display" style="font-size: 11px">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="5%">No</th>
                                        <th class="text-center" width="10%">NIP</th>                                            
                                        <th class="text-left">Nama</th>
                                        <th class="text-center" width="30%">Posisi</th>
                                        <th class="text-center" width="8%">Org Unit</th>                               
                                    </tr>
                                </thead>
                                <tbody id="body_unmapping">

                                </tbody>            
                            </table>
                        </div>   
                           
                                                                     
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal"><i class="fa fa-close"></i> {{ language:Close }}</button>
            </div>
            
        </div>
    </div>
</div>

<style type="text/css">    
#nav_tools .navbar-nav > li > a, #nav_tools .navbar-brand {
    padding-top:5px !important; padding-bottom:0 !important;
    height: 30px;
    font-size: 12px;
}
#nav_tools.navbar {
    min-height:30px !important;
}

.scrollit {
    overflow-y:scroll;
    background:#ffffff;

}
.box-body
{
    direction: ltr;
}

.box-header
{
    direction: ltr;
}
/*
.modal-title{
    cursor: pointer !important;
}
*/

.call-edit{
    cursor: pointer !important;
}
.found{
    color:green;
    background-color: rgb(0,255,0);
    font-weight: bold;
}

.remarks{
    font-size: 10px !important;
}
dt{
    text-decoration: underline;
}
label[name="title_txt"]{
    cursor: pointer !important;
}
</style>

<script type="text/javascript">
    $(document).ready(function(){
        var height_my_device = screen.height;
        var height_my_header = $('.navbar-static-top:visible').height();
        var height_my_footer = $('.main-footer:visible').height();
        var height_my_nav    = $('.content-header:visible').height();
        var height_my_tool   = $('#nav_tools:visible').height();
        var height_my_diagram = (height_my_device-(height_my_header+height_my_footer+height_my_nav+height_my_tool+200));
        $(".scrollit").css("height", height_my_diagram+"px");         
    })
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'bottom',
        trigger : 'hover',
        html: true,
    });

 });
</script>

<script type="text/javascript">
$(document).ready(function(){   
    $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});
});
</script>

<!--
<script src="{{ base_url }}assets/js/search.js"></script>
-->