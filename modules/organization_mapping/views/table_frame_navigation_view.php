<?php
	$asset = new CMS_Asset();
	$asset->add_module_css('styles/navigation.css','organization_mapping');
	foreach($css_files as $file){
		$asset->add_css($file);
	}
	echo $asset->compile_css();

	foreach($js_files as $file){
		$asset->add_js($file);
	}
	$asset->add_module_js('scripts/navigation.js','organization_mapping');
	echo $asset->compile_js();

    if(count($navigation_path)>0){
        echo '<div style="padding-bottom:10px;">';
        echo '<a class="btn btn-primary" href="'.site_url('organization_mapping/diagram_frame/table_frame_nav').'">First Level Diagram</a>';
        for($i=0; $i<count($navigation_path)-1; $i++){
            $navigation = $navigation_path[$i];
            echo '&nbsp;<a class="btn btn-primary" href="'.site_url('organization_mapping/diagram_frame/table_frame_nav/'.$navigation['navigation_id']).'">'.
                $navigation['navigation_name'].' ('.$navigation['title'].')'.'</a>';
        }
        echo '</div>';
    }
	echo $output;
?>
<script type="text/javascript">
    $(document).ready(function(){
        // override default_layout view
        $('#field-default_layout').hide();
        $('#default_layout_input_box').append('<select id="select-default_layout"></select>');
        // fetch layout
       
        // adjust real input
        $('#select-default_layout').live('change', function(){
            var selected_layout = $('#select-default_layout option:selected').val();
            $('#field-default_layout').val(selected_layout);
        });
    });

   
    $(document).ajaxComplete(function(){
        // remove sorting
        $('.field-sorting').removeClass('field-sorting');
        // add children
        $('.need-child').each(function(){
            $(this).removeClass('need-child');
            var navigation_id = $(this).val();
            var $current_tr = $(this).parent().parent().parent();
            var $table = $current_tr.parent().parent();
            var child_id = 'child-' + navigation_id;
            var filler_id = 'filler-' + navigation_id;
            // make child
            var html = '<tr id="'+child_id+'"><td style="padding-left:25px; padding-right:0px; border-top:0px;" colspan="2">{{ language:No-Children }}</td></tr>';
            $table.append(html);
            var $child = $('#'+child_id);
            // make filler
            var html = '<tr id="'+filler_id+'"><td colspan="2"></td></tr>';
            $table.append(html);
            var $filler = $('#'+filler_id);
            // move it
            $child.insertAfter($current_tr);
            $filler.insertAfter($current_tr);
            // hide everything for surprise :)
            //$child.hide();
            $filler.hide();
            // ajax thing
            $.ajax({
                'url' : '{{ MODULE_SITE_URL }}diagram_frame/table_frame_nav/'+navigation_id+'/ajax_list',
                'success' : function(response){
                    $('#' + child_id + ' td').html(response);
                    $('#' + child_id + ' .bDiv').css('padding-right', '0px');
                    $('#' + child_id + ' thead').remove();
                    hash = window.location.hash;
                    hash = hash.replace('#', '');
                    if($('a[name="' + hash + '"]').offset() != undefined){
                        $(document.body).scrollTop($('a[name="' + hash + '"]').offset().top);
                    }
                }
            });
        });
    })
</script>