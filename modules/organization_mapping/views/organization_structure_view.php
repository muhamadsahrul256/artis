<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<link rel="stylesheet" href="{{ base_url }}assets/plugins/OrgChart-master/dist/css/jquery.orgchart.css">
<link rel="stylesheet" href="{{ base_url }}assets/plugins/OrgChart-master/examples/css/style.css">

<script src="{{ base_url }}assets/plugins/Panning-Zooming/dist/jquery.panzoom.js"></script>
<script src="{{ base_url }}assets/plugins/Panning-Zooming/test/libs/jquery.mousewheel.js"></script>


<script type="text/javascript" src="{{ base_url }}themes/AdminLTE/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="{{ base_url }}assets/plugins/OrgChart-master/dist/js/jquery.orgchart.js"></script>


<script type="text/javascript" src="{{ base_url }}assets/plugins/OrgChart-master/dist/js/es6-promise.auto.min.js"></script>
<script type="text/javascript" src="{{ base_url }}assets/plugins/OrgChart-master/dist/js/html2canvas.min.js"></script>
<script type="text/javascript" src="{{ base_url }}assets/plugins/OrgChart-master/dist/js/jspdf.debug.js"></script>


<nav class="navbar navbar-default" id="nav_tools">
    <div class="container-fluid">    
        <ul class="nav navbar-nav">
            <!--<li><a href="{{ base_url }}organization_mapping/diagram_frame/table_frame"><i class="fa fa-th-large"></i> {{ language:Table View }}</a></li>-->
            <li><a href="javascript:void(0);" class="show-all-tree"><i class="fa fa-plus"></i> {{ language:Show All Tree }}</a></li>
            <!--<li><a href="javascript:void(0);" class="hide-all-tree"><i class="fa fa-minus"></i> {{ language:Hide All Tree }}</a></li>-->
            
            <li class="dropdown">
                <?php echo $period_option; ?>
            </li>

            
            
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="#Kantor_Induk_TJBB"><i class="fa fa-sitemap"></i> TJBB</a></li>
            <li><a href="#APP_Cawang"><i class="fa fa-sitemap"></i> Cawang</a></li>
            <li><a href="#APP_Pulogadung"><i class="fa fa-sitemap"></i> Pulogadung</a></li>
            <li><a href="#APP_Durikosambi"><i class="fa fa-sitemap"></i> Durikosambi</a></li>
            <li><a href="#APP_Cilegon"><i class="fa fa-sitemap"></i> Cilegon</a></li>            
        </ul>

        <!--     
        <div class="input-group col-sm-3 pull-right">
            <input type="text" class="form-control" placeholder="{{ language:Search something, see the green text... }}" name="search_val" id="search_val" value="">         
        </div>
        -->
        
        

    </div>    
</nav>

<div class="scrollit">

        <section id="y-only">     
        <style>
            #set .parent { text-align: center; }
            #set .parent .panzoom { border: 3px solid; }
            #set .parent > div { display: inline-block; width: 30%; }
            #set .parent img { width: 100%; }
            
            
        </style>

        <div class="parent clearfix">
            <div class="panzoom">               
                <div class="box-header">
                    <i class="fa fa-sitemap"></i><h3 class="box-title"><?php echo $subject; ?></h3>       
                </div>
                
                <div class="content-chart" id="content">
                    <section id="Kantor_Induk_TJBB">
                        <div class="charts" id="chart-container-3401"></div>
                    </section>
                    <section id="APP_Cawang">
                        <div class="charts" id="chart-container-3411"></div>
                    </section>
                    <section id="APP_Pulogadung">
                        <div class="charts" id="chart-container-3412"></div>
                    </section>
                    <section id="APP_Durikosambi">
                        <div class="charts" id="chart-container-3413"></div>
                    </section>
                    <section id="APP_Cilegon">
                        <div class="charts" id="chart-container-3414"></div>
                    </section>            
                </div>

            </div>
        </div>
    

        <script>
            (function() {
              var $section = $('#y-only');
              var $panzoom = $section.find('.panzoom').panzoom();
             
            })();
        </script>

        </section>
    
</div>


<?php echo $Kantor_Induk_TJBB; ?>  
<?php echo $APP_Cawang; ?>
<?php echo $APP_Pulogadung; ?>
<?php echo $APP_Durikosambi; ?>
<?php echo $APP_Cilegon; ?>


<script type="text/javascript">


$('a.show-all-tree').click(function() {
   
    $(".orgchart").find("tr").removeClass("hidden");
    $(".orgchart").find("ul").removeClass("hidden");
    $(".orgchart").find("div").removeClass("slide-up");
    $(".orgchart").find(".lines").css("visibility", "visible");
    $(".orgchart").find("ul").removeClass("slide");
    $(".orgchart").find(".toggleBtn").removeClass("fa-plus-square").addClass("fa-minus-square");

});




</script>


<style type="text/css">    
#nav_tools .navbar-nav > li > a, #nav_tools .navbar-brand {
    padding-top:5px !important; padding-bottom:0 !important;
    height: 30px;
    font-size: 12px;
}
#nav_tools.navbar {
    min-height:30px !important;
}

.scrollit {
    height:450px;
    overflow-y:scroll;
    background:#ffffff;
}
.box-body
{
    direction: ltr;
}

.box-header
{
    direction: ltr;
}

</style>
<script type="text/javascript">
    $(document).ready(function(){
        var height_my_device = screen.height;
        var height_my_header = $('.navbar-static-top:visible').height();
        var height_my_footer = $('.main-footer:visible').height();
        var height_my_nav    = $('.content-header:visible').height();
        var height_my_tool   = $('#nav_tools:visible').height();
        var height_my_diagram = (height_my_device-(height_my_header+height_my_footer+height_my_nav+height_my_tool+200));
        $(".scrollit").css("height", height_my_diagram+"px");         
    })
</script>
<script type="text/javascript">
    function edit_person(id) {
        var url = "<?php echo site_url('organization_mapping/organization_structure/ajax_edit')?>/"+id;

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                console.log(data.get_by_id);
                $('#edit_form').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error display data');
                // $('#btnSave').text('save'); 
                // $('#btnSave').attr('disabled',false); 

            }
        });
    }
</script>
<!-- Modal untuk pencarian struktural -->
<div class="modal fade" id="edit_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ language:Searching }} </h4>
            </div>
            <div class="modal-body form">
            <form action="{{ base_url }}" method="post" class="form-horizontal" id="form-edit-struktur">
                <div class="col-md-11">
                    <input type="hidden" value="" name="COID"/>
                    <div class="form-group">
                        <label id="fakyu"></label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-flat pull-left"><i class="fa fa-search"></i> {{ language:Search }}</button>
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal"><i class="fa fa-close"></i> {{ language:Close }}</button>
            </div>
            </form>
        </div>
    </div>
</div>