<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>


<div class="box box-solid bg-teal-grey">
            <div class="box-header">
              <i class="fa fa-th"></i>

              <h3 class="box-title">Sales Graph</h3>

              
            </div>
            <div class="box-body border-radius-none">
              
            

 <div class="content" id="content">
  <figure class="org-chart cf">
    <ul class="administration">
      <li>                  
        <ul class="director">
          <li>
            <a href="javascript:void(0);">
                <span class="garis-atas judul">GENERAL MANAJER</span>
                <span class="garis-atas ket-nama-user">TRINO ERWIN</span>
                <span class="pull-left ket-nik">7192162K3</span>
                <span class="pull-right ket-jabatan">OPT03-18</span>
            </a>


            <ul class="subdirector cf">
              <li class="section">              
                <a href="javascript:void(0);">
                    <span class="garis-atas judul">PLT Pejabat Pelaksana Pengadaan2</span>
                    <span class="garis-atas ket-nama-user">IMAM GUNAWARMAN</span>
                    <span class="pull-left ket-nik">6283122K3</span>
                    <span class="pull-right ket-jabatan">SYS03-31</span>                
                  </a>



                  <a href="javascript:void(0);" class="jabatan-fungsional">
                    <span class="judul"  style="text-align: left;">- AN/AS Perencanaan Pengusahaan AS REN PENGUSHN</span>
                    <span class="ket-nama-user" style="text-align: left;">HERMAWAN ISTIONO</span>
                    <span class="ket-nik">7192162K3 | OPT03-18</span>
                    <span class="judul" style="text-align: left;">- AS Perencana Pengadaan</span>
                    <span class="ket-nama-user"  style="text-align: left;">HERMAWAN ISTIONO</span>
                    <span class="ket-nik">7192162K3 | OPT03-18</span>
                    <span class="judul" style="text-align: left;">- AS Perencana Pengadaan</span>
                    <span class="ket-nama-user" style="text-align: left;">JEZZY DWI PUSPO</span>
                    <span class="ket-nik">9216949ZY | SPE04-01</span>                
                  </a>
              </li>              
            </ul>



            <ul class="subdirector-dua cf">
              <li class="section">
              <a href="javascript:void(0);">
                    <span class="garis-atas judul">PLT Pejabat Pelaksana Pengadaan</span>
                    <span class="garis-atas ket-nama-user">IMAM GUNAWARMAN</span>
                    <span class="pull-left ket-nik">6283122K3</span>
                    <span class="pull-right ket-jabatan">SYS03-31</span>                
                  </a>

                  <a href="javascript:void(0);" class="jabatan-fungsional">
                    <span class="judul"  style="text-align: left;">- AN/AS Perencanaan Pengusahaan AS REN PENGUSHN</span>
                    <span class="ket-nama-user" style="text-align: left;">HERMAWAN ISTIONO</span>
                    <span class="ket-nik">7192162K3 | OPT03-18</span>
                    <span class="judul" style="text-align: left;">- AS Perencana Pengadaan</span>
                    <span class="ket-nama-user"  style="text-align: left;">HERMAWAN ISTIONO</span>
                    <span class="ket-nik">7192162K3 | OPT03-18</span>
                    <span class="judul" style="text-align: left;">- AS Perencana Pengadaan</span>
                    <span class="ket-nama-user" style="text-align: left;">JEZZY DWI PUSPO</span>
                    <span class="ket-nik">9216949ZY | SPE04-01</span>                
                  </a>

              </li>
                      
              
            </ul>


            <ul class="departments cf">                             
              <li class="section secretary">
                  <a href="javascript:void(0);">
                   Secretary              
                  </a>
              </li>
              
              <li class="department">
                <a href="javascript:void(0);">
                    <span class="garis-atas judul">Manajer Perencanaan </span>
                    <span class="garis-atas ket-nama-user">ONDA IRAWAN</span>
                    <span class="pull-left ket-nik">7192162K3</span>
                    <span class="pull-right ket-jabatan">OPT03-18</span>
                </a>
                <ul class="sections">
                  <li class="section">
                        <a href="javascript:void(0);">
                            <span class="garis-atas judul">DM Perencanaan Pengusahaan dan Teknologi Informasi</span>
                            <span class="garis-atas ket-nama-user">R. YUDIANTO</span>
                            <span class="pull-left ket-nik">7094246K3</span>
                            <span class="pull-right ket-jabatan">SYS04-21</span>
                        </a>
                    </li>
                  <li class="section level-satu jabatan-fungsional">
                        <a href="javascript:void(0);">
                            <span class="garis-atas judul">AN/AS Perencanaan Pengusahaan AS REN PENGUSHN</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- JEZZY DWI PUSPO</span>
                            <span class="pull-right ket-nik">9216949ZY | SPE04-01</span>

                        </a>
                  </li>

                  <li class="section level-satu jabatan-fungsional">
                        <a href="javascript:void(0);">
                            <span class="garis-atas">AN/AS Manajemen Risiko</span>
                           

                        </a>
                  </li>

                  <li class="section level-satu">
                        <a href="javascript:void(0);">
                            <span class="garis-atas judul">Supervisor Infrastruktur Teknologi Informasi</span>
                            <span class="garis-atas ket-nama-user">ONDA IRAWAN</span>
                            <span class="pull-left ket-nik">7192162K3</span>
                            <span class="pull-right ket-jabatan">OPT03-18</span>
                        </a>
                  </li>

                  <li class="section level-dua">
                        <a href="javascript:void(0);">
                            <span class="garis-atas judul">AN/AS Perencanaan Pengusahaan AS REN PENGUSHN</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- JEZZY DWI PUSPO</span>
                            <span class="pull-right ket-nik">9216949ZY | SPE04-01</span>

                        </a>
                  </li>

                  <li class="section level-tiga jabatan-fungsional">
                        <a href="javascript:void(0);">
                            <span class="garis-atas judul">AN/AS Perencanaan Pengusahaan AS REN PENGUSHN</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- JEZZY DWI PUSPO</span>
                            <span class="pull-right ket-nik">9216949ZY | SPE04-01</span>

                        </a>
                  </li>


                  <li class="section"><a href="#"><span>Section A4</span></a></li>
                  <li class="section"><a href="#"><span>Section A5</span></a></li>
                </ul>
              </li>


              <li class="department dep-b">
                <a href="javascript:void(0);">
                    <span class="garis-atas judul">Manajer Pemeliharaan Transmisi</span>
                    <span class="garis-atas ket-nama-user">RITO SURYATSO</span>
                    <span class="pull-left ket-nik">6382245K3</span>
                    <span class="pull-right ket-jabatan">OPT03-23</span>
                </a>

                <ul class="sections">
                  <li class="section"><a href="#"><span>Section B1</span></a></li>
                  <li class="section"><a href="#"><span>Section B2</span></a></li>
                  <li class="section"><a href="#"><span>Section B3</span></a></li>
                  <li class="section"><a href="#"><span>Section B4</span></a></li>
                </ul>
              </li>

              <li class="department ghost"></li>

              <li class="department">
                <a href="javascript:void(0);">
                    <span class="garis-atas judul">Manajer Konstruksi</span>
                    <span class="garis-atas ket-nama-user">FRANS LISI</span>
                    <span class="pull-left ket-nik">6585013E</span>
                    <span class="pull-right ket-jabatan">OPT04-18</span>
                </a>
                <ul class="sections">
                  <li class="section"><a href="#"><span>Section C1</span></a></li>
                  <li class="section"><a href="#"><span>Section C2</span></a></li>
                  <li class="section"><a href="#"><span>Section C3</span></a></li>
                  <li class="section"><a href="#"><span>Section C4</span></a></li>
                </ul>
              </li>
              <li class="department dep-d">
                <a href="javascript:void(0);">
                    <span class="garis-atas judul">Manajer Keuangan, SDM dan Administrasi</span>
                    <span class="garis-atas ket-nama-user">DIDIK SUKRISTIYO YUWONO</span>
                    <span class="pull-left ket-nik">7094090A</span>
                    <span class="pull-right ket-jabatan">OPT02-24</span>
                </a>
                <ul class="sections">
                  <li class="section"><a href="#"><span>Section D1</span></a></li>
                  <li class="section"><a href="#"><span>Section D2</span></a></li>
                  <li class="section"><a href="#"><span>Section D3</span></a></li>
                  <li class="section"><a href="#"><span>Section D4</span></a></li>
                  <li class="section"><a href="#"><span>Section D5</span></a></li>
                  <li class="section"><a href="#"><span>Section D6</span></a></li>
                </ul>
              </li>
              
            </ul>



            


          </li>
        </ul>   
      </li>



    </ul>           
  </figure>


  <figure class="org-chart cf">
    <ul class="administration">
      <li>                  
        <ul class="director">
          <li>
            <a href="javascript:void(0);">
                <span class="garis-atas judul">GENERAL MANAJER</span>
                <span class="garis-atas ket-nama-user">TRINO ERWIN</span>
                <span class="pull-left ket-nik">7192162K3</span>
                <span class="pull-right ket-jabatan">OPT03-18</span>
            </a>


            <ul class="subdirector cf">
              <li class="section">              
                <a href="javascript:void(0);">
                    <span class="garis-atas judul">PLT Pejabat Pelaksana Pengadaan2</span>
                    <span class="garis-atas ket-nama-user">IMAM GUNAWARMAN</span>
                    <span class="pull-left ket-nik">6283122K3</span>
                    <span class="pull-right ket-jabatan">SYS03-31</span>                
                  </a>



                  <a href="javascript:void(0);" class="jabatan-fungsional">
                    <span class="judul"  style="text-align: left;">- AN/AS Perencanaan Pengusahaan AS REN PENGUSHN</span>
                    <span class="ket-nama-user" style="text-align: left;">HERMAWAN ISTIONO</span>
                    <span class="ket-nik">7192162K3 | OPT03-18</span>
                    <span class="judul" style="text-align: left;">- AS Perencana Pengadaan</span>
                    <span class="ket-nama-user"  style="text-align: left;">HERMAWAN ISTIONO</span>
                    <span class="ket-nik">7192162K3 | OPT03-18</span>
                    <span class="judul" style="text-align: left;">- AS Perencana Pengadaan</span>
                    <span class="ket-nama-user" style="text-align: left;">JEZZY DWI PUSPO</span>
                    <span class="ket-nik">9216949ZY | SPE04-01</span>                
                  </a>
              </li>              
            </ul>



            <ul class="subdirector-dua cf">
              <li class="section">
              <a href="javascript:void(0);">
                    <span class="garis-atas judul">PLT Pejabat Pelaksana Pengadaan</span>
                    <span class="garis-atas ket-nama-user">IMAM GUNAWARMAN</span>
                    <span class="pull-left ket-nik">6283122K3</span>
                    <span class="pull-right ket-jabatan">SYS03-31</span>                
                  </a>

                  <a href="javascript:void(0);" class="jabatan-fungsional">
                    <span class="judul"  style="text-align: left;">- AN/AS Perencanaan Pengusahaan AS REN PENGUSHN</span>
                    <span class="ket-nama-user" style="text-align: left;">HERMAWAN ISTIONO</span>
                    <span class="ket-nik">7192162K3 | OPT03-18</span>
                    <span class="judul" style="text-align: left;">- AS Perencana Pengadaan</span>
                    <span class="ket-nama-user"  style="text-align: left;">HERMAWAN ISTIONO</span>
                    <span class="ket-nik">7192162K3 | OPT03-18</span>
                    <span class="judul" style="text-align: left;">- AS Perencana Pengadaan</span>
                    <span class="ket-nama-user" style="text-align: left;">JEZZY DWI PUSPO</span>
                    <span class="ket-nik">9216949ZY | SPE04-01</span>                
                  </a>

              </li>
                      
              
            </ul>


            <ul class="departments cf">                             
              <li class="section secretary">
                  <a href="javascript:void(0);">
                   Secretary              
                  </a>
              </li>
              
              <li class="department">
                <a href="javascript:void(0);">
                    <span class="garis-atas judul">Manajer Perencanaan </span>
                    <span class="garis-atas ket-nama-user">ONDA IRAWAN</span>
                    <span class="pull-left ket-nik">7192162K3</span>
                    <span class="pull-right ket-jabatan">OPT03-18</span>
                </a>
                <ul class="sections">
                  <li class="section">
                        <a href="javascript:void(0);">
                            <span class="garis-atas judul">DM Perencanaan Pengusahaan dan Teknologi Informasi</span>
                            <span class="garis-atas ket-nama-user">R. YUDIANTO</span>
                            <span class="pull-left ket-nik">7094246K3</span>
                            <span class="pull-right ket-jabatan">SYS04-21</span>
                        </a>
                    </li>
                  <li class="section level-satu jabatan-fungsional">
                        <a href="javascript:void(0);">
                            <span class="garis-atas judul">AN/AS Perencanaan Pengusahaan AS REN PENGUSHN</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- JEZZY DWI PUSPO</span>
                            <span class="pull-right ket-nik">9216949ZY | SPE04-01</span>

                        </a>
                  </li>

                  <li class="section level-satu jabatan-fungsional">
                        <a href="javascript:void(0);">
                            <span class="garis-atas">AN/AS Manajemen Risiko</span>
                           

                        </a>
                  </li>

                  <li class="section level-satu">
                        <a href="javascript:void(0);">
                            <span class="garis-atas judul">Supervisor Infrastruktur Teknologi Informasi</span>
                            <span class="garis-atas ket-nama-user">ONDA IRAWAN</span>
                            <span class="pull-left ket-nik">7192162K3</span>
                            <span class="pull-right ket-jabatan">OPT03-18</span>
                        </a>
                  </li>

                  <li class="section level-dua">
                        <a href="javascript:void(0);">
                            <span class="garis-atas judul">AN/AS Perencanaan Pengusahaan AS REN PENGUSHN</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- JEZZY DWI PUSPO</span>
                            <span class="pull-right ket-nik">9216949ZY | SPE04-01</span>

                        </a>
                  </li>

                  <li class="section level-tiga jabatan-fungsional">
                        <a href="javascript:void(0);">
                            <span class="garis-atas judul">AN/AS Perencanaan Pengusahaan AS REN PENGUSHN</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- HERMAWAN ISTIONO</span>
                            <span class="pull-right ket-nik">7192162K3 | OPT03-18</span>
                            <span class="pull-left ket-nama-user">- JEZZY DWI PUSPO</span>
                            <span class="pull-right ket-nik">9216949ZY | SPE04-01</span>

                        </a>
                  </li>


                  <li class="section"><a href="#"><span>Section A4</span></a></li>
                  <li class="section"><a href="#"><span>Section A5</span></a></li>
                </ul>
              </li>


              <li class="department dep-b">
                <a href="javascript:void(0);">
                    <span class="garis-atas judul">Manajer Pemeliharaan Transmisi</span>
                    <span class="garis-atas ket-nama-user">RITO SURYATSO</span>
                    <span class="pull-left ket-nik">6382245K3</span>
                    <span class="pull-right ket-jabatan">OPT03-23</span>
                </a>

                <ul class="sections">
                  <li class="section"><a href="#"><span>Section B1</span></a></li>
                  <li class="section"><a href="#"><span>Section B2</span></a></li>
                  <li class="section"><a href="#"><span>Section B3</span></a></li>
                  <li class="section"><a href="#"><span>Section B4</span></a></li>
                </ul>
              </li>

              <li class="department ghost"></li>

              <li class="department">
                <a href="javascript:void(0);">
                    <span class="garis-atas judul">Manajer Konstruksi</span>
                    <span class="garis-atas ket-nama-user">FRANS LISI</span>
                    <span class="pull-left ket-nik">6585013E</span>
                    <span class="pull-right ket-jabatan">OPT04-18</span>
                </a>
                <ul class="sections">
                  <li class="section"><a href="#"><span>Section C1</span></a></li>
                  <li class="section"><a href="#"><span>Section C2</span></a></li>
                  <li class="section"><a href="#"><span>Section C3</span></a></li>
                  <li class="section"><a href="#"><span>Section C4</span></a></li>
                </ul>
              </li>
              <li class="department dep-d">
                <a href="javascript:void(0);">
                    <span class="garis-atas judul">Manajer Keuangan, SDM dan Administrasi</span>
                    <span class="garis-atas ket-nama-user">DIDIK SUKRISTIYO YUWONO</span>
                    <span class="pull-left ket-nik">7094090A</span>
                    <span class="pull-right ket-jabatan">OPT02-24</span>
                </a>
                <ul class="sections">
                  <li class="section"><a href="#"><span>Section D1</span></a></li>
                  <li class="section"><a href="#"><span>Section D2</span></a></li>
                  <li class="section"><a href="#"><span>Section D3</span></a></li>
                  <li class="section"><a href="#"><span>Section D4</span></a></li>
                  <li class="section"><a href="#"><span>Section D5</span></a></li>
                  <li class="section"><a href="#"><span>Section D6</span></a></li>
                </ul>
              </li>
              
            </ul>



            


          </li>
        </ul>   
      </li>



    </ul>           
  </figure>

  
</div>

</div>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
</div>
<style type="text/css">
    

#content *{
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    position: relative;
}

#content .cf:before,
#content .cf:after {
    content: " "; /* 1 */
    display: table; /* 2 */
}

#content .cf:after {
    clear: both;
}

/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
#content .cf {
    *zoom: 1;
}

/* Generic styling */


/* ketika diklik bagan */
#content a:focus{
    outline: 2px solid #000000;
}


#content ul{
    padding: 0;
    margin: 0;
    list-style: none;

}

/* untuk box bagan */
#content ul a{
    display: block;
    background: #FFFFFF;
    border: 1px solid #000000;
    text-align: center;
    overflow: hidden;
    font-size: .7em;
    text-decoration: none;
    font-weight: bold;
    color: #333;
    height: 70px;
    box-shadow: 4px 4px 9px -4px rgba(0,0,0,0.4);
    -webkit-transition: all linear .1s;
    -moz-transition: all linear .1s;
    transition: all linear .1s;
}

 li.secretary a{
   
    background: #00FF00 !important;
    border: 1px dashed #000000 !important;
    vertical-align:middle !important;
    text-align: center !important;
    overflow: hidden;
    font-size: .7em;
    text-decoration: none;
    font-weight: bold;
    color: #333;
    height: 40px !important;
    padding-top: 10px;
    box-shadow: 4px 4px 9px -4px rgba(0,0,0,0.4);
    -webkit-transition: all linear .1s;
    -moz-transition: all linear .1s;
    transition: all linear .1s;
    top:-270px;
    left:100px;
}




    /* untuk box bagan */
li.ghost a{    
    background: none !important;
    border: 0px solid #000000 !important;
    height: 0px !important;
}


@media all and (max-width: 767px){
    #content ul a{
        font-size: 1em;
    }
}


#content ul a span{
    
    
    display: block;
}

/*
 
 */

#content > li > a{
    margin-bottom: 20px;
}

.director > li > a{
    width: 20%;
    margin: 0 auto 0px auto;
    left:-50px;
}

/* garis vertikal panajang dari atas ke bawah */
.subdirector:after{
    content: "";
    display: block;
    width: 0;
    height: 1025px;
    background: red;
    border-left: 2px solid #000000;
    left: 45.45%;
    position: absolute;
}

.subdirector-dua:after{
    content: "";
    display: block;
    
    width: 0;
    background: red;
    border-left: 1px solid #000000;
    left: 45.45%;
    position: absolute;
}

.subdirector,
.departments{
    position: absolute;
    width: 100%;
}


.departments > li:first-child{  
    width: 18.4%;
    height: 64px;
    margin: 0 auto 92px auto;       
    padding-top: 250px;
    border-bottom: 0px solid #000000;
    z-index: 1; 
}

 li .secretary{
    top: -1px !important;
    left: 514px !important;  
    
    
    margin: 0px auto 92px auto !important;

    
    border-top: 1px solid #000000 !important;
    border-left: 0px solid #000000 !important;
    z-index: 1; 
}


.subdirector > li:first-child{  
    width: 18.4%;
    height: 64px;
    margin: 0 auto 92px auto;       
    padding-top: 45px;
    border-bottom: 1px solid #000000;
    z-index: 1; 
    left: -410px;
}


.subdirector-dua > li:first-child{  
    width: 18.4%;
    height: 64px;
    margin: 0 auto 92px auto;       
    padding-top: 45px;
    border-bottom: 1px solid #000000;
    z-index: 1; 
    left: -400px;
}


.subdirector.ghost > li:first-child,
.departments.ghost > li:first-child{  
    width: 18.4%;
    height: 64px;
    margin: 0 auto 92px auto;       
    padding-top: 45px;
    border-bottom: 0px solid #000000 !important;
    z-index: 1; 
}

.subdirector > li:first-child{
    float: right;
    right: 36.1%;
    border-left: 0px solid #000000;
    top: -20px;
    height: 10px
    
}

.subdirector-dua > li:first-child{
    float: right;
    right: 40.1%;
    border-left: 0px solid #000000;
    top: -20px;
    height: 10px
    
}

.departments > li:first-child{  
    float: left;
    left: 67.2%;
    border-right: 0px solid #000000;
    top:-100px;  
}

.subdirector > li:first-child a,
.departments > li:first-child a{
    width: 100%;
}

.subdirector > li:first-child a{    
    left: 50px;
}

@media all and (max-width: 767px){
    .subdirector > li:first-child,
    .departments > li:first-child{
        width: 40%; 
    }

    .subdirector > li:first-child{
        right: 10%;
        margin-right: 2px;
    }

    .subdirector:after{
        left: 49.8%;
    }

    .departments > li:first-child{
        left: 10%;
        margin-left: 2px;
    }
}


.departments > li:first-child a{
    right: 50px;
}

.department:first-child,
.departments li:nth-child(2){
    margin-left: 0;
    clear: left;    
}


/* garis atas panjang horinzontal */
.departments:after{
    content: "";
    display: block;
    position: absolute;
    width: 70.5%;
    height: 25px;   
    border-top: 1px solid #000000;
    border-right: 1px solid #000000;
    border-left: 0px solid #000000;
    margin: 0 auto;
    top: 318px;
    left: 8.9999%
}


/* garis atas panjang horinzontal */
.departments.regional:after{
    content: "";
    display: block;
    position: absolute;
    width: 70.5%;
    height: 95px;   
    border-top: 1px solid #000000;
    border-right: 1px solid #000000;
    border-left: 1px solid #000000;
    margin: 0 auto;
    top: 1255px !important;
    left: 5.9999%
}



@media all and (max-width: 767px){
    .departments:after{
        border-right: none;
        left: 0;
        width: 49.8%;
    }
}

.department:before{
    content: "";
    display: block;
    position: absolute;
    width: 0;
    height: 25px;
    border-left: 1px solid #000000;
    z-index: 1;
    top: -25px;
    left: 50%;
    margin-left: -2px;
}

.department.regional:before{
    content: "";
    display: block;
    position: absolute;
    width: 0;
    height: 93px !important;
    border-left: 1px solid #000000;
    z-index: 1;
    top: -93px;
    left: 50%;
    margin-left: -2px;
}

.department.regional.pertama:before{
    content: "";
    display: block;
    position: absolute;
    width: 0;
    height: 93px !important;
    border-left: 0px solid #000000;
    z-index: 1;
    top: -93px;
    left: 50%;
    margin-left: -2px;
}

.department.ghost:before{
    content: "";
    display: block;
    position: absolute;    
    border-left: 0px solid #000000 !important;   
  
}

.department:first-child:before,
.department:last-child:before{
    border:none;
}

/* posisi kepala department */
.department{
    border-left: 1px solid #000000;
    width: 18.5%;
    float: left;
    margin-left: 1.5%;
    margin-bottom: 10px;
}


/* posisi kepala department */
.department.ghost{
    border-left: 0px solid #000000 !important;
    width: 10% !important;
    float: left !important;
    margin-left: 0% !important;
    margin-bottom: 10px;
}


.lt-ie8 .department{
    width: 18.25%;
}

@media all and (max-width: 767px){
    .department{
        float: none;
        width: 100%;
        margin-left: 0;
    }

    .department:before{
        content: "";
        display: block;
        position: absolute;
        width: 0;
        height: 60px;
        border-left: 1px solid #000000;
        z-index: 1;
        top: -60px;
        left: 0%;
        margin-left: -4px;
    }

    .department:nth-child(2):before{
        display: none;
    }
}

.department > a{
    margin: 0 0px -26px -5px;
    z-index: 1;
}

.department > a:hover{  
    height: 70px;
}

.department > ul{
    margin-top: 0px;
    margin-bottom: 0px;
}

/* garis horizontal penghubung */
.department li{ 
    padding-left: 5px;
    border-bottom: 1px solid #000000;
    height: 75px;
    width: 100%;
    display: inline-block;   
}


.department li.level-satu{ 
    display: inline-block;
    padding-left: 10px !important;
    border-bottom: 1px solid #000000 !important;
    border-left: 1px solid #000000 !important;
    height: 70px !important;
    width: 94% !important;
    margin-left: 7px;   

}

.department li.level-dua{ 
    display: inline-block;
    padding-left: 10px !important;
    border-bottom: 1px solid #000000 !important;
    border-left: 1px solid #000000 !important;
    height: 70px !important;
    width: 86% !important;
    margin-left: 23px;   

}

.department li.level-tiga{ 
    display: inline-block;
    padding-left: 10px !important;
    border-bottom: 1px solid #000000 !important;
    border-left: 1px solid #000000 !important;
    height: 70px !important;
    width: 79% !important;
    margin-left: 39px;   

}


.department li.level-satu.jabatan-fungsional{ 
    display: inline-block;
    padding-left: 10px !important;
    border-bottom: 1px solid #000000 !important;
    border-left: 1px solid #000000 !important;
    height: 155px !important;
    width: 94% !important;
    margin-left: 7px;   
}

.department li.level-dua.jabatan-fungsional{ 
    display: inline-block;
    padding-left: 10px !important;
    border-bottom: 1px solid #000000 !important;
    border-left: 1px solid #000000 !important;
    height: 155px !important;
    width: 86% !important;
    margin-left: 23px;   
}

.department li.level-tiga.jabatan-fungsional{ 
    display: inline-block;
    padding-left: 10px !important;
    border-bottom: 1px solid #000000 !important;
    border-left: 1px solid #000000 !important;
    height: 155px !important;
    width: 79% !important;
    margin-left: 39px;   
}



.department.unit li{ 
    padding-left: 10px;
    border-bottom: 1px solid #000000;
    height: 500px;
    width: 100%;   
}

.department.ghost li{ 
    padding-left: 10px;
    border-bottom: 0px solid #000000 !important;
    height: 500px;
    width: 100%;   
}

/* kotak section */
.department li a{
    background: #000000;
    top: 35px;  
    position: absolute;
    z-index: 1;
    width: 99%;
    height: 70px;
    vertical-align: middle;
    right: -2px;
    display: inline-block;
    
    
    /*
    background-image: -moz-linear-gradient(-45deg,  rgba(0,0,0,0.25) 0%, rgba(0,0,0,0) 100%) !important;
    background-image: -webkit-gradient(linear, left top, right bottom, color-stop(0%,rgba(0,0,0,0.25)), color-stop(100%,rgba(0,0,0,0)))!important;
    background-image: -webkit-linear-gradient(-45deg,  rgba(0,0,0,0.25) 0%,rgba(0,0,0,0) 100%)!important;
    background-image: -o-linear-gradient(-45deg,  rgba(0,0,0,0.25) 0%,rgba(0,0,0,0) 100%)!important;
    background-image: -ms-linear-gradient(-45deg,  rgba(0,0,0,0.25) 0%,rgba(0,0,0,0) 100%)!important;
    background-image: linear-gradient(135deg,  rgba(0,0,0,0.25) 0%,rgba(0,0,0,0) 100%)!important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#40000000', endColorstr='#00000000',GradientType=1 );
    */
}

/* kotak section */



.department li.level-satu a{
    display: inline-block;
    background: #000000;
    z-index: 1;
    width: 100% !important;
    vertical-align: middle;
    right: -5px !important;
}

.department li.level-dua a{
    display: inline-block;
    background: #000000;
    z-index: 1;
    width: 100% !important;
    vertical-align: middle;
    right: -5px !important;
}


.department.unit li a{
    background: #000000;
    top: 480px;  
    position: absolute;
    z-index: 1;
    width: 92% !important;
    height: 60px;
    vertical-align: middle;
    right: -25px;
    position: relative;
}

.department.ghost li a{
    background: #000000;
    top: 480px;  
    position: absolute;
    z-index: 1;
    width: 92% !important;
    height: 60px;
    vertical-align: middle;
    right: -25px;
    position: relative;
}


#content li a:hover{
    /*box-shadow: 8px 8px 9px -4px rgba(0,0,0,0.1);
    height: 80px;
    width: 95%;
    top: 39px;
    background-image: none!important;
    */
    background-color: #FFFF66 !important;
}

/* Department/ section colors */
/*
.department.dep-a a{ background: #FFD600 !important; }
.department.dep-b a{ background: #AAD4E7 !important; }
.department.dep-c a{ background: #FDB0FD !important; }
.department.dep-d a{ background: #A3A2A2 !important; }
.department.dep-e a{ background: #f0f0f0 !important; }
*/

    .garis-atas{
       border-bottom: 1px solid black;
        
        width: 100%;
    }
    .judul{
        font-weight: bold;       
        padding-bottom: 2px;
        padding-top: 2px;
    }
    .ket-nik{
        margin-right: 2px;
        margin-left: 2px;
    }
    .ket-jabatan{
        margin-right: 2px;
        margin-left: 2px;
        text-transform: uppercase;

    }
    .ket-nama-user{
        padding-top: 3px;
        padding-bottom: 3px;
        text-transform: uppercase;
    }
    .jabatan-fungsional{
        background-color: none !important;
        
    }

    li.jabatan-fungsional a{
        display: block;
        background: #E0E0E0 !important;
        border: 0px solid #000000 !important;
        text-align: center;
        height: 150px !important;


    }

    .subdirector .jabatan-fungsional{
        margin-top: 10px;
        display: inline-block !important;
        background: #E0E0E0 !important;
        border: 0px solid #000000 !important;
        text-align: center !important;
        height: 180px !important;

    }

    .subdirector-dua .jabatan-fungsional{
        margin-top: 10px;
        display: inline-block !important;
        background: #E0E0E0 !important;
        border: 0px solid #000000 !important;
        text-align: center !important;
        height: 180px !important;

    }

    


</style>
