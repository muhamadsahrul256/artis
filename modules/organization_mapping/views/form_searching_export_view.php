<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

header("Content-type: application/ms-excel");
header("Content-Disposition: attachment; filename=".$title.".xls");//ganti nama sesuai keperluan
header("Pragma: no-cache");
header("Expires: 0");


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo $title ?></title>

        <style type="text/css">
            body{
                width: 100%;
            }
            .border-all{
                border-left: solid 1px black;
                border-right: solid 1px black;
                border-top: solid 1px black;
                border-bottom: solid 1px black; 
            }
            .chd-check{
                border-left: solid 1px;
                border-right: solid 1px;
                border-top: solid 1px;
                border-bottom: solid 1px; 
            }
            .tebalin{
                font-weight: bold;
            }
            table {
              border-collapse:collapse;
            }
            table td {
                word-wrap:break-word;
            }
            .header{
                border-top: solid 2px black;
            }
            .no-print{
                display: none;
            }

        </style>
    </head>
    <body>
        <?php echo $contents; ?>
    </body>
</html>

<?php exit; ?>