


    <link rel="stylesheet" href="{{ base_url }}assets/plugins/OrgChart-master/dist/css/jquery.orgchart.css">
    <link rel="stylesheet" href="{{ base_url }}assets/plugins/OrgChart-master/examples/css/style.css">

    <style type="text/css">
      #chart-container-2 { height:  620px; }
      .orgchart { background: white; }
    </style>
    
  
    <div class="charts" id="chart-container-3401"></div>
    <div class="charts" id="chart-container-3411"></div>
    <div class="charts" id="chart-container-3412"></div>
    <div class="charts" id="chart-container-3413"></div>
    <div class="charts" id="chart-container-3414"></div>

  
  
    <script type="text/javascript" src="{{ base_url }}themes/AdminLTE/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="{{ base_url }}assets/plugins/OrgChart-master/dist/js/jquery.orgchart.js"></script>

    <script type="text/javascript" src="https://cdn.rawgit.com/stefanpenner/es6-promise/master/dist/es6-promise.auto.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/niklasvh/html2canvas/master/dist/html2canvas.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
 

<?php echo $Kantor_Induk_TJBB; ?>  
<?php echo $APP_Cawang; ?>
<?php echo $APP_Pulogadung; ?>
<?php echo $APP_Durikosambi; ?>
<?php echo $APP_Cilegon; ?>






<!--
<script type="text/javascript">
      'use strict';

(function($){

  $(function() {

    var datascource = {
      'name': 'PLT GM',
      'title': 'DOMPAK PETRUS SINAMBELA',
      'employee': '6283122K3',
      'grade': 'SYS03',
      'COID': '1',
      'item': 'Struktural',
      'class': 'natural',
      'children': [
        { 'name': 'Bo Miao', 'title': 'department manager', 'employee': 'SYS','grade': 'SYS01','COID': '1', 'item': 'Struktural','class': 'natural',

            'children': [
                { 'name': '', 'title': '','employee': '','grade': '','COID': '','item': 'Struktural','class': 'ghost-node',

                    'children': [
                        { 'name': 'AN Pelaksana Pengadaan asadas asda sadsada asdasd', 'title': '<ol class="member-staff"><li>ONDA IRAWAN SINAMBELA <br/><span class="label bg-gray">SYS02 | 12345678</span></li><li>Tea</li><li>Milk</li></ol>','employee': '','grade': '','COID': '','item': '','class': 'fungsional' },
                        { 'name': 'AN Pelaksana Pengadaan', 'title': 'DOMPAK</br>PETRUS','employee': '','grade': '','COID': '','item': '','class': 'fungsional' },
                    ]

                },
                
              ]

        },
        
        { 'name': '', 'title': '','employee': '','grade': '','COID': '1','item': 'Struktural','class': 'ghost-node',
          'children': [
            { 'name': 'Tie Hua1', 'title': 'senior engineer','employee': 'SYS','grade': 'SYS01','COID': '1', 'item': 'Struktural','class': 'natural',
                'children': [
                { 'name': 'Pang Pang 1', 'title': 'engineer','employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural' },
                { 'name': 'Xiang Xiang', 'title': 'UE engineer','employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural' }
              ]          
            },

            { 'name': 'Hei Hei 1', 'title': 'senior engineer','employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural',
              'children': [
                { 'name': 'Pang Pang', 'title': 'engineer','employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural' },
                { 'name': 'Xiang Xiang', 'title': 'UE engineer' ,'employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural',
                'children': [
            
            { 'name': 'Tie Hua 2', 'title': 'senior engineer','employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural' },


            { 'name': 'Hei Hei', 'title': 'senior engineer','employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural',
              'children': [
                { 'name': 'Pang Pang', 'title': 'engineer','employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural' },
                { 'name': 'Xiang Xiang', 'title': 'UE engineer','employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural' }
              ]
            }
          ]}
              ]
            },

            { 'name': '', 'title': '','employee': '','grade': '','COID': '','item': '','className': 'ghost-node' },
            

            { 'name': 'Tie Hua', 'title': 'senior engineer','employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural' },
            { 'name': 'Tie Hua2', 'title': 'senior engineer','employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural' }
          ]
        },
        

        { 'name': 'Hong Miao', 'title': 'department manager','employee': 'SYS','grade': 'SYS01','COID': '1','item': 'Struktural','class': 'natural' },
        
      ]
    };

    $('#chart-container').orgchart({
      'data' : datascource,
      'nodePejabat': 'title',
      'nodeEmployee': 'employee',
      'nodeGrade': 'grade',
      'nodeCOID': 'COID',
      'nodeItem': 'item',
      'nodeClass': 'class',
      'verticalDepth': 4,
      'depth': 4,
      'collapsed' : true
    });

  });

})(jQuery);
</script>

-->