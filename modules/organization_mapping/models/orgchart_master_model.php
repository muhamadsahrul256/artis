<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class orgchart_master_model extends CMS_Model{

    public $table_diagram_frame        = NULL;
    public $table_diagram_frame_detail = NULL;
    protected $periode_id  = '';
    protected $start_date  = '';
    protected $end_date    = '';
    protected $periode_upload_id  = '';
    protected $business_id = '';


    public function set_table_frame($period_id){

      $this->set_current_period($period_id);

        if ($this->periode_upload_id <= 0 ){
            $table_diagram_frame        = 'tp_diagram_frame';
            $table_diagram_frame_detail = 'tp_diagram_frame_detail';

            $this->table_diagram_frame        = $table_diagram_frame;
            $this->table_diagram_frame_detail = $table_diagram_frame_detail;
        }
        else{
            $table_diagram_frame        = 'tp_diagram_frame_'.$this->periode_upload_id;
            $table_diagram_frame_detail = 'tp_diagram_frame_detail_'.$this->periode_upload_id;

            if ($this->db->table_exists($table_diagram_frame)){
                $this->table_diagram_frame        = $table_diagram_frame;
                $this->table_diagram_frame_detail = $table_diagram_frame_detail;
            }
            else{
                $this->table_diagram_frame        = 'tp_diagram_frame';
                $this->table_diagram_frame_detail = 'tp_diagram_frame_detail';
            }
        }
    }

    public function set_current_period($period_id){        

        $this->db->select('PeriodID, PStartDate, PEndDate')
                 ->from('mst_period')
                 ->where('PeriodID', $period_id)
                 ->order_by('PStartDate', 'ASC');
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        /*
        $this->db->select('upload_history_id')
                 ->from('tp_profile_upload_history')
                 ->where('upload_history_created >=', $data->PStartDate)
                 ->where('upload_history_created <=', $data->PEndDate)
                 ->order_by('upload_history_created', 'DESC')
                 ->limit(1);
        $db      = $this->db->get();
        $row    = $db->row(0);
        */

        $SQL = "SELECT upload_history_id FROM tp_profile_upload_history 
                WHERE upload_history_created >='".$data->PStartDate."' AND 
                upload_history_created <='".$data->PEndDate."' ORDER BY upload_history_created DESC LIMIT 1";

        $query  = $this->db->query($SQL);
        $num_row = $query->num_rows();
        $row = $query->row(0);        
       
        $this->period_id  = $data->PeriodID;
        $this->start_date = $data->PStartDate;
        $this->end_date   = $data->PEndDate;

        if ($num_row > 0){
            $this->periode_upload_id   = $row->upload_history_id;
        }
        else{
            $this->periode_upload_id   = 0;
        }               

        return $this;
    }

	
    public function generate_frame($period_id = NULL, $business_id, $director_id=NULL){

        $this->period_id   = $period_id;
        $this->business_id = $business_id;

        $this->set_table_frame($period_id);

        //$html = 'frame_'.$business_id;        

        $html = '<script type="text/javascript">
          "use strict";

            (function($){

              $(function() {

                var datascource_'.$business_id.' = {
                  '.$this->callback_structural_frame($director_id, $style=NULL, $className='').'
                  "children": [';

                $query = $this->db->select('COID, Functional, rowspan, className')
                         ->from($this->table_diagram_frame)
                         ->where('ParentID', $director_id)
                         ->where('APP', $business_id)
                         ->where('Status', 1)
                         ->where('Description IS NOT NULL')
                         ->order_by('No_','ASC')
                         ->get();
                          
                          foreach($query->result() as $data){

                              $html .= $this->callback_box_frame($data->COID, $style=NULL, $data->className, $data->rowspan);

                          }                     
                  
                      
                $html .= ']};

                $("#chart-container-'.$business_id.'").orgchart({
                  "data" : datascource_'.$business_id.',
                  "nodeId": "id",
                  "nodePejabat": "title",
                  "nodeEmployee": "employee",
                  "nodeGrade": "grade",
                  "nodeItem": "item",
                  "nodeClass": "class",
                  "nodeClassName": "className",
                  "nodeRows": "rows",
                  "nodeTotal": "total",
                  "verticalDepth": 3,
                  "depth": 2,                      
                  "collapsed" : true,
                  "pan": false,
                  "exportButton": false,
                  "exportFilename": "MyOrgChart",
                  "exportFileextension": "png",                   

                });

              });

            })(jQuery);
            </script>';

    	  return $html;
    }



    public function callback_structural_frame($primary_key, $style, $className=NULL){

        $empty_employee = $this->callback_diagram_data($primary_key, 'EmployeeID');

        if (is_null($empty_employee) || empty($empty_employee)){
            $jabatan_kosong = 'jabatan-kosong';
            $total = 0;
        }
        else{
            $jabatan_kosong = '';
            $total = 1;
        }

        $output = '"id": "'.$primary_key.'",';
        if(strpos($this->callback_diagram_data($primary_key, 'Description'),'PLT') !== false){
          $name =  "<p class='label label-danger'>".$this->callback_diagram_data($primary_key, 'Description').'</p>';
        }else{
          $name =  $this->callback_diagram_data($primary_key, 'Description');
        }
        // $output .= '"name": "'.$this->callback_diagram_data($primary_key, 'Description').'",';
        $output .= '"name": "'.$name.'",';
        $output .= '"title": "'.$this->callback_diagram_data($primary_key, 'EmployeeName').'",';
        $output .= '"employee": "'.$this->callback_diagram_data($primary_key, 'EmployeeID').'",';
        $output .= '"grade": "'.$this->callback_diagram_data($primary_key, 'GradeID').'",';
        $output .= '"item": "'.$this->callback_diagram_data($primary_key, 'Functional').'",';
        $output .= '"class": "",';
        $output .= '"className": "'.$className.'",';
        $output .= '"rows": "0",';
        $output .= '"total": "'.$total.'",';
        //$output .= '"collapsed": true,';        

        return $output;    
    }



    public function callback_box_frame($primary_key, $style, $className=NULL, $rows=NULL){        

        $output  = '{"id": "'.$primary_key.'",';
        if(strpos($this->callback_diagram_data($primary_key, 'Description'),'PLT') !== false){
          $name =  "<p class='label label-danger'>".$this->callback_diagram_data($primary_key, 'Description').'</p>';
        }else{
          $name =  $this->callback_diagram_data($primary_key, 'Description');
        }
        // $output .= '"name": "'.$this->callback_diagram_data($primary_key, 'Description').'",';
        $output .= '"name": "'.$name.'",';        

        if ($this->callback_diagram_data($primary_key, 'Functional') ==1){

            $empty_employee = $this->callback_diagram_detail($primary_key, 'EmployeeID');

            if (is_null($empty_employee) || empty($empty_employee)){
                $jabatan_kosong = 'jabatan-kosong';
                    $total = 0;
            }
            else{
                $jabatan_kosong = '';
                $total = 1;
            }

            $output .= '"title": "'.$this->callback_functional_frame($primary_key, $style, $className='').'",';
            $output .= '"class": "fungsional",';
            $output .= '"total": "'.$total.'",';
        }
        else{

            $empty_employee = $this->callback_diagram_data($primary_key, 'EmployeeID');

            if (is_null($empty_employee) || empty($empty_employee)){
                $jabatan_kosong = 'jabatan-kosong';
                $total = 0;
            }
            else{
                $jabatan_kosong = '';
                $total = 1;
            }
        
            $output .= '"title": "'.$this->callback_diagram_data($primary_key, 'EmployeeName').'",';
            $output .= '"class": "",';
            $output .= '"total": "'.$total.'",';
        }

      
        $output .= '"employee": "'.$this->callback_diagram_data($primary_key, 'EmployeeID').'",';
        $output .= '"grade": "'.$this->callback_diagram_data($primary_key, 'GradeID').'",';
        $output .= '"item": "'.$this->callback_diagram_data($primary_key, 'Functional').'",';
        //$output .= '"class": "'.$className.' '.$jabatan_kosong.'",';
        $output .= '"className": "'.$className.'",';
        $output .= '"rows": "'.$rows.'",';        
        //$output .= '"collapsed": true,';
        $output .= '"children": [';
        $output .= $this->callback_box_child_frame($primary_key, $style, $className=NULL);

        $output .= ']},';        

        return $output;    
    }


    public function callback_box_child_frame($primary_key, $style, $className=NULL){        

        $query = $this->db->select('COID, Functional, EmployeeName, rowspan')
               ->from($this->table_diagram_frame)
               ->where('ParentID', $primary_key)
               ->where('APP', $this->business_id)
               ->where('Status', 1)
               ->where('Description IS NOT NULL')
               ->order_by('No_','ASC')
               ->get();
        $no=1;

        $output = '';

        foreach($query->result() as $data){
      

            $output .= '{"id": "'.$data->COID.'",';
            $output .= '"name": "'.htmlspecialchars($this->callback_diagram_data($data->COID, 'Description'), ENT_QUOTES).'",';
            
            if ($data->Functional ==1){

                $empty_employee = $this->callback_diagram_detail($data->COID, 'EmployeeID');

                if (is_null($empty_employee) || empty($empty_employee)){
                    $jabatan_kosong = 'jabatan-kosong';
                    $total = 0;
                }
                else{
                    $jabatan_kosong = '';
                    $total = 1;
                }

                $output .= '"title": "'.$this->callback_functional_frame($data->COID, $style, $className='').'",';
                $output .= '"class": "fungsional",';
            }
            else{
                $empty_employee = $this->callback_diagram_data($data->COID, 'EmployeeID');

                if (is_null($empty_employee) || empty($empty_employee)){
                    $jabatan_kosong = 'jabatan-kosong';
                    $total = 0;
                }
                else{
                    $jabatan_kosong = '';
                    $total = 1;
                }

                $output .= '"title": "'.$this->callback_diagram_data($data->COID, 'EmployeeName').'",';
                $output .= '"class": "'.$jabatan_kosong.'",';
            }
            $output .= '"employee": "'.$this->callback_diagram_data($data->COID, 'EmployeeID').'",';
            $output .= '"grade": "'.$this->callback_diagram_data($data->COID, 'GradeID').'",';
            $output .= '"className": "'.$className.'",';
            $output .= '"item": "'.$this->callback_diagram_data($data->COID, 'Functional').'",';
            $output .= '"rows": "'.$data->rowspan.'",';
            $output .= '"total": "'.$total.'",';
            //$output .= '"collapsed": true,';

            $output .= '"children": [';            
            $output .= $this->callback_box_child_frame($data->COID, $style=NULL, $className='');
            $output .=']},';
        }                

        return $output;    
    }


    public function callback_functional_frame($primary_key, $style, $className=''){

        
        $output  = '<ol>';        
        $query = $this->db->select('EmployeeID, EmployeeName, GradeID')
               ->from($this->table_diagram_frame_detail)
               ->where('ParentID', $primary_key)
               ->order_by('COIDNo','ASC')
               ->get();
        $no=1;

        foreach($query->result() as $data){
            $output .= '<li>'.$data->EmployeeName.'<br/>';
            $output .= '<span>'.$data->EmployeeID.' | '.$data->GradeID.'</span></li>';
        }

        $output .= '</ol>';

        return $output;    
    }


    public function callback_user_profile($primary_key, $result_column){

        $this->db->select($result_column)
                 ->from('tp_profile')
                 ->where('Prev_Per_No', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            return $data->$result_column;
        } 
        else{
            return '-';
        }
    }

    public function callback_diagram_data($primary_key, $result_column){

        $this->db->select($result_column)
                 ->from($this->table_diagram_frame)
                 //->where('APP', $this->business_id)
                 ->where('COID', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            return $data->$result_column;
        } 
        else{
            return '';
        }
    }


    public function callback_diagram_detail($primary_key, $result_column){

        $this->db->select($result_column)
                 ->from($this->table_diagram_frame_detail)
                 ->where('ParentID', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        
        return $num_row;
    }

    public function count_children($primary_key){

        $SQL = "SELECT COUNT(COID) AS Total FROM tp_diagram_frame WHERE ParentID ='".$primary_key."' AND Status ='1'";
        $query  = $this->db->query($SQL);
        $num_row = $query->num_rows();
        $data = $query->row(0);

        /*
        $this->db->select('COID')
                 ->from('tp_diagram_frame')
                 ->where('ParentID', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        */

        return $data->Total;
    }   



}