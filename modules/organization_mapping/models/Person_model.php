<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Person_model extends CI_Model {

	var $table = 'tp_diagram_frame';
	var $column_order = array('COID','EmployeeID','Description',null); //set column field database for datatable orderable
	var $column_search = array('COID','EmployeeID','Description'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	var $order = array('COID' => 'desc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->select('COID,APP,UnitID,ParentID,EmployeeID,EmployeeName,Description,GradeID,GradeCode,PositionID,Full_Mapping_Code,Functional,Status');
		$this->db->from('tp_diagram_frame');
		$this->db->where('COID', $id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_unmapping_data($id){

		$this->db->select('PeriodID,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PeriodID', $id)
                 ->order_by('PStartDate', 'ASC');
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        $this->db->select('EmployeeID,Full_Name,new_position,unit_id,bisnis_area,description');
		$this->db->from('tp_mutation_movement');
		$this->db->join('mst_business_area','business_area_id=bisnis_area','INNER');
		$this->db->where('cStatus', 4);
		$this->db->where('CreatedTime >=', $data->PStartDate);
		$this->db->where('CreatedTime <=', $data->PEndDate);
		$this->db->order_by('bisnis_area,unit_id', 'ASC');
		$query = $this->db->get();

		$result = $query->result();

		return $result;
	}



	public function get_option_position_data($id, $grade)
	{
		
        $this->db->select('position_id, position_name, igrade_code');
		$this->db->from('mst_position_ladder');
		$this->db->where('FIND_IN_SET (igrade_code,"'.$grade.'")');
		$this->db->group_by('position_name');
		$this->db->order_by('position_name', 'ASC');
		$query = $this->db->get();

		$result = $query->result();
		return $result;
	}


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}


}
