<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class organization_chart extends CMS_Priv_Strict_Controller {

    protected $URL_MAP = array();

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('orgchart_model');
    }

    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    private function make_crud(){
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // initialize groceryCRUD
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $crud = $this->new_crud();
        // this is just for code completion
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        // $crud->unset_add();
        // $crud->unset_edit();
        // $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        // $crud->unset_print();
        // $crud->unset_export();

      
        $crud->set_language($this->cms_language());

        // table name
        $crud->set_table($this->cms_complete_table_name('tp_organization'));

        // primary key
        $crud->set_primary_key('COID');

        // set subject
        $crud->set_subject('Hobby');

        // displayed columns on list
        $crud->columns('Description');
        // displayed columns on edit operation
        $crud->edit_fields('name');
        // displayed columns on add operation
        $crud->add_fields('name');

      


        // caption of each columns
        $crud->display_as('name','Name');

        
        $crud->required_fields('name');

       
        $crud->unique_fields('name');

       
        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));


        $this->crud = $crud;
        return $crud;
    }

    public function index(){

        $crud = $this->new_crud();

        $output = '<div class="box box-default" style="width:3000px" id="clonetext">';
        $output .= '<div class="box-header with-border">';
        $output .= '<h3 class="box-title">'.$this->cms_lang('Organization Mapping').'</h3> ';
        $output .= '<a href="javascript:void(0)" id="btnPrint" class="btn btn-default btn-flat pull-right"><i class="fa fa-print"></i> '.$this->cms_lang('Print').'</a>';
        $output .= '</div>';
        $output .= '<div class="box-body">'.$this->orgchart_model->piramida_orgchart_data_css().'</div>';    
        $output .= '</div>';

        $data = array(
            'output' => $output
        );

        $this->view($this->cms_module_path().'/organization_chart_view', $data, $this->cms_complete_navigation_name('organization_chart'));
    }

    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_delete($primary_key){

        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){

        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }

    public function chart_update(){

        $action      = $this->input->post('form_button');
        $primary_key = $this->input->post('COID');
        //$void_note      = $this->input->post('void_note');
        //$void_nik       = $this->input->post('void_nik');

        switch ($action) {

            case "form_save":
                
                $this->db->update('tp_organization',
                    array('Description' =>$this->input->post('Description'),
                          'EmployeeID' => $this->input->post('EmployeeID'),
                          'ParentID' => $this->input->post('ParentID'),
                          'GradeID' => $this->input->post('GradeIDORG')
                          ),
                    array('COID'=> $primary_key));

                redirect($this->cms_module_path().'/organization_chart/');

            break;

            case "form_add_child":
                
                $this->db->insert('tp_organization',
                    array('Description' => $this->input->post('Description'),
                          'EmployeeID'=>$this->input->post('EmployeeID'),
                          'ParentID' => $this->input->post('ParentID'),
                          'GradeID' => $this->input->post('GradeIDORG')
                          ));

                redirect($this->cms_module_path().'/organization_chart/');

            break;

            case "form_delete":

                $this->db->delete('tp_organization', array('COID'=> $primary_key));

                redirect($this->cms_module_path().'/organization_chart/');                

            break;

            case "form_search":


            redirect($this->cms_module_path().'/organization_chart/form_searching/?id='.$primary_key.'&grade='.$this->input->post('GradeID').'&position='.$this->input->post('PositionID').'&edu='.$this->input->post('EducationID').'&training='.$this->input->post('TrainingID'));                              

            break;


            default:
            //echo "Your favorite color is neither red, blue, nor green!";
        }


    }


    public function open_form_chart($primary_key){

        $this->db->select('COID,ParentID,EmployeeID,Description')
                 ->from('tp_organization')
                 ->where('COID', $primary_key);
        $db     = $this->db->get();
        $data   = $db->row(0);
        $num_row= $db->num_rows();


        $table = '<input type="hidden" name="COID" value="'.$primary_key.'" placeholder="" class="form-control">';
        $table .= '<div class="nav-tabs-custom">';
        $table .= '<ul class="nav nav-tabs">';
        $table .= '<li class="active"><a href="#tab_1" data-toggle="tab">'.$this->cms_lang('Chart').'</a></li>';
        $table .= '<li><a href="#tab_2" data-toggle="tab">'.$this->cms_lang('Searching').'</a></li>';             
        $table .= '</ul>';
        $table .= '<div class="tab-content">';
        $table .= '<div class="tab-pane active" id="tab_1">';

        $table .= '<div class="form-group">';
        $table .= '<label for="inputDescription" class="col-sm-3 control-label">'.$this->cms_lang('Description').'</label>';
        $table .= '<div class="col-sm-9">';
        $table .= '<input type="text" name="Description" value="'.$data->Description.'" placeholder="" class="form-control">';
        $table .= '</div>';
        $table .= '</div>';

        $table .= '<div class="form-group">';
        $table .= '<label for="inputEmployeeID" class="col-sm-3 control-label">'.$this->cms_lang('Level Grade').'</label>';
        $table .= '<div class="col-sm-9">'.$this->select_option_GradeIDORG($primary_key).'</div>';
        $table .= '</div>';


        $table .= '<div class="form-group">';
        $table .= '<label for="inputEmployeeID" class="col-sm-3 control-label">'.$this->cms_lang('Employee ID').'</label>';
        $table .= '<div class="col-sm-9">'.$this->select_option_EmployeeID($primary_key).'</div>';
        $table .= '</div>';

        $table .= '<div class="form-group">';
        $table .= '<label for="inputParentID" class="col-sm-3 control-label">'.$this->cms_lang('Parent').'</label>';
        $table .= '<div class="col-sm-9">'.$this->select_option_ParentID($primary_key).'</div>';
        $table .= '</div>';

        $table .= '<div class="btn-toolbar">';
        $table .= '<button type="submit" class="btn btn-primary pull-left" name="form_button" value="form_save"><i class="fa fa-floppy-o"></i> '.$this->cms_lang('Update').'</button>';
        $table .= '<button type="submit" class="btn btn-success pull-left" name="form_button" value="form_add_child"><i class="fa fa-plus-square"></i> '.$this->cms_lang('Add Child').'</button>';
        $table .= '<button type="submit" class="btn btn-danger pull-left" name="form_button" value="form_delete"><i class="fa fa-trash-o"></i> '.$this->cms_lang('Delete').'</button>';
        $table .= '<button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i class="fa fa-close"></i> '.$this->cms_lang('Close').'</button>';
        $table .= '</div>';                

        $table .= '</div>';


              
        $table .= '<div class="tab-pane" id="tab_2">';

        $table .= '<div class="form-group">';
        $table .= '<label for="Grade" class="col-sm-3 control-label">'.$this->cms_lang('Level Grade').'</label>';
        $table .= '<div class="col-sm-9">';
        $table .= $this->select_option_GradeID($primary_key);
        $table .= '</div>';
        $table .= '</div>';

        $table .= '<div class="form-group">';
        $table .= '<label for="Grade" class="col-sm-3 control-label">'.$this->cms_lang('Position').'</label>';
        $table .= '<div class="col-sm-9">';
        $table .= $this->select_option_PositionID($primary_key);
        $table .= '</div>';
        $table .= '</div>';

        $table .= '<div class="form-group">';
        $table .= '<label for="Grade" class="col-sm-3 control-label">'.$this->cms_lang('Education').'</label>';
        $table .= '<div class="col-sm-9">';
        $table .= $this->select_option_EducationID($primary_key);
        $table .= '</div>';
        $table .= '</div>';

        $table .= '<div class="form-group">';
        $table .= '<label for="TrainingID" class="col-sm-3 control-label"></label>';
        $table .= '<div class="col-sm-9">';
        $table .= '<label><input type="checkbox" name="TrainingID" id="TrainingID" value="0"> '.$this->cms_lang('Show Training Data').'</label>';       
        $table .= '</div>';
        $table .= '</div>';


        $table .= '<div class="btn-toolbar">';
        $table .= '<button type="submit" class="btn btn-primary pull-left" name="form_button" value="form_search"><i class="fa fa-search"></i> '.$this->cms_lang('Search').'</button>';
        $table .= '<button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i class="fa fa-close"></i> '.$this->cms_lang('Close').'</button>';
        $table .= '</div>';

        $table .= '</div>';
              
             
        $table .= '</div>';    
        $table .= '</div>';

        return $table;

    }

    public function open_form_chart_search($primary_key){

    
        $GradeID        = $this->input->get('grade');
        $PositionID     = $this->input->get('position');
        $EducationID    = $this->input->get('edu');
        $TrainingID     = $this->input->get('training');


        $this->db->select('COID,ParentID,EmployeeID')
                 ->from('tp_organization')
                 ->where('COID', $primary_key);
        $db     = $this->db->get();
        $data   = $db->row(0);
        $num_row= $db->num_rows();


        $table = '<input type="hidden" name="COID" value="'.$primary_key.'" placeholder="" class="form-control">';
        
        $table .= '<div class="form-group">';
        $table .= '<label for="Grade" class="col-sm-3 control-label">'.$this->cms_lang('Level Grade').'</label>';
        $table .= '<div class="col-sm-9">';
        $table .= $this->select_option_GradeID($primary_key);
        $table .= '</div>';
        $table .= '</div>';

        $table .= '<div class="form-group">';
        $table .= '<label for="Grade" class="col-sm-3 control-label">'.$this->cms_lang('Position').'</label>';
        $table .= '<div class="col-sm-9">';
        $table .= $this->select_option_PositionID($primary_key);
        $table .= '</div>';
        $table .= '</div>';

        $table .= '<div class="form-group">';
        $table .= '<label for="Grade" class="col-sm-3 control-label">'.$this->cms_lang('Education').'</label>';
        $table .= '<div class="col-sm-9">';
        $table .= $this->select_option_EducationID($primary_key);
        $table .= '</div>';
        $table .= '</div>';

        $table .= '<div class="form-group">';
        $table .= '<label for="TrainingID" class="col-sm-3 control-label"></label>';
        $table .= '<div class="col-sm-9">';
        $table .= '<label><input type="checkbox" name="TrainingID" id="TrainingID" value="0"> '.$this->cms_lang('Show Training Data').'</label>';       
        $table .= '</div>';
        $table .= '</div>';


        $table .= '<div class="btn-toolbar">';
        $table .= '<button type="submit" class="btn btn-primary pull-left" name="form_button" value="form_search"><i class="fa fa-search"></i> '.$this->cms_lang('Search').'</button>';
        $table .= '<button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i class="fa fa-close"></i> '.$this->cms_lang('Close').'</button>';
        $table .= '</div>';

      

        return $table;

    }

    public function select_option_GradeIDORG($primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%" name="GradeIDORG" id="GradeIDORG" data-header="'.$this->cms_lang('Select').' '.$this->cms_lang('Grade').'">';
        
        $empty_select_closed = '</select>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();      
      

            $this->db->select('GradeID')
                     ->from('tp_organization')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);                                 
    
                $this->db->select('Basic_Salary_Scale,Main_Grp_Text')
                         ->from('tp_raw_data')
                         ->where('Basic_Salary_Scale !=', '')
                         ->group_by('Basic_Salary_Scale')
                         ->order_by('Basic_Salary_Scale','ASC');
                        
                $db2 = $this->db->get();            

                foreach($db2->result() as $row):
                    if($row->Basic_Salary_Scale == $data->GradeID) {
                        $empty_select .= '<option value="'.$row->Basic_Salary_Scale.'" data-subtext="'.$row->Main_Grp_Text.'" selected="selected">'.$row->Basic_Salary_Scale.'</option>';
                    } else {
                        $empty_select .= '<option value="'.$row->Basic_Salary_Scale.'" data-subtext="'.$row->Main_Grp_Text.'">'.$row->Basic_Salary_Scale.'</option>';
                    }
                endforeach;                                 
            
            return $empty_select.$empty_select_closed;
        
    }

    public function select_option_EmployeeID($primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%" name="EmployeeID" id="EmployeeID" data-header="'.$this->cms_lang('Select').' '.$this->cms_lang('Employee').'">';
        
        $empty_select_closed = '</select>';

        $empty_select .= '<option value="" data-subtext="">---'.$this->cms_lang('Empty').'---</option>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();      
      

            $this->db->select('EmployeeID')
                     ->from('tp_organization')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);
                                 
    
                $this->db->select('Prev_Per_No,Full_Name')
                         ->from('tp_raw_data')
                         //->where('Prev_Per_No', $data->EmployeeID)
                         ->order_by('Full_Name','ASC');
                        
                $db2 = $this->db->get();            

                foreach($db2->result() as $row):
                    if($row->Prev_Per_No == $data->EmployeeID) {
                        $empty_select .= '<option value="'.$row->Prev_Per_No.'" data-subtext="" selected="selected">'.$row->Full_Name.'</option>';
                    } else {
                        $empty_select .= '<option value="'.$row->Prev_Per_No.'" data-subtext="">'.$row->Full_Name.'</option>';
                    }
                endforeach;                                 
            
            return $empty_select.$empty_select_closed;

        
    }

    public function select_option_ParentID($primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%" name="ParentID" id="ParentID" data-header="'.$this->cms_lang('Select').' '.$this->cms_lang('Parent').'">';
        
        $empty_select_closed = '</select>';

        $empty_select .= '<option value="0" data-subtext="">---'.$this->cms_lang('TOP').'---</option>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();      
      

            $this->db->select('ParentID')
                     ->from('tp_organization')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);                                 
    
                $this->db->select('COID,Description')
                         ->from('tp_organization')
                         //->where('COID !=', $primary_key)
                         ->order_by('Description','ASC');
                        
                $db2 = $this->db->get();            

                foreach($db2->result() as $row):
                    if($row->COID == $data->ParentID) {
                        $empty_select .= '<option value="'.$row->COID.'" data-subtext="***" selected="selected">'.$row->Description.'</option>';
                    } else {
                        $empty_select .= '<option value="'.$row->COID.'">'.$row->Description.'</option>';
                    }
                endforeach;                                 
            
            return $empty_select.$empty_select_closed;
        
    }

    public function select_option_GradeID($primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%" name="GradeID" id="GradeID" data-header="'.$this->cms_lang('Select').' '.$this->cms_lang('Grade').'">';
        
        $empty_select_closed = '</select>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();      
      

            $this->db->select('GradeID')
                     ->from('tp_organization')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);                                 
    
                $this->db->select('Basic_Salary_Scale,Main_Grp_Text')
                         ->from('tp_raw_data')
                         ->where('Basic_Salary_Scale !=', '')
                         ->group_by('Basic_Salary_Scale')
                         ->order_by('Basic_Salary_Scale','ASC');
                        
                $db2 = $this->db->get();            

                foreach($db2->result() as $row):
                    if($row->Basic_Salary_Scale == $data->GradeID) {
                        $empty_select .= '<option value="'.$row->Basic_Salary_Scale.'" data-subtext="('.$this->count_employee_group($where_column='Basic_Salary_Scale', $row->Basic_Salary_Scale).')" selected="selected">'.$row->Basic_Salary_Scale.'</option>';
                    } else {
                        $empty_select .= '<option value="'.$row->Basic_Salary_Scale.'" data-subtext="('.$this->count_employee_group($where_column='Basic_Salary_Scale', $row->Basic_Salary_Scale).')">'.$row->Basic_Salary_Scale.'</option>';
                    }
                endforeach;                                 
            
            return $empty_select.$empty_select_closed;
        
    }

    public function select_option_PositionID($primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%" name="PositionID" id="PositionID" data-header="'.$this->cms_lang('Select').' '.$this->cms_lang('Position').'">';
        
        $empty_select_closed = '</select>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();      
      

            $this->db->select('PositionID')
                     ->from('tp_organization')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);                                 
    
                $this->db->select('Position_ID,Position_Name')
                         ->from('tp_raw_data')
                         ->where('Position_ID !=', '')
                         ->group_by('Position_ID')
                         ->order_by('Position_Name','ASC');
                        
                $db2 = $this->db->get();            

                foreach($db2->result() as $row):
                    if($row->Position_ID == $data->PositionID) {
                        $empty_select .= '<option value="'.$row->Position_ID.'" data-subtext="'.$row->Position_ID.'" selected="selected">'.$row->Position_Name.'</option>';
                    } else {
                        $empty_select .= '<option value="'.$row->Position_ID.'" data-subtext="'.$row->Position_ID.'">'.$row->Position_Name.'</option>';
                    }
                endforeach;                                 
            
            return $empty_select.$empty_select_closed;
        
    }


    public function select_option_EducationID($primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%" name="EducationID" id="EducationID" data-header="'.$this->cms_lang('Select').' '.$this->cms_lang('Education').'">';
        
        $empty_select_closed = '</select>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();      

        $this->db->select('education_id,description')
                 ->from('mst_education')
                 ->order_by('description','ASC');
        $db = $this->db->get();     
                   

        foreach($db->result() as $data):
                    
            $empty_select .= '<option value="'.$data->education_id.'" data-subtext="'.$data->education_id.'">'.$data->description.'</option>';
                    
        endforeach;                                 
            
        return $empty_select.$empty_select_closed;        
    }


    public function form_searching(){

        $primary_key    = $this->input->get('id');
        $GradeID        = $this->input->get('grade');
        $PositionID     = $this->input->get('position');
        $EducationID    = $this->input->get('edu');
        $TrainingID     = $this->input->get('training');


        $query = $this->db->select('Prev_Per_No,Full_Name,Position_ID,Position_Name,Basic_Salary_Scale,Last_Date_Grade')
            ->from('tp_raw_data')
            ->where('Basic_Salary_Scale', $GradeID)
            ->where('Position_ID', $PositionID)
            ->order_by('Full_Name','ASC')
            ->get();
        $num_row = $query->num_rows();


        $output = '<div class="box">
            <div class="box-header">
              <h3 class="box-title">'.$this->cms_lang('Talent Pool Data').'</h3>
                <div class="pull-right">
                    <a href="javascript:void(0)" id="btnPrint" class="btn btn-default btn-flat"><i class="fa fa-print"></i> '.$this->cms_lang('Print').'</a>
                    <a href="javascript:void(0)" id="btnExcel" class="btn btn-default btn-flat"<i class="fa fa-file-excel-o"></i> '.$this->cms_lang('Export').'</a>               
                </div>              
            </div>
            <div class="box-body" id="clonetext">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>'.$this->cms_lang('Employee ID').'</th>
                  <th>'.$this->cms_lang('Name').'</th>
                  <th>'.$this->cms_lang('Position ID').'</th>
                  <th>'.$this->cms_lang('Position Name').'</th>
                  <th>'.$this->cms_lang('Grade').'</th>
                  <th>'.$this->cms_lang('Last Date Grade').'</th>
                  <th>'.$this->cms_lang('Action').'</th>
                </tr>
                </thead>
                <tbody>';
            $no = 1;

            foreach ($query->result() as $data){

                $output .= '<tr>';
                $output .= '<td>'.$no.'</td>';
                $output .= '<td>'.$data->Prev_Per_No.'</td>';
                $output .= '<td>'.$data->Full_Name.'</td>';
                $output .= '<td>'.$data->Position_ID.'</td>';
                $output .= '<td>'.$data->Position_Name.'</td>';
                $output .= '<td>'.$data->Basic_Salary_Scale.'</td>';
                $output .= '<td>'.$this->basic_date_format($data->Last_Date_Grade).'</td>';
                $output .= '<td>X</td>';
                $output .= '</tr>';
                
                $no++;

            }               
                
                
            $output .= '</tfoot>';
            $output .= '</table>';
            $output .= '</div>';
            
            $output .= '<div class="box-footer">';
            $output .= '<a href="'.site_url($this->cms_module_path().'/organization_chart/').'" class="btn btn-default pull-left"><i class="fa fa-sitemap"></i> '.$this->cms_lang('Back to chart').'</a>&nbsp;';
            $output .= '<a href="javascript:void(0);" class="btn btn-default launch-search" data-id="'.$primary_key.'"><i class="fa fa-search"></i> '.$this->cms_lang('Search Again').'</a>';
            $output .= '</div>';

            $output .= '</div>';       

        
        $data = array(
            'output' => $output,
        );

        $this->view($this->cms_module_path().'/organization_chart_view', $data, $this->cms_complete_navigation_name('organization_chart'));
        
    }

    public function detail_date_format($date){
        if (is_null($date) || $date =='0000-00-00'){
            $tanggal = '';
        }
        else{
            $tanggal = date('d-M-Y H:i', strtotime($date));
        }
        return $tanggal;
    }

    public function basic_date_format($date){

        if (is_null($date) || $date =='0000-00-00'){
            $tanggal = '';
        }
        else{
            $tanggal = date('d-M-Y', strtotime($date));
        }
        return $tanggal;
    }

    public function count_employee_group($where_column, $value){

        $this->db->select('Prev_Per_No')
                 ->from('tp_raw_data')
                 ->where($where_column, $value)
                 ->group_by('Prev_Per_No');
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        return $num_row.' '.$this->cms_lang('Person');
    }



}