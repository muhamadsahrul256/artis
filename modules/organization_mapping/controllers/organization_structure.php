<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class organization_structure extends CMS_Controller {

    protected $subject_title = 'Diagram Frame';

    protected $candidate  = '';
    protected $periode_id  = '';
    protected $start_date  = '';
    protected $end_date    = '';
    protected $period_name = '';


    public function set_current_period($period_id =NULL){

        $today = date('Y-m-d');

        if(isset($period_id)){
            $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PeriodID', $period_id)
                 ->order_by('PStartDate', 'ASC');
            $db      = $this->db->get();
            $data    = $db->row(0);
            $num_row = $db->num_rows();

            $this->period_id  = $data->PeriodID;
            $this->start_date = $data->PStartDate;
            $this->end_date   = $data->PEndDate;
            $this->period_name  = $data->PeriodName;              
        }
        else{

            $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PStartDate <=', $today)
                 ->where('PEndDate >=', $today)
                 ->order_by('PStartDate', 'ASC');
            $db      = $this->db->get();
            $data    = $db->row(0);
            $num_row = $db->num_rows();

            $this->period_id  = $data->PeriodID;
            $this->start_date = $data->PStartDate;
            $this->end_date   = $data->PEndDate;
            $this->period_name  = $data->PeriodName; 
        }

        return $this;
    }
    
    
    public function index(){

        $primary_key = $this->input->get('period');

        $this->set_current_period($primary_key);

       
        $this->load->model('orgchart_master_model');

        $data['period_option'] = $this->period_option();
        $data['period_id'] = $this->period_id;

        $data['subject'] = $this->cms_lang($this->subject_title).' '.$this->period_name;    
        

        $data['Kantor_Induk_TJBB'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3401', $director_id=1);
        $data['APP_Cawang'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3411', $director_id=114);
        $data['APP_Pulogadung'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3412', $director_id=151);
        $data['APP_Durikosambi'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3413', $director_id=95);
        $data['APP_Cilegon'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3414', $director_id=113);

        $this->view($this->cms_module_path().'/organization_structure_view', $data);
    }

    public function ajax_edit($id)
    {
        $this->load->model($this->cms_module_path().'/person_model');
        $get_by_id = $this->person_model->get_by_id($id);
        $array = ["get_by_id" => $get_by_id, 'ajax_edit' => 'ajax_edit'];
        echo json_encode($array);
    }


    public function period_option(){


        if (isset($this->period_id)){
            $period_id = $this->input->get('period');

            $this->db->select('PeriodID, PeriodName, PStartDate, PEndDate')
                     ->from('mst_period')
                     ->where('PStatus', 1)
                     ->where('PeriodID', $this->period_id);
            $sql    = $this->db->get();
            $row    = $sql->row(0);
            $num_row= $sql->num_rows();

            if ($num_row > 0){
                //$current = $this->cms_lang('Period').' '.date('d-M-Y H:i', strtotime($row->upload_history_date));
                $current = $this->cms_lang('Period').' '.$row->PeriodName;
            }
            else{
                $current = $this->cms_lang('Period');
            }

        }
        else{
            $period_id = 0;
            $current   = $this->cms_lang('Period');
        }


        $empty_select = '<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">'.$current.' <span class="caret"></span></a>';
        $empty_select .= '<ul class="dropdown-menu" role="menu">';
        
        $empty_select_closed = '</ul>';

        $this->db->select('PeriodID, PeriodName, PStartDate, PEndDate')
                 ->from('mst_period')
                 ->where('PStatus', 1)
                 ->order_by('PStartDate','ASC');
                        
        $db = $this->db->get();            

        foreach($db->result() as $data):

            //$upload_history_date = date('d-M-Y H:i', strtotime($data->upload_history_date));

            if($data->PeriodID == $this->period_id) {
                $empty_select .= '<li class="active"><a href="{{ base_url }}organization_mapping/organization_structure/?period='.$data->PeriodID.'">'.$data->PeriodName.'</a></li>';
            }
            else{
                $empty_select .= '<li><a href="{{ base_url }}organization_mapping/organization_structure/?period='.$data->PeriodID.'">'.$data->PeriodName.'</a></li>';
            }      
        endforeach;

        return $empty_select.$empty_select_closed; 
    }

}