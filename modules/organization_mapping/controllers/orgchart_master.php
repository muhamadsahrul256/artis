<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class orgchart_master extends CMS_Controller {

    protected $URL_MAP = array();

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('Pdf');
        $this->load->model($this->cms_module_path().'/person_model');        
    }

    protected $subject_title = 'Diagram Frame';

    protected $semester_1 = '';
    protected $semester_2 = '';
    protected $semester_3 = '';
    protected $semester_4 = '';
    protected $semester_5 = '';
    protected $candidate  = '';
    protected $periode_id  = '';
    protected $start_date  = '';
    protected $end_date    = '';
    protected $period_name = '';


    public function set_current_period($period_id =NULL){

        $today = date('Y-m-d');

        if(isset($period_id)){
            $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PeriodID', $period_id)
                 ->order_by('PStartDate', 'ASC');
            $db      = $this->db->get();
            $data    = $db->row(0);
            $num_row = $db->num_rows();

            $this->period_id  = $data->PeriodID;
            $this->start_date = $data->PStartDate;
            $this->end_date   = $data->PEndDate;
            $this->period_name  = $data->PeriodName;              
        }
        else{

            $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PStartDate <=', $today)
                 ->where('PEndDate >=', $today)
                 ->order_by('PStartDate', 'ASC');
            $db      = $this->db->get();
            $data    = $db->row(0);
            $num_row = $db->num_rows();

            $this->period_id  = $data->PeriodID;
            $this->start_date = $data->PStartDate;
            $this->end_date   = $data->PEndDate;
            $this->period_name  = $data->PeriodName; 
        }

        return $this;
    }

    public function index(){

        $primary_key = $this->input->get('period');

        $this->set_current_period($primary_key);

        $this->load->model('orgchart_master_model');
         
        

        $data['subject'] = $this->cms_lang($this->subject_title).' '.$this->period_name;
        
        $data['Kantor_Induk_TJBB'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3401', $director_id=1);
        $data['APP_Cawang'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3411', $director_id=114);
        $data['APP_Pulogadung'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3412', $director_id=151);
        $data['APP_Durikosambi'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3413', $director_id=95);
        $data['APP_Cilegon'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3414', $director_id=113);
        
       
        $data['period_id'] = $this->period_id;
        
        
        $this->view($this->cms_module_path().'/orgchart_master_view', $data);
    }


}
