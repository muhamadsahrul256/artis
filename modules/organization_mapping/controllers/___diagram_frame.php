<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class diagram_frame extends CMS_Controller {

    protected $URL_MAP = array();

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('Pdf');
        $this->load->model($this->cms_module_path().'/person_model');        
    }

    protected $subject_title = 'Diagram Frame';

    protected $candidate  = '';
    protected $periode_id  = '';
    protected $start_date  = '';
    protected $end_date    = '';
    protected $period_name = '';


    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    public function set_current_period($period_id =NULL){

        $today = date('Y-m-d');

        if(isset($period_id)){
            $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PeriodID', $period_id)
                 ->order_by('PStartDate', 'ASC');
            $db      = $this->db->get();
            $data    = $db->row(0);
            $num_row = $db->num_rows();

            $this->period_id  = $data->PeriodID;
            $this->start_date = $data->PStartDate;
            $this->end_date   = $data->PEndDate;
            $this->period_name  = $data->PeriodName;              
        }
        else{

            $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PStartDate <=', $today)
                 ->where('PEndDate >=', $today)
                 ->order_by('PStartDate', 'ASC');
            $db      = $this->db->get();
            $data    = $db->row(0);
            $num_row = $db->num_rows();

            $this->period_id  = $data->PeriodID;
            $this->start_date = $data->PStartDate;
            $this->end_date   = $data->PEndDate;
            $this->period_name  = $data->PeriodName; 
        }

        return $this;
    }

    public function index(){

        $primary_key = $this->input->get('period');

        $this->set_current_period($primary_key);

        $this->load->model('orgchart_master_model');
        

        $data['subject'] = $this->cms_lang($this->subject_title).' '.$this->period_name;
    

        $data['Kantor_Induk_TJBB'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3401', $director_id=1);
        $data['APP_Cawang'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3411', $director_id=114);
        $data['APP_Pulogadung'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3412', $director_id=151);
        $data['APP_Durikosambi'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3413', $director_id=95);
        $data['APP_Cilegon'] = $this->orgchart_master_model->generate_frame($this->period_id, $business_id='3414', $director_id=113);


        $data['period_option'] = $this->period_option();
        $data['period_id'] = $this->period_id;
        $data['unmapping_qty'] = $this->count_unmapping($this->start_date, $this->end_date);
        
        $this->view($this->cms_module_path().'/diagram_frame_view', $data);
    }



    public function select_option_EmployeeID($primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%" name="EmployeeID" id="EmployeeID" data-header="'.$this->cms_lang('Select').' '.$this->cms_lang('Employee').'">';
        
        $empty_select_closed = '</select>';

        $empty_select .= '<option value="" data-subtext="">---'.$this->cms_lang('Empty').'---</option>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();      
      

            $this->db->select('EmployeeID')
                     ->from('tp_diagram_frame')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);
                                 
    
                $this->db->select('Prev_Per_No,Full_Name')
                         ->from('tp_profile')
                         //->where('Prev_Per_No', $data->EmployeeID)
                         ->order_by('Full_Name','ASC');
                        
                $db_sql = $this->db->get();            

                foreach($db_sql->result() as $row):
                    if($row->Prev_Per_No == $data->EmployeeID) {
                        $empty_select .= '<option value="'.$row->Prev_Per_No.'" data-subtext="" selected="selected">'.$row->Full_Name.'</option>';
                    } else {
                        $empty_select .= '<option value="'.$row->Prev_Per_No.'" data-subtext="">'.$row->Full_Name.'</option>';
                    }
                endforeach;                                 
            
            return $empty_select.$empty_select_closed;        
    }

    



    public function select_option_class_link($primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%" name="class_li" id="class_li" data-header="'.$this->cms_lang('Select').' '.$this->cms_lang('Class').'">';
        
        $empty_select_closed = '</select>';

        $empty_select .= '<option value="" data-subtext="">---'.$this->cms_lang('Empty').'---</option>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();      
      

            $this->db->select('class_li')
                     ->from('tp_diagram_frame')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);
                                 
    
                $this->db->select('class_li')
                         ->from('tp_diagram_frame')
                         ->where('class_li !=', '')
                         ->group_by('class_li')
                         //->where('Prev_Per_No', $data->EmployeeID)
                         ->order_by('class_li','ASC');
                        
                $db_sql = $this->db->get();            

                foreach($db_sql->result() as $row):
                    if($row->class_li == $data->class_li) {
                        $empty_select .= '<option value="'.$row->class_li.'" data-subtext="" selected="selected">'.$row->class_li.'</option>';
                    } else {
                        $empty_select .= '<option value="'.$row->class_li.'" data-subtext="">'.$row->class_li.'</option>';
                    }
                endforeach;                                 
            
            return $empty_select.$empty_select_closed;

        
    }


    public function select_option_warehouse($primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%" name="WarehouseID" id="WarehouseID" data-header="'.$this->cms_lang('Select').' '.$this->cms_lang('APP').'">';
        
        $empty_select_closed = '</select>';

        //$empty_select .= '<option value="" data-subtext="">---'.$this->cms_lang('Empty').'---</option>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();      
      

            $this->db->select('APP')
                     ->from('tp_diagram_frame')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);                                 
    
                $this->db->select('business_area_id, description')
                         ->from('mst_business_area')
                         ->order_by('business_area_id','ASC');
                        
                $db_sql = $this->db->get();            

                foreach($db_sql->result() as $row):
                    if($row->business_area_id == $data->APP) {
                        $empty_select .= '<option value="'.$row->business_area_id.'" data-subtext="" selected="selected">'.$row->description.'</option>';
                    } else {
                        $empty_select .= '<option value="'.$row->business_area_id.'" data-subtext="">'.$row->description.'</option>';
                    }
                endforeach;                                 
            
            return $empty_select.$empty_select_closed;        
    }

    public function select_option_functional($primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%" name="Functional" id="Functional" data-header="'.$this->cms_lang('Select').' '.$this->cms_lang('Functional').'">';
        
        $empty_select_closed = '</select>';

        $empty_select .= '<option value="0" data-subtext="">'.$this->cms_lang('NO').'</option>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();      
      

            $this->db->select('Functional')
                     ->from('tp_diagram_frame')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);
                                 
    
                $this->db->select('class_li')
                         ->from('tp_diagram_frame')
                         ->where('class_li !=', '')
                         ->group_by('class_li')
                         //->where('Prev_Per_No', $data->EmployeeID)
                         ->order_by('class_li','ASC');
                        
                $db_sql = $this->db->get();            

                
                    if(1 == $data->Functional) {
                        $empty_select .= '<option value="1" data-subtext="" selected="selected">YES</option>';
                    } else {
                        $empty_select .= '<option value="1" data-subtext="">YES</option>';
                    }
                                             
            
            return $empty_select.$empty_select_closed;

        
    }

    public function select_option_functional_employee($primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select data-live-search="true" class="selectpicker form-control" data-show-subtext="true" multiple data-selected-text-format="count > 3" data-container="body" data-width="100%" name="FunctionalID[]" id="FunctionalID" data-header="'.$this->cms_lang('Select').' '.$this->cms_lang('Functional').'">';
        
        $empty_select_closed = '</select>';

        //$empty_select .= '<option value="0" data-subtext="">'.$this->cms_lang('EMPTY').'</option>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();    
        

        $options = array();

        $query = $this->db->select('ParentID, EmployeeID')
                          ->from('tp_diagram_frame_detail')
                          ->where('ParentID', $primary_key)
                          ->get();
        foreach($query->result() as $data){
            $options[] = $data->EmployeeID;
        }

                                 
        $this->db->select('Prev_Per_No, Full_Name')
                         ->from('tp_profile')
                         ->group_by('Prev_Per_No')
                         ->order_by('Full_Name','ASC');
                        
        $db_sql = $this->db->get();

          
        foreach($db_sql->result() as $row):

            if (in_array($row->Prev_Per_No, $options)) {
                $empty_select .= '<option value="'.$row->Prev_Per_No.'" data-subtext="'.$row->Prev_Per_No.'" selected="selected">'.$row->Full_Name.'</option>';
            }
            else{
                $empty_select .= '<option value="'.$row->Prev_Per_No.'" data-subtext="'.$row->Prev_Per_No.'">'.$row->Full_Name.'</option>';
            }

        endforeach;    
                                
                                             
            
        return $empty_select.$empty_select_closed;        
    }

    public function generate_array_data($post_data){

        $post_data = json_encode($post_data, TRUE);

        $total = count($post_data);        
        $no    = 1;
        if ($total > 0){           
            $data  = '';
            foreach ($post_data as $value){
                $data .= $value;
                if ($no == $total){
                    $data .= '';
                }else{
                    $data .= ',';
                }
                $no++;
            }
        }
        else{
            $data  = '';
        }

        return $data;
    }


    public function chart_update(){

        //$action      = $this->input->post('form_button');
        $primary_key = $this->input->post('COID');
        

        $num_talent  = count($this->input->post('TalentID'));
        $talent = '';
        if ($num_talent > 0){
            foreach($this->input->post('TalentID') AS $key=> $values){
                $talent .= $values.',';
            }
        }

        $num_position  = count($this->input->post('PositionID'));
        $position = '';
        if ($num_position > 0){
            foreach($this->input->post('PositionID') AS $key=> $values){
                $position .= $values.',';
            }
        }

        $num_education  = count($this->input->post('EducationID'));
        $education = '';
        if ($num_education > 0){
            foreach($this->input->post('EducationID') AS $key=> $values){
                $education .= $values.',';
            }
        }


        $num_profession  = count($this->input->post('ProfessionID'));
        $profession = '';
        if ($num_profession > 0){
            foreach($this->input->post('ProfessionID') AS $key=> $values){
                $profession .= $values.',';
            }
        }          

        redirect($this->cms_module_path().'/diagram_frame/form_searching/?id='.$primary_key.'&grade='.$this->input->post('GradeID').'&talent='.$talent.'&position='.$position.'&profession='.$profession.'&education='.$education.'&training='.$this->input->post('TrainingID'));                              
           
    }

    public function form_searching(){

        $this->cms_guard_page('organization_mapping_diagram_frame');

        $primary_key    = $this->input->get('id');
        $GradeID        = $this->input->get('grade');
        $PositionID     = $this->input->get('position');
        $ProfessionID   = $this->input->get('profession');
        $EducationID    = $this->input->get('education');
        $TrainingID     = $this->input->get('training');


        $crud = $this->make_crud();
        $state = $crud->getState();
       
        $output = $crud->render();

        $data = array(
            'state' => $state,
            'primary_key' => $primary_key,
            'title' => '<small>('.$this->cms_lang('Currently searching').': '.$this->callback_diagram_data($primary_key, 'Description').')ss</small>'            
        );

        $output   = array_merge((array)$output, $data);

        $this->view($this->cms_module_path().'/form_searching_view', $output, $this->cms_complete_navigation_name('diagram_frame'));        
    }


    public function callback_user_profile($primary_key, $result_column){

        $this->db->select($result_column)
                 ->from('tp_raw_data')
                 ->where('Prev_Per_No', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            return $data->$result_column;
        } 
        else{
            return '--';
        }
    }

    public function count_employee_group($where_column, $value){

        $this->db->select('Prev_Per_No')
                 ->from('tp_profile')
                 ->where($where_column, $value)
                 ->group_by('Prev_Per_No');
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        return $num_row.' '.$this->cms_lang('Person');
    }



    private function make_crud(){
        $crud = $this->new_crud();
        // this is just for code completion
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        $crud->unset_export();

        //$crud->set_model('talent_searching_query_model');

        $primary_key    = $this->input->get('id');
        $GradeID        = $this->input->get('grade');
        $PositionID     = $this->input->get('position');
        $TalentID     = $this->input->get('talent');
        $EducationID    = $this->input->get('education');
        $ProfessionID    = $this->input->get('profession');
        $TrainingID     = $this->input->get('training');

        $crud->set_theme('no-flexigrid-diagram-search');

        if ($state !='edit' AND $state != 'add' AND $state !='read'){            
        }
        else{
            $crud->set_relation('Position_ID', 'mst_position_ladder', 'position_name');
        }

        $crud->set_language($this->cms_language());

        $current_employee = $this->cms_table_data($table_name='tp_diagram_frame', $where_column='COID', $result_column='EmployeeID', $primary_key);

        //$grading_data = $this->_grading_array_data($GradeID);

        // table name
        $crud->set_table('tp_profile');

        /*

        $query = "SELECT tp_profile.Prev_Per_No AS Prev_Per_No, tp_profile.Full_Name AS Full_Name,PS_group,Tgl_SK_Organizational_Assignmen,Pendidikan_Terakhir, tp_profile_education.Branch_of_study AS Branch_of_study     
                  FROM tp_profile INNER JOIN tp_profile_talent ON Prev_Per_No=EmployeeID INNER JOIN 
                  tp_profile_position_history ON tp_profile.Prev_Per_No=tp_profile_position_history.EmployeeID INNER JOIN 
                  tp_profile_education ON tp_profile_education.EmployeeID=tp_profile.Prev_Per_No 
                  WHERE Prev_Per_No !='".$current_employee."' ";

       
        if (!empty($GradeID) && !is_null($GradeID) && $GradeID != ''){

            $SQL    = "SELECT grade_code FROM mst_grade WHERE grade_level <= (SELECT grade_level AS level FROM `mst_grade` WHERE grade_code='".$GradeID."') ORDER BY grade_level DESC";
            $grade  = $this->db->query($SQL);        
            $num_row= $grade->num_rows();

            $lol = 1;

            $query .= " AND ( ";
            foreach($grade->result() as $data){

                if ($lol == $num_row){
                    $query .= " PS_group LIKE '".$data->grade_code."%' ";
                }
                else{
                    $query .= " PS_group LIKE '".$data->grade_code."%' OR ";
                }                            

            $lol++;
            }

            $query .= " ) ";

        }

        if (!empty($TalentID) && !is_null($TalentID) && $TalentID != ''){
            $query .= " AND FIND_IN_SET (Talent_Value,'".$TalentID."') ";
        }        

        if (!empty($PositionID) && !is_null($PositionID) && $PositionID != ''){
            $query .= " AND FIND_IN_SET (tp_profile_position_history.Position_Name,'".$PositionID."') ";
        }



        if (!empty($EducationID) && !is_null($EducationID) && $EducationID != ''){

            $tags = preg_replace('/,+/', ',', $EducationID);

            $myArray = array_diff(explode(",", $EducationID),array(""));
           
            $last  = end($myArray);

            $mam = 1;
            $query .= " AND ( ";
            foreach($myArray as $my_Array){

                if ($last == $my_Array){
                    $query .= " tp_profile_education.Branch_of_study LIKE '%".$my_Array."%' ";
                }
                else{
                    $query .= " tp_profile_education.Branch_of_study LIKE '%".$my_Array."%' OR ";
                }

                $mam++;        
            }

            $query .= " ) ";

        }

        $query .= " GROUP BY tp_profile.Prev_Per_No";

        $crud->basic_model->set_query_str($query);

        */       
    
        $title = '<small>'.$this->cms_lang('Currently searching').' : '.$this->callback_diagram_data($primary_key, 'Description').'</small>';            

        // primary key
        $crud->set_primary_key('Prev_Per_No');
        // set subject
        $crud->set_subject($title);

        /*
        $array1 = array('Prev_Per_No','Full_Name','Pendidikan_Terakhir','Branch_of_study','PS_group','Tgl_SK_Organizational_Assignmen');

        $array2 = array();

        $sql = $this->db->select('talent_period_id')
                        ->from('mst_talent_period')                          
                        ->order_by('talent_period_finish','DESC')
                        ->where('status',1)
                        ->limit(5)
                        ->get();    
        $no = 1;
        foreach($sql->result() as $ws){
            $array2[] = 'SM_'.$ws->talent_period_id;

            $var = 'semester_'.$no;

            $this->$var = $ws->talent_period_id;

            $crud->display_as('SM_'.$ws->talent_period_id, $this->cms_lang('SM '.$ws->talent_period_id));
            $crud->callback_column('SM_'.$ws->talent_period_id, array($this, '_callback_column_SM_'.$no));

            $no++;
        }

        if ($TrainingID == 1){
            $array3 = array('Training');
            $crud->callback_column('Training',array($this, '_callback_column_Training'));
        }
        else{
            $array3 = array();
        }        
    

        $result = array_merge($array1, $array2, $array3);
        */

        // displayed columns on list
        $crud->columns('Prev_Per_No','Full_Name');

        // displayed columns on edit operation
        $crud->edit_fields('Prev_Per_No','Full_Name','Position_ID','Position_Description','Unit_ID','Unit_Description','diklat');
        // displayed columns on add operation
        $crud->add_fields('Prev_Per_No','Full_Name','Position_ID','Position_Name','Last_Updated');

        //$crud->set_field_upload('Photos','assets/uploads/files');   

        // caption of each columns
        $crud->display_as('Prev_Per_No', $this->cms_lang('Employee ID'));
        $crud->display_as('Full_Name', $this->cms_lang('Name'));
        $crud->display_as('Position_ID', $this->cms_lang('Position ID'));
        $crud->display_as('Position_Name', $this->cms_lang('Position Name'));
        $crud->display_as('PS_group', $this->cms_lang('Grade'));
        $crud->display_as('Tgl_SK_Organizational_Assignmen', $this->cms_lang('TGL / MK JABATAN'));
        $crud->display_as('Branch_of_study', $this->cms_lang('Education History'));

        $crud->display_as('Training', $this->cms_lang('Training'));
        $crud->display_as('Pendidikan_Terakhir', $this->cms_lang('Last Education'));       
       
        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        //$crud->callback_field('diklat',array($this, '_callback_field_diklat'));        

        $crud->callback_column('Tgl_SK_Organizational_Assignmen',array($this, '_callback_column_Tgl_SK_Organizational_Assignmen'));
        

        $this->crud = $crud;
        return $crud;
    }

    public function callback_diagram_data($primary_key, $result_column){

        $this->db->select($result_column)
                 ->from('tp_diagram_frame')
                 ->where('COID', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            return $data->$result_column;
        } 
        else{
            return '';
        }
    }


    // returned on insert and edit
    public function callback_field_functional($value=NULL, $primary_key){

        $module_path = $this->cms_module_path();
        $this->config->load('grocery_crud');
        $date_format = $this->config->item('grocery_crud_date_format');

        if(!isset($primary_key)) $primary_key = -1;
        $query = $this->db->select('COIDNo, ParentID, EmployeeID, GradeID, PositionID')
            ->from('tp_diagram_frame_detail')
            ->where('ParentID', $primary_key)
            ->get();
        $result = $query->result_array();
            
        // get options
        $options = array();
        
        $options['EmployeeID'] = array();
        $query = $this->db->select('Prev_Per_No, Full_Name')
                          ->from('tp_profile')
                          ->group_by('Prev_Per_No')
                          ->order_by('Full_Name', 'ASC')
                          ->get();
        foreach($query->result() as $row){
            $options['EmployeeID'][] = array('value' => $row->Prev_Per_No, 'caption' => $row->Prev_Per_No.'. '.$row->Full_Name);
        }
        

        $data = array(
            'result' => $result,
            'options' => $options,
            'date_format' => $date_format,
            'primary_key' => $primary_key,
            'button_add_lang' => $this->cms_lang('Add Employee'),
        );
        return $this->load->view($this->cms_module_path().'/field_profile_functional',$data, TRUE);
    }


    // returned on insert and edit
    public function add_functional(){

        $primary_key = $this->uri->segment(4);

        $module_path = $this->cms_module_path();
        $this->config->load('grocery_crud');
        $date_format = $this->config->item('grocery_crud_date_format');

        if(!isset($primary_key)) $primary_key = -1;
        $query = $this->db->select('*')
            ->from('tp_diagram_frame_detail')
            ->where('ParentID', $primary_key)
            ->get();
        $result = $query->result_array();
            
        // get options
        $options = array();
        
        $options['EmployeeID'] = array();
        $query = $this->db->select('Prev_Per_No, Full_Name')
                          ->from('tp_profile')
                          ->group_by('Prev_Per_No')
                          ->order_by('Full_Name', 'ASC')
                          ->get();
        foreach($query->result() as $row){
            $options['EmployeeID'][] = array('value' => $row->Prev_Per_No, 'caption' => $row->Full_Name);
        }
        

        $data = array(
            'result' => $result,
            'options' => $options,
            'date_format' => $date_format,
            'primary_key' => $primary_key,
            'button_add_lang' => $this->cms_lang('Add Employee'),
        );
        
        $this->view($this->cms_module_path().'/field_profile_functional',$data);
    }

    private function make_crud_frame(){
      
        $crud = $this->new_crud();
        // this is just for code completion
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        //$crud->unset_add();
        // $crud->unset_edit();
        //$crud->unset_delete();

        if ($this->cms_have_privilege('add') != 1) $crud->unset_add();
        if ($this->cms_have_privilege('edit') != 1) $crud->unset_edit();
        if ($this->cms_have_privilege('delete') != 1) $crud->unset_delete();
        if ($this->cms_have_privilege('export') != 1) $crud->unset_export();

        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        // $crud->unset_export();
        $crud->unset_texteditor('Description');
        $crud->set_dialog_forms(false);


        $crud->set_theme('no-flexigrid-table-frame');

        $crud->set_language($this->cms_language());

        // table name
        $crud->set_table('tp_diagram_frame');
        $crud->where('tp_diagram_frame.ParentID >=', 0);
        //$crud->where('tp_diagram_frame.class_li !=', 'ghost');
        
        $crud->order_by('APP,ParentID,No_', 'ASC');

        
        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->display_as('No_','Index');
            $crud->set_relation('EmployeeID', 'tp_profile', '{Full_Name}');
            $crud->set_relation('UnitID', 'mst_organizational_unit', '{unit_id}');
            $crud->display_as('Full_Mapping_Code','Keyword');
        }
        else{
            $crud->display_as('Put_After',$this->cms_lang('Put after (index)'));
            $crud->set_relation('EmployeeID', 'tp_profile', '{Prev_Per_No} - {Full_Name}');
            $crud->set_relation('UnitID', 'mst_organizational_unit', '{unit_id} - {description}');
            $crud->display_as('Full_Mapping_Code','Keyword metadata (comma separated)');
        }

        // primary key
        $crud->set_primary_key('COID');

        // set subject
        $crud->set_subject($this->cms_lang($this->subject_title));

        // displayed columns on list
        $crud->columns('APP','UnitID','Description','Full_Mapping_Code','EmployeeID','GradeID','PositionID','Functional','Status');
        // displayed columns on edit operation
        $crud->edit_fields('APP','ParentID','UnitID','Functional','EmployeeID','Description','Full_Mapping_Code','GradeID','GradeCode','rowspan','Status','Detail');
        // displayed columns on add operation
        $crud->add_fields('APP','ParentID','Put_After','UnitID','Functional','EmployeeID','Description','Full_Mapping_Code','GradeID','GradeCode','rowspan','Status','Detail');

    
        // caption of each columns
        $crud->display_as('ParentID', $this->cms_lang('Atasan Langsung'));
        $crud->display_as('EmployeeID', $this->cms_lang('EmployeeID'));        
        $crud->display_as('Detail', $this->cms_lang('Functional Member'));        
        $crud->display_as('GradeID', $this->cms_lang('Grade Employee'));
        $crud->display_as('Description', $this->cms_lang('Position Name'));
        $crud->display_as('UnitID', $this->cms_lang('Org Unit'));
        $crud->display_as('APP', $this->cms_lang('Business Area'));
        $crud->display_as('Functional', $this->cms_lang('Functional'));
        $crud->display_as('EmployeeName', $this->cms_lang('Employee'));
        $crud->display_as('GradeCode', $this->cms_lang('Grade (maximum)'));
        

        $crud->required_fields('APP','ParentID','UnitID','Description','Full_Mapping_Code','GradeCode','Status','Functional');
        
        //$crud->unique_fields('Description');
        
        //$crud->set_relation('sub_group_text', 'mst_jenjang_sub_grp_text', '{jenjang_text}');
        //$crud->set_relation('sub_group_text', 'mst_jenjang_sub_grp_text', '{jenjang_text}'); 
        
        $crud->set_relation('class_li', 'mst_org_class', 'class_code');
        //$crud->set_relation('GradeID', 'mst_basic_salary_scale', 'salary_scale_id');
        //$crud->set_relation('GradeID', 'mst_grade_system', 'Level_Kompetensi_Id');
        
        $crud->set_relation('APP', 'mst_business_area', 'description');        

        $crud->set_relation('ParentID', 'tp_diagram_frame', 'Description');
        $crud->set_relation('GradeCode', 'mst_grade', 'grade_code');

        $crud->field_type('Functional', 'true_false', array(0=>$this->cms_lang('No'), 1=> $this->cms_lang('Yes'))); 

        $crud->field_type('rowspan','enum',array(0, 1, 2, 3, 4, 5, 6));
    

        
        //$crud->callback_column('citizen',array($this, '_callback_column_citizen'));
        
        //$crud->callback_field('ParentID',array($this, '_callback_field_parent_id'));


        
        $crud->callback_column('tp_diagram_frame'.'.Description', array(
            $this,
            'column_atasan_langsung'
        ));

        //$crud->callback_field('APP',array($this, '_callback_field_APP'));
        $crud->callback_field('ParentID', array($this, '_callback_field_parent_id'));
        //$crud->callback_field('Put_After', array($this, '_callback_field_index_chart'));
        $crud->callback_field('Detail',array($this, 'callback_field_functional'));
        //$crud->callback_field('EmployeeID', array($this, '_callback_field_EmployeeID')); 
        
        //$crud->callback_before_insert(array($this,'_before_insert'));
        //$crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));


        $this->crud = $crud;
        return $crud;
    }


    public function table_frame(){

        $crud = $this->make_crud_frame();

        $dd_data = array(               
            'dd_state' =>  $crud->getState(),              
            'dd_dropdowns' => array('APP','ParentID','Put_After'),               
            'dd_url' => array('', site_url().'/organization_mapping/diagram_frame/get_app/', site_url().'/organization_mapping/diagram_frame/get_parent/'),
            'dd_ajax_loader' => base_url().'ajax-loader.gif'
        );

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $output->dropdown_setup = $dd_data;
        $this->view($this->cms_module_path().'/table_frame_view', $output,$this->cms_complete_navigation_name('diagram_frame'));
    }

    public function __table_frame(){
        $crud = $this->make_crud_frame();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view($this->cms_module_path().'/table_frame_view', $output,
            $this->cms_complete_navigation_name('diagram_frame'));
    }


    public function column_atasan_langsung($value, $row){

        $this->db->select('COID,Description,Functional')
            ->from('tp_diagram_frame')
            ->where('ParentID', $row->COID);
        $query = $this->db->get();
        $child_count = $query->num_rows();
        // determine need_child class
        if($child_count>0){
            $can_be_expanded = TRUE;
            $need_child = ' need-child';
        }else{
            $can_be_expanded = FALSE;
            $need_child = '';
        }

        if ($row->class_li == 'section level-satu' || $row->class_li =='section level-satu penutup-satu' || $row->class_li =='section level-satu penutup-satu3x' || $row->class_li =='section level-dua'){
            $html  = '&nbsp;&nbsp;&nbsp;&nbsp;<a class="no-link" href="'.base_url('organization_mapping/diagram_frame/table_frame/edit/'.$row->COID).'">'.$value.'</a>';
        }
        /*
        elseif ($row->class_li == 'section level-dua jabatan-fungsional' || $row->class_li =='section level-satu jabatan-fungsional' ){
            $html  = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$value;
        }
        */
        elseif ($row->Functional == 1 ){
            $html  = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="no-link" href="'.base_url('organization_mapping/diagram_frame/table_frame/edit/'.$row->COID).'"><small>'.$value.'</small></a>';
        }
        else{
            $html  = '<a class="no-link" href="'.base_url('organization_mapping/diagram_frame/table_frame/edit/'.$row->COID).'">'.$value.'</a>';
        }

        /*

        $html  = '<a name="'.$row->COID.'"></a>';
        $html .= '<span>' .$value . '<br /></span>';
        $html .= '<input type="hidden" class="navigation_id'.$need_child.'" value="'.$row->COID.'" /><br />';
        // active or not
        
       
        // add children
        $html .= '&nbsp;&nbsp;&nbsp;<a href="'.site_url($this->cms_module_path().'/diagram_frame/table_frame/'.$row->COID).'/add">'.
            '<i class="glyphicon glyphicon-plus-sign"></i> '.$this->cms_lang('Add Child')
            .'</a>';

        */

        
        
        /*
        if(isset($_SESSION['__mark_move_navigation_id'])){
            $mark_move_navigation_id = $_SESSION['__mark_move_navigation_id'];
            if($row->navigation_id == $mark_move_navigation_id){
                // cancel link
                $html .= ' | <a href="'.site_url($this->cms_module_path().'/diagram_frame/navigation_move_cancel').'"><i class="glyphicon glyphicon-repeat"></i> Undo</a>';
            }else{
                // paste before, paste after, paste inside
                $html .= ' | <a href="'.site_url($this->cms_module_path().'/diagram_frame/navigation_move_before/'.$row->COID).'"><i class="glyphicon glyphicon-open"></i> Put Before</a>';
                $html .= ' | <a href="'.site_url($this->cms_module_path().'/diagram_frame/navigation_move_after/'.$row->COID).'"><i class="glyphicon glyphicon-save"></i> Put After</a>';
                $html .= ' | <a href="'.site_url($this->cms_module_path().'/diagram_frame/navigation_move_into/'.$row->COID).'"><i class="glyphicon glyphicon-import"></i> Put Into</a>';
            }
        }else{
            $html .= ' | <a href="'.site_url($this->cms_module_path().'/diagram_frame/navigation_mark_move/'.$row->COID).'"><i class="glyphicon glyphicon-share-alt"></i> Move</a>';
        }
        */

        return $html;
    }


    

    //CALLBACK FUNCTIONS
    public function _callback_field_parent_id($value, $primary_key){
        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select name="ParentID" id="ParentID" class="chosen-select" data-placeholder="Select Parent" style="width: 300px; display: none;">';
        //$empty_select = '<select name="ParentID" id="ParentID" data-live-search="false" class="selectpicker form-control" data-container="body" data-header="Select Division ID" data-width="350px" style="width:100%">';
        $empty_select .= '<option value="0" selected="selected">---'.$this->cms_lang('Empty').'---</option>';

        $empty_select_closed = '</select>';
        //GET THE ID OF THE LISTING USING URI
        $listingID = $this->uri->segment(5);
        
        //LOAD GCRUD AND GET THE STATE
        $crud = new grocery_CRUD();
        $state = $crud->getState();
        
        //CHECK FOR A URI VALUE AND MAKE SURE ITS ON THE EDIT STATE
        if(isset($listingID) && $state == "edit") {
            //GET THE STORED STATE ID
            $this->db->select('COID, APP, ParentID, Description')
                     ->from('tp_diagram_frame')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);
            
            $this->db->select('COID, APP, Description')
                     ->from('tp_diagram_frame')
                     //->where('COID', $data->ParentID)
                     ->where('Functional', 0)
                     ;
            $db = $this->db->get();
        
            foreach($db->result() as $row):
                if($row->COID == $data->ParentID) {
                    $empty_select .= '<option value="'.$row->COID.'" selected="selected">'.$row->Description.'</option>';
                } else {
                    $empty_select .= '<option value="'.$row->COID.'">'.$row->Description.'</option>';
                }
            endforeach;
            
            return $empty_select.$empty_select_closed;
        } else {
            return $empty_select.$empty_select_closed;  
        }
    }

    

    public function _callback_field_index_chart($value, $primary_key){
        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select name="Put_After" id="Put_After" class="chosen-select" data-placeholder="Select Atasan Langsung" style="width: 300px; display: none;">';
        //$empty_select = '<select name="DepartmentID" id="DepartmentID" data-live-search="false" class="selectpicker form-control" data-container="body" data-header="Select Department ID" data-width="350px" style="width:100%">';

        $empty_select_closed = '</select>';
        //GET THE ID OF THE LISTING USING URI
        $listingID = $this->uri->segment(5);
        
        //LOAD GCRUD AND GET THE STATE
        $crud = new grocery_CRUD();
        $state = $crud->getState();
        
        //CHECK FOR A URI VALUE AND MAKE SURE ITS ON THE EDIT STATE
        if(isset($listingID) && $state == "edit") {
            //GET THE STORED STATE ID
            $this->db->select('COID, APP, ParentID, No_, Description')
                     ->from('tp_diagram_frame')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);
            
            //GET THE CITIES PER STATE ID
            $this->db->select('COID, APP, No_, Description')
                     ->from('tp_diagram_frame')
                     //->where('Functional', 0)
                     ->where('ParentID', $data->ParentID)
                     ->where('COID !=', $data->COID)
                     ->order_by('No_', 'ASC')
                     ;
            $db = $this->db->get();
            
            //APPEND THE OPTION FIELDS WITH VALUES FROM THE STATES PER THE COUNTRY ID
            foreach($db->result() as $row):
                if($row->No_ < $data->No_) {
                    $empty_select .= '<option value="'.$row->COID.'" selected="selected">'.$row->Description.'</option>';
                } else {
                    $empty_select .= '<option value="'.$row->COID.'">'.$row->Description.'</option>';
                }
            endforeach;
            
            //RETURN SELECTION COMBO
            return $empty_select.$empty_select_closed;
        } else {
            //RETURN SELECTION COMBO
            return $empty_select.$empty_select_closed;  
        }
    }


    //GET JSON OF STATES
    public function get_app(){
        $APP_ID = $this->uri->segment(4);
        
        $this->db->select('COID,Description')
                 ->from('tp_diagram_frame')
                 ->where('Functional', 0)
                 ->where('APP', $APP_ID);
        $db = $this->db->get();
        
        $array = array();
        foreach($db->result() as $row):
            $array[] = array("value" => $row->COID, "property" => $row->Description);
        endforeach;
        
        echo json_encode($array);
        exit;
    }
    
    //GET JSON OF CITIES
    public function get_parent()
    {
        $ParentID = $this->uri->segment(4);
        
        $this->db->select('COID,Description')
                 ->from('tp_diagram_frame')
                 //->where('Functional', 0)
                 ->where('ParentID', $ParentID);
        $db = $this->db->get();
        
        $array = array();
        foreach($db->result() as $row):
            $array[] = array("value" => $row->COID, "property" => $row->Description);
        endforeach;
        
        echo json_encode($array);
        exit;
    }

    public function get_index()
    {
        $ParentID = $this->uri->segment(4);
        
        $this->db->select('COID,Description')
                 ->from('tp_diagram_frame')
                 //->where('Functional', 0)
                 ->where('ParentID', $ParentID);
        $db = $this->db->get();
        
        $array = array();
        foreach($db->result() as $row):
            $array[] = array("value" => $row->COID, "property" => $row->Description);
        endforeach;
        
        echo json_encode($array);
        exit;
    }



    public function table_frame_edit(){

        $crud = $this->make_crud_frame();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view($this->cms_module_path().'/table_frame_view', $output);
    }

    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_delete($primary_key){

        $this->db->delete('tp_diagram_frame_detail',
              array('ParentID'=> $primary_key));
        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){

        
        $data = json_decode($this->input->post('md_real_field_citizen_col'), TRUE);
        $insert_records = $data['insert'];
        $update_records = $data['update'];
        $delete_records = $data['delete'];
        $real_column_names = array('COIDNo', 'EmployeeID');
        $set_column_names = array();
       
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  DELETED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($delete_records as $delete_record){
            $detail_primary_key = $delete_record['primary_key'];
            // delete many to many
            $this->db->delete('tp_diagram_frame_detail', array('COIDNo'=>$detail_primary_key));
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  UPDATED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($update_records as $update_record){
            $detail_primary_key = $update_record['primary_key'];
            $data = array();
            foreach($update_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['ParentID'] = $primary_key;
            $this->db->update('tp_diagram_frame_detail', $data, array('COIDNo'=>$detail_primary_key));
            
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  INSERTED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($insert_records as $insert_record){
            $data = array();
            foreach($insert_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['ParentID'] = $primary_key;
            $this->db->insert('tp_diagram_frame_detail', $data);
            $detail_primary_key = $this->db->insert_id();
            
        }

        $this->save_many_column($primary_key);
        

        return TRUE;
    }

    public function save_many_column($primary_key){

        $SQL = "SELECT EmployeeID,Functional FROM tp_diagram_frame WHERE COID='".$primary_key."' ";
        $query  = $this->db->query($SQL);
        $num_row = $query->num_rows();
        $data = $query->row(0);

        if ($data->Functional == 1){

            $wquery = $this->db->select('COIDNo, EmployeeID')
                    ->from('tp_diagram_frame_detail')
                    ->where('ParentID', $primary_key)
                    ->get();
            $no=1;

            $num_row = $wquery->num_rows();
            foreach($wquery->result() as $row){

                $data_save = array(
                    'EmployeeName'=> $this->cms_table_data($table_name='tp_profile', $where_column='Prev_Per_No', $result_column='Full_Name', $row->EmployeeID),
                    'GradeID'=> $this->cms_table_data($table_name='tp_profile', $where_column='Prev_Per_No', $result_column='PS_group', $row->EmployeeID),
                    'PositionID'=> $this->cms_table_data($table_name='tp_profile', $where_column='Prev_Per_No', $result_column='Position_ID', $row->EmployeeID),                        
                );       
                
                $this->db->update('tp_diagram_frame_detail', $data_save, array('COIDNo'=> $row->COIDNo));
            }
        }
        else{
            $data_save = array(
                    'EmployeeName'=> $this->cms_table_data($table_name='tp_profile', $where_column='Prev_Per_No', $result_column='Full_Name', $data->EmployeeID),                        
                );
            $this->db->update('tp_diagram_frame', $data_save, array('COID'=> $primary_key));
        }        

    }


    public function _callback_field_ParentID($value, $primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select class="chosen-select" name="ParentID" id="ParentID" data-placeholder="Select Parent">';
        //$empty_select = '<select name="DestinationID" id="DestinationID" class="chzn-select form-control" data-placeholder="Select Project ID" style="width: 100% !important;" disabled>';

        $empty_select .= '<option value="0" selected="selected" disabled>Select</option>';
        $empty_select_closed = '</select>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();
       
        if(isset($listingID) && $state == "edit"){

            $this->db->select('COID, ParentID, Description')
                     ->from('tp_diagram_frame')
                     ->where('COID', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);
                         
    
            $this->db->select('COID, Description')
                     ->from('tp_diagram_frame')
                     ->where('Status', 1)
                     ->order_by('COID','ASC');
                        
            $db_sql = $this->db->get();            

            foreach($db_sql->result() as $row):
                if($row->COID == $data->ParentID) {
                    $empty_select .= '<option value="'.$row->COID.'" selected="selected">'.$row->COID.'. '.$row->Description.'</option>';
                } else {
                    $empty_select .= '<option value="'.$row->COID.'">'.$row->COID.'. '.$row->Description.'</option>';
                }
            endforeach;                                  
            
            return $empty_select.$empty_select_closed;

        } else {

            $this->db->select('COID, Description')
                     ->from('tp_diagram_frame')
                     ->where('Status', 1)
                     ->order_by('COID','ASC');
                        
            $db_sql = $this->db->get();            

            foreach($db_sql->result() as $row):
                   
                $empty_select .= '<option value="'.$row->COID.'">'.$row->COID.'. '.$row->Description.'</option>';
                    
            endforeach;

            return $empty_select.$empty_select_closed;  
        }
    }


    


    public function period_option(){


        if (isset($this->period_id)){
            $period_id = $this->input->get('period');

            $this->db->select('PeriodID, PeriodName, PStartDate, PEndDate')
                     ->from('mst_period')
                     ->where('PStatus', 1)
                     ->where('PeriodID', $this->period_id);
            $sql    = $this->db->get();
            $row    = $sql->row(0);
            $num_row= $sql->num_rows();

            if ($num_row > 0){
                //$current = $this->cms_lang('Period').' '.date('d-M-Y H:i', strtotime($row->upload_history_date));
                $current = $this->cms_lang('Period').' '.$row->PeriodName;
            }
            else{
                $current = $this->cms_lang('Period');
            }

        }
        else{
            $period_id = 0;
            $current   = $this->cms_lang('Period');
        }


        $empty_select = '<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">'.$current.' <span class="caret"></span></a>';
        $empty_select .= '<ul class="dropdown-menu" role="menu">';
        
        $empty_select_closed = '</ul>';

        $this->db->select('PeriodID, PeriodName, PStartDate, PEndDate')
                 ->from('mst_period')
                 ->where('PStatus', 1)
                 ->order_by('PStartDate','ASC');
                        
        $db = $this->db->get();            

        foreach($db->result() as $data):

            //$upload_history_date = date('d-M-Y H:i', strtotime($data->upload_history_date));

            if($data->PeriodID == $this->period_id) {
                $empty_select .= '<li class="active"><a href="{{ base_url }}organization_mapping/diagram_frame/?period='.$data->PeriodID.'">'.$data->PeriodName.'</a></li>';
            }
            else{
                $empty_select .= '<li><a href="{{ base_url }}organization_mapping/diagram_frame/?period='.$data->PeriodID.'">'.$data->PeriodName.'</a></li>';
            }      
        endforeach;

        return $empty_select.$empty_select_closed; 
    }


    public function print_selection(){
        $crud = $this->make_crud();        
        $id_list = json_decode($this->input->post('data'));

        foreach($id_list as $id){            
        }
    }

    public function jabatan_lama_user($employee_id){

        $this->db->select('Business_Area, Nama_Panjang_Posisi_SIMKP')
        ->from('tp_profile_position_history')
        ->where('EmployeeID', $employee_id)
        ->limit('1')
        ->order_by('End_Date','DESC');
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            return $data->Nama_Panjang_Posisi_SIMKP.' '.$data->Business_Area;
        }
        else{
            return '';
        }
    }


    public function nama_panjang_jabatan_pada_organisasi($employee_id){

        $this->db->select('Business_Area, Nama_Panjang_Posisi_SIMKP, Nama_Panjang_Posisi, tNamaPanjangOrg')
        ->from('tp_profile_position_history')
        ->join('mst_organizational_unit', 'mst_organizational_unit.unit_id=Org_unit')
        ->where('EmployeeID', $employee_id)
        ->limit('1')
        ->order_by('End_Date','DESC');
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            return $data->Nama_Panjang_Posisi.' <strong>PADA</strong> '.$data->tNamaPanjangOrg;
        }
        else{

            $this->db->select('Business_Area, Nama_Panjang_Posisi, tNamaPanjangOrg')
            ->from('tp_profile')
            ->join('mst_organizational_unit', 'mst_organizational_unit.unit_id=Org_unit')
            ->where('Prev_Per_No', $employee_id)
            ->limit('1');
            $db      = $this->db->get();
            $row    = $db->row(0);
            $num_row_sec = $db->num_rows();

            if ($num_row_sec > 0){
                return $row->Nama_Panjang_Posisi.' <strong>PADA</strong> '.$row->tNamaPanjangOrg;
            }else{
                return '';
            }
            
        }
    }

    public function nama_panjang_jabatan_pada_organisasi_by_unit($unit_id){

        $this->db->select('tNamaPanjangOrg')
        ->from('mst_organizational_unit')
        ->where('unit_id', $unit_id)
        ->limit('1');
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            return $data->tNamaPanjangOrg;
        }
        else{
            return '';
        }
    }

    public function tanggal_jabatan_lama_user($employee_id){

        $this->db->select('Business_Area, Nama_Panjang_Posisi_SIMKP,Tgl_SK_Organizational_Assignmen')
        ->from('tp_profile_position_history')
        ->where('EmployeeID', $employee_id)
        ->limit('1')
        ->order_by('End_Date','DESC');
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){

            $tanggal = date('d-M-Y', strtotime($data->Tgl_SK_Organizational_Assignmen));
        }
        else{
            $tanggal = '&nbsp;';
        }
        return $tanggal;

    }


    public function kriteria_talenta_data($employee_id, $period_id){

        $this->db->select('Last_Talent_Date, SemesterID, Talent_Value')
        ->from('tp_profile_talent')
        ->where('EmployeeID', $employee_id)
        ->where('SemesterID', $period_id)
        //->limit('1')
        ->order_by('Last_Talent_Date','DESC');
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            return $data->Talent_Value;
        }
        else{
            return '-';
        }
    }

    public function cms_table_data($table_name, $where_column, $result_column, $value){

        $this->db->select($result_column)->from($table_name)->where($where_column, $value);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            return $data->$result_column;
        }
        else{
            return '&nbsp;';
        }
    }

    
    

    public function _callback_column_Tgl_SK_Organizational_Assignmen($value, $row){   

        return $this->basic_date_format($row->Tgl_SK_Organizational_Assignmen);
    }

    public function _callback_column_Training($value, $row){
        
        $diklat = '';
        $SQL    = "SELECT nama_diklat, tanggal_sertifikat FROM tp_profile_diklat WHERE EmployeeID = '".$row->Prev_Per_No."' AND tanggal_sertifikat >= curdate() - interval 4 year";
        $query  = $this->db->query($SQL);        

        foreach($query->result() as $data){

            $diklat .= '<small>'.$data->nama_diklat.', tgl sertifikat '.$this->basic_date_format($data->tanggal_sertifikat).'</small>';

        }
        return $diklat;
    }


    public function basic_date_format($date){

        if (is_null($date) || $date =='0000-00-00'){
            $tanggal = '';
        }
        else{
            $tanggal = date('d-M-Y', strtotime($date));
        }
        return $tanggal;
    }

    public function generate_talent_pool(){

        $this_year = date('Y');

        $this->cms_guard_page('organization_mapping_diagram_frame');

        $primary_key    = $this->input->get('id');
        $GradeID        = $this->input->get('grade');
        $PositionID     = $this->input->get('position');
        $ProfessionID   = $this->input->get('profession');
        $EducationID    = $this->input->get('education');
        $TrainingID     = $this->input->get('training');

        if (!is_null($this->candidate) && !empty($this->candidate)){

        $query = "SELECT tp_profile.Prev_Per_No AS Prev_Per_No, tp_profile.Full_Name AS Full_Name,PS_group,tp_profile.Start_Date AS Start_Date,Tgl_SK_Organizational_Assignmen,Pendidikan_Terakhir, tp_profile_education.Branch_of_study AS Branch_of_study     
                  FROM tp_profile LEFT JOIN 
                  tp_profile_talent ON Prev_Per_No=EmployeeID LEFT JOIN 
                  tp_profile_position_history ON tp_profile.Prev_Per_No=tp_profile_position_history.EmployeeID LEFT JOIN 
                  tp_profile_education ON tp_profile_education.EmployeeID=tp_profile.Prev_Per_No LEFT JOIN 
                  tp_profile_profesi ON cEmployeeID=tp_profile.Prev_Per_No ";

            if ($TrainingID == 1){
                $query .= " INNER JOIN tp_profile_diklat ON tp_profile_diklat.EmployeeID=tp_profile.Prev_Per_No ";
            }

            $query .= " WHERE FIND_IN_SET (Prev_Per_No,'".$this->candidate."') GROUP BY tp_profile.Prev_Per_No";       

        }

        else{
        $current_employee = $this->cms_table_data($table_name='tp_diagram_frame', $where_column='COID', $result_column='EmployeeID', $primary_key);
        $statu_bagan      = $this->cms_table_data($table_name='tp_diagram_frame', $where_column='COID', $result_column='Functional', $primary_key);



        $query = "SELECT tp_profile.Prev_Per_No AS Prev_Per_No, tp_profile.Full_Name AS Full_Name,PS_group,tp_profile.Start_Date AS Start_Date,Tgl_SK_Organizational_Assignmen,Pendidikan_Terakhir, tp_profile_education.Branch_of_study AS Branch_of_study,grade_level     
                  FROM tp_profile LEFT JOIN 
                  tp_profile_talent ON Prev_Per_No=EmployeeID LEFT JOIN 
                  tp_profile_position_history ON tp_profile.Prev_Per_No=tp_profile_position_history.EmployeeID LEFT JOIN 
                  tp_profile_education ON tp_profile_education.EmployeeID=tp_profile.Prev_Per_No LEFT JOIN 
                  tp_profile_profesi ON tp_profile_profesi.cEmployeeID=tp_profile.Prev_Per_No LEFT JOIN 
                  mst_grade ON LEFT(tp_profile.PS_group, 3)=mst_grade.grade_code ";

        if ($TrainingID == 1){
            $query .= " LEFT JOIN tp_profile_diklat ON tp_profile_diklat.EmployeeID=tp_profile.Prev_Per_No ";
        }

        $query .= " WHERE Prev_Per_No !='".$current_employee."' ";

        if ($statu_bagan == 0){
            $query .= " AND RIGHT(Prev_Per_No , 3) !='P3Y' AND RIGHT(Prev_Per_No , 3) !='TAY' AND (Pendidikan_Terakhir NOT LIKE 'SD%' AND Pendidikan_Terakhir NOT LIKE 'SMP%' AND Pendidikan_Terakhir NOT LIKE 'SLTP%' AND Pendidikan_Terakhir NOT LIKE 'Sekolah Dasar%') ";
        }
        else{
            $query .= " AND (PS_group NOT LIKE 'ADV%' AND  PS_group NOT LIKE 'SPE%' AND  PS_group NOT LIKE 'OPT%') ";
        }

       
        if (!empty($GradeID) && !is_null($GradeID) && $GradeID != ''){

            //$SQL    = "SELECT grade_code FROM mst_grade WHERE grade_level <= (SELECT grade_level AS level FROM `mst_grade` WHERE grade_code='".$GradeID."') ORDER BY grade_level DESC";
            $SQL    = "SELECT grade_code FROM mst_grade WHERE FIND_IN_SET (grade_code,'".$GradeID."') ORDER BY grade_level ASC";
            $grade  = $this->db->query($SQL);        
            $num_row= $grade->num_rows();

            $lol = 1;

            $query .= " AND ( ";
            foreach($grade->result() as $data){

                if ($lol == $num_row){
                    $query .= " PS_group LIKE '".$data->grade_code."%' ";
                }
                else{
                    $query .= " PS_group LIKE '".$data->grade_code."%' OR ";
                }                            

            $lol++;
            }

            $query .= " ) ";

        }

        if (!empty($TalentID) && !is_null($TalentID) && $TalentID != ''){
            $query .= " AND FIND_IN_SET (Talent_Value,'".$TalentID."') ";
        }        

        if (!empty($PositionID) && !is_null($PositionID) && $PositionID != ''){
            $query .= " AND (FIND_IN_SET (tp_profile_position_history.Position_Name,'".$PositionID."') OR FIND_IN_SET (tp_profile.Position_Name,'".$PositionID."')) ";
        }

        if (!empty($ProfessionID) && !is_null($ProfessionID) && $ProfessionID != ''){
            $query .= " AND FIND_IN_SET (tp_profile_profesi.ckode_profesi,'".$ProfessionID."') ";
        }



        if (!empty($EducationID) && !is_null($EducationID) && $EducationID != ''){

            $tags = preg_replace('/,+/', ',', $EducationID);

            $myArray = array_diff(explode(",", $EducationID),array(""));
           
            $last  = end($myArray);

            $mam = 1;
            $query .= " AND ( ";
            foreach($myArray as $my_Array){

                if ($last == $my_Array){
                    $query .= " tp_profile_education.Branch_of_study LIKE '%".$my_Array."%' ";
                }
                else{
                    $query .= " tp_profile_education.Branch_of_study LIKE '%".$my_Array."%' OR ";
                }

                $mam++;        
            }

            $query .= " ) ";

        }

        $query .= " GROUP BY tp_profile.Prev_Per_No ORDER BY grade_level,tp_profile.Full_Name DESC";

        }

        $SQL = $this->db->query($query);


        /* Header */
        $table = '<table cellspacing="0" cellpadding="0" border="0" id="flex1">';
        $table .='<tr>';
        $table .='<td colspan="18" class="text-left"><strong>Komite Appraisal</strong></td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td colspan="18" class="text-left"><strong>PT PLN (Persero) Transmisi Jawa Bagian Barat</strong></td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<th colspan="18" class="text-center">BERITA ACARA</th>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<th colspan="18"><div align="center">UJI KELAYAKAN KANDIDAT PEMANGKU JABATAN</div></th>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<th colspan="18"><div align="center">NO :</div></th>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';
        $table .='<td>&nbsp;</td>';

        $table .='</tr>';
        $table .='<tr>';
        $table .='<td colspan="18">Dalam rangka meningkatkan efektivitas dan produktivitas kerja pada PT PLN (Persero) TRANSMISI JAWA BAGIAN BARAT, dan setelah diadakan evaluasi jabatan untuk menunjuk pegawai yang tepat untuk diangkat pada jabatan tersebut, maka pada :</td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td>&nbsp;</td><td>Hari</td><td>:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td>&nbsp;</td><td>Tanggal</td><td>:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td colspan="18">Ditetapkan rekomendasi sbb:</td>';
        $table .='</tr>';

        $table .='<tr>';
        $table .='<td rowspan="3" width="2%" class="border-all"><div align="center"><strong>NO</strong><div class="prt-check"></div></div></td>';
        $table .='<td rowspan="3" width="15%" class="border-all"><div align="center"><strong>NAMA / NIP</strong></div></td>';
        $table .='<td rowspan="3" width="5%" class="border-all"><div align="center"><strong>PDDKN</strong></div></td>';
        $table .='<td colspan="10" width="20%" class="border-all"><div align="center"><strong>TALENTA</strong></div></td>';
        $table .='<td rowspan="3" width="2%" class="border-all"><div align="center"><strong>GRADE SEJAK</strong></div></td>';
        $table .='<td colspan="2" width="15%" rowspan="2" class="border-all"><div align="center"><strong>JABATAN SAAT INI</strong></div></td>';
        $table .='<td rowspan="3" width="15%" class="border-all"><div align="center"><strong>USULAN JABATAN BARU</strong></div></td>';

        if (!empty($GradeID) && !is_null($GradeID) && $GradeID != ''){
            $table .='<td rowspan="3" class="border-all"><div align="center"><strong>DIKLAT</strong></div></td>';
        }
        else{
            $table .='<td rowspan="3" class="border-all"><div align="center"><strong>Rekomendasi Komite Appraisal</strong></div></td>';
        }


        $table .='</tr>';
        $table .='<tr>';

        $pSQL    = "SELECT * FROM ( SELECT LEFT(talent_period_start , 4) AS Tahun,count(talent_period_start) AS Jumlah FROM `mst_talent_period` WHERE status = 1 AND YEAR(talent_period_start) <='".$this_year."' GROUP By YEAR(talent_period_start) ORDER BY talent_period_start DESC LIMIT 5) sub ORDER BY Tahun ASC";
        $pQUERY  = $this->db->query($pSQL);        

        foreach($pQUERY->result() as $data){

            $table .='<td colspan="'.$data->Jumlah.'" width="4%" class="border-all"><div align="center"><strong>'.$data->Tahun.'</strong></div></td>';

        }
        /*
        $table .='<td colspan="2" width="4%" class="border-all"><div align="center"><strong>13</strong></div></td>';
        $table .='<td colspan="2" width="4%" class="border-all"><div align="center"><strong>14</strong></div></td>';
        $table .='<td colspan="2" width="4%" class="border-all"><div align="center"><strong>15</strong></div></td>';
        $table .='<td colspan="2" width="4%" class="border-all"><div align="center"><strong>16</strong></div></td>';
        $table .='<td colspan="2" width="4%" class="border-all"><div align="center"><strong>17</strong></div></td>';
        */

        $table .='</tr>';
  
        $table .='<tr>';

        $ssSQL    = "SELECT * FROM (SELECT RIGHT(talent_period_id , 1) AS Semester,talent_period_start AS Tahun FROM mst_talent_period WHERE status = 1 AND YEAR(talent_period_start) <='".$this_year."' GROUP By YEAR(talent_period_start), MONTH (talent_period_start) ORDER BY talent_period_start DESC LIMIT 10) sub ORDER BY Tahun ASC";
        $ssQUERY  = $this->db->query($ssSQL);        

        foreach($ssQUERY->result() as $ssDATA){

            $table .='<td width="3%" class="border-all"><div align="center">'.$ssDATA->Semester.'</div></td>';

        }

        $table .='<td class="border-all"><div align="center"><strong>Sebutan Jabatan</strong></div></td>';
        $table .='<td class="border-all"><div align="center"><strong>Sejak</strong></div></td>';
        $table .='</tr>';

        /* Data Talent Pool */
        $no = 1;
        foreach ($SQL->result() as $key => $data){

            if($no % 2 == 1){
                $erow = 'erows';
            }
            else{
                $erow = '';
            }

            $table .='<tr class="'.$erow.' result">';
            $table .='<td rowId="'.$data->Prev_Per_No.'" class="chd-check">&nbsp;</td>';
            $table .='<td class="border-all">'.$data->Full_Name.'<br/>'.$data->Prev_Per_No.'</td>';
            $table .='<td class="border-all"><div align="center">'.$data->Pendidikan_Terakhir.'</div></td>';

                $tpSQL    = "SELECT * FROM (SELECT talent_period_id AS Semester,talent_period_start AS Tahun FROM mst_talent_period GROUP By YEAR(talent_period_start), MONTH (talent_period_start) ORDER BY talent_period_start DESC LIMIT 10) sub ORDER BY Tahun ASC";
                $tpQUERY  = $this->db->query($tpSQL);        

                foreach($tpQUERY->result() as $tpDATA){

                    $table .='<td width="3%" class="border-all"><div align="center">'.$this->kriteria_talenta_data($data->Prev_Per_No, $tpDATA->Semester).'</div></td>';

                }            

            $table .='<td class="border-all"><div align="center">'.$data->PS_group.'<br/>'.$this->basic_date_format($data->Start_Date).'</div></td>';
            $table .='<td class="border-all">'.$this->nama_panjang_jabatan_pada_organisasi($data->Prev_Per_No).'</td>';
            $table .='<td class="border-all"><div align="center">'.$this->basic_date_format($data->Tgl_SK_Organizational_Assignmen).'</div></td>';
            $table .='<td class="border-all">'.$this->cms_table_data($table_name='tp_diagram_frame', $where_column='COID', $result_column='Description', $primary_key).'</td>';
            
            if (!empty($GradeID) && !is_null($GradeID) && $GradeID != ''){
                $table .='<td class="border-all">&nbsp;</td>';
            }
            else{
                $table .='<td class="border-all">&nbsp;</td>';
            }

            
            $table .='</tr>';

        $no++;
        }


        /* Footer */  
        $table .='<tr>';
        $table .='<td colspan="18">Demikian Evaluasi Jabatan ini dibuat untuk dipergunakan sebagaimana mestinya.</td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><th colspan="2">KOMITE APPRAISAL PT PLN (PERSERO) TRANSMISI JAWA BAGIAN BARAT</th>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td class="tebalin"><strong>KETUA</strong></td><td style="border-bottom:dashed 1px">&nbsp;</td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td height="30">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td></td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><strong>SEKRETARIS</strong></td>
                    <td style="border-bottom:dashed 1px">&nbsp;</td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td></td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td height="29">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><strong>ANGGOTA</strong></td>
                    <td style="border-bottom:dashed 1px"></td>';
        $table .='</tr>';
        $table .='<tr>';
        $table .='<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td></td>';
        $table .='</tr>';
        $table .='</table>';

        return $table;
    }


    public function set_candidate_value(){

        $id_list = json_decode($this->input->post('data'), true); 

        $array_primary_key = '';
        foreach($id_list as $primary_key){
            if ($primary_key != 'on' && $primary_key != 1){
                $array_primary_key .= $primary_key.',';
                $no++;
            }                        
        }

        $this->candidate = $array_primary_key;
        return $this;
    }

    public function export_to_excel(){

        $bagan_key = $this->input->get('id');
        $id_list   = json_decode($this->input->get('json'));

        $search_position = $this->cms_table_data($table_name='tp_diagram_frame', $where_column='COID', $result_column='Description', $bagan_key);
        $search_unit_id  = $this->cms_table_data($table_name='tp_diagram_frame', $where_column='COID', $result_column='UnitID', $bagan_key);


        $no=1;
        $array_primary_key = '';
        foreach($id_list as $primary_key){
            if ($primary_key != 'on' && $primary_key != 1){

                $search_name     = $this->cms_table_data($table_name='tp_profile', $where_column='Prev_Per_No', $result_column='Full_Name', $primary_key);

                $data = array('cCanEmployeeID'=>$primary_key, 'cCanEmployeeName'=>$search_name ,'cCanPosition'=> $search_position, 'iCanUnitID'=> $search_unit_id,'CreatedBy'=> $this->cms_user_id());

                $this->db->insert('tp_candidate', $data);

                $array_primary_key .= $primary_key.',';
                $no++;
            }                        
        }

        $today = date('YmdHis');

        $this->candidate = $array_primary_key;

        $data['title'] = 'BERITA_ACARA_'.$today;
        $data['contents'] = $this->generate_talent_pool();
       
        $this->view($this->cms_module_path().'/form_searching_export_view', $data);
    }

    public function count_unmapping($start_date, $end_date){

        $SQL   = "SELECT EmployeeID FROM tp_mutation_movement WHERE cStatus=4 AND CreatedTime >='".$start_date."' AND CreatedTime <='".$end_date."'";
        $query = $this->db->query($SQL);
        $count = $query->num_rows();
        return $count;
    }
    
    public function slide(){
        
        $data['slide_list'] = $this->slide_model->get();
        $data['slide_height'] = cms_module_config($this->cms_module_path(), 'slideshow_height');
        $data['module_path'] = $this->cms_module_path();
        if(count($data['slide_list'])>0){
            $this->view($this->cms_module_path().'/example_view', $data);
        }
    }
    
    public function tab(){
        $this->load->model('tab_model');
        $data['tab_list'] = $this->tab_model->get();
        if(count($data['tab_list'])>0){
            $this->view($this->cms_module_path().'/widget_tab', $data);
        }
    }
    
    public function visitor_counter(){
        $this->load->model('visitor_counter_model');
        $data['visitor_count'] = $this->visitor_counter_model->get();
        $this->view($this->cms_module_path().'/widget_visitor_counter', $data);
    }

    public function login_slide(){
        $this->load->model('slide_model');
        $data['slide_list'] = $this->slide_model->get();
        //$data['slide_height'] = cms_module_config($this->cms_module_path(), 'slideshow_height');
        $data['module_path'] = $this->cms_module_path();
        if(count($data['slide_list'])>0){
            $this->view($this->cms_module_path().'/widget_login_slide', $data);
        }
    }

    public function grading_searching($value=NULL){

        if (isset($value)){
            $grade = substr($value, 0, 3);

            if ($grade == 'SYS'){
                return 'SPE,SYS';
            }
            elseif($grade == 'OPT'){
                return 'SYS,OPT';
            }
            elseif($grade == 'ADV'){
                return 'OPT,ADV';
            }
            elseif($grade == 'INT'){
                return 'ADV,INT';
            }
            elseif($grade == 'SPE'){
                return 'BAS,SPE';
            }
            elseif($grade == 'BAS'){
                return 'BAS';
            }
            else{
                return $this->cms_lang('Undefined');
            }
        }
        else{
            return $this->cms_lang('Undefined');
        }
    }


    public function ajax_edit($id){
        $data = $this->person_model->get_by_id($id);

        $data->option_position = $this->person_model->get_option_position_data($id, $this->grading_searching($data->GradeCode));
        $data->search_grade = $this->grading_searching($data->GradeCode);
        $data->long_name_position = $data->Description.' PADA '.$this->nama_panjang_jabatan_pada_organisasi_by_unit($data->UnitID);
        echo json_encode($data);
    }

    public function ajax_unmapping($id){

        $data = $this->person_model->get_unmapping_data($id);
        
        echo json_encode($data);
    }


}
