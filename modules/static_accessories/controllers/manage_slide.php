<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Manage_Slide extends CMS_Priv_Strict_Controller {

	protected $URL_MAP = array();

	public function index(){
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// initialize groceryCRUD
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $crud = $this->new_crud();
        $crud->unset_jquery();
        $crud->unset_read();
        $crud->unset_print();

        // set model
        //$crud->set_model($this->cms_module_path().'/grocerycrud_slide_model');

        $crud->set_language($this->cms_language());

        // table name
        $crud->set_table('static_slide');

        // set subject
        $crud->set_subject('Slide');

        // displayed columns on list
        $crud->columns('image_url','content','cpage','status');
        // displayed columns on edit operation
        $crud->edit_fields('image_url','content','cpage','status');
        // displayed columns on add operation
        $crud->add_fields('image_url','content','cpage','status');

        // caption of each columns
        $crud->display_as('image_url','Image Url');
        $crud->display_as('content','Content');
        $crud->display_as('cpage','Page');

        $crud->required_fields('image_url');

        $crud->set_theme('no-flexigrid-default');

        $crud->field_type('cpage','multiselect', array( "1"  => "login", "2" => "dashboard"));


		
        $crud->set_field_upload('image_url','modules/'.$this->cms_module_path().'/assets/images/slides');

		$crud->callback_before_insert(array($this,'before_insert'));
		$crud->callback_before_update(array($this,'before_update'));
		$crud->callback_before_delete(array($this,'before_delete'));
		$crud->callback_after_insert(array($this,'after_insert'));
		$crud->callback_after_update(array($this,'after_update'));
		$crud->callback_after_delete(array($this,'after_delete'));



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view($this->cms_module_path().'/manage_slide_view', $output,
            $this->cms_complete_navigation_name('manage_slide'));

    }

    public function before_insert($post_array){
		return TRUE;
	}

	public function after_insert($post_array, $primary_key){
		$success = $this->after_insert_or_update($post_array, $primary_key);
		return $success;
	}

	public function before_update($post_array, $primary_key){
		return TRUE;
	}

	public function after_update($post_array, $primary_key){
		$success = $this->after_insert_or_update($post_array, $primary_key);
		return $success;
	}

	public function before_delete($primary_key){

		return TRUE;
	}

	public function after_delete($primary_key){
		return TRUE;
	}

	public function after_insert_or_update($post_array, $primary_key){

        return TRUE;
	}

}