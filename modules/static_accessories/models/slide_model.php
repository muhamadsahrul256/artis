<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slide_Model extends CMS_Model{
    public function get(){
        $query = $this->db->select('image_url, content')
            ->from('static_slide')
            ->where('status', 1)
            ->where('FIND_IN_SET(2,cpage) !=', 0)
            ->get();
        return $query->result_array();
    }

    public function get_slide_login(){
        $query = $this->db->select('image_url, content')
            ->from('static_slide')
            ->where('status', 1)
            ->where('FIND_IN_SET(1,cpage) !=', 0)
            ->get();
        return $query->result_array();
    }

}
