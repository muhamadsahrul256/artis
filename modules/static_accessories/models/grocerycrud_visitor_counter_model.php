<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of GroceryCrud_Visitor_Counter_Model
 *
 * @author Haseamada Module Generator
 */
class GroceryCrud_Visitor_Counter_Model  extends grocery_CRUD_Model{
	public function __construct(){
		parent::__construct();
	}	
}