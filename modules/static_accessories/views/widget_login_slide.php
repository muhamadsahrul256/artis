<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$li_indicator_list = array();
$div_item_list = array();
for($i=0; $i<count($slide_list); $i++){
    $slide = $slide_list[$i];
    if($i==0){
        $class = 'active';
    }else{
        $class = '';
    }
    $li_indicator_list[] = '<li data-target="#slideshow-widget" data-slide-to="'.$i.'" class="'.$class.'"></li>';
    $div_item_list[]     = '"'.base_url('modules/'.$module_path.'/assets/images/slides/'.$slide['image_url']).'",';
}
?>

<script type="text/javascript">
    $(document).ready(function(){        
        $.backstretch([<?php foreach($div_item_list as $div_item){ echo $div_item;} ?>], {duration: 3000, fade: 750});
    });
</script>
