<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['module_table_prefix']  = 'static';
$config['module_prefix']        = 'static_accessories';
$config['slideshow_height']     = '400px';
$config['grocery_crud_file_upload_allow_file_types'] 		= 'gif|jpeg|jpg|png|tiff';
$config['grocery_crud_file_upload_max_file_size'] 			= '20MB'; //ex. '10MB' (Mega Bytes), '1067KB' (Kilo Bytes), '5000B' (Bytes)
