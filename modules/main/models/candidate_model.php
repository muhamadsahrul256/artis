<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Candidate_Model extends CMS_Model{

    public function get_total(){
        $today = date('Y-m-d');
        $query = $this->db->select('cCanEmployeeID')
            ->from('tp_candidate')
            ->where('DATE(CreatedTime)', $today)
            ->get();
        return $query->num_rows();
    }

    public function get_candidate_data(){

        $today = date('Y-m-d');
        $query = $this->db->select('cCanEmployeeID, cCanEmployeeName, cCanPosition, iCanUnitID')
            ->from('tp_candidate')
            ->where('DATE(CreatedTime)', $today)
            ->group_by('cCanPosition,cCanEmployeeID')
            ->order_by('cCanEmployeeID','ASC')
            ->get();
        return $query->result();
    }

}
