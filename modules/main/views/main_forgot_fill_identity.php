<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    echo form_open('main/forgot', 'class="form"');
    echo form_label('{{ language:Identity }}', 'class="form-label"');
    echo form_input('identity', $identity, 'class="form-control" placeholder="{{ language:email or username }}"').br();
    echo form_submit('send_activation_code', $send_activation_code_caption, 'class="btn btn-primary btn-lg" style="width:100%;"');
    echo form_close();

    echo br().anchor(site_url('main/login'), $login_caption, array('class'=>'link pull-left')).br();