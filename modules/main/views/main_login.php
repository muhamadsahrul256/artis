<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
    #login_message:empty{
        display:none;
    }
</style>
<?php    
    echo form_open('main/login');
    echo form_label('{{ language:Identity }}');
    echo form_input('identity', $identity, 'placeholder="username or email" class="form-control"');
    echo form_label('{{ language:Password }}');
    echo form_password('password','','placeholder="password" class="form-control"').br();
    echo form_submit('login', $login_caption, 'class="btn btn-primary btn-lg" style="width:100%;"');    
    echo form_close();
    /*
    if(count($providers)>0){
        echo '{{ language:Or Login with }}:'.br();
        foreach($providers as $provider=>$connected){
            echo anchor(site_url('main/hauth/login/'.$provider), '<img src="'.base_url('modules/main/assets/third_party/'.$provider.'.png').'" />');
        }
    }
    */
    echo br().anchor(site_url('main/forgot'), $forgot_caption, array('class'=>'link pull-left')).br();
?>

<div id="login_message" class="alert alert-danger"><?php echo isset($message)?$message:''; ?></div>
