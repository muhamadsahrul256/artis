<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    echo form_open('main/forgot/'.$activation_code);
    echo form_label('{{ language:New Password }}').br();
    echo form_password('password', '', 'class="form-control" placeholder="{{ language:New Password }}"').br();
    echo form_label('{{ language:New Password (again) }}', 'class="form-label"').br();
    echo form_password('confirm_password', '', 'class="form-control" placeholder="{{ language:New Password (again) }}"').br();
    echo form_submit('change', $change_caption,'class="btn btn-primary btn-lg" style="width:100%;"');
    echo form_close();
    echo br();