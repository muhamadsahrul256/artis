<?php
// forgot_fill_identity.php
$lang['Identity'] = 'Identity';
$lang['Send activation code to my email'] = 'Send activation code to my email';

// forgot_change_password.php
$lang['New Password'] = 'New Password';
$lang['New Password (again)'] = 'New Password (again)';
$lang['Change'] = 'Change';

// change_profile.php
$lang['Password is empty'] = 'Password is empty';
$lang['Confirm password doesn\'t match'] = 'Confirm password doesn\'t match';
$lang['User Name'] = 'User Name';
$lang['Email'] = 'Email';
$lang['Real Name'] = 'Real Name';
$lang['Change Password'] = 'Change Password';
$lang['Password'] = 'Password';
$lang['Confirm Password'] = 'Confirm Password';
$lang['Change Profile'] = 'Change Profile';

// change_theme.php
$lang['Use'] = 'Use';
$lang['Currently use theme'] = 'Currently use theme';
$lang['Use theme'] = 'Use theme';
$lang['No Preview'] = 'No Preview.';
$lang['Error'] = 'Error';
$lang['Upload New Theme'] = 'Upload New Theme';

// login.php
$lang['User Name'] = 'User Name';
$lang['Password'] = 'Password';
$lang['Register'] = 'Register';
$lang['Or Login with'] = 'Or Login with';
$lang['Login'] = 'Login';
$lang['Login Failed'] = 'Login Failed!';

// module_activation_error.php
$lang['Installation Failed'] = 'Installation Failed';
$lang['Cannot activate'] = 'Cannot activate';
$lang['Back'] = 'Back';

// module_deactivation_error.php
$lang['Uninstallation Failed'] = 'Uninstallation Failed';
$lang['Cannot deactivate'] = 'Cannot deactivate';

// module_management.php
$lang['Activate'] = 'Activate';
$lang['Upgrade'] = 'Upgrade';
$lang['Deactivate'] = 'Deactivate';
$lang['Settings'] = 'Settings';
$lang['Need Upgrade'] = 'Need Upgrade';
$lang['Active'] = 'Active';
$lang['Inactive'] = 'Inactive';
$lang['Registered Version'] = 'Registered Version';
$lang['Current Version'] = 'Current Version';
$lang['Status'] = 'Status';
$lang['Error'] = 'Error';
$lang['Just another module'] = 'Just another module';
$lang['Upload New Module'] = 'Upload New Module';

// module_upgrade_error.php
$lang['Cannot upgrade'] = 'Cannot upgrade';

// register.php (most is shared with change_profile.php)
$lang['New User Registration'] = 'New User Registration';
$lang['Register'] = 'Register';


// rest, grocery crud related (add button, column titles)
$lang['Active'] = 'Active';
$lang['Inactive'] = 'Inactive';
$lang['Groups'] = 'Groups';
$lang['User'] = 'User';
$lang['Authorization'] = 'Authorization';
$lang['You cannot delete super admin user or your own account'] = 'You cannot delete super admin user or your own account';
$lang['User Group'] = 'User Group';
$lang['Group'] = 'Group';
$lang['Description'] = 'Description';
$lang['Users'] = 'Users';
$lang['Navigations'] = 'Navigations';
$lang['Privileges'] = 'Privileges';
$lang['Title'] = 'Title';
$lang['Privilege Code'] = 'Privilege Code';
$lang['You cannot delete admin group or group which is not empty, please empty the group first'] = 'You cannot delete admin group or group which is not empty, please empty the group first';
$lang['Navigation (Page)'] = 'Navigation (Page)';
$lang['Navigation Code'] = 'Navigation Code';
$lang['Is Root'] = 'Is Root';
$lang['Parent'] = 'Parent';
$lang['Navigation Title (What visitor see)'] = 'Navigation Title (What visitor see)';
$lang['Page Title'] = 'Page Title';
$lang['Page Keyword (Comma Separated)'] = 'Page Keyword (Comma Separated)';
$lang['URL (Where it is point to)'] = 'URL (Where it is point to)';
$lang['Static'] = 'Static';
$lang['Static Content'] = 'Static Content';
$lang['Only show content'] = 'Only show content';
$lang['Default Theme'] = 'Default Theme';
$lang['Quick Link'] = 'Quick Link';
$lang['Privilege'] = 'Privilege';
$lang['Widget'] = 'Widget';
$lang['Widget Code'] = 'Widget Code';
$lang['Title (What visitor see)'] = 'Title (What visitor see)';
$lang['Order'] = 'Order';
$lang['Slug'] = 'Slug';
$lang['Configuration'] = 'Configuration';
$lang['Configuration Key'] = 'Configuration Key';
$lang['Configuration Value'] = 'Configuration Value';

$lang['Password'] = 'Password';
$lang['Statistic'] = 'Statistic';
$lang['Configurations'] = 'Configurations';
$lang['Images'] = 'Images';
$lang['Default Layout'] = 'Default Layout';
$lang['Site Configurations'] = 'Site Configurations';
$lang['Default layout used'] = 'Default layout used';
$lang['Site Themes (e.g: Admin LTE, etc)'] = 'Site Themes (e.g: Admin LTE, etc)';
$lang['Default language used'] = 'Default language used';
$lang['Site name (e.g: My Company website, etc)'] = 'Site name (e.g: My Company website, etc)';
$lang['Your site slogan (e.g: "There is no place like home", etc)'] = 'Your site slogan (e.g: "There is no place like home", etc)';
$lang['Your site version (e.g: 1.0)'] = 'Your site version (e.g: 1.0)';
$lang['Site footer &amp; attribution (e.g: "Powered by My Company © 2020", etc)'] = 'Site footer &amp; attribution (e.g: "Powered by My Company © 2020", etc)';
$lang['User Activation (Automatic, By Email, or Manual)'] = 'User Activation (Automatic, By Email, or Manual)';
$lang['Automatic Add subsite when user register (only works if multisite module is installed)'] = 'Automatic Add subsite when user register (only works if multisite module is installed)';
$lang['You should has "wildcard" DNS in order to make this works'] = 'You should has "wildcard" DNS in order to make this works';
$lang['Default Subsite Homepage Content'] = 'Default Subsite Homepage Content';
$lang['Reply address for all generated email'] = 'Reply address for all generated email';
$lang['Reply name for all generated email'] = 'Reply name for all generated email';
$lang['Forgot password mail subject'] = 'Forgot password mail subject';
$lang['Forgot password mail message'] = 'Forgot password mail message';
$lang['Email User Agent'] = 'Email User Agent';
$lang['SMTP Host (e.g: ssl://smtp.googlemail.com)'] = 'SMTP Host (e.g: ssl://smtp.googlemail.com)';
$lang['SMTP User (e.g: your.gmail.account@gmail.com)'] = 'SMTP User (e.g: your.gmail.account@gmail.com)';
$lang['SMTP Password'] = 'SMTP Password';
$lang['SMTP Port (e.g: 465)'] = 'SMTP Port (e.g: 465)';
$lang['SMTP Timeout (e.g: 30)'] = 'SMTP Timeout (e.g: 30)';
$lang['Image used as site Logo (200px &times; 45px)'] = 'Image used as site Logo (200px &times; 45px)';
$lang['Image used as favicon (50px &times; 50px)'] = 'Image used as favicon (50px &times; 50px)';

$lang['Choose your language'] = 'Choose your language';
$lang['long in this position'] = 'Position Time (Year)';