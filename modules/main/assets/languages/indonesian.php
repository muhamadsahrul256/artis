<?php
// forgot_fill_identity.php
$lang['Identity'] = 'Identitas';
$lang['Send activation code to my email'] = 'Kirimkan kode aktivasi ke email saya';

// forgot_change_password.php
$lang['New Password'] = 'Kata sandi baru';
$lang['New Password (again)'] = 'Kata sandi baru (lagi)';
$lang['Change'] = 'Ubah';

// change_profile.php
$lang['Password is empty'] = 'Kata sandi kosong';
$lang['Confirm password doesn\'t match'] = 'Konfirmasi password tidak sesuai';
$lang['User Name'] = 'Nama Pengguna';
$lang['Email'] = 'Email';
$lang['Real Name'] = 'Nama Asli';
$lang['Change Password'] = 'Ubah Kata Sandi';
$lang['Password'] = 'Kata Sandi';
$lang['Confirm Password'] = 'Konfirmasi Kata Sandi';
$lang['Change Profile'] = 'Ubah Profil';

// change_theme.php
$lang['Use'] = 'Gunakan';
$lang['Currently use theme'] = 'Sedang menggunakan tema';
$lang['Use theme'] = 'Gunakan tema';
$lang['No Preview'] = 'Tidak Ada Tampilan';
$lang['Error'] = 'Error';
$lang['Upload New Theme'] = 'Unggah Tema Baru';

// login.php
$lang['User Name'] = 'Nama Pengguna';
$lang['Password'] = 'Kata Sandi';
$lang['Register'] = 'Daftar';
$lang['Or Login with'] = 'Atau Masuk dengan';
$lang['Login'] = 'Masuk';
$lang['Login Failed'] = 'Gagal Masuk!';

// module_activation_error.php
$lang['Installation Failed'] = 'Instalasi Gagal';
$lang['Cannot activate'] = 'Tidak dapat mengaktifkan';
$lang['Back'] = 'Kembali';

// module_deactivation_error.php
$lang['Uninstallation Failed'] = 'Uninstalasi Gagal';
$lang['Cannot deactivate'] = 'Tidak dapat menonaktifkan';

// module_management.php
$lang['Activate'] = 'Aktifkan';
$lang['Upgrade'] = 'Upgrade';
$lang['Deactivate'] = 'Nonaktifkan';
$lang['Settings'] = 'Pengaturan';
$lang['Need Upgrade'] = 'Butuh Upgrade';
$lang['Active'] = 'Aktif';
$lang['Inactive'] = 'Tidak Aktif';
$lang['Registered Version'] = 'Versi Terdaftar';
$lang['Current Version'] = 'Versi Saat Ini';
$lang['Status'] = 'Status';
$lang['Error'] = 'Error';
$lang['Just another module'] = 'Hanya sebuah modul';
$lang['Upload New Module'] = 'Unggah Modul Baru';

// module_upgrade_error.php
$lang['Cannot upgrade'] = 'Tidak bisa upgrade';

// register.php (most is shared with change_profile.php)
$lang['New User Registration'] = 'Registrasi Pengguna Baru';
$lang['Register'] = 'Daftar';


// rest, grocery crud related (add button, column titles)
$lang['Active'] = 'Aktif';
$lang['Inactive'] = 'Nonaktif';
$lang['Groups'] = 'Grup';
$lang['User'] = 'Pengguna';
$lang['Authorization'] = 'Autorisasi';
$lang['You cannot delete super admin user or your own account'] = 'Anda tidak dapat menghapus super admin atau akun anda sendiri';
$lang['User Group'] = 'Grup Pengguna';
$lang['Group'] = 'Grup';
$lang['Description'] = 'Keterangan';
$lang['Users'] = 'Pengguna';
$lang['Navigations'] = 'Navigasi';
$lang['Privileges'] = 'Hak Akses';
$lang['Title'] = 'Judul';
$lang['Privilege Code'] = 'Kode Hak Akses';
$lang['You cannot delete admin group or group which is not empty, please empty the group first'] = 'Anda tidak dapat menghapus grup admin atau grup yang tidak kosong, silahkan kosongkan grup terlebih dahulu';
$lang['Navigation (Page)'] = 'Navigasi (Halaman)';
$lang['Navigation Code'] = 'Kode Navigasi';
$lang['Is Root'] = 'Adalah Induk Utama';
$lang['Parent'] = 'Orang Tua';
$lang['Navigation Title (What visitor see)'] = 'Judul Navigasi (Yang dilihat pengunjung)';
$lang['Page Title'] = 'Judul Halaman';
$lang['Page Keyword (Comma Separated)'] = 'Kata kunci halaman (Dipisahkan koma)';
$lang['URL (Where it is point to)'] = 'URL (Mengarah ke mana)';
$lang['Static'] = 'Statik';
$lang['Static Content'] = 'Konten Statik';
$lang['Only show content'] = 'Hanya Menunjukkan Konten';
$lang['Default Theme'] = 'Tema Default';
$lang['Quick Link'] = 'Penghubung Cepat';
$lang['Privilege'] = 'Hak Akses';
$lang['Widget'] = 'Widget';
$lang['Widget Code'] = 'Kode Widget';
$lang['Title (What visitor see)'] = 'Judul (Yang dilihat pengunjung)';
$lang['Order'] = 'Urutan';
$lang['Slug'] = 'Slug';
$lang['Configuration'] = 'Konfigurasi';
$lang['Configuration Key'] = 'Kunci Konfigurasi';
$lang['Configuration Value'] = 'Nilai Konfigurasi';

$lang['Password'] = 'Kata sandi';
$lang['Statistic'] = 'Statistik';
$lang['Configurations'] = 'Konfigurasi';
$lang['Images'] = 'Gambar';
$lang['Default Layout'] = 'Default Layout';
$lang['Site Configurations'] = 'Konfigurasi Situs';
$lang['Default layout used'] = 'Standar tata ruang yg digunakan';
$lang['Site Themes (e.g: Admin LTE, etc)'] = 'Tema Situs (cth: Admin LTE, dll)';
$lang['Default language used'] = 'Standar bahasa yang digunakan';
$lang['Site name (e.g: My Company website, etc)'] = 'Nama Situs (cth: Situs Perusahaan Saya, dll)';
$lang['Your site slogan (e.g: "There is no place like home", etc)'] = 'Slogan situs anda (cth: "Tidak ada tempat seperti rumah sendiri", dll)';
$lang['Your site version (e.g: 1.0)'] = 'Versi situs anda (cth: 1.0)';
$lang['Site footer &amp; attribution (e.g: "Powered by My Company © 2020", etc)'] = 'Kaki penutup & atribusi situs (cth: "Dikembangkan oleh Perusahaan saya © 2020", dll)';
$lang['User Activation (Automatic, By Email, or Manual)'] = 'Aktifasi Pengguna (Otomatis, Dengan Email, atau Manual)';
$lang['Automatic Add subsite when user register (only works if multisite module is installed)'] = 'Otomatis tambah subsite ketika pendaftaran register (Bekerja hanya jika modul multisite terpasang)';
$lang['You should has "wildcard" DNS in order to make this works'] = 'Harusnya anda punya "wildcard" DNS supaya membuat ini bekerja';
$lang['Default Subsite Homepage Content'] = 'Konten standar Halaman beranda subsite';
$lang['Reply address for all generated email'] = 'Alamat membalas untuk semua email yg dihasilkan';
$lang['Reply name for all generated email'] = 'Nama untuk membalas untuk semua email yg dihasilkan';
$lang['Forgot password mail subject'] = 'Subyek email untuk Lupa password';
$lang['Forgot password mail message'] = 'Pesan email untuk Lupa password';
$lang['Email User Agent'] = 'Pengguna Agen Email';
$lang['SMTP Host (e.g: ssl://smtp.googlemail.com)'] = 'SMTP Host (cth: ssl://smtp.googlemail.com)';
$lang['SMTP User (e.g: your.gmail.account@gmail.com)'] = 'SMTP User (cth: your.gmail.account@gmail.com)';
$lang['SMTP Password'] = 'Kata sandi SMTP';
$lang['SMTP Port (e.g: 465)'] = 'SMTP Port (cth: 465)';
$lang['SMTP Timeout (e.g: 30)'] = 'SMTP Timeout (cth: 30)';
$lang['Image used as site Logo (200px &times; 45px)'] = 'Gambar digunakan sebagai Logo situs (200px &times; 45px)';
$lang['Image used as favicon (50px &times; 50px)'] = 'Gambar digunakan sebagai favicon (50px &times; 50px)';

$lang['Choose your language'] = 'Pilih bahasa anda';
$lang['You have'] = 'Anda memiliki';
$lang['notifications'] = 'notifikasi';
$lang['Version'] = 'Versi';
$lang['Remove photo when saving'] = 'Hapus gambar ketika menyimpan';
$lang['Change Photo'] = 'Ganti Gambar';
$lang['Upload Photo'] = 'Unggah Gambar';
$lang['(No photo)'] = '(Tidak ada gambar)';
$lang['long in this position'] = 'Waktu Menjabat (Tahun)';

