<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adjustment extends CMS_Priv_Strict_Controller {

    protected function do_override_url_map($URL_MAP){
        $module_path = $this->cms_module_path();
        $navigation_name = $this->cms_complete_navigation_name('index');
        $URL_MAP[$module_path.'/'.$module_path] = $navigation_name;
        $URL_MAP[$module_path] = $navigation_name;
        return $URL_MAP;
    }

    public function index(){
    	$data['content'] = $this->cms_submenu_screen($this->cms_complete_navigation_name('index'));
        $data['title'] = $this->cms_lang('Adjustment');
        $this->view($this->cms_module_path().'/adjustment_index', $data,
            $this->cms_complete_navigation_name('index'));
    }
}