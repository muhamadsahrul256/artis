<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class chart_model extends CMS_Model{

    public $period_id = '';
    public $period_name = '';
    public $start_date = '';
    public $end_date = '';

    public $table_diagram_frame        = NULL;
    public $table_diagram_frame_detail = NULL;
    protected $periode_upload_id  = '';
    protected $business_id = '';
    protected $business_name = '';
    protected $where_business = '';


    public function set_table_frame($period_id, $business_id){

      $this->set_current_period($period_id);

        if ($this->periode_upload_id <= 0 ){
            $table_diagram_frame        = 'tp_diagram_frame';
            $table_diagram_frame_detail = 'tp_diagram_frame_detail';

            $this->table_diagram_frame        = $table_diagram_frame;
            $this->table_diagram_frame_detail = $table_diagram_frame_detail;
        }
        else{
            $table_diagram_frame        = 'tp_diagram_frame_'.$this->periode_upload_id;
            $table_diagram_frame_detail = 'tp_diagram_frame_detail_'.$this->periode_upload_id;

            if ($this->db->table_exists($table_diagram_frame)){
                $this->table_diagram_frame        = $table_diagram_frame;
                $this->table_diagram_frame_detail = $table_diagram_frame_detail;
            }
            else{
                $this->table_diagram_frame        = 'tp_diagram_frame';
                $this->table_diagram_frame_detail = 'tp_diagram_frame_detail';
            }
        }

        if(isset($business_id)){            
            $this->business_id = $business_id;

            if($business_id == 0){
                $this->business_name = $this->cms_lang('ALL');
                $this->where_business = '>0';
            }
            else{
                $this->business_name = $this->data_table_value('mst_business_area', 'business_area_id', 'description', $business_id);
                $this->where_business = '='.$business_id;
            }
        }
        else{
            $this->business_id = 0;
            $this->business_name = $this->cms_lang('ALL');
            $this->where_business = '>0';
        }
    }

    public function set_current_period($period_id){        

        $this->db->select('PeriodID, PStartDate, PEndDate')
                 ->from('mst_period')
                 ->where('PeriodID', $period_id)
                 ->order_by('PStartDate', 'ASC');
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();  
       
    

        if ($num_row > 0){

            $SQL = "SELECT upload_history_id FROM tp_profile_upload_history 
                WHERE upload_history_created >='".$data->PStartDate."' AND 
                upload_history_created <='".$data->PEndDate."' ORDER BY upload_history_created DESC LIMIT 1";

            $query  = $this->db->query($SQL);
            $num_row2 = $query->num_rows();
            $row = $query->row(0);
            if($num_row2 > 0){
                $this->periode_upload_id   = $row->upload_history_id;
            }
            else{
                $this->periode_upload_id   = 0;
            } 

            
        }
        else{
            $this->periode_upload_id   = 0;
        }               

        return $this;
    }


	public function get_statistic_data($primary_key){

        $this->db->select('description')
                 ->from($this->cms_complete_table_name('mst_statistic_type'))
                 ->where('statistic_type_id', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            return $this->cms_lang($data->description);
        } 
        else{
            return '';
        }
    }


    public function list_chart_statictic($period_id, $business_area_id){

        $this->set_table_frame($period_id, $business_area_id);

        $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PeriodID', $period_id);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            $this->period_id = $data->PeriodID;
            $this->period_name = $data->PeriodName;
            $this->start_date = $data->PStartDate;
            $this->end_date = $data->PEndDate;
        }
        else{
            $this->period_name = $this->cms_lang('Current');
        }



        $query = $this->db->select('statistic_type_id,description,bar_color,type,angle')
            ->from('mst_statistic_type')
            //->join($this->cms_complete_table_name('mst_statistic_type'), 'statistic_type_id=graph_id')
            //->where('employee_id', $this->cms_user_id())
            //->where('st_user_graph.status', 1)
            ->where('mst_statistic_type.status', 1)
            ->order_by('statistic_type_id', 'ASC')
            ->get();

        $chart = '';
        $no=1;
        
        foreach($query->result() as $row){

            $element = 'chart_'.$row->statistic_type_id;
            $callback_text    = 'callback_value_chart_'.$row->statistic_type_id;

            $value   = $this->$callback_text($period_id);

            if($row->statistic_type_id == 6){
                $title = '';
            }
            else{
                $title = strtoupper($this->business_name);
            }


            $chart .= '<div class="col-md-6">';         
          	$chart .= '<div class="box box-primary">';
            $chart .= '<div class="box-header with-border">';
            $chart .= '<h3 class="box-title">'.$no.'. '.$row->description.' ('.$title.' '.strtoupper($this->period_name).')</h3>';
            $chart .= '<div class="box-tools pull-right">';
            $chart .= '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>';
            $chart .= '<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>';
            $chart .= '</div>';
            $chart .= '</div>';
            $chart .= '<div class="box-body chart-responsive">';    

            $chart .= '<div class="chart" id="'.$element.'" ></div>';            
           
            $chart .= '</div>';           
          	$chart .= '</div>';        

        	$chart .= '</div>';

            if ($row->type =='bar'){
                $chart .= $this->generate_chart_single_vertically($period_id, $element, $value, $labels='{{ language:Employee }}', $row->bar_color, $row->angle);
            }
            else if($row->type =='line'){
                $chart .= $this->generate_chart_line($period_id, $element, $value, $labels='{{ language:Employee }}', $row->bar_color, $row->angle);
            }
            else if($row->type =='donut'){
                $chart .= $this->generate_chart_donut($period_id, $element, $value, $labels='{{ language:Employee }}', $row->bar_color, $row->angle);
            }
            else{
                $chart .= $this->generate_chart_single_vertically($period_id, $element, $value, $labels='{{ language:Employee }}', $row->bar_color, $row->angle);
            }
        $no++;
           
        }

        return $chart;
        

    }


    public function generate_chart_single_vertically($period_id, $element, $value, $labels, $colors, $angle){

        $output ='<script>
                    $(function () {
                    "use strict";   
                        var bar = new Morris.Bar({
                        element: "'.$element.'",
                        resize: true,
                        gridTextSize:11,
                        xLabelAngle:'.$angle.',
                        xLabelMargin:0,
                        padding:20,                        
                        data: ['.$value.'],
                        barColors: ["#'.$colors.'"],
                        xkey: "label",
                        ykeys: ["value"],
                        labels: ["'.$labels.'"],
                        hideHover: "auto"
                    });
                  });
                </script>';

        return $output;
    }

    public function generate_chart_donut($period_id, $element, $value, $labels, $colors, $angle){

        $output ='<script>
                    $(function () {
                    "use strict"; 
                    var donut = new Morris.Donut({
                      element: "'.$element.'",
                      resize: true,
                      /*colors: ["#3c8dbc", "#f56954", "#00a65a"],*/
                      data: ['.$value.'],
                      hideHover: "auto"
                    });
                    });
                </script>';

        return $output;
    }


    public function generate_chart_line($period_id, $element, $value, $labels, $colors, $angle){

        $output ='<script>
                    $(function () {
                    "use strict";
                    var line = new Morris.Line({
                      element: "'.$element.'",
                      resize: true,
                      data: ['.$value.'],
                      xkey: "label",
                      ykeys: ["value"],
                      labels: ["Item 1"],
                      lineColors: ["#3c8dbc"],
                      hideHover: "auto"
                    });
                    });
                </script>';

        return $output;
    }



    public function callback_value_chart_1($period_id){


        $SQL    = "SELECT SUM(CASE WHEN Age_of_employee <= 30 THEN 1 ELSE 0 END) AS 'AA',
                   SUM(CASE WHEN (Age_of_employee >= 31 AND Age_of_employee <=35) THEN 1 ELSE 0 END) AS 'BB',
                   SUM(CASE WHEN (Age_of_employee >= 36 AND Age_of_employee <=40) THEN 1 ELSE 0 END) AS 'CC',
                   SUM(CASE WHEN (Age_of_employee >= 41 AND Age_of_employee <=45) THEN 1 ELSE 0 END) AS 'DD',
                   SUM(CASE WHEN (Age_of_employee >= 46 AND Age_of_employee <=50) THEN 1 ELSE 0 END) AS 'EE',
                   SUM(CASE WHEN (Age_of_employee > 50) THEN 1 ELSE 0 END) AS 'FF' FROM (SELECT COALESCE(a.EmployeeID,b.EmployeeID) AS Gabung,b.APP AS APP
                FROM tp_diagram_frame_detail AS a 
                RIGHT JOIN tp_diagram_frame AS b ON a.ParentID=b.COID 
                WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 AND b.APP ".$this->where_business.") AS h INNER JOIN tp_profile j ON h.Gabung=j.Prev_Per_No";
        
       
        $query  = $this->db->query($SQL);

        $query = $query->result_array();

        $chart_value = '';        

        foreach($query as $data){
            $chart_value .= '{label: "<= 30", value: '.$data['AA'].'},';
            $chart_value .= '{label: "31-35", value: '.$data['BB'].'},';
            $chart_value .= '{label: "36-40", value: '.$data['CC'].'},';
            $chart_value .= '{label: "41-45", value: '.$data['DD'].'},';
            $chart_value .= '{label: "46-50", value: '.$data['EE'].'},';
            $chart_value .= '{label: "> 50", value: '.$data['FF'].'},';
        }

        return $chart_value;
    }


    public function callback_value_chart_2($period_id){

        $SQL    = "SELECT COUNT(Prev_Per_No) AS Total,Pendidikan_Terakhir FROM (SELECT COALESCE(a.EmployeeID,b.EmployeeID) AS Gabung,b.APP AS APP
                FROM ".$this->table_diagram_frame_detail." AS a 
                RIGHT JOIN ".$this->table_diagram_frame." AS b ON a.ParentID=b.COID 
                WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 AND b.APP ".$this->where_business.") AS h INNER JOIN tp_profile j ON h.Gabung=j.Prev_Per_No GROUP BY j.Pendidikan_Terakhir";
        

        $query  = $this->db->query($SQL);        

        $chart_value = '';        

        foreach($query->result() as $data){
            
            $chart_value .= '{label: "'.$data->Pendidikan_Terakhir.'", value: '.$data->Total.'},';

        }

        return $chart_value;
    }    


    public function callback_value_chart_3($period_id){

        $SQL    = "SELECT COUNT(Prev_Per_No) AS Total,Gender_Key FROM (SELECT COALESCE(a.EmployeeID,b.EmployeeID) AS Gabung,b.APP AS APP
                FROM ".$this->table_diagram_frame_detail." AS a 
                RIGHT JOIN ".$this->table_diagram_frame." AS b ON a.ParentID=b.COID 
                WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 AND b.APP ".$this->where_business.") AS h INNER JOIN tp_profile j ON h.Gabung=j.Prev_Per_No GROUP BY j.Gender_Key";

        $query  = $this->db->query($SQL);        

        $chart_value = '';        

        foreach($query->result() as $data){
            
            $chart_value .= '{label: "'.$data->Gender_Key.'", value: '.$data->Total.'},';

        }

        return $chart_value;
    }


    public function callback_value_chart_4($period_id){

        $SQL    = "SELECT COUNT(Prev_Per_No) AS Total,LEFT(PS_group,3) AS PS_group FROM (SELECT COALESCE(a.EmployeeID,b.EmployeeID) AS Gabung,b.APP AS APP
                FROM ".$this->table_diagram_frame_detail." AS a 
                RIGHT JOIN ".$this->table_diagram_frame." AS b ON a.ParentID=b.COID 
                WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 AND b.APP ".$this->where_business.") AS h INNER JOIN tp_profile j ON h.Gabung=j.Prev_Per_No GROUP BY substr(j.PS_group,1,3)";


        $query  = $this->db->query($SQL);        

        $chart_value = '';        

        foreach($query->result() as $data){
            
            $chart_value .= '{label: "'.$data->PS_group.'", value: '.$data->Total.'},';

        }

        return $chart_value;
    }


    public function callback_value_chart_5($period_id){


        $SQL    = "SELECT COUNT(Prev_Per_No) AS Total,Jenjang_Sub_Grp_Text FROM (SELECT COALESCE(a.EmployeeID,b.EmployeeID) AS Gabung,b.APP AS APP
                FROM ".$this->table_diagram_frame_detail." AS a 
                RIGHT JOIN ".$this->table_diagram_frame." AS b ON a.ParentID=b.COID 
                WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 AND b.APP ".$this->where_business.") AS h INNER JOIN tp_profile j ON h.Gabung=j.Prev_Per_No GROUP BY j.Jenjang_Sub_Grp_Text";


        $query  = $this->db->query($SQL);        

        $chart_value = '';        

        foreach($query->result() as $data){
            
            $chart_value .= '{label: "'.$data->Jenjang_Sub_Grp_Text.'", value: '.$data->Total.'},';

        }

        return $chart_value;
    }

    public function callback_value_chart_6($period_id){


        $SQL    = "SELECT APP AS Business_Area,count(COALESCE(a.EmployeeID,b.EmployeeID)) AS Total
                FROM ".$this->table_diagram_frame_detail." AS a 
                RIGHT JOIN ".$this->table_diagram_frame." AS b ON a.ParentID=b.COID 
                WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 GROUP BY b.APP";


        $query  = $this->db->query($SQL);        

        $chart_value = '';        

        foreach($query->result() as $data){
            
            $chart_value .= '{label: "'.$this->data_table_value('mst_business_area', 'business_area_id  ', 'description', $data->Business_Area).'", value: '.$data->Total.'},';

        }

        return $chart_value;
    }



    public function callback_value_chart_2222($period_id){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }

    public function callback_value_chart_333(){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }

    public function callback_value_chart_444(){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }

    public function callback_value_chart_555(){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }


    public function callback_value_chart_666(){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }

    public function callback_value_chart_7(){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }


    public function data_filter_period($session_nik, $period_id=NULL, $modules=NULL, $pages=NULL){
      
      $empty_select = '<form class="form-horizontal" role="form">';
      //$empty_select .='<div class="col-sm-3">';
      $empty_select .='<div class="form-group">';

      $empty_select .='<label class="col-sm-2 control-label">{{ language:Select saved period }}</label>';
      $empty_select .='<div class="col-sm-3">';

      //$empty_select .='</div>';
      //$empty_select .='<div class="form-group">';
      $empty_select .='<select data-live-search="true" style="width: 100%; " class="selectpicker form-control show-tick" data-show-subtext="true" data-container="body" onchange=goToPage("select") name="select" id="select">';
      $empty_select .='<option value="'.site_url($modules.'/'.$pages).'" SELECTED>{{ language:All Period }}</option>';
      //$empty_select .='<option value="'.site_url($modules.'/'.$pages.'/index/0').'">---ALL---</option>';
      $empty_select_closed = '</select></div></div></form><br/>';

        $java_script = '<script type = "text/javascript">
            function goToPage(id) {
              var node = document.getElementById(id);  
              if( node &&
                node.tagName == "SELECT" ) {
                window.location.href = node.options[node.selectedIndex].value;    
              }  
            }
            </script>';



            $query = $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
               ->from('mst_period')
               ->where('PStatus', 1)
               ->order_by('PStartDate','ASC')
               ->get();

            foreach($query->result() as $data){

              if($this->period_id == $data->PeriodID){  
                  $empty_select .='<option value="'.site_url($modules.'/'.$pages.'/index/'.$data->PeriodID).'" data-subtext="" SELECTED>'.$data->PeriodName.'</option>';
              }
              else {
                  $empty_select .='<option value="'.site_url($modules.'/'.$pages.'/index/'.$data->PeriodID).'" data-subtext="">'.$data->PeriodName.'</option>';
              }
            }

          

      return $empty_select.$empty_select_closed.$java_script;
    }

    public function data_table_value($table_name, $where_column, $result_column, $value){

        $this->db->select($result_column)
                 ->from($table_name)
                 ->where($where_column, $value);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            return $data->$result_column;
        }
        else{
            return '';
        }
    } 


}