<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class chart_model extends CMS_Model{

    public $period_id = '';
    public $period_name = '';
    public $start_date = '';
    public $end_date = '';



	public function get_statistic_data($primary_key){

        $this->db->select('description')
                 ->from($this->cms_complete_table_name('mst_statistic_type'))
                 ->where('statistic_type_id', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            return $this->cms_lang($data->description);
        } 
        else{
            return '';
        }
    }


    public function list_chart_statictic($period_id){

        $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PeriodID', $period_id);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            $this->period_id = $data->PeriodID;
            $this->period_name = $data->PeriodName;
            $this->start_date = $data->PStartDate;
            $this->end_date = $data->PEndDate;
        }



        $query = $this->db->select('statistic_type_id,description,bar_color,type,angle')
            ->from('mst_statistic_type')
            //->join($this->cms_complete_table_name('mst_statistic_type'), 'statistic_type_id=graph_id')
            //->where('employee_id', $this->cms_user_id())
            //->where('st_user_graph.status', 1)
            ->where('mst_statistic_type.status', 1)
            ->order_by('statistic_type_id', 'ASC')
            ->get();

        $chart = '';
        $no=1;
        
        foreach($query->result() as $row){

            $element = 'chart_'.$row->statistic_type_id;
            $callback_text    = 'callback_value_chart_'.$row->statistic_type_id;

            $value   = $this->$callback_text($period_id);


            $chart .= '<div class="col-md-6">';
         
          	$chart .= '<div class="box box-primary">';
            $chart .= '<div class="box-header with-border">';
            $chart .= '<h3 class="box-title">'.$no.'. '.$row->description.' '.$this->period_name.'</h3>';
            $chart .= '<div class="box-tools pull-right">';
            $chart .= '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>';
            $chart .= '<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>';
            $chart .= '</div>';
            $chart .= '</div>';
            $chart .= '<div class="box-body chart-responsive">';    

            $chart .= '<div class="chart" id="'.$element.'" ></div>';            
           
            $chart .= '</div>';           
          	$chart .= '</div>';        

        	$chart .= '</div>';

            if ($row->type =='bar'){
                $chart .= $this->generate_chart_single_vertically($period_id, $element, $value, $labels='{{ language:Employee }}', $row->bar_color, $row->angle);
            }
            else if($row->type =='line'){
                $chart .= $this->generate_chart_line($period_id, $element, $value, $labels='{{ language:Employee }}', $row->bar_color, $row->angle);
            }
            else if($row->type =='donut'){
                $chart .= $this->generate_chart_donut($period_id, $element, $value, $labels='{{ language:Employee }}', $row->bar_color, $row->angle);
            }
            else{
                $chart .= $this->generate_chart_single_vertically($period_id, $element, $value, $labels='{{ language:Employee }}', $row->bar_color, $row->angle);
            }
        $no++;
           
        }

        return $chart;
        

    }


    public function generate_chart_single_vertically($period_id, $element, $value, $labels, $colors, $angle){

        $output ='<script>
                    $(function () {
                    "use strict";   
                        var bar = new Morris.Bar({
                        element: "'.$element.'",
                        resize: true,
                        gridTextSize:11,
                        xLabelAngle:'.$angle.',
                        xLabelMargin:0,
                        padding:20,                        
                        data: ['.$value.'],
                        barColors: ["#'.$colors.'"],
                        xkey: "label",
                        ykeys: ["value"],
                        labels: ["'.$labels.'"],
                        hideHover: "auto"
                    });
                  });
                </script>';

        return $output;
    }

    public function generate_chart_donut($period_id, $element, $value, $labels, $colors, $angle){

        $output ='<script>
                    $(function () {
                    "use strict"; 
                    var donut = new Morris.Donut({
                      element: "'.$element.'",
                      resize: true,
                      /*colors: ["#3c8dbc", "#f56954", "#00a65a"],*/
                      data: ['.$value.'],
                      hideHover: "auto"
                    });
                    });
                </script>';

        return $output;
    }


    public function generate_chart_line($period_id, $element, $value, $labels, $colors, $angle){

        $output ='<script>
                    $(function () {
                    "use strict";
                    var line = new Morris.Line({
                      element: "'.$element.'",
                      resize: true,
                      data: ['.$value.'],
                      xkey: "label",
                      ykeys: ["value"],
                      labels: ["Item 1"],
                      lineColors: ["#3c8dbc"],
                      hideHover: "auto"
                    });
                    });
                </script>';

        return $output;
    }



    public function callback_value_chart_1($period_id){


        $SQL    = "SELECT SUM(CASE WHEN Age_of_employee <= 30 THEN 1 ELSE 0 END) AS 'A',
                   SUM(CASE WHEN (Age_of_employee >= 31 AND Age_of_employee <=35) THEN 1 ELSE 0 END) AS 'B',
                   SUM(CASE WHEN (Age_of_employee >= 36 AND Age_of_employee <=40) THEN 1 ELSE 0 END) AS 'C',
                   SUM(CASE WHEN (Age_of_employee >= 41 AND Age_of_employee <=45) THEN 1 ELSE 0 END) AS 'D',
                   SUM(CASE WHEN (Age_of_employee >= 46 AND Age_of_employee <=50) THEN 1 ELSE 0 END) AS 'E',
                   SUM(CASE WHEN (Age_of_employee > 50) THEN 1 ELSE 0 END) AS 'F' 
                   FROM tp_profile";
        
       
        if (!is_null($this->period_id) && !empty($this->period_id)){
            $SQL    .= " WHERE CreatedTime <='".$this->end_date."'";
        }
       
        $query  = $this->db->query($SQL);

        $query = $query->result_array();

        $chart_value = '';        

        foreach($query as $data){
            $chart_value .= '{label: "<= 30", value: '.$data['A'].'},';
            $chart_value .= '{label: "31-35", value: '.$data['B'].'},';
            $chart_value .= '{label: "36-40", value: '.$data['C'].'},';
            $chart_value .= '{label: "41-45", value: '.$data['D'].'},';
            $chart_value .= '{label: "46-50", value: '.$data['D'].'},';
            $chart_value .= '{label: "> 50", value: '.$data['D'].'},';
        }

        return $chart_value;
    }


    public function callback_value_chart_2($period_id){


        $SQL    = "SELECT Pendidikan_Terakhir, count(Prev_Per_No) AS Total FROM tp_profile";

        if (!is_null($this->period_id) && !empty($this->period_id)){
            $SQL    .= " WHERE CreatedTime <='".$this->end_date."'";
        }
        $SQL    .= " GROUP BY Pendidikan_Terakhir";        

        $query  = $this->db->query($SQL);        

        $chart_value = '';        

        foreach($query->result() as $data){
            
            $chart_value .= '{label: "'.$data->Pendidikan_Terakhir.'", value: '.$data->Total.'},';

        }

        return $chart_value;
    }    


    public function callback_value_chart_3($period_id){

        $SQL    = "SELECT Gender_Key, count(Prev_Per_No) AS Total FROM tp_profile";

        if (!is_null($this->period_id) && !empty($this->period_id)){
            $SQL    .= " WHERE CreatedTime <='".$this->end_date."'";
        }
        $SQL    .= " GROUP BY Gender_Key";

        $query  = $this->db->query($SQL);        

        $chart_value = '';        

        foreach($query->result() as $data){
            
            $chart_value .= '{label: "'.$data->Gender_Key.'", value: '.$data->Total.'},';

        }

        return $chart_value;
    }


    public function callback_value_chart_4($period_id){


        $SQL    = "SELECT LEFT(PS_group,3) AS PS_group, count(Prev_Per_No) AS Total FROM tp_profile";

        if (!is_null($this->period_id) && !empty($this->period_id)){
            $SQL    .= " WHERE CreatedTime <='".$this->end_date."'";
        }
        $SQL    .= " GROUP BY substr(PS_group,1,3)";

        $query  = $this->db->query($SQL);        

        $chart_value = '';        

        foreach($query->result() as $data){
            
            $chart_value .= '{label: "'.$data->PS_group.'", value: '.$data->Total.'},';

        }

        return $chart_value;
    }


    public function callback_value_chart_5($period_id){


        $SQL    = "SELECT Jenjang_Sub_Grp_Text, count(Prev_Per_No) AS Total FROM tp_profile ";

        if (!is_null($this->period_id) && !empty($this->period_id)){
            $SQL    .= " WHERE CreatedTime <='".$this->end_date."'";
        }
        $SQL    .= " GROUP BY Jenjang_Sub_Grp_Text";

        $query  = $this->db->query($SQL);        

        $chart_value = '';        

        foreach($query->result() as $data){
            
            $chart_value .= '{label: "'.$data->Jenjang_Sub_Grp_Text.'", value: '.$data->Total.'},';

        }

        return $chart_value;
    }

    public function callback_value_chart_6($period_id){


        $SQL    = "SELECT Business_Area, count(Prev_Per_No) AS Total FROM tp_profile ";

        if (!is_null($this->period_id) && !empty($this->period_id)){
            $SQL    .= " WHERE CreatedTime <='".$this->end_date."'";
        }
        $SQL    .= " GROUP BY BusA";

        $query  = $this->db->query($SQL);        

        $chart_value = '';        

        foreach($query->result() as $data){
            
            $chart_value .= '{label: "'.$data->Business_Area.'", value: '.$data->Total.'},';

        }

        return $chart_value;
    }



    public function callback_value_chart_2222($period_id){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }

    public function callback_value_chart_333(){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }

    public function callback_value_chart_444(){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }

    public function callback_value_chart_555(){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }


    public function callback_value_chart_666(){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }

    public function callback_value_chart_7(){

        return '{y: "2006", a: 100},
                                {y: "2007", a: 75},
                                {y: "2008", a: 50},
                                {y: "2009", a: 75},
                                {y: "2010", a: 50},
                                {y: "2011", a: 75},
                                {y: "2012", a: 100}';


    }


    public function data_filter_period($session_nik, $period_id=NULL, $modules=NULL, $pages=NULL){
      
      $empty_select = '<form class="form-horizontal" role="form">';
      //$empty_select .='<div class="col-sm-3">';
      $empty_select .='<div class="form-group">';

      $empty_select .='<label class="col-sm-2 control-label">{{ language:Select saved period }}</label>';
      $empty_select .='<div class="col-sm-3">';

      //$empty_select .='</div>';
      //$empty_select .='<div class="form-group">';
      $empty_select .='<select data-live-search="true" style="width: 100%; " class="selectpicker form-control show-tick" data-show-subtext="true" data-container="body" onchange=goToPage("select") name="select" id="select">';
      $empty_select .='<option value="'.site_url($modules.'/'.$pages).'" SELECTED>{{ language:All Period }}</option>';
      //$empty_select .='<option value="'.site_url($modules.'/'.$pages.'/index/0').'">---ALL---</option>';
      $empty_select_closed = '</select></div></div></form><br/>';

        $java_script = '<script type = "text/javascript">
            function goToPage(id) {
              var node = document.getElementById(id);  
              if( node &&
                node.tagName == "SELECT" ) {
                window.location.href = node.options[node.selectedIndex].value;    
              }  
            }
            </script>';



            $query = $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
               ->from('mst_period')
               ->where('PStatus', 1)
               ->order_by('PStartDate','ASC')
               ->get();

            foreach($query->result() as $data){

              if($this->period_id == $data->PeriodID){  
                  $empty_select .='<option value="'.site_url($modules.'/'.$pages.'/index/'.$data->PeriodID).'" data-subtext="" SELECTED>'.$data->PeriodName.'</option>';
              }
              else {
                  $empty_select .='<option value="'.site_url($modules.'/'.$pages.'/index/'.$data->PeriodID).'" data-subtext="">'.$data->PeriodName.'</option>';
              }
            }

          

      return $empty_select.$empty_select_closed.$java_script;
    }


}