<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class chart_report extends CMS_Controller {

	protected $URL_MAP = array();

	public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    public function index(){

        $this->load->model('chart_model');

        $modules = $this->uri->segment(1);
        $pages   = $this->uri->segment(2);
        $period_id   = $this->uri->segment(4);

        $data['output'] = $this->chart_model->list_chart_statictic($period_id);
        $data['filter_period'] = $this->chart_model->data_filter_period($this->cms_user_id(), $period_id, $modules, $pages);
        
        $this->view($this->cms_module_path().'/chart_report_view', $data);        
    }

    public function setting(){

        //$this->cms_guard_page('setting');
        $crud = $this->new_crud();
        $crud->set_theme('no-flexigrid-chart-setting');
        $crud->unset_jquery();

        $crud->set_table('st_user_graph');
        $crud->where('employee_id', $this->cms_user_id() ,' group_by graph_id');       


        $crud->set_subject($this->cms_lang('Setting'));
        $crud->set_primary_key('user_graph_id');

        $crud->unique_fields('description');
        $crud->unset_read();
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_print();
        $crud->unset_export();

        //$crud->required_fields('description');

        $crud->columns('graph_id', 'status', 'No_', 'color_picker');
        $crud->edit_fields('graph_id', 'status', 'No_', 'color_picker','UpdatedTime','graph_value');
        $crud->add_fields('description', 'status');

        $crud->field_type('graph_id', 'readonly');

        $crud->set_relation('graph_id', $this->cms_complete_table_name('mst_statistic_type'), 'description');

        $crud->unset_edit_fields('UpdatedTime');
        $crud->field_type('UpdatedTime','hidden', date('Y-m-d'));


        $crud->display_as('graph_id', $this->cms_lang('Chart Name'))
            ->display_as('status', $this->cms_lang('Status'))
            ->display_as('color_picker', $this->cms_lang('Bar Color'))
            ->display_as('graph_value', $this->cms_lang('Configuration'));       

        $crud->callback_after_insert(array(
            $this,
            '_after_insert_setting'
        ));
        $crud->callback_after_update(array(
            $this,
            '_after_update_setting'
        ));
        $crud->callback_before_delete(array(
            $this,
            '_before_delete_setting'
        ));

        $crud->callback_field('graph_value',array($this, '_callback_field_graph_value'));
        $crud->callback_field('color_picker',array($this, '_callback_field_color_picker'));

        $crud->callback_column('color_picker',array($this,'_callback_column_color_picker'));

        $crud->set_language($this->cms_language());
        

        $output = $crud->render();

        $this->view($this->cms_module_path().'/chart_setting_view', $output,
            $this->cms_complete_navigation_name('chart_report'));
    }


    public function _callback_field_graph_value($value=NULL, $primary_key){

        $module_path = $this->cms_module_path();
        $this->config->load('grocery_crud');
        $date_format = $this->config->item('grocery_crud_date_format');

        if(!isset($primary_key)) $primary_key = -1;
        $query = $this->db->select('user_graph_id, graph_id, value_1, value_2')
            ->from($this->cms_complete_table_name('st_user_graph'))
            ->where('user_graph_id', $primary_key)
            ->where('employee_id', $this->cms_user_id())
            ->get();
        $result = $query->result_array();
            
        // get options
        $options = array();
        
        /*
        $options['EmployeeID'] = array();
        $query = $this->db->select('Prev_Per_No, Full_Name')
                          ->from($this->cms_complete_table_name('tp_profile'))
                          ->group_by('Prev_Per_No')
                          ->order_by('Full_Name', 'ASC')
                          ->get();
        foreach($query->result() as $row){
            $options['EmployeeID'][] = array('value' => $row->Prev_Per_No, 'caption' => $row->Full_Name);
        }
        */
        

        $data = array(
            'result' => $result,
            'options' => $options,
            'date_format' => $date_format,
            'primary_key' => $primary_key,
            'button_add_lang' => $this->cms_lang('Add Employee'),
        );

        $graph_id = $this->get_statistic_data($primary_key);       

        
        if ($graph_id ==1){
            return $this->load->view($this->cms_module_path().'/field_setting_graph_1', $data, TRUE);
        }
        else if ($graph_id ==2){
            return $this->load->view($this->cms_module_path().'/field_setting_graph_1', $data, TRUE);
        }
              


    }


    public function _before_insert_setting($post_array){
        $post_array = $this->_before_insert_or_update_setting($post_array);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_insert_setting($post_array, $primary_key){
        $success = $this->_after_insert_or_update_setting($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_update_setting($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update_setting($post_array, $primary_key);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_update_setting($post_array, $primary_key){
        $success = $this->_after_insert_or_update_setting($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_delete_setting($primary_key){
        return TRUE;
    }

    public function _after_delete_setting($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update_setting($post_array, $primary_key){

        $data = json_decode($this->input->post('md_real_field_citizen_col'), TRUE);
        $insert_records = $data['insert'];
        $update_records = $data['update'];
        $delete_records = $data['delete'];
        $real_column_names = array('value_1', 'value_2');
        $set_column_names = array();


        foreach($update_records as $update_record){
            $detail_primary_key = $update_record['primary_key'];
            $data = array();
            foreach($update_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            
            //$data['employee_id'] = $primary_key;
            $this->db->update($this->cms_complete_table_name('st_user_graph'),
                 $data, array('user_graph_id'=> $detail_primary_key));
            
        }

        return TRUE;
    }

    public function _before_insert_or_update_setting($post_array, $primary_key=NULL){
        return $post_array;
    }

    public function _callback_field_color_picker($value=NULL, $primary_key){        

        return '<input type="text" id="color_picker" name="color_picker" value="'.$value.'" class="form-control color">';
        
    }

    public function _callback_column_color_picker($value, $row){
        
        return '<small class="label" style="background-color:#'.$value.'">'.$value.'</small>';
    } 

    public function get_statistic_data($primary_key){

        $this->db->select('graph_id')
                 ->from('st_user_graph')
                 ->where('user_graph_id', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            return $data->graph_id;
        } 
        else{
            return '';
        }

    }


}