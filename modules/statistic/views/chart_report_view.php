<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<?php $crud = new chart_report(); ?>        
    
<div id="search_block" class="panel-body">
    <form role="form" class="form-horizontal" method="get" action="">   
        <div class="form-group">
            <label class="col-xs-10 col-sm-2 col-md-1 control-label" for="filters">{{ language:Period }}:</label>
            <div class="col-xs-10 col-sm-8 col-md-2" id="filters">
                <select data-live-search="true" class="selectpicker form-control show-tick" data-show-subtext="true" data-container="body"  name="period" id="period">
                    <?php echo $crud->select_option_period();?>
                </select>
            </div>
            <label class="col-xs-10 col-sm-2 col-md-1 control-label" for="filters">{{ language:Business Area }}:</label>
            <div class="col-xs-10 col-xs-offset-0 col-sm-8 col-sm-offset-2 col-md-2 col-md-offset-0">
                <select data-live-search="true" class="selectpicker form-control show-tick" data-show-subtext="true" data-container="body" name="business" id="business">                      
                    <?php echo $crud->select_option_business_unit();?>
                </select>
            </div>
            <div class="button-group col-xs-10 col-xs-offset-0 col-sm-8 col-sm-offset-2 col-md-3 col-md-offset-0">
                <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-search"></i> {{ language:Show }}</button>
            </div>
        </div>
    </form>
</div>

<div class="row">
  <?php echo $output; ?>
</div>

<script type="text/javascript">
    $('.selectpicker').selectpicker({
      style: 'btn-default',size: "auto"
    });
</script>