<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class HCR_OCR extends CMS_Priv_Strict_Controller {

    protected $URL_MAP = array();

    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    private function make_crud(){
       
        $crud = $this->new_crud();
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        $this->load->config('grocery_crud');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types','gif|jpeg|jpg|png');

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        //$crud->unset_add();
        // $crud->unset_edit();
        // $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        // $crud->unset_export();

        $crud->set_language($this->cms_language());
        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->set_theme('datatables-default');
            $crud->set_relation('Position_ID', $this->cms_complete_table_name('mst_position_ladder'), 'position_name');
            $crud->set_relation('Unit_ID', $this->cms_complete_table_name('mst_organizational_unit'), 'description');
        }
        else{
            $crud->set_theme('no-flexigrid-hcr-ocr');
        }

    
        $crud->set_table($this->cms_complete_table_name('tp_raw_data'));

        $crud->set_primary_key('Pers_No');
        $crud->set_subject($this->cms_lang('Personal info'));

        $crud->columns('Pers_No','Full_Name','Position_ID','Unit_ID','Date_IN','Employee_Group');
        $crud->edit_fields('Photos','Pers_No','Full_Name','Position_ID','Unit_ID','Date_IN','evidence');
        $crud->add_fields('Pers_No','Full_Name','Position_ID','Unit_ID','Date_IN','Employee_Group');

        $crud->display_as('Pers_No', $this->cms_lang('ID'));
        $crud->display_as('description', $this->cms_lang('Description'));
     
        $crud->required_fields('Pers_No','Full_Name','Position_ID','Unit_ID','Date_IN');
        $crud->unique_fields('Pers_No');

        $crud->set_field_upload('Photos','assets/uploads');
        //$crud->set_field_avatar('Photos','assets/uploads');

        // /$crud->field_type('Employee_Group','dropdown', array('Aktif' => 'Aktif', 'Tidak Aktif' => 'Tidak Aktif','3' => 'spam' , '4' => 'deleted'));

        //$crud->change_field_type('Employee_Group', 'true_false', array('Tidak Aktif', 'Aktif'));
       
     
        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        $crud->callback_field('Position_ID',array($this, '_callback_field_Position_ID'));
        $crud->callback_field('Unit_ID',array($this, '_callback_field_Unit_ID'));


        $crud->callback_field('evidence',array($this, '_callback_field_evidence'));

        $this->crud = $crud;
        return $crud;
    }

    public function index(){
        $crud = $this->make_crud();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view($this->cms_module_path().'/HCR_OCR_view', $output,
            $this->cms_complete_navigation_name('HCR_OCR'));
    }

    public function delete_selection(){
        $crud = $this->make_crud();
        if(!$crud->unset_delete){
            $id_list = json_decode($this->input->post('data'));
            foreach($id_list as $id){
                if($this->_before_delete($id)){
                    $this->db->delete($this->cms_complete_table_name('mst_business_area'),array('business_area_id'=>$id));
                    $this->_after_delete($id);
                }
            }
        }
    }

    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        return $success;
    }

    public function _before_delete($primary_key){
        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){


        $data = json_decode($this->input->post('md_real_field_photos_col'), TRUE);
        $insert_records = $data['insert'];
        $update_records = $data['update'];
        $delete_records = $data['delete'];
        $real_column_names = array('evidence_id', 'evidence_url');
        $set_column_names = array();
        $many_to_many_column_names = array();
        $many_to_many_relation_tables = array();
        $many_to_many_relation_table_columns = array();
        $many_to_many_relation_selection_columns = array();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  DELETED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($delete_records as $delete_record){
            $detail_primary_key = $delete_record['primary_key'];
            $this->db->delete($this->cms_complete_table_name('tp_raw_evidence'),
                 array('evidence_id'=> $detail_primary_key));
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  INSERTED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($insert_records as $insert_record){
            $this->load->library('image_moo');
            $upload_path = FCPATH.'modules/'.$this->cms_module_path().'/assets/uploads/';

            $record_index = $insert_record['record_index'];
            $tmp_name = $_FILES['md_field_photos_col_evidence_url_'.$record_index]['tmp_name'];
            $file_name = $_FILES['md_field_photos_col_evidence_url_'.$record_index]['name'];
            $file_name = $this->randomize_string($file_name).$file_name;
            move_uploaded_file($tmp_name, $upload_path.$file_name);
            $data = array(
                'evidence_url' => $file_name,
            );
            $data['Pers_No'] = $primary_key;
            $this->db->insert($this->cms_complete_table_name('tp_raw_evidence'), $data);

            $thumbnail_name = 'thumb_'.$file_name;
            $this->image_moo->load($upload_path.$file_name)->resize(800,75)->save($upload_path.$thumbnail_name,true);
        }

        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }

    public function _callback_field_Position_ID($value, $primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select name="Position_ID" id="Position_ID" data-header="Select Location" data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%">';
        $empty_select_closed = '</select>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();

       
        if(isset($listingID) && $state == "edit"){

            $this->db->select('*')
                     ->from('tp_raw_data')
                     ->where('Pers_No', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);                           
    
            $this->db->select('*')
                     ->from('mst_position_ladder')
                     ->order_by('position_name','ASC');                        
            $db2 = $this->db->get();            

            foreach($db2->result() as $row):
                if($row->position_id == $data->Position_ID) {
                    $empty_select .= '<option value="'.$row->position_id.'" data-subtext="" selected="selected">'.$row->position_name.'</option>';
                } else {
                    $empty_select .= '<option value="'.$row->position_id.'" data-subtext="">'.$row->position_name.'</option>';
                }
            endforeach;                           
            
            return $empty_select.$empty_select_closed;

        } else {
            $this->db->select('*')
                         ->from('mst_position_ladder')
                         ->order_by('position_id','ASC');                        
            $db2 = $this->db->get();            

            foreach($db2->result() as $row):                   
                $empty_select .= '<option value="'.$row->position_id.'" data-subtext="">'.$row->position_name.'</option>';                    
            endforeach;

            return $empty_select.$empty_select_closed;  
        }
    }


    public function _callback_field_Unit_ID($value, $primary_key){

        //CREATE THE EMPTY SELECT STRING
        $empty_select = '<select name="Unit_ID" id="Unit_ID" data-header="Select Unit" data-live-search="true" class="selectpicker form-control" data-show-subtext="true" data-container="body" data-width="100%">';
        $empty_select_closed = '</select>';

        $listingID = $this->uri->segment(5);       
              
        $crud = new grocery_CRUD();
        $state = $crud->getState();

       
        if(isset($listingID) && $state == "edit"){

            $this->db->select('*')
                     ->from('tp_raw_data')
                     ->where('Pers_No', $primary_key);
            $db = $this->db->get();
            $data = $db->row(0);                           
    
            $this->db->select('*')
                     ->from('mst_organizational_unit')
                     ->order_by('description','ASC');                        
            $db2 = $this->db->get();            

            foreach($db2->result() as $row):
                if($row->unit_id == $data->Unit_ID) {
                    $empty_select .= '<option value="'.$row->unit_id.'" data-subtext="" selected="selected">'.$row->description.'</option>';
                } else {
                    $empty_select .= '<option value="'.$row->unit_id.'" data-subtext="">'.$row->description.'</option>';
                }
            endforeach;                           
            
            return $empty_select.$empty_select_closed;

        } else {
            $this->db->select('*')
                         ->from('mst_organizational_unit')
                         ->order_by('description','ASC');                        
            $db2 = $this->db->get();            

            foreach($db2->result() as $row):                   
                $empty_select .= '<option value="'.$row->unit_id.'" data-subtext="">'.$row->description.'</option>';                    
            endforeach;

            return $empty_select.$empty_select_closed;  
        }
    }

    // returned on insert and edit
    public function _callback_field_evidence($value, $primary_key){
        $module_path = $this->cms_module_path();
        $this->config->load('grocery_crud');
        $date_format = $this->config->item('grocery_crud_date_format');

        if(!isset($primary_key)) $primary_key = -1;
        $query = $this->db->select('*')
            ->from($this->cms_complete_table_name('tp_raw_evidence'))
            ->where('Pers_No', $primary_key)
            ->get();
        $result = $query->result_array();

        // get options
        $options = array();
        $data = array(
            'result' => $result,
            'options' => $options,
            'date_format' => $date_format,
            'module_path' => $this->cms_module_path(),
        );
        return $this->load->view($this->cms_module_path().'/field_evidence_file',$data, TRUE);
    }

    private function randomize_string($value){
        $time = date('Y:m:d H:i:s');
        return substr(md5($value.$time),0,6);
    }

}