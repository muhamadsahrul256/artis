<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class raw_data_management extends CMS_Priv_Strict_Controller {

    protected $URL_MAP = array();

    public $navigation_name = '';
    public $allowed_types = '';
    public $hari_ini = '';

    public function navigation_name($value){

        $navigation_name = $this->uri->segment(3);

        if ($value == $navigation_name){
            return 'active';
        }
        else{
            return '';
        }

        $this->subject_title = $navigation_name;
    }

    public function cms_nav_name(){

        $navigation_name = $this->uri->segment(3);

        $this->subject_title = $navigation_name;

        return $navigation_name;
    }

    public function set_today(){

        $time = date('Y-m-d H:i:s');
        $this->hari_ini = $time;

    }

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->helper('cookie');
    }

    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    private function randomize_string($value){
        $time = date('Y:m:d H:i:s');
        return substr(md5($value.$time),0,9);
    }

    private function make_crud(){
        $this->today = date('Y-m-d H:i:s');
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // initialize groceryCRUD
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $crud = $this->new_crud();
        // this is just for code completion
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        $crud->unset_texteditor('Street_and_House_Number');
        $crud->unset_texteditor('Birthplace');

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        //$crud->unset_add();

        if ($this->cms_have_privilege('edit') != 1) $crud->unset_edit();
        if ($this->cms_have_privilege('delete') != 1) $crud->unset_delete();
        if ($this->cms_have_privilege('export') != 1) $crud->unset_export();
        if ($this->cms_have_privilege('add') != 1) $crud->unset_add();
        //$crud->unset_edit();
        // $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        // $crud->unset_export();
        $crud->set_theme('no-flexigrid-raw-data-management');

        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            //$crud->set_theme('no-flexigrid-raw-data-management');
            $crud->set_relation('Position_ID', 'mst_position_ladder', '{position_name}');
        }
        else{
            //$crud->set_theme('no-flexigrid-raw-data-management');
            $crud->set_relation('Position_ID', 'mst_position_ladder', '{position_id}. {position_name}');
        }

        $key_words = $this->input->get('q');

        $crud->set_language($this->cms_language());

        // table name
        $crud->set_table('tp_profile');
        $year = date('Y');
        $crud->columns('*','{{$year}} - YEAR(Start_Date) as long_position');
        //$crud->where("TP_ID > 0 AND Full_Name LIKE '%".$key_words."%'");



        if (!isset($key_words)){

        }

        else{
            $crud->like('Full_Name', $key_words ,'both');
            $crud->or_like('Prev_Per_No', $key_words ,'both');
            //$crud->where("TP_ID > 0 AND Full_Name LIKE '%".$key_words."%'");

        }

        /*
        $cookie = array (
            'name' => 'ajax_list',
            'value' => '',
            'expire' => '0',
            'domain' => site_url($this->cms_module_path() . '/import_raw_data'),
            'prefix' => 'ajax_list'
            );

        delete_cookie($cookie);
        */



        // primary key
        $crud->set_primary_key('Prev_Per_No');

        // set subject
        $crud->set_subject($this->cms_lang('Data Management'));

        // displayed columns on list
        $crud->columns('Full_Name','Prev_Per_No','Position_ID','Start_Date','PS_group','Jenjang_Main_Grp_Text','Business_Area','Tanggal_Pegawai_Tetap','Birth_date','Street_and_House_Number');
        $crud->order_by('Full_Name','ASC');
        // displayed columns on edit operation
        $crud->edit_fields('Prev_Per_No','Full_Name','Org_unit','Birth_date','Position_ID','Nama_Panjang_Posisi','Organizational_Unit','Start_Date','Jenjang_Main_Grp_Text','Tanggal_Masuk','Tanggal_Pegawai_Tetap','Birth_date','Email','Street_and_House_Number','Telephone_No','UpdatedBy');
        // displayed columns on add operation
        $crud->add_fields('Pers_No','Prev_Per_No','Full_Name','Org_unit','Position_ID','Position_Name','BusA','Start_Date','PS_group','Jenjang_Main_Grp_Text','Tanggal_Masuk','Tanggal_CAPEG','Tanggal_Pegawai_Tetap','Birth_date','Employee_Group','Email','Street_and_House_Number','City','Postal_code','Telephone_No','Birthplace','Pendidikan_Terakhir','Age_of_employee','CreatedBy','UpdatedBy','CreatedTime');

        $crud->set_config_upload('jpg|jpeg', '1MB');

        $crud->set_field_upload('Photos','modules/talent_pool/assets/uploads');

        // caption of each columns
        $crud->display_as('Prev_Per_No', $this->cms_lang('Employee ID'));
        $crud->display_as('Start_Date',$this->cms_lang('Position Time'));
        $crud->display_as('Full_Name', $this->cms_lang('Name'));
        $crud->display_as('Position_ID', $this->cms_lang('Position ID'));
        $crud->display_as('Position_Name', $this->cms_lang('Position Name'));
        $crud->display_as('Last_Updated', $this->cms_lang('Last Updated'));
        $crud->display_as('diklat', $this->cms_lang('Training'));
        $crud->display_as('Education', $this->cms_lang('Education'));
        $crud->display_as('Evidence', $this->cms_lang('Evidence'));
        $crud->display_as('Birth_Date', $this->cms_lang('Birth Date'));
        $crud->display_as('Basic_Salary_Scale', $this->cms_lang('Salary Scale'));
        $crud->display_as('Photos', $this->cms_lang('Photos'));
        $crud->display_as('Pers_No', $this->cms_lang('ID'));

        $crud->required_fields('Pers_No','Prev_Per_No','Full_Name','Birth_date','Position_ID');
        $crud->unique_fields('Pers_No');

        $crud->set_relation('Org_unit', 'mst_organizational_unit', '{unit_id}. {description}');
        $crud->set_relation('BusA', 'mst_business_area', '{business_area_id}. {description}');

        $crud->field_type('Jenjang_Main_Grp_Text','enum', array('Fungsional', 'Struktural'));


        $crud->unset_add_fields('CreatedBy','CreatedTime','UpdatedBy');
        $crud->unset_edit_fields('UpdatedBy');
        $crud->field_type('CreatedBy', 'hidden', $this->cms_user_id());
        $crud->field_type('CreatedTime', 'hidden', $this->today);
        $crud->field_type('UpdatedBy', 'hidden', $this->cms_user_id());


        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        $crud->callback_field('diklat',array($this, '_callback_field_diklat'));
        $crud->callback_field('Education',array($this, '_callback_field_Education'));
        $crud->callback_field('Evidence',array($this, '_callback_field_Evidence'));
        $crud->callback_field('Occupation',array($this, '_callback_field_Occupation'));
        $crud->callback_field('Talent',array($this, '_callback_field_Talent'));

        $crud->callback_column('Start_Date',array($this,'_callback_column_start_date'));
        /*
        $crud->set_tabs(array(
                $this->cms_lang('Profile')  => 16,
                $this->cms_lang('Occupation History') => 1,
                $this->cms_lang('Training History') => 1,
                $this->cms_lang('Education History') => 1,
                $this->cms_lang('Talent History') => 1,
                $this->cms_lang('Evidence') => 1,

             ));
        */

        $this->crud = $crud;
        return $crud;
    }

    public function index(){


        $crud = $this->make_crud();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view($this->cms_module_path().'/raw_data_management_view', $output,
            $this->cms_complete_navigation_name('raw_data_management'));
    }

    public function delete_selection(){
        $crud = $this->make_crud();
        if(!$crud->unset_delete){
            $id_list = json_decode($this->input->post('data'));
            foreach($id_list as $id){
                if($this->_before_delete($id)){
                    $this->db->delete('tp_profile',array('Prev_Per_No'=>$id));
                    $this->_after_delete($id);
                }
            }
        }
    }

    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_delete($primary_key){

        $this->db->delete('tp_profile_diklat',  array('EmployeeID'=> $primary_key));
        $this->db->delete('tp_profile_education',  array('EmployeeID'=> $primary_key));
        $this->db->delete('tp_profile_evidence',  array('EvidenceID'=> $primary_key));

        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){

        /* Post data Diklat atau Pelatihan Detail*/

        $data = json_decode($this->input->post('md_real_field_citizen_col'), TRUE);
        $insert_records = $data['insert'];
        $update_records = $data['update'];
        $delete_records = $data['delete'];
        $real_column_names = array('DiklatID', 'nama_diklat', 'kode_diklat', 'nilai_kelulusan', 'tanggal_mulai', 'tanggal_akhir','ranking','grade','no_sertifikat','tanggal_sertifikat','udiklat_penyelenggara', 'keterangan');
        $set_column_names = array();

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  DELETED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($delete_records as $delete_record){
            $detail_primary_key = $delete_record['primary_key'];
            // delete many to many

            $this->db->delete('tp_profile_diklat',
                 array('DiklatID'=> $detail_primary_key));
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  UPDATED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($update_records as $update_record){
            $detail_primary_key = $update_record['primary_key'];
            $data = array();
            foreach($update_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['EmployeeID'] = $primary_key;
            $this->db->update('tp_profile_diklat',
                 $data, array('DiklatID'=> $detail_primary_key));

        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  INSERTED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($insert_records as $insert_record){
            $data = array();
            foreach($insert_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['EmployeeID'] = $primary_key;
            $this->db->insert('tp_profile_diklat', $data);
            $detail_primary_key = $this->db->insert_id();

        }


        /* Post data Education Detail*/
        $data = json_decode($this->input->post('md_real_field_education_col'), TRUE);
        $insert_records = $data['insert'];
        $update_records = $data['update'];
        $delete_records = $data['delete'];
        $real_column_names = array('EducationID', 'Educational_establishment', 'Institute_location', 'Start_Date', 'End_Date', 'Kode_Keterangan_Pendidikan_Text');
        $set_column_names = array();

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  DELETED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($delete_records as $delete_record){
            $detail_primary_key = $delete_record['primary_key'];
            // delete many to many

            $this->db->delete('tp_profile_education',
                 array('EducationID'=> $detail_primary_key));
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  UPDATED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($update_records as $update_record){
            $detail_primary_key = $update_record['primary_key'];
            $data = array();
            foreach($update_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['EducationID'] = $primary_key;
            $this->db->update('tp_profile_education',
                 $data, array('EducationID'=> $detail_primary_key));

        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  INSERTED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($insert_records as $insert_record){
            $data = array();
            foreach($insert_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['EmployeeID'] = $primary_key;
            $this->db->insert('tp_profile_education', $data);
            $detail_primary_key = $this->db->insert_id();

        }




        /* Post data Jabatan Detail*/

        $data = json_decode($this->input->post('md_real_field_occupation_col'), TRUE);
        $insert_records = $data['insert'];
        $update_records = $data['update'];
        $delete_records = $data['delete'];
        $real_column_names = array('OccupationID', 'PositionID', 'UnitID', 'Description', 'StartDate', 'EndDate');
        $set_column_names = array();

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  DELETED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($delete_records as $delete_record){
            $detail_primary_key = $delete_record['primary_key'];
            // delete many to many

            $this->db->delete('tp_profile_occupation',
                 array('OccupationID'=> $detail_primary_key));
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  UPDATED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($update_records as $update_record){
            $detail_primary_key = $update_record['primary_key'];
            $data = array();
            foreach($update_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['EmployeeID'] = $primary_key;
            $this->db->update('tp_profile_occupation',
                 $data, array('OccupationID'=> $detail_primary_key));

        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  INSERTED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($insert_records as $insert_record){
            $data = array();
            foreach($insert_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['EmployeeID'] = $primary_key;
            $this->db->insert('tp_profile_occupation', $data);
            $detail_primary_key = $this->db->insert_id();

        }



        /* Post data Talenta Detail*/

        $data = json_decode($this->input->post('md_real_field_talent_col'), TRUE);
        $insert_records = $data['insert'];
        $update_records = $data['update'];
        $delete_records = $data['delete'];
        $real_column_names = array('TalentID', 'SemesterID', 'Talent_Value', 'Last_Talent_Date');
        $set_column_names = array();

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  DELETED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($delete_records as $delete_record){
            $detail_primary_key = $delete_record['primary_key'];
            // delete many to many

            $this->db->delete('tp_profile_talent',
                 array('TalentID'=> $detail_primary_key));
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  UPDATED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($update_records as $update_record){
            $detail_primary_key = $update_record['primary_key'];
            $data = array();
            foreach($update_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['EmployeeID'] = $primary_key;
            $this->db->update('tp_profile_talent',
                 $data, array('TalentID'=> $detail_primary_key));

        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  INSERTED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($insert_records as $insert_record){
            $data = array();
            foreach($insert_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['EmployeeID'] = $primary_key;
            $this->db->insert('tp_profile_talent', $data);
            $detail_primary_key = $this->db->insert_id();

        }




        // Post data Evidence */
        $data = json_decode($this->input->post('md_real_field_photos_col'), TRUE);
        $insert_records = $data['insert'];
        $update_records = $data['update'];
        $delete_records = $data['delete'];
        $real_column_names = array('EvidenceID', 'EvidenceName', 'EvidenceUrl');
        $set_column_names = array();

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  DELETED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($delete_records as $delete_record){
            $detail_primary_key = $delete_record['primary_key'];
            $this->db->delete('tp_profile_evidence',
                 array('EvidenceID'=> $detail_primary_key));
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  INSERTED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        foreach($insert_records as $insert_record){

            // Size must under 15 KB
            $MAXIMUM_FILESIZE = 1000 * 1000;

            $upload_path  = FCPATH.'modules/'.$this->cms_module_path().'/assets/uploads/';
            $record_index = $insert_record['record_index'];
            $rEFileTypes  = "/^\.(jpg|jpeg){1}$/i";

            $safe_filename = preg_replace(
                     array("/\s+/", "/[^-\.\w]+/"),
                     array("_", " ","","-"),
                     trim($_FILES['md_field_photos_col_EvidenceUrl_'.$record_index]['name']));

            $fsize          = $_FILES['md_field_photos_col_EvidenceUrl_'.$record_index]['size'];
            $tmp_name       = $_FILES['md_field_photos_col_EvidenceUrl_'.$record_index]['tmp_name'];
            $file_name      = $_FILES['md_field_photos_col_EvidenceUrl_'.$record_index]['name'];
            $file_name      = $this->randomize_string($file_name).$safe_filename;
            $file_code      = $_FILES['md_field_photos_col_EvidenceUrl_'.$record_index]['name'];
            $file_server    = $_SERVER['SERVER_NAME'];

            $data = array(
                'EvidenceUrl' => $file_name,
                'EvidenceName' => $file_code,
            );

            $data['EmployeeID'] = $primary_key;

            if ($fsize <= $MAXIMUM_FILESIZE && preg_match($rEFileTypes, strrchr($safe_filename, '.'))){
                move_uploaded_file($tmp_name, $upload_path.$file_name);
                $this->db->insert('tp_profile_evidence', $data);
            }

        }

        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }


    public function convertXLStoCSV($infile,$outfile)
    {
        $fileType = IOFactory::identify($infile);
        $objReader = IOFactory::createReader($fileType);

        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($infile);

        $objWriter = IOFactory::createWriter($objPHPExcel, 'CSV');
        //$objWriter->setSheetIndex(1);   // Select which sheet.
        $objWriter->setDelimiter(';');  // Define delimiter
        $inputFileName = './assets/files/import/'.$outfile;
        $objWriter->save($inputFileName);


    }


    public function do_upload(){
        $this->set_today();
        $today = date('Ymd_His');

        $fileName = $today.'_'.$_FILES['userfile']['name'];

        if (is_null($_FILES['userfile']['name']) || empty($_FILES['userfile']['name'])){
            $this->session->set_flashdata('empty_excel', $this->cms_lang('Please select data first'));
        }
        else{

        $config['upload_path'] = './assets/files/upload/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('userfile') )
        $this->upload->display_errors();

        $media = $this->upload->data('userfile');
        $inputFileName = './assets/files/upload/'.$fileName;

        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                //$this->session->set_flashdata('msg_excel', $this->cms_lang('Inserting data success'));
            } catch(Exception $e) {
                //$this->session->set_flashdata('msg_excel', 'Error!!!');
                //die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));

            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            if ($highestColumn != 'AE'){
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            }
            else{

                $this->db->update('tp_profile', array('Bagan_Status' => 0), array('Bagan_Status'=> 1));

                for ($row = 2; $row <= $highestRow; $row++){
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);

                    $primary_key = $rowData[0][1];

                    $query = $this->db->select('Pers_No,Prev_Per_No')
                                      ->from('tp_profile')
                                      ->where('Prev_Per_No', $primary_key)
                                      ->get();

                    if($query->num_rows() > 0){

                        $data = array(
                            'Position_ID'=> htmlspecialchars($rowData[0][3], ENT_QUOTES),
                            'Position_Name'=> $rowData[0][4],
                            'Nama_Panjang_Posisi' => $rowData[0][5],
                            'Org_unit' => $rowData[0][6],
                            'Organizational_Unit' => $rowData[0][7],
                            'BusA' => $rowData[0][8],
                            'Business_Area' => $rowData[0][9],
                            'Start_Date' => $rowData[0][10],
                            'PS_group' => $rowData[0][11],
                            'Skala_Gaji_Dasar' => $rowData[0][12],
                            'Jenjang_Main_Grp_Text' => $rowData[0][13],
                            'Jenjang_Sub_Grp_Text' => $rowData[0][14],
                            'Tanggal_Masuk' => $rowData[0][15],
                            'Tanggal_CAPEG' => $rowData[0][16],
                            'Tanggal_Pegawai_Tetap' => $rowData[0][17],
                            'Birth_date' => $rowData[0][18],
                            'Employee_Group' => $rowData[0][19],
                            'ESgrp' => $rowData[0][20],
                            'Email' => $rowData[0][21],
                            'Street_and_House_Number' => $rowData[0][22],
                            'City' => $rowData[0][23],
                            'Postal_code' => $rowData[0][24],
                            'Telephone_No' => $rowData[0][25],
                            'Birthplace' => $rowData[0][26],
                            'Pendidikan_Terakhir' => $rowData[0][27],
                            'Study_of_Branch' => $rowData[0][28],
                            'Gender_Key' => $rowData[0][29],
                            'Age_of_employee' => $rowData[0][30],
                            'Bagan_Status' => 1,
                            'UpdatedBy' => $this->cms_user_id(),
                            );

                        $this->db->update('tp_profile', $data, array('Prev_Per_No' => $primary_key));

                    }
                    else{

                        $data = array(
                            'Pers_No'=> $rowData[0][0],
                            'Prev_Per_No'=> $rowData[0][1],
                            'Full_Name'=> $rowData[0][2],
                            'Position_ID'=> htmlspecialchars($rowData[0][3], ENT_QUOTES),
                            'Position_Name'=> $rowData[0][4],
                            'Nama_Panjang_Posisi' => $rowData[0][5],
                            'Org_unit' => $rowData[0][6],
                            'Organizational_Unit' => $rowData[0][7],
                            'BusA' => $rowData[0][8],
                            'Business_Area' => $rowData[0][9],
                            'Start_Date' => $rowData[0][10],
                            'PS_group' => $rowData[0][11],
                            'Skala_Gaji_Dasar' => $rowData[0][12],
                            'Jenjang_Main_Grp_Text' => $rowData[0][13],
                            'Jenjang_Sub_Grp_Text' => $rowData[0][14],
                            'Tanggal_Masuk' => $rowData[0][15],
                            'Tanggal_CAPEG' => $rowData[0][16],
                            'Tanggal_Pegawai_Tetap' => $rowData[0][17],
                            'Birth_date' => $rowData[0][18],
                            'Employee_Group' => $rowData[0][19],
                            'ESgrp' => $rowData[0][20],
                            'Email' => $rowData[0][21],
                            'Street_and_House_Number' => $rowData[0][22],
                            'City' => $rowData[0][23],
                            'Postal_code' => $rowData[0][24],
                            'Telephone_No' => $rowData[0][25],
                            'Birthplace' => $rowData[0][26],
                            'Pendidikan_Terakhir' => $rowData[0][27],
                            'Study_of_Branch' => $rowData[0][28],
                            'Gender_Key' => $rowData[0][29],
                            'Age_of_employee' => $rowData[0][30],
                            'Bagan_Status' => 1,
                            'CreatedBy' => $this->cms_user_id(),
                            'UpdatedBy' => $this->cms_user_id(),
                            'CreatedTime' => $this->hari_ini,
                            );

                        $this->db->insert('tp_profile', $data);
                    }

                }

                $this->session->set_flashdata('msg_excel', $this->cms_lang('Inserting data success'));
            }

        }

        $tahun = date('Y');
        $bulan = date('m');

        $this->mst_position_ladder();
        $this->mst_organizational_unit();
        $this->mst_business_area();
        $this->mst_basic_salary_scale();

        $this->load->model('mapping_organization_model');

        /*
        $this->db->insert('tp_profile_upload_history', array('employee_id' => $this->cms_user_id()));
        $upload_history_id = $this->db->insert_id();
        */
        $upload_history_id = $this->check_period_by_month($tahun, $bulan);

        //$this->mapping_organization_model->copy_table_history($upload_history_id);

        $this->db->empty_table('tp_diagram_frame_detail');

        $this->db->delete('tp_mutation_movement', array('YEAR(CreatedTime)'=> $tahun, 'MONTH(CreatedTime)'=> $bulan));

        $this->mapping_organization_model->do_update_diagram_frame($upload_history_id, $business_area=3401);
        $this->mapping_organization_model->do_update_diagram_frame($upload_history_id, $business_area=3411);
        $this->mapping_organization_model->do_update_diagram_frame($upload_history_id, $business_area=3412);
        $this->mapping_organization_model->do_update_diagram_frame($upload_history_id, $business_area=3413);
        $this->mapping_organization_model->do_update_diagram_frame($upload_history_id, $business_area=3414);

        // fungsi untuk mengkalkulasi karyawan yg tidak termaping
        $this->mapping_organization_model->set_unmapping_employee_struktural_fungsional();

        // fungsi untuk mengkalkulasi ulang karyawan yg tidak termaping berdasarkan unit saja
        $this->mapping_organization_model->recalculate_unmapping_employee_struktural_fungsional($tahun, $bulan);

        $this->mapping_organization_model->set_mutation_movement_combine_employee($business_area=NULL, $upload_history_id);

        //$this->mapping_organization_model->set_unmapping_employee_fungsional();


        redirect($this->cms_module_path().'/raw_data_management/');
    }


    /*
    Fungsi untuk proses upload data ke database
    */

    public function do_upload_position_history(){
        $this->set_today();

        $today = date('Ymd_His');

        $fileName = $today.'_'.$_FILES['userfile']['name'];

        if (is_null($_FILES['userfile']['name']) || empty($_FILES['userfile']['name'])){
            $this->session->set_flashdata('empty_excel', $this->cms_lang('Please select data first'));
        }
        else{

        $config['upload_path'] = './assets/files/upload/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'csv';
        $config['max_size'] = 10000000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('userfile') )
        $this->upload->display_errors();

        $media = $this->upload->data('userfile');
        $inputFileName = './assets/files/upload/'.$fileName;

        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            if ($highestColumn != 'AL'){
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            }
            else{

                for ($row = 2; $row <= $highestRow; $row++){
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);

                    $primary_key = $rowData[0][1];
                    $employee_id = $rowData[0][2];
                    $Start_Date  = $rowData[0][3];
                    $End_Date    = $rowData[0][4];

                    $query = $this->db->select('Pers_No, EmployeeID')
                                      ->from('tp_profile_position_history')
                                      ->where('Pers_No', $primary_key)
                                      ->where('EmployeeID', $employee_id)
                                      ->where('Start_Date', $Start_Date)
                                      ->where('End_Date', $End_Date)
                                      ->get();

                    //if($query->num_rows() <= 0){

                        $data = array(
                            'Pers_No'=> $rowData[0][0],
                            'EmployeeID'=> $rowData[0][1],
                            'Full_Name'=> $rowData[0][2],
                            'Start_Date'=> $rowData[0][3],
                            'End_Date'=> $rowData[0][4],
                            'Jenjang_Main_Grp_ID'=> $rowData[0][5],
                            'Jenjang_Main_Grp_Text'=> $rowData[0][6],
                            'Jenjang_Sub_Grp_ID'=> $rowData[0][7],
                            'Jenjang_Sub_Grp_Text'=> $rowData[0][8],
                            'CoCd'=> $rowData[0][9],
                            'Company_Code'=> $rowData[0][10],
                            'PA'=> $rowData[0][11],
                            'Personnel_Area'=> $rowData[0][12],
                            'EEGrp'=> $rowData[0][13],
                            'Employee_Group'=> $rowData[0][14],
                            'ESgrp'=> $rowData[0][15],
                            'Employee_Subgroup'=> $rowData[0][16],
                            'Organizational_key'=> $rowData[0][17],
                            'BusA'=> $rowData[0][18],
                            'Business_Area'=> $rowData[0][19],
                            'PSubarea'=> $rowData[0][20],
                            'Personnel_Subarea'=> $rowData[0][21],
                            'Org_unit'=> $rowData[0][22],
                            'Organizational_Unit'=> $rowData[0][23],
                            'Position_ID'=> $rowData[0][24],
                            'Position_Name'=> $rowData[0][25],
                            'Job_ID'=> $rowData[0][26],
                            'Job_Name'=> $rowData[0][27],
                            'Job_Name2'=> $rowData[0][28],
                            'No_SK_Organizational_Assignmen'=> $rowData[0][29],
                            'Tgl_SK_Organizational_Assignmen'=> $rowData[0][30],
                            'Kode_Jabatan'=> $rowData[0][31],
                            'Kelompok_Jabatan'=> $rowData[0][32],
                            'Keterangan_Jabatan'=> $rowData[0][33],
                            'Nama_Panjang_Posisi_SIMKP'=> $rowData[0][34],
                            'PArea'=> $rowData[0][35],
                            'Payroll_Area'=> $rowData[0][36],
                            'Nama_Panjang_Posisi'=> $rowData[0][37],
                            'CreatedBy' => $this->cms_user_id(),
                            'UpdatedBy' => $this->cms_user_id(),
                            'CreatedTime' => $this->hari_ini,

                            );

                        $this->db->insert('tp_profile_position_history', $data);

                    //}

                }

                $this->session->set_flashdata('msg_excel', $this->cms_lang('Inserting data success'));
            }

        }

        redirect($this->cms_module_path().'/raw_data_management/position_history');

    }



    public function do_upload_training_history(){
        $this->set_today();
        $today = date('Ymd_His');

        $fileName = $today.'_'.$_FILES['userfile']['name'];

        if (is_null($_FILES['userfile']['name']) || empty($_FILES['userfile']['name'])){
            $this->session->set_flashdata('empty_excel', $this->cms_lang('Please select data first'));
        }
        else{

        $config['upload_path'] = './assets/files/upload/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'csv';
        $config['max_size'] = 10000000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('userfile') )
        $this->upload->display_errors();

        $media = $this->upload->data('userfile');
        $inputFileName = './assets/files/upload/'.$fileName;

        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            if ($highestColumn != 'Q'){
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            }
            else{

                for ($row = 2; $row <= $highestRow; $row++){
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);

                    $primary_key = $rowData[0][1];
                    $employee_id = $rowData[0][2];
                    $no_sertifikat  = $rowData[0][10];


                    $query = $this->db->select('Pers_No, EmployeeID, no_sertifikat')
                                      ->from('tp_profile_diklat')
                                      ->where('Pers_No', $primary_key)
                                      ->where('EmployeeID', $employee_id)
                                      ->where('no_sertifikat', $no_sertifikat)
                                      ->get();

                    if($query->num_rows() <= 0){
                        $data = array(
                            'Pers_No'=> $rowData[0][0],
                            'EmployeeID'=> $rowData[0][1],
                            'Full_Name'=> $rowData[0][2],
                            'nama_diklat'=> $rowData[0][3],
                            'kode_diklat'=> $rowData[0][4],
                            'nilai_kelulusan'=> $rowData[0][5],
                            'tanggal_mulai'=> $rowData[0][6],
                            'tanggal_akhir'=> $rowData[0][7],
                            'ranking'=> $rowData[0][8],
                            'grade'=> $rowData[0][9],
                            'no_sertifikat'=> $rowData[0][10],
                            'tanggal_sertifikat'=> $rowData[0][11],
                            'udiklat_penyelenggara'=> $rowData[0][12],
                            'judul_pa'=> $rowData[0][13],
                            'jabatan'=> $rowData[0][14],
                            'unit'=> $rowData[0][15],
                            'keterangan'=> $rowData[0][16],
                            'CreatedBy' => $this->cms_user_id(),
                            'UpdatedBy' => $this->cms_user_id(),
                            'CreatedTime' => $this->hari_ini,
                            );

                        $this->db->insert('tp_profile_diklat', $data);

                    }

                }

                $this->session->set_flashdata('msg_excel', $this->cms_lang('Inserting data success'));
            }

        }

        redirect($this->cms_module_path().'/raw_data_management/training_history');

    }


    public function do_upload_education_history(){
        $this->set_today();

        $today = date('Ymd_His');

        $fileName = $today.'_'.$_FILES['userfile']['name'];

        if (is_null($_FILES['userfile']['name']) || empty($_FILES['userfile']['name'])){
            $this->session->set_flashdata('empty_excel', $this->cms_lang('Please select data first'));
        }
        else{

        $config['upload_path'] = './assets/files/upload/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'csv';
        $config['max_size'] = 10000000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('userfile') )
        $this->upload->display_errors();

        $media = $this->upload->data('userfile');
        $inputFileName = './assets/files/upload/'.$fileName;

        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            if ($highestColumn != 'O'){
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            }
            else{

                for ($row = 2; $row <= $highestRow; $row++){
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);

                    $primary_key = $rowData[0][0];
                    $employee_id = $rowData[0][1];
                    $Start_Date  = $rowData[0][3];
                    $End_Date    = $rowData[0][4];


                    $query = $this->db->select('Pers_No, EmployeeID, Start_Date, End_Date, End_Date')
                                      ->from('tp_profile_education')
                                      ->where('Pers_No', $primary_key)
                                      //->where('EmployeeID', $employee_id)
                                      //->where('Start_Date', $Start_Date)
                                      //->where('End_Date', $End_Date)
                                      ->get();

                    if($query->num_rows() <= 0){
                        $data = array(
                            'Pers_No'=> $rowData[0][0],
                            'EmployeeID'=> $rowData[0][1],
                            'Full_Name'=> $rowData[0][2],
                            'Start_Date'=> $rowData[0][3],
                            'End_Date'=> $rowData[0][4],
                            'EE'=> $rowData[0][5],
                            'Educational_establishment'=> $rowData[0][6],
                            'Course_name'=> $rowData[0][7],
                            'Institute_location'=> $rowData[0][8],
                            'BrStu'=> $rowData[0][9],
                            'Branch_of_study'=> $rowData[0][10],
                            'Cty'=> $rowData[0][11],
                            'Gelar_Pendidikan'=> $rowData[0][12],
                            'Kode_Keterangan_Pendidikan'=> $rowData[0][13],
                            'Kode_Keterangan_Pendidikan_Text'=> $rowData[0][14],
                            'CreatedBy' => $this->cms_user_id(),
                            'UpdatedBy' => $this->cms_user_id(),
                            'CreatedTime' => $this->hari_ini,

                            );

                        $this->db->insert('tp_profile_education', $data);

                    }

                }

                $this->session->set_flashdata('msg_excel', $this->cms_lang('Inserting data success'));
            }

        }

        redirect($this->cms_module_path().'/raw_data_management/education_history');

    }


    public function do_upload_talent_history(){
        $this->set_today();

        $today = date('Ymd_His');

        $fileName = $today.'_'.$_FILES['userfile']['name'];

        if (is_null($_FILES['userfile']['name']) || empty($_FILES['userfile']['name'])){
            $this->session->set_flashdata('empty_excel', $this->cms_lang('Please select data first'));
        }
        else{

        $config['upload_path'] = './assets/files/upload/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'csv|xls|xlsx';
        $config['max_size'] = 10000000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('userfile') )
        $this->upload->display_errors();

        $media = $this->upload->data('userfile');
        $inputFileName = './assets/files/upload/'.$fileName;

        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            if ($highestColumn != 'G'){
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            }
            else{

                for ($row = 2; $row <= $highestRow; $row++){
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);

                    $primary_key = $rowData[0][0];
                    $employee_id = $rowData[0][1];
                    $SemesterID  = $rowData[0][4];

                    $query = $this->db->select('Pers_No, EmployeeID, SemesterID')
                                      ->from('tp_profile_talent')
                                      ->where('Pers_No', $primary_key)
                                      ->where('EmployeeID', $employee_id)
                                      ->where('SemesterID', $SemesterID)
                                      ->get();

                    if($query->num_rows() <= 0){
                        $data = array(
                            'Pers_No'=> $rowData[0][0],
                            'EmployeeID'=> $rowData[0][1],
                            'Full_Name'=> $rowData[0][2],
                            'Last_Talent_Date'=> $rowData[0][3],
                            'SemesterID'=> $rowData[0][4],
                            'Talent_Value'=> $rowData[0][5],
                            'Remarks'=> $rowData[0][6],
                            'CreatedBy' => $this->cms_user_id(),
                            'UpdatedBy' => $this->cms_user_id(),
                            'CreatedTime' => $this->hari_ini,
                            );

                        $this->db->insert('tp_profile_talent', $data);

                    }

                    else{

                         $data = array(
                            'Talent_Value'=> $rowData[0][5],
                            'Remarks'=> $rowData[0][6],
                            'UpdatedBy' => $this->cms_user_id(),
                            );
                        $this->db->update('tp_profile_talent', $data, array('Pers_No' => $primary_key, 'EmployeeID' => $employee_id, 'SemesterID' => $SemesterID));
                    }

                }

                $this->session->set_flashdata('msg_excel', $this->cms_lang('Inserting data success'));
            }

        }

        redirect($this->cms_module_path().'/raw_data_management/talent_history');

    }



    public function do_upload_profession_history(){
        $this->set_today();

        $today = date('Ymd_His');
        $fileName = $today.'_'.$_FILES['userfile']['name'];

        if (is_null($_FILES['userfile']['name']) || empty($_FILES['userfile']['name'])){
            $this->session->set_flashdata('empty_excel', $this->cms_lang('Please select data first'));
        }
        else{

        $config['upload_path'] = './assets/files/upload/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload('userfile') )
        $this->upload->display_errors();

        $media = $this->upload->data('userfile');
        $inputFileName = './assets/files/upload/'.$fileName;

        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            if ($highestColumn != 'M'){
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your data'));
            }
            else{

                for ($row = 2; $row <= $highestRow; $row++){
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);

                    $primary_key = $rowData[0][12];
                    $employee_id = $rowData[0][0];


                    $query = $this->db->select('cEmployeeID, ckode_profesi')
                                      ->from('tp_profile_profesi')
                                      ->where('cEmployeeID', $employee_id)
                                      ->where('ckode_profesi', $primary_key)
                                      ->get();

                    if($query->num_rows() <= 0){
                        $data = array(
                            'cEmployeeID'=> $rowData[0][0],
                            'cEmployeeName'=> $rowData[0][1],
                            'cGrade'=> $rowData[0][2],
                            'cJabatan'=> $rowData[0][3],
                            'cJenjang_Jabatan'=> $rowData[0][4],
                            'cPersonnel_Area'=> $rowData[0][5],
                            'cPohon_Profesi'=> $rowData[0][6],
                            'cDahan_Profesi'=> $rowData[0][7],
                            'cNama_Profesi'=> $rowData[0][8],
                            'iPosition'=> $rowData[0][9],
                            'iBusiness_Area'=> $rowData[0][10],
                            'iOrganizational_Unit'=> $rowData[0][11],
                            'ckode_profesi'=> $rowData[0][12],
                            'CreatedBy' => $this->cms_user_id(),
                            'UpdatedBy' => $this->cms_user_id(),
                            'CreatedTime' => $this->hari_ini,

                            );

                        $this->db->insert('tp_profile_profesi', $data);

                    }

                    else{

                         $data = array(
                            'ckode_profesi'=> $rowData[0][12],
                            'UpdatedBy' => $this->cms_user_id(),
                            );
                        $this->db->update('tp_profile_profesi', $data, array('cEmployeeID' => $employee_id, 'ckode_profesi' => $primary_key));
                    }

                }

                $this->session->set_flashdata('msg_excel', $this->cms_lang('Inserting data success'));
            }

        }

        redirect($this->cms_module_path().'/raw_data_management/profession_history');

    }


    public function mst_position_ladder(){

        $query = $this->db->select('Position_ID, Position_Name, Nama_Panjang_Posisi')
               ->from('tp_profile')
               ->group_by('Position_ID')
               ->order_by('Position_ID','ASC')
               ->get();
        foreach($query->result() as $data){

            $primary_key = $data->Position_ID;

            $this->db->select('position_id')
                 ->from('mst_position_ladder')
                 ->where('position_id', $primary_key);
            $db  = $this->db->get();
            $row = $db->row(0);
            $num_row = $db->num_rows();

            if ($num_row > 0){
                $this->db->update('mst_position_ladder', array('position_name' => $data->Position_Name, 'description' => $data->Nama_Panjang_Posisi), array('position_id'=> $primary_key));
            }
            else{
                $this->db->insert('mst_position_ladder', array('position_id' => $data->Position_ID,'position_name' => $data->Position_Name, 'description' => $data->Nama_Panjang_Posisi));
            }

        }

    }

    public function mst_organizational_unit(){

        $query = $this->db->select('Org_unit, Organizational_Unit')
               ->from('tp_profile')
               ->group_by('Org_unit')
               ->order_by('Org_unit','ASC')
               ->get();
        foreach($query->result() as $data){

            $primary_key = $data->Org_unit;

            $this->db->select('unit_id')
                 ->from('mst_organizational_unit')
                 ->where('unit_id', $primary_key);
            $db  = $this->db->get();
            $row = $db->row(0);
            $num_row = $db->num_rows();

            if ($num_row > 0){
                $this->db->update('mst_organizational_unit', array('description' => $data->Organizational_Unit), array('unit_id'=> $primary_key));
            }
            else{
                $this->db->insert('mst_organizational_unit', array('unit_id' => $primary_key, 'description' => $data->Organizational_Unit));
            }
        }
    }


    public function mst_basic_salary_scale(){

        $query = $this->db->select('Position_Name, Skala_Gaji_Dasar')
               ->from('tp_profile')
               ->where('Skala_Gaji_Dasar IS NOT NULL')
               ->group_by('Skala_Gaji_Dasar')
               ->order_by('Skala_Gaji_Dasar','ASC')
               ->get();
        foreach($query->result() as $data){

            $primary_key = $data->Skala_Gaji_Dasar;

            $this->db->select('salary_scale_id')
                 ->from('mst_basic_salary_scale')
                 ->where('salary_scale_id', $primary_key);
            $db  = $this->db->get();
            $row = $db->row(0);
            $num_row = $db->num_rows();

            if ($num_row > 0){
                $this->db->update('mst_basic_salary_scale', array('Description' => $data->Position_Name), array('salary_scale_id'=> $primary_key));
            }
            else{
                $this->db->insert('mst_basic_salary_scale', array('salary_scale_id' => $primary_key, 'Description' => $data->Position_Name));
            }
        }
    }


    public function mst_business_area(){

        $query = $this->db->select('BusA, Business_Area')
               ->from('tp_profile')
               ->group_by('BusA')
               ->order_by('BusA','ASC')
               ->get();
        foreach($query->result() as $data){

            $primary_key = $data->BusA;

            $this->db->select('business_area_id')
                 ->from('mst_business_area')
                 ->where('business_area_id', $primary_key);
            $db  = $this->db->get();
            $row = $db->row(0);
            $num_row = $db->num_rows();

            if ($num_row > 0){
                $this->db->update('mst_business_area', array('description' => $data->Business_Area), array('business_area_id'=> $primary_key));
            }
            else{
                $this->db->insert('mst_business_area', array('business_area_id' => $primary_key, 'description' => $data->Business_Area));
            }
        }
    }


    public function convert_date_format($date){

        if (is_null($date) || $date =='0000-00-00'){
            $tanggal = '';
        }
        else{
            $tanggal = date('Y-m-d', strtotime($date));
        }

        return $tanggal;
    }

    public function new_convert_date_format($date){

        $n = $date;
        $dateTime = new DateTime("1899-12-30 + ".$n." days");


        return $dateTime->format("d M Y");

    }

    public function ExcelToPHPObject($dateValue = 0) {

        $dateTime = self::ExcelToPHP($dateValue);
        $days = floor($dateTime / 86400);
        $time = round((($dateTime / 86400) - $days) * 86400);
        $hours = round($time / 3600);
        $minutes = round($time / 60) - ($hours * 60);
        $seconds = round($time) - ($hours * 3600) - ($minutes * 60);

        $dateObj = date_create('1-Jan-1970+'.$days.' days');
        $dateObj->setTime($hours,$minutes,$seconds);

        return $dateObj;
    }    //


    public function excelDateToDate($readDate){

        $timestamp = ($readDate - 25569) * 86400;


        $tanggal = date('Y-m-d', strtotime($timestamp));

        return $tanggal;


    }

    public function ExcelToPHP($dateValue = 0) {
        if (self::$ExcelBaseDate == self::CALENDAR_WINDOWS_1900) {
            $myExcelBaseDate = 25569;
            //    Adjust for the spurious 29-Feb-1900 (Day 60)
            if ($dateValue < 60) {
                $myExcelBaseDate;
            }
        } else {
            $myExcelBaseDate = 24107;
        }

        // Perform conversion
        if ($dateValue >= 1) {
            $utcDays = $dateValue - $myExcelBaseDate;
            $returnValue = round($utcDays * 86400);
            if (($returnValue <= PHP_INT_MAX) && ($returnValue >= -PHP_INT_MAX)) {
                $returnValue = (integer) $returnValue;
            }
        } else {
            $hours = round($dateValue * 24);
            $mins = round($dateValue * 1440) - round($hours * 60);
            $secs = round($dateValue * 86400) - round($hours * 3600) - round($mins * 60);
            $returnValue = (integer) gmmktime($hours, $mins, $secs);
        }

        // Return
        return $returnValue;
    }    //    function Excel

    public function excel_number_to_date($num){

        $num = $num-25570;
        return $this->addday('1970/01/02', $num);

    }

    public function addday($dat, $days){

        $dat = str_replace("/", "-", $dat);
        $dat = date('Y-m-d', strtotime($dat));

        return date('Y-m-d', strtotime($days));
    }

    // returned on insert and edit
    public function _callback_field_diklat($value, $primary_key){
        $module_path = $this->cms_module_path();
        $this->config->load('grocery_crud');
        $date_format = $this->config->item('grocery_crud_date_format');

        if(!isset($primary_key)) $primary_key = -1;
        $query = $this->db->select('DiklatID, EmployeeID, nama_diklat, kode_diklat, nilai_kelulusan, tanggal_mulai, tanggal_akhir, ranking, grade, no_sertifikat, tanggal_sertifikat, udiklat_penyelenggara,keterangan')
            ->from('tp_profile_diklat')
            ->where('EmployeeID', $primary_key)
            ->get();
        $result = $query->result_array();
        // add "hobby" to $result

        /*
        for($i=0; $i<count($result); $i++){
            $query_detail = $this->db->select('hobby_id')
               ->from($this->cms_complete_table_name('citizen_hobby'))
               ->where(array('citizen_id'=>$result[$i]['citizen_id']))->get();
            $value = array();
            foreach($query_detail->result() as $row){
                $value[] = $row->hobby_id;
            }
            $result[$i]['hobby'] = $value;
        }
        */

        // get options
        $options = array();

        /*
        $options['job_id'] = array();
        $query = $this->db->select('job_id,name')
           ->from($this->cms_complete_table_name('job'))
           ->get();
        foreach($query->result() as $row){
            $options['job_id'][] = array('value' => $row->job_id, 'caption' => $row->name);
        }
        $options['hobby'] = array();
        $query = $this->db->select('hobby_id,name')
           ->from($this->cms_complete_table_name('hobby'))->get();
        foreach($query->result() as $row){
            $options['hobby'][] = array('value' => $row->hobby_id, 'caption' => strip_tags($row->name));
        }

        */
        $data = array(
            'result' => $result,
            'options' => $options,
            'date_format' => $date_format,
        );
        return $this->load->view($this->cms_module_path().'/field_profile_training',$data, TRUE);
    }

    public function _callback_column_start_date($value, $row)
    {
        $date1 = $row->Start_Date;
        $date2 = date('Y-m-d');
        $diff = abs(strtotime($date2) - strtotime($date1));

        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        return $years.' Years '.$months.' Months';
    }

    // returned on insert and edit
    public function _callback_field_Education($value, $primary_key){
        $module_path = $this->cms_module_path();
        $this->config->load('grocery_crud');
        $date_format = $this->config->item('grocery_crud_date_format');

        if(!isset($primary_key)) $primary_key = -1;
        $query = $this->db->select('EducationID, EmployeeID, Educational_establishment, Kode_Keterangan_Pendidikan_Text, Institute_location, Start_Date, End_Date')
            ->from('tp_profile_education')
            ->where('EmployeeID', $primary_key)
            ->get();
        $result = $query->result_array();
        // add "hobby" to $result


        // get options
        $options = array();

        $options['Educational_establishment'] = array();
        $query = $this->db->select('education_id,description')
           ->from('mst_education')
           ->get();
        foreach($query->result() as $row){
            $options['Educational_establishment'][] = array('value' => $row->education_id, 'caption' => $row->education_id);
        }

        /*
        $options['StartYear'] = array();
        $query = $this->db->select('Year')
                          ->from('mst_year_period')
                          ->order_by('Year', 'ASC')
                          ->get();
        foreach($query->result() as $row){
            $options['StartYear'][] = array('value' => $row->Year, 'caption' => strip_tags($row->Year));
        }

        $options['EndYear'] = array();
        $query = $this->db->select('Year')
                          ->from('mst_year_period')
                          ->order_by('Year', 'ASC')
                          ->get();
        foreach($query->result() as $row){
            $options['EndYear'][] = array('value' => $row->Year, 'caption' => strip_tags($row->Year));
        }
        */

        $data = array(
            'result' => $result,
            'options' => $options,
            'date_format' => $date_format,
        );
        return $this->load->view($this->cms_module_path().'/field_profile_education',$data, TRUE);
    }


    // returned on insert and edit
    public function _callback_field_Occupation($value, $primary_key){
        $module_path = $this->cms_module_path();
        $this->config->load('grocery_crud');
        $date_format = $this->config->item('grocery_crud_date_format');

        if(!isset($primary_key)) $primary_key = -1;
        $query = $this->db->select('OccupationID, EmployeeID, PositionID, Description, UnitID, StartDate, EndDate')
            ->from('tp_profile_occupation')
            ->where('EmployeeID', $primary_key)
            ->get();
        $result = $query->result_array();
        // add "hobby" to $result

        // get options
        $options = array();

        $options['PositionID'] = array();
        $query = $this->db->select('position_id,position_name')
           ->from('mst_position_ladder')
           ->get();
        foreach($query->result() as $row){
            $options['PositionID'][] = array('value' => $row->position_id, 'caption' => $row->position_name);
        }

        $options['UnitID'] = array();
        $query = $this->db->select('unit_id,description')
           ->from('mst_organizational_unit')
           ->get();
        foreach($query->result() as $row){
            $options['UnitID'][] = array('value' => $row->unit_id, 'caption' => $row->description);
        }

        $data = array(
            'result' => $result,
            'options' => $options,
            'date_format' => $date_format,
        );
        return $this->load->view($this->cms_module_path().'/field_profile_occupation',$data, TRUE);
    }


    // returned on insert and edit
    public function _callback_field_Talent($value, $primary_key){
        $module_path = $this->cms_module_path();
        $this->config->load('grocery_crud');
        $date_format = $this->config->item('grocery_crud_date_format');

        if(!isset($primary_key)) $primary_key = -1;
        $query = $this->db->select('TalentID, EmployeeID, Last_Talent_Date, SemesterID, Talent_Value')
            ->from('tp_profile_talent')
            ->where('EmployeeID', $primary_key)
            ->get();
        $result = $query->result_array();
        // add "hobby" to $result

        // get options
        $options = array();

        $options['SemesterID'] = array();
        $query = $this->db->select('talent_period_id')
           ->from('mst_talent_period')
           ->get();
        foreach($query->result() as $row){
            $options['SemesterID'][] = array('value' => $row->talent_period_id, 'caption' => $row->talent_period_id);
        }

        $data = array(
            'result' => $result,
            'options' => $options,
            'date_format' => $date_format,
        );
        return $this->load->view($this->cms_module_path().'/field_profile_talent',$data, TRUE);
    }

    // returned on insert and edit
    public function _callback_field_Evidence($value, $primary_key){
        $module_path = $this->cms_module_path();
        $this->config->load('grocery_crud');
        $date_format = $this->config->item('grocery_crud_date_format');

        if(!isset($primary_key)) $primary_key = -1;
        $query = $this->db->select('EvidenceID, EmployeeID, EvidenceName, EvidenceUrl')
            ->from('tp_profile_evidence')
            ->where('EmployeeID', $primary_key)
            ->get();
        $result = $query->result_array();
        // add "hobby" to $result


        // get options
        $options = array();

        $data = array(
            'result' => $result,
            'options' => $options,
            'date_format' => $date_format,
            'module_path' => $this->cms_module_path(),
        );
        return $this->load->view($this->cms_module_path().'/field_profile_evidence',$data, TRUE);
    }

    public function user_have_privilege($privilege_name){

        return $this->cms_have_privilege($privilege_name);

    }

    public function upload_sap_time(){

        $day  = date('d');

        $time = $this->cms_get_config($name='sap_upload_time', $raw = FALSE);
        $segment = explode(',', $time);

        if (isset($segment[0]) && isset($segment[1])){

            if ($day >= $segment[0] && $day <= $segment[1]){
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }

    }

    public function upload_sap_time_text(){

        $day  = date('M-Y');

        $time = $this->cms_get_config($name='sap_upload_time', $raw = FALSE);
        $segment = explode(',', $time);

        if (isset($segment[0]) && isset($segment[1])){
            return $segment[0].'-'.$day.' {{ language:until }} '.$segment[1].'-'.$day;
        }
        else{
            return 'N/A';
        }

    }




















    private function make_crud_position_history(){

        $crud = $this->new_crud();
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        $crud->unset_print();

        if ($this->cms_have_privilege('edit') != 1) $crud->unset_edit();
        if ($this->cms_have_privilege('delete') != 1) $crud->unset_delete();
        if ($this->cms_have_privilege('export') != 1) $crud->unset_export();
        if ($this->cms_have_privilege('add') != 1) $crud->unset_add();


        $crud->unset_texteditor('Description');

        $crud->set_language($this->cms_language());
        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->set_theme('no-flexigrid-profile-history');
        }
        else{
            $crud->set_theme('no-flexigrid-profile-history');
            //$crud->set_relation('EmployeeID', 'tp_profile', '{Prev_Per_No} {Full_Name}');
            $crud->set_relation('Position_ID', 'mst_position_ladder', '{position_id} {position_name}');
        }


        $crud->set_table('tp_profile_position_history');

        $crud->set_primary_key('OccupationID');

        $crud->set_subject($this->cms_lang('Position History'));

        $crud->columns('EmployeeID','Full_Name','Start_Date','End_Date','Position_ID','Jenjang_Main_Grp_Text','Organizational_Unit','Job_Name','No_SK_Organizational_Assignmen','Tgl_SK_Organizational_Assignmen','Nama_Panjang_Posisi_SIMKP');
        $crud->edit_fields('EmployeeID','Full_Name','Start_Date','End_Date','Position_ID','Jenjang_Main_Grp_Text','Organizational_Unit','Job_Name','No_SK_Organizational_Assignmen','Tgl_SK_Organizational_Assignmen','Nama_Panjang_Posisi_SIMKP');
        $crud->add_fields('EmployeeID','Full_Name','Start_Date','End_Date','Position_ID','Jenjang_Main_Grp_Text','Organizational_Unit','Job_Name','No_SK_Organizational_Assignmen','Tgl_SK_Organizational_Assignmen','Nama_Panjang_Posisi_SIMKP');

        $crud->display_as('EmployeeID', $this->cms_lang('Employee ID'));
        $crud->display_as('Full_Name', $this->cms_lang('Full Name'));


        $crud->required_fields('EmployeeID','Full_Name','Start_Date','End_Date','Position_ID');



        //$crud->unique_fields('profession_id','profession_code');

        $crud->callback_before_insert(array($this,'_before_insert_detail'));
        $crud->callback_before_update(array($this,'_before_update_detail'));
        $crud->callback_before_delete(array($this,'_before_delete_detail'));
        $crud->callback_after_insert(array($this,'_after_insert_detail'));
        $crud->callback_after_update(array($this,'_after_update_detail'));
        $crud->callback_after_delete(array($this,'_after_delete_detail'));

        //$crud->callback_column('Full_Name',array($this, '_callback_column_full_name'));

        $this->crud = $crud;
        return $crud;
    }

    public function position_history(){
        $crud = $this->make_crud_position_history();
        $output = $crud->render();
        $this->view($this->cms_module_path().'/position_history_view', $output, $this->cms_complete_navigation_name('raw_data_management'));
    }



    private function make_crud_training_history(){

        $crud = $this->new_crud();
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        $crud->unset_print();

        if ($this->cms_have_privilege('edit') != 1) $crud->unset_edit();
        if ($this->cms_have_privilege('delete') != 1) $crud->unset_delete();
        if ($this->cms_have_privilege('export') != 1) $crud->unset_export();
        if ($this->cms_have_privilege('add') != 1) $crud->unset_add();


        $crud->unset_texteditor('Description');
        $crud->unset_texteditor('udiklat_penyelenggara');
        $crud->unset_texteditor('jabatan');

        $crud->set_language($this->cms_language());
        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->set_theme('no-flexigrid-profile-history');
        }
        else{
            $crud->set_theme('no-flexigrid-profile-history');
            //$crud->set_relation('EmployeeID', 'tp_profile', '{Prev_Per_No} {Full_Name}');
        }


        $crud->set_table('tp_profile_diklat');
        $crud->order_by('EmployeeID');

        $crud->set_primary_key('DiklatID');

        $crud->set_subject($this->cms_lang('Training History'));

        $crud->columns('EmployeeID','Full_Name','nama_diklat','nilai_kelulusan','tanggal_mulai','tanggal_akhir','ranking','grade','no_sertifikat','tanggal_sertifikat','udiklat_penyelenggara','keterangan');
        $crud->edit_fields('EmployeeID','Full_Name','nama_diklat','nilai_kelulusan','tanggal_mulai','tanggal_akhir','ranking','grade','no_sertifikat','tanggal_sertifikat','udiklat_penyelenggara','keterangan','jabatan');
        $crud->add_fields('EmployeeID','Full_Name','nama_diklat','nilai_kelulusan','tanggal_mulai','tanggal_akhir','ranking','grade','no_sertifikat','tanggal_sertifikat','udiklat_penyelenggara','keterangan','jabatan');

        //$crud->display_as('profession_id', $this->cms_lang('ID'));
        //$crud->display_as('profession_code', $this->cms_lang('Code'));
        //$crud->display_as('profession_name', $this->cms_lang('Name'));


        $crud->required_fields('EmployeeID','Full_Name','nama_diklat','nilai_kelulusan','tanggal_mulai','tanggal_akhir');
        //$crud->unique_fields('profession_id','profession_code');

        $crud->callback_before_insert(array($this,'_before_insert_detail'));
        $crud->callback_before_update(array($this,'_before_update_detail'));
        $crud->callback_before_delete(array($this,'_before_delete_detail'));
        $crud->callback_after_insert(array($this,'_after_insert_detail'));
        $crud->callback_after_update(array($this,'_after_update_detail'));
        $crud->callback_after_delete(array($this,'_after_delete_detail'));



        //$crud->callback_column('Full_Name',array($this, '_callback_column_full_name'));

        $this->crud = $crud;
        return $crud;
    }

    public function training_history(){
        $crud = $this->make_crud_training_history();
        $output = $crud->render();
        $this->view($this->cms_module_path().'/training_history_view', $output, $this->cms_complete_navigation_name('raw_data_management'));
    }


    //
    private function make_crud_talent_history(){
        $this->today = date('Y-m-d H:i:s');

        $crud = $this->new_crud();
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        $crud->unset_print();

        if ($this->cms_have_privilege('edit') != 1) $crud->unset_edit();
        if ($this->cms_have_privilege('delete') != 1) $crud->unset_delete();
        if ($this->cms_have_privilege('export') != 1) $crud->unset_export();
        if ($this->cms_have_privilege('add') != 1) $crud->unset_add();


        $crud->unset_texteditor('Description');
        $crud->unset_texteditor('Remarks');

        $crud->set_language($this->cms_language());
        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->set_theme('no-flexigrid-profile-history');
        }
        else{
            $crud->set_theme('no-flexigrid-profile-history');
            //$crud->set_relation('EmployeeID', 'tp_profile', '{Prev_Per_No} {Full_Name}');
        }


        $crud->set_table('tp_profile_talent');
        $crud->order_by('EmployeeID');

        $crud->set_primary_key('TalentID');

        $crud->set_subject($this->cms_lang('Talent History'));

        $crud->columns('EmployeeID','Full_Name','SemesterID','Talent_Value','Remarks');
        $crud->edit_fields('EmployeeID','Full_Name','SemesterID','Talent_Value','Last_Talent_Date','Remarks');
        $crud->add_fields('EmployeeID','Full_Name','SemesterID','Talent_Value','Last_Talent_Date','Remarks');

        $crud->display_as('EmployeeID', $this->cms_lang('Employee ID'));
        $crud->display_as('Full_Name', $this->cms_lang('Full Name'));
        $crud->display_as('SemesterID', $this->cms_lang('Semester'));
        $crud->display_as('Talent_Value', $this->cms_lang('Value'));
        $crud->display_as('Last_Talent_Date', $this->cms_lang('Date'));

        $crud->unset_add_fields('CreatedBy','CreatedTime','UpdatedBy');
        $crud->unset_edit_fields('UpdatedBy');
        $crud->field_type('CreatedBy', 'hidden', $this->cms_user_id());
        $crud->field_type('CreatedTime', 'hidden', $this->today);
        $crud->field_type('UpdatedBy', 'hidden', $this->cms_user_id());


        $crud->required_fields('EmployeeID','Full_Name','Last_Talent_Date','SemesterID','Talent_Value');
        //$crud->unique_fields('profession_id','profession_code');

        $crud->callback_before_insert(array($this,'_before_insert_detail'));
        $crud->callback_before_update(array($this,'_before_update_detail'));
        $crud->callback_before_delete(array($this,'_before_delete_detail'));
        $crud->callback_after_insert(array($this,'_after_insert_detail'));
        $crud->callback_after_update(array($this,'_after_update_detail'));
        $crud->callback_after_delete(array($this,'_after_delete_detail'));


        $crud->set_relation('SemesterID', 'mst_talent_period', '{talent_period_id}');
        //$crud->set_relation('Talent_Value', 'mst_talents', '{TalentID}');

        //$crud->callback_column('Full_Name',array($this, '_callback_column_full_name'));

        $this->crud = $crud;
        return $crud;
    }

    public function talent_history(){
        $crud = $this->make_crud_talent_history();
        $output = $crud->render();
        $this->view($this->cms_module_path().'/talent_history_view', $output, $this->cms_complete_navigation_name('raw_data_management'));
    }



    private function make_crud_education_history(){
        $this->today = date('Y-m-d H:i:s');

        $crud = $this->new_crud();
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        $crud->unset_print();

        if ($this->cms_have_privilege('edit') != 1) $crud->unset_edit();
        if ($this->cms_have_privilege('delete') != 1) $crud->unset_delete();
        if ($this->cms_have_privilege('export') != 1) $crud->unset_export();
        if ($this->cms_have_privilege('add') != 1) $crud->unset_add();


        $crud->unset_texteditor('Institute_location');

        $crud->set_language($this->cms_language());
        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->set_theme('no-flexigrid-profile-history');
        }
        else{
            $crud->set_theme('no-flexigrid-profile-history');
            //$crud->set_relation('EmployeeID', 'tp_profile', '{Prev_Per_No} {Full_Name}');
        }


        $crud->set_table('tp_profile_education');
        $crud->order_by('EmployeeID','ASC');

        $crud->set_primary_key('EducationID');

        $crud->set_subject($this->cms_lang('Education History'));

        $crud->columns('EmployeeID','Full_Name','Start_Date','End_Date','Educational_establishment','Institute_location','Branch_of_study','Kode_Keterangan_Pendidikan_Text');
        $crud->edit_fields('EmployeeID','Full_Name','Start_Date','End_Date','Educational_establishment','Institute_location','BrStu','Branch_of_study','Kode_Keterangan_Pendidikan','Kode_Keterangan_Pendidikan_Text','UpdatedBy');
        $crud->add_fields('EmployeeID','Full_Name','Start_Date','End_Date','Educational_establishment','Institute_location','BrStu','Branch_of_study','Kode_Keterangan_Pendidikan','Kode_Keterangan_Pendidikan_Text','CreatedBy','CreatedTime','UpdatedBy');

        //$crud->display_as('profession_id', $this->cms_lang('ID'));
        //$crud->display_as('profession_code', $this->cms_lang('Code'));
        //$crud->display_as('profession_name', $this->cms_lang('Name'));

        $crud->unset_add_fields('CreatedBy','CreatedTime','UpdatedBy');
        $crud->unset_edit_fields('UpdatedBy');
        $crud->field_type('CreatedBy', 'hidden', $this->cms_user_id());
        $crud->field_type('CreatedTime', 'hidden', $this->today);
        $crud->field_type('UpdatedBy', 'hidden', $this->cms_user_id());


        $crud->required_fields('EmployeeID','Full_Name','Start_Date','End_Date','Educational_establishment','Institute_location','BrStu','Branch_of_study','Kode_Keterangan_Pendidikan','Kode_Keterangan_Pendidikan_Text');
        //$crud->unique_fields('profession_id','profession_code');

        $crud->callback_before_insert(array($this,'_before_insert_detail'));
        $crud->callback_before_update(array($this,'_before_update_detail'));
        $crud->callback_before_delete(array($this,'_before_delete_detail'));
        $crud->callback_after_insert(array($this,'_after_insert_detail'));
        $crud->callback_after_update(array($this,'_after_update_detail'));
        $crud->callback_after_delete(array($this,'_after_delete_detail'));


        $crud->set_relation('Kode_Keterangan_Pendidikan', 'mst_kode_keterangan_pendidikan', '{keterangan_nama}');
        //$crud->set_relation('Talent_Value', 'mst_talents', '{TalentID}');

        //$crud->callback_column('Full_Name',array($this, '_callback_column_full_name'));

        $this->crud = $crud;
        return $crud;
    }

    public function education_history(){
        $crud = $this->make_crud_education_history();
        $output = $crud->render();
        $this->view($this->cms_module_path().'/education_history_view', $output, $this->cms_complete_navigation_name('raw_data_management'));
    }



    private function make_crud_profession_history(){

        $crud = $this->new_crud();
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        $crud->unset_print();

        if ($this->cms_have_privilege('edit') != 1) $crud->unset_edit();
        if ($this->cms_have_privilege('delete') != 1) $crud->unset_delete();
        if ($this->cms_have_privilege('export') != 1) $crud->unset_export();
        if ($this->cms_have_privilege('add') != 1) $crud->unset_add();


        $crud->unset_texteditor('cJabatan');

        $crud->set_language($this->cms_language());
        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->set_theme('no-flexigrid-profile-history');
        }
        else{
            $crud->set_theme('no-flexigrid-profile-history');
            $crud->set_relation('EmployeeID', 'tp_profile', '{Prev_Per_No} {Full_Name}');
            $crud->set_relation('ckode_profesi', 'mst_profession', '{profession_code} {profession_name}');
        }


        $crud->set_table('tp_profile_profesi');
        $crud->order_by('cEmployeeID');

        $crud->set_primary_key('iProfesiID');

        $crud->set_subject($this->cms_lang('Profession History'));

        $crud->columns('cEmployeeID','cEmployeeName','cGrade','cJabatan','cJenjang_Jabatan','cPersonnel_Area','cPohon_Profesi','ckode_profesi','cNama_Profesi','iPosition','iBusiness_Area','iOrganizational_Unit');
        $crud->edit_fields('cEmployeeID','cEmployeeName','cGrade','cJabatan','cJenjang_Jabatan','cPersonnel_Area','cPohon_Profesi','cDahan_Profesi','ckode_profesi','cNama_Profesi','iPosition','iBusiness_Area','iOrganizational_Unit');
        $crud->add_fields('cEmployeeID','cEmployeeName','cGrade','cJabatan','cJenjang_Jabatan','cPersonnel_Area','cPohon_Profesi','cDahan_Profesi','ckode_profesi','cNama_Profesi','iPosition','iBusiness_Area','iOrganizational_Unit');

        $crud->display_as('cEmployeeID', $this->cms_lang('NIP'));
        $crud->display_as('cEmployeeName', $this->cms_lang('Name'));
        $crud->display_as('cGrade', $this->cms_lang('Grade'));
        $crud->display_as('cJabatan', $this->cms_lang('Position'));
        $crud->display_as('cJenjang_Jabatan', $this->cms_lang('Jenjang Jabatan'));
        $crud->display_as('cPersonnel_Area', $this->cms_lang('Personnel Area'));
        $crud->display_as('cPohon_Profesi', $this->cms_lang('Pohon Profesi'));

        $crud->display_as('cDahan_Profesi', $this->cms_lang('Dahan Profesi'));
        $crud->display_as('cNama_Profesi', $this->cms_lang('Nama Profesi'));
        $crud->display_as('iPosition', $this->cms_lang('Position'));

        $crud->display_as('iBusiness_Area', $this->cms_lang('Business Area'));
        $crud->display_as('iOrganizational_Unit', $this->cms_lang('Organizational Unit'));
        $crud->display_as('ckode_profesi', $this->cms_lang('Kode Profesi'));

        $crud->required_fields('cEmployeeID','cEmployeeName','cGrade','cJabatan','cJenjang_Jabatan','cPersonnel_Area','cPohon_Profesi','cDahan_Profesi','cNama_Profesi','iPosition','iBusiness_Area','iOrganizational_Unit','ckode_profesi');
        //$crud->unique_fields('profession_id','profession_code');

        $crud->callback_before_insert(array($this,'_before_insert_detail'));
        $crud->callback_before_update(array($this,'_before_update_detail'));
        $crud->callback_before_delete(array($this,'_before_delete_detail'));
        $crud->callback_after_insert(array($this,'_after_insert_detail'));
        $crud->callback_after_update(array($this,'_after_update_detail'));
        $crud->callback_after_delete(array($this,'_after_delete_detail'));


        $crud->set_relation('iBusiness_Area', 'mst_business_area', '{description}');


        //$crud->callback_column('Full_Name',array($this, '_callback_column_full_name'));

        $this->crud = $crud;
        return $crud;
    }

    public function profession_history(){
        $this->allowed_types = '(xls|xlsx|csv)';

        $crud = $this->make_crud_profession_history();
        $output = $crud->render();
        $this->view($this->cms_module_path().'/profession_history_view', $output, $this->cms_complete_navigation_name('raw_data_management'));
    }


    public function _callback_column_full_name($value, $row){

        return $this->cms_table_data($table_name='tp_profile', $where_column='Prev_Per_No', $result_column='Full_Name', $row->EmployeeID);

    }

    public function _before_insert_detail($post_array){
        $post_array = $this->_before_insert_or_update_detail($post_array);
        return $post_array;
    }

    public function _after_insert_detail($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        return $success;
    }

    public function _before_update_detail($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update_detail($post_array, $primary_key);
        return $post_array;
    }

    public function _after_update_detail($post_array, $primary_key){
        $success = $this->_after_insert_or_update_detail($post_array, $primary_key);
        return $success;
    }

    public function _before_delete_detail($primary_key){
        return TRUE;
    }

    public function _after_delete_detail($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update_detail($post_array, $primary_key){

        return TRUE;
    }

    public function _before_insert_or_update_detail($post_array, $primary_key=NULL){
        return $post_array;
    }

    public function cms_table_data($table_name, $where_column, $result_column, $value){

        $this->db->select($result_column)->from($table_name)->where($where_column, $value);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            return $data->$result_column;
        }
        else{
            return '';
        }
    }

    public function allowed_types(){

        $nama = $this->allowed_types;

        return $nama;

    }

    public function check_period_by_month($tahun, $bulan){

        $this->load->model('mapping_organization_model');

        $SQL = "SELECT upload_history_id, counter, COUNT(upload_history_id) AS Total FROM tp_profile_upload_history
                WHERE YEAR(upload_history_created) ='".$tahun."' AND MONTH(upload_history_created) ='".$bulan."'";

        $query  = $this->db->query($SQL);
        $num_row = $query->num_rows();
        $data = $query->row(0);

        if ($data->Total > 0){
            $data_save = array(
                    'employee_updated'=> $this->cms_user_id(),
                    'upload_history_date' => date('Y-m-d H:i:s'),
                    'counter' => $data->counter+1,
            );

            $this->db->update('tp_profile_upload_history', $data_save, array('upload_history_id'=> $data->upload_history_id));
            $upload_history_id = $data->upload_history_id;

            if ($this->db->table_exists('tp_diagram_frame_'.$upload_history_id)){
                $this->db->query("DROP TABLE tp_diagram_frame_".$upload_history_id);
            }
            if ($this->db->table_exists('tp_diagram_frame_detail_'.$upload_history_id)){
                $this->db->query("DROP TABLE tp_diagram_frame_detail_".$upload_history_id);
            }

        }
        else{

            $data_save = array(
                    'employee_updated'=> $this->cms_user_id(),
                    'upload_history_created' => date('Y-m-d H:i:s'),
                    'employee_id' => $this->cms_user_id(),
            );

            $this->db->insert('tp_profile_upload_history', $data_save);
            $upload_history_id = $this->db->insert_id();

        }
        $this->mapping_organization_model->copy_table_history($upload_history_id);

        return $upload_history_id;
    }




}
