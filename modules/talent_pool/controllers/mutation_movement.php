<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mutation_movement extends CMS_Controller {

    protected $URL_MAP = array();

    public $period_id = '';
    public $period_name = '';
    public $start_date = '';
    public $end_date = '';


    private function make_crud(){

        $crud = $this->new_crud();
        // this is just for code completion
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        //$crud->set_model('custom_query_model');

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        $crud->set_theme('no-flexigrid-default');

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        $crud->unset_add();
        $crud->unset_edit();
        //$crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        //$crud->unset_export();

        $crud->set_language($this->cms_language());

        // table name
        $crud->set_table('tp_mutation_movement');
        $today = date('Y-m-d');

        if ($this->cms_have_privilege('delete') != 1) $crud->unset_delete();

        $crud->columns('EmployeeID','Full_Name','new_position','old_position','bisnis_area','unit_id','cStatus','CreatedTime');

        if (!is_null($this->period_id) && !empty($this->period_id)){
            $crud->where('CreatedTime <=', $this->end_date);
            $crud->where('CreatedTime >=', $this->start_date);
        }
        $crud->order_by('CreatedTime','DESC');
        //$crud->order_by('cStatus,bisnis_area,unit_id','ASC');


        // primary key
        $crud->set_primary_key('mutation_id');

        // set subject
        $crud->set_subject($this->cms_lang('Mutation Movement').' '.$this->period_name);

        // displayed columns on list
        //$crud->order_by('Full_Name','ASC');
        // displayed columns on edit operation
        $crud->edit_fields('name');
        // displayed columns on add operation
        $crud->add_fields('name');

        $crud->display_as('new_position', $this->cms_lang('Old Position'));
        $crud->display_as('old_position', $this->cms_lang('New Position'));
        $crud->display_as('new_unit', $this->cms_lang('New Unit'));
        $crud->display_as('old_unit', $this->cms_lang('Old Unit'));
        $crud->display_as('Full_Name', $this->cms_lang('Full Name'));
        $crud->display_as('cStatus', $this->cms_lang('Status'));
        $crud->display_as('EmployeeID', $this->cms_lang('ID'));
        $crud->display_as('CreatedTime', $this->cms_lang('Period'));

        $crud->set_relation('cStatus', 'mst_employee_status', '{cStatusName}');
        $crud->set_relation('bisnis_area', 'mst_business_area', '{description}');

        $crud->required_fields('name');


        $crud->unique_fields('name');


        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        $crud->callback_column('CreatedTime',array($this, '_callback_column_CreatedTime'));

        //$crud->add_action($this->cms_lang('Detail'), 'fa fa-list-ol fa-lg', '','btn btn-default btn-xs btn-flat',array($this,'_callback_column_detail'));

        $this->crud = $crud;
        return $crud;
    }

    public function index(){

        $this->set_period_variable();
        $crud = $this->make_crud();
        $state = $crud->getState();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();

        $data = array(
            'state' => $state,
            'primary_key' => $this->uri->segment(4),
            'filter_period' => $this->select_option_period(),
            'dropdown_period' => $this->dropdown_option_period(),
            'result_data' => $this->result_data_group(),
        );

        $output   = array_merge((array)$output, $data);

        $this->view($this->cms_module_path().'/mutation_movement_view', $output,
            $this->cms_complete_navigation_name('mutation_movement'));
    }


    public function select_option_period(){

        $modules = $this->uri->segment(1);
        $pages   = $this->uri->segment(2);

        $query = $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
               ->from('mst_period')
               ->where('PStatus', 1)
               ->order_by('PStartDate','ASC')
               ->get();

        $empty_select = '<option value="'.site_url($modules.'/'.$pages).'" SELECTED>{{ language:All Period }}</option>';
        foreach($query->result() as $data){
            if($this->period_id == $data->PeriodID){
                $empty_select .='<option value="'.site_url($modules.'/'.$pages.'/index/'.$data->PeriodID).'" data-subtext="" SELECTED>'.$data->PeriodName.'</option>';
            }
            else {
                $empty_select .='<option value="'.site_url($modules.'/'.$pages.'/index/'.$data->PeriodID).'" data-subtext="">'.$data->PeriodName.'</option>';
            }
        }

        return $empty_select;
    }


    public function dropdown_option_period(){

        $modules = $this->uri->segment(1);
        $pages   = $this->uri->segment(2);

        $query = $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
               ->from('mst_period')
               ->where('PStatus', 1)
               ->order_by('PStartDate','ASC')
               ->get();

        $empty_select = '<li><a href="'.site_url($modules.'/'.$pages).'">{{ language:All Period }}</a></li>';
        foreach($query->result() as $data){
            if($this->period_id == $data->PeriodID){
                $empty_select .='<li class="active"><a href="'.site_url($modules.'/'.$pages.'/index/'.$data->PeriodID).'">'.$data->PeriodName.'</a></li>';
            }
            else {
                $empty_select .='<li><a href="'.site_url($modules.'/'.$pages.'/index/'.$data->PeriodID).'">'.$data->PeriodName.'</a></li>';
            }
        }

        return $empty_select;
    }

    public function result_data_group(){

        $SQL    = "SELECT cStatusName,cStatusCode, count(EmployeeID) AS Total FROM tp_mutation_movement INNER JOIN mst_employee_status ON iStatusID=cStatus ";

        if (!is_null($this->period_id) && !empty($this->period_id)){
            $SQL    .= " WHERE CreatedTime <='".$this->end_date."' AND CreatedTime >='".$this->start_date."'";
        }
        $SQL    .= " GROUP BY cStatus";

        $query  = $this->db->query($SQL);

        $chart_value = '';

        foreach($query->result() as $data){

            $chart_value .= '<span class="label label-'.$data->cStatusCode.'">'.$data->cStatusName.'= '.$data->Total.'</span>&nbsp';

        }
        return $chart_value;
    }

    public function set_period_variable(){

        $period_id   = $this->uri->segment(4);

        $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PeriodID', $period_id);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            $this->period_id = $data->PeriodID;
            $this->period_name = $data->PeriodName;
            $this->start_date = $data->PStartDate;
            $this->end_date = $data->PEndDate;
        }
    }

    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_delete($primary_key){

        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){

        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }

    public function _callback_column_detail($value, $primary_key){

        return site_url('talent_pool/mutation_movement/detail/'.$value);

    }

    public function detail(){

        $data['subject'] = 'OK';
        $data['primary_key'] = $this->uri->segment(4);

        $this->view($this->cms_module_path().'/mutation_movement_detail_view', $data);


    }

    public function _callback_column_CreatedTime($value, $row){

        return $this->date_format_period($value);
    }

    public function date_format_period($date){

        $tanggal = date('Y-m-d', strtotime($date));

        $SQL = "SELECT PeriodID, PeriodName FROM mst_period WHERE PStartDate <='".$tanggal."' AND PEndDate >='".$tanggal."'";

        $query  = $this->db->query($SQL);
        $num_row = $query->num_rows();
        $data = $query->row(0);

        if ($num_row > 0){
            return $data->PeriodName;
        }
        else{
            return '';
        }

    }


}
