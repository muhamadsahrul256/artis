<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class import_raw_data extends CMS_Priv_Strict_Controller {

    protected $URL_MAP = array();

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->helper('cookie');
    }

    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    private function make_crud(){
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // initialize groceryCRUD
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $crud = $this->new_crud();
        // this is just for code completion
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        $crud->unset_add();
        //$crud->unset_edit();
        // $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        // $crud->unset_export();


        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->set_theme('no-flexigrid-tp');
        }
        else{
            $crud->set_theme('no-flexigrid-tp');
            $crud->set_relation('Position_ID', $this->cms_complete_table_name('mst_position_ladder'), 'position_name');
        }

        $key_words = $this->input->get('q');

        $crud->set_language($this->cms_language());

        // table name
        $crud->set_table($this->cms_complete_table_name('tp_raw_data'));
        //$crud->where("TP_ID > 0 AND Full_Name LIKE '%".$key_words."%'");

        

        if (isset($key_words)){
            $crud->like('Full_Name', $key_words ,'both');
            $crud->or_like('Prev_Per_No', $key_words ,'both');
            //$crud->where("TP_ID > 0 AND Full_Name LIKE '%".$key_words."%'");
        }

        /*
        $cookie = array (
            'name' => 'ajax_list',
            'value' => '',
            'expire' => '0',
            'domain' => site_url($this->cms_module_path() . '/import_raw_data'),
            'prefix' => 'ajax_list'
            );

        delete_cookie($cookie);
        */


       


        // primary key
        $crud->set_primary_key('TP_ID');

        // set subject
        $crud->set_subject($this->cms_lang('Import Raw Data'));

        // displayed columns on list
        $crud->columns('Prev_Per_No','Full_Name','Position_ID');
        // displayed columns on edit operation
        $crud->edit_fields('Pers_No','Prev_Per_No','Full_Name','Position_ID','Position_Description','Unit_ID','Unit_Description','diklat');
        // displayed columns on add operation
        $crud->add_fields('Pers_No','Prev_Per_No','Full_Name','Position_ID','Position_Name','Last_Updated');

        //$crud->set_field_upload('Photos','assets/uploads/files');   

        // caption of each columns
        $crud->display_as('Prev_Per_No', $this->cms_lang('Employee ID'));
        $crud->display_as('Full_Name', $this->cms_lang('Name'));
        $crud->display_as('Position_ID', $this->cms_lang('Position ID'));
        $crud->display_as('Position_Name', $this->cms_lang('Position Name'));
        $crud->display_as('Last_Updated', $this->cms_lang('Last Updated'));

       
       
        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        $crud->callback_field('diklat',array($this, '_callback_field_diklat'));


        $crud->set_tabs(array(
                'Profile'  => 7,
                'Diklat' => 1,
             ));    


        $this->crud = $crud;
        return $crud;
    }

    public function index(){

       
        $crud = $this->make_crud();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view($this->cms_module_path().'/import_raw_data_view', $output,
            $this->cms_complete_navigation_name('import_raw_data'));
    }

    public function delete_selection(){
        $crud = $this->make_crud();
        if(!$crud->unset_delete){
            $id_list = json_decode($this->input->post('data'));
            foreach($id_list as $id){
                if($this->_before_delete($id)){
                    $this->db->delete($this->cms_complete_table_name('tp_raw_data'),array('TP_ID'=>$id));
                    $this->_after_delete($id);
                }
            }
        }
    }

    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_delete($primary_key){
       
        $this->db->delete($this->cms_complete_table_name('tp_profile_diklat'),  array('TP_ID'=>$primary_key));
        
        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){

        $data = json_decode($this->input->post('md_real_field_citizen_col'), TRUE);
        $insert_records = $data['insert'];
        $update_records = $data['update'];
        $delete_records = $data['delete'];
        $real_column_names = array('DiklatID', 'DiklatName', 'Description', 'DiklatDate');
        $set_column_names = array();
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  DELETED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($delete_records as $delete_record){
            $detail_primary_key = $delete_record['primary_key'];
            // delete many to many
            for($i=0; $i<count($many_to_many_column_names); $i++){
                $table_name = $this->cms_complete_table_name($many_to_many_relation_tables[$i]);
                $relation_column_name = $many_to_many_relation_table_columns[$i];
                $relation_selection_column_name = $many_to_many_relation_selection_columns[$i];
                $where = array(
                    $relation_column_name => $detail_primary_key
                );
                $this->db->delete($table_name, $where);
            }
            $this->db->delete($this->cms_complete_table_name('tp_profile_diklat'),
                 array('DiklatID'=>$detail_primary_key));
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  UPDATED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($update_records as $update_record){
            $detail_primary_key = $update_record['primary_key'];
            $data = array();
            foreach($update_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['TP_ID'] = $primary_key;
            $this->db->update($this->cms_complete_table_name('tp_profile_diklat'),
                 $data, array('DiklatID'=>$detail_primary_key));
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Adjust Many-to-Many Fields of Updated Data
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  INSERTED DATA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($insert_records as $insert_record){
            $data = array();
            foreach($insert_record['data'] as $key=>$value){
                if(in_array($key, $set_column_names)){
                    $data[$key] = implode(',', $value);
                }else if(in_array($key, $real_column_names)){
                    $data[$key] = $value;
                }
            }
            $data['TP_ID'] = $primary_key;
            $this->db->insert($this->cms_complete_table_name('tp_profile_diklat'), $data);
            $detail_primary_key = $this->db->insert_id();
            
        }

        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }

    


    public function _________do_upload(){
        $today = date('Ymd_His');

        $fileName = $today.'_'.$_FILES['userfile']['name'];
         
        $config['upload_path'] = './assets/files/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 1000000;
         
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
         
        if($this->upload->do_upload('userfile') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('userfile');
        $inputFileName = './assets/files/'.$fileName;
         
        //$inputFileType = array('Excel2007','Excel5','Excel2003XML','OOCalc','SYLK','Gnumeric','HTML','CSV');
        $inputFileType = IOFactory::identify($inputFileName);
        //$inputFileName = 'YOUR_EXCEL_FILE_PATH';

        $objReader = IOFactory::createReader($inputFileType);
        $objPHPExcelReader = $objReader->load($inputFileName);

        $loadedSheetNames = $objPHPExcelReader->getSheetNames();

        $objWriter = IOFactory::createWriter($objPHPExcelReader, 'CSV');

        foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {
            $objWriter->setSheetIndex($sheetIndex);
            $objWriter->save($loadedSheetName.'.csv');
        }

        $this->upload->do_upload($loadedSheetName.'.csv');

                      

        redirect('talent_pool/import_raw_data/');
    }

    public function __do_upload(){
        $today = date('Ymd_His');

        $fileName = $today.'_'.$_FILES['userfile']['name'];
         
        $config['upload_path'] = './assets/files/upload/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 1000000;
         
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
         
        if($this->upload->do_upload('userfile') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('userfile');
        $inputFileName = './assets/files/upload/'.$fileName;
        
        $this->convertXLStoCSV($inputFileName,$today.'_output.csv');
                      

        redirect('talent_pool/import_raw_data/');
    }

    public function convertXLStoCSV($infile,$outfile)
    {
        $fileType = IOFactory::identify($infile);
        $objReader = IOFactory::createReader($fileType);
     
        $objReader->setReadDataOnly(true);   
        $objPHPExcel = $objReader->load($infile);    
     
        $objWriter = IOFactory::createWriter($objPHPExcel, 'CSV');
        //$objWriter->setSheetIndex(1);   // Select which sheet.
        $objWriter->setDelimiter(';');  // Define delimiter
        $inputFileName = './assets/files/import/'.$outfile;
        $objWriter->save($inputFileName);

        /*

        //$objWriter->save(str_replace('.xlsx', '.csv',$outfile));

        $config['upload_path']      = './assets/files/'; //buat folder dengan nama assets di root folder
        $config['file_name']        = $outfile;
        $config['allowed_types']    = '.csv';
        $config['max_size']         = 1000000;
         
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $this->upload->do_upload('userfile');

        $inputFileName = './assets/files/'.$outfile;

        try {
                $inputFileType  = IOFactory::identify($inputFileName);
                $objReader      = IOFactory::createReader($inputFileType);
                $objPHPExcel    = $objReader->load($inputFileName);
                $this->session->set_flashdata('msg_excel', $this->cms_lang('Inserting data success'));
            } catch(Exception $e) {
                //$this->session->set_flashdata('msg_excel', 'Error!!!');
                //die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());

                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull'));

            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 2; $row <= $highestRow; $row++){ //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    'Pers_No'=> htmlspecialchars($rowData[0][0], ENT_QUOTES),
                    'Prev_Per_No'=> $rowData[0][1],
                    'Full_Name'=> $rowData[0][2],
                    'Position_ID'=> htmlspecialchars($rowData[0][3], ENT_QUOTES),
                    'Position_Name'=> $rowData[0][4]
                );
                 
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert('tp_raw_data', $data);                
                     
            }

            */
    }


    public function do_upload(){
        $today = date('Ymd_His');

        $fileName = $today.'_'.$_FILES['userfile']['name'];

        if (is_null($_FILES['userfile']['name']) || empty($_FILES['userfile']['name'])){
            $this->session->set_flashdata('empty_excel', $this->cms_lang('Please select data first'));
        }
        else{        
         
        $config['upload_path'] = './assets/files/upload/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000000;
         
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
         
        if($this->upload->do_upload('userfile') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('userfile');
        $inputFileName = './assets/files/upload/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                //$this->session->set_flashdata('msg_excel', $this->cms_lang('Inserting data success'));
            } catch(Exception $e) {
                //$this->session->set_flashdata('msg_excel', 'Error!!!');
                //die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your'));

            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            if ($highestColumn < 'AK'){
                $this->session->set_flashdata('error_excel', $this->cms_lang('Inserting data not successfull, please check your'));
            }
            else{
             
                for ($row = 2; $row <= $highestRow; $row++){                
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);

                    $primary_key = $rowData[0][0];

                    $query = $this->db->select('*')
                                      ->from(cms_table_name('tp_raw_data'))
                                      ->where('Pers_No', $primary_key)
                                      ->get();

                    if($query->num_rows()>0){

                        $data = array(                            
                            'Prev_Per_No'=> $rowData[0][1],
                            'Full_Name'=> $rowData[0][2],
                            'Position_ID'=> htmlspecialchars($rowData[0][3], ENT_QUOTES),
                            'Position_Name'=> $rowData[0][4],
                            'Position_Description' => $rowData[0][5],
                            'Unit_ID' => $rowData[0][6],
                            'Unit_Description' => $rowData[0][7],
                            'PA' => $rowData[0][8],
                            'Personnel_Area' => $rowData[0][9],
                            'PSubarea' => $rowData[0][10],
                            'Personnel_Subarea' => $rowData[0][11],
                            'BusA' => $rowData[0][12],
                            'Business_Area' => $rowData[0][13],
                            'Start_Date' => $this->convert_date_format($rowData[0][14]),
                            'PS_Group' => $rowData[0][15],
                            'LV' => $rowData[0][16],
                            'Last_Date_Grade' => $this->convert_date_format($rowData[0][17]),
                            'Basic_Salary_Scale' => $rowData[0][18],
                            'Main_Grp_Text' => $rowData[0][19],
                            'Sub_Grp_Text' => $rowData[0][20],
                            'Sub_Grp_ID' => $rowData[0][21],
                            'Employee_Grouping_Travel' => $rowData[0][22],
                            'Reimbursement_Group_Meals' => $rowData[0][23],
                            'Date_IN' => $this->convert_date_format($rowData[0][24]),
                            'CAPEG_Date' => $this->convert_date_format($rowData[0][25]),
                            'Permanent_Date' => $this->convert_date_format($rowData[0][26]),
                            'Birth_Date' => $this->convert_date_format($rowData[0][27]),
                            'Employee_Group' => $rowData[0][28],
                            'ESgrp' => $rowData[0][29],
                            'Employee_Subgroup' => $rowData[0][30],
                            'Payroll_Area' => $rowData[0][31],
                            'Start_Date2' => $this->convert_date_format($rowData[0][32]),
                            'TAdmin' => $rowData[0][33],
                            'Administrator_Record' => $rowData[0][34],
                            'WS_Rule' => $rowData[0][35],
                            'Work_Schedule_Rule' => $rowData[0][36],
                            'Bank_Account' => $rowData[0][37],
                            'Work_Time_Type_Group' => $rowData[0][38],
                            //'Grouping_Attendance' => $rowData[0][39],
                            //'Grouping_Employee_Expenses' => $rowData[0][40],
                            //'ID_Number' => $rowData[0][41],
                            //'Age_Employee' => $rowData[0][42],
                            //'Blood_Type' => $rowData[0][43],
                            //'Tax_Dependents_No' => $rowData[0][44],
                            //'Cost_Center' => $rowData[0][45],
                            );                     
                    
                        $this->db->update('tp_raw_data', $data, array('Pers_No' => $primary_key));
                        
                    }
                    else{
                        $data = array(
                            'Pers_No'=> htmlspecialchars($rowData[0][0], ENT_QUOTES),
                            'Prev_Per_No'=> $rowData[0][1],
                            'Full_Name'=> $rowData[0][2],
                            'Position_ID'=> htmlspecialchars($rowData[0][3], ENT_QUOTES),
                            'Position_Name'=> $rowData[0][4],
                            'Position_Description' => $rowData[0][5],
                            'Unit_ID' => $rowData[0][6],
                            'Unit_Description' => $rowData[0][7],
                            'PA' => $rowData[0][8],
                            'Personnel_Area' => $rowData[0][9],
                            'PSubarea' => $rowData[0][10],
                            'Personnel_Subarea' => $rowData[0][11],
                            'BusA' => $rowData[0][12],
                            'Business_Area' => $rowData[0][13],
                            'Start_Date' => $this->convert_date_format($rowData[0][14]),
                            'PS_Group' => $rowData[0][15],
                            'LV' => $rowData[0][16],
                            'Last_Date_Grade' => $this->convert_date_format($rowData[0][17]),
                            'Basic_Salary_Scale' => $rowData[0][18],
                            'Main_Grp_Text' => $rowData[0][19],
                            'Sub_Grp_Text' => $rowData[0][20],
                            'Sub_Grp_ID' => $rowData[0][21],
                            'Employee_Grouping_Travel' => $rowData[0][22],
                            'Reimbursement_Group_Meals' => $rowData[0][23],
                            'Date_IN' => $this->convert_date_format($rowData[0][24]),
                            'CAPEG_Date' => $this->convert_date_format($rowData[0][25]),
                            'Permanent_Date' => $this->convert_date_format($rowData[0][26]),
                            'Birth_Date' => $this->convert_date_format($rowData[0][27]),
                            'Employee_Group' => $rowData[0][28],
                            'ESgrp' => $rowData[0][29],
                            'Employee_Subgroup' => $rowData[0][30],
                            'Payroll_Area' => $rowData[0][31],
                            'Start_Date2' => $this->convert_date_format($rowData[0][32]),
                            'TAdmin' => $rowData[0][33],
                            'Administrator_Record' => $rowData[0][34],
                            'WS_Rule' => $rowData[0][35],
                            'Work_Schedule_Rule' => $rowData[0][36],
                            'Bank_Account' => $rowData[0][37],
                            'Work_Time_Type_Group' => $rowData[0][38],
                            //'Grouping_Attendance' => $rowData[0][39],
                            //'Grouping_Employee_Expenses' => $rowData[0][40],
                            //'ID_Number' => $rowData[0][41],
                            //'Age_Employee' => $rowData[0][42],
                            //'Blood_Type' => $rowData[0][43],
                            //'Tax_Dependents_No' => $rowData[0][44],
                            //'Cost_Center' => $rowData[0][45],

                            );                    
                    
                        $this->db->insert('tp_raw_data', $data);
                    }            
                         
                }

                $this->session->set_flashdata('msg_excel', $this->cms_lang('Inserting data success'));
            }

        }

        $this->mst_position_ladder();
        $this->mst_organizational_unit();
        $this->mst_business_area();

        redirect('talent_pool/import_raw_data/');
    }


    public function mst_position_ladder(){

        $query = $this->db->select('*')
               ->from('tp_raw_data')
               ->group_by('Position_ID')            
               ->order_by('Pers_No','ASC')
               ->get();
        foreach($query->result() as $data){

            $primary_key = $data->Position_ID;

            $this->db->select('*')
                 ->from('mst_position_ladder')
                 ->where('position_id', $primary_key);
            $db  = $this->db->get();
            $row = $db->row(0);
            $num_row = $db->num_rows();

            if ($num_row > 0){
                $this->db->update('mst_position_ladder', array('position_name' => $data->Position_Name, 'description' => $data->Position_Description), array('position_id'=> $primary_key));
            }
            else{
                $this->db->insert('mst_position_ladder', array('position_id' => $data->Position_ID,'position_name' => $data->Position_Name, 'description' => $data->Position_Description));
            }
           
        }

    }

    public function mst_organizational_unit(){

        $query = $this->db->select('*')
               ->from('tp_raw_data')
               ->group_by('Unit_ID')            
               ->order_by('Unit_ID','ASC')
               ->get();
        foreach($query->result() as $data){

            $primary_key = $data->Unit_ID;

            $this->db->select('*')
                 ->from('mst_organizational_unit')
                 ->where('unit_id', $primary_key);
            $db  = $this->db->get();
            $row = $db->row(0);
            $num_row = $db->num_rows();

            if ($num_row > 0){
                $this->db->update('mst_organizational_unit', array('description' => $data->Unit_Description), array('unit_id'=> $primary_key));
            }
            else{
                $this->db->insert('mst_organizational_unit', array('unit_id' => $primary_key, 'description' => $data->Unit_Description));
            }           
        }
    }

    public function mst_business_area(){

        $query = $this->db->select('*')
               ->from('tp_raw_data')
               ->group_by('BusA')            
               ->order_by('BusA','ASC')
               ->get();
        foreach($query->result() as $data){

            $primary_key = $data->BusA;

            $this->db->select('*')
                 ->from('mst_business_area')
                 ->where('business_area_id', $primary_key);
            $db  = $this->db->get();
            $row = $db->row(0);
            $num_row = $db->num_rows();

            if ($num_row > 0){
                $this->db->update('mst_business_area', array('description' => $data->Business_Area), array('business_area_id'=> $primary_key));
            }
            else{
                $this->db->insert('mst_business_area', array('business_area_id' => $primary_key, 'description' => $data->Business_Area));
            }           
        }
    }   


    public function convert_date_format($date){

        if (is_null($date) || $date =='0000-00-00'){
            $tanggal = '';
        }
        else{
            $tanggal = date('Y-m-d', strtotime($date));
        }       

        return $tanggal;
    }

    // returned on insert and edit
    public function _callback_field_diklat($value, $primary_key){
        $module_path = $this->cms_module_path();
        $this->config->load('grocery_crud');
        $date_format = $this->config->item('grocery_crud_date_format');

        if(!isset($primary_key)) $primary_key = -1;
        $query = $this->db->select('DiklatID, TP_ID, EmployeeID, DiklatName, Description, DiklatDate')
            ->from($this->cms_complete_table_name('tp_profile_diklat'))
            ->where('TP_ID', $primary_key)
            ->get();
        $result = $query->result_array();
        // add "hobby" to $result

        /*
        for($i=0; $i<count($result); $i++){
            $query_detail = $this->db->select('hobby_id')
               ->from($this->cms_complete_table_name('citizen_hobby'))
               ->where(array('citizen_id'=>$result[$i]['citizen_id']))->get();
            $value = array();
            foreach($query_detail->result() as $row){
                $value[] = $row->hobby_id;
            }
            $result[$i]['hobby'] = $value;
        }
        */

        // get options
        $options = array();

        /*
        $options['job_id'] = array();
        $query = $this->db->select('job_id,name')
           ->from($this->cms_complete_table_name('job'))
           ->get();
        foreach($query->result() as $row){
            $options['job_id'][] = array('value' => $row->job_id, 'caption' => $row->name);
        }
        $options['hobby'] = array();
        $query = $this->db->select('hobby_id,name')
           ->from($this->cms_complete_table_name('hobby'))->get();
        foreach($query->result() as $row){
            $options['hobby'][] = array('value' => $row->hobby_id, 'caption' => strip_tags($row->name));
        }

        */
        $data = array(
            'result' => $result,
            'options' => $options,
            'date_format' => $date_format,
        );
        return $this->load->view($this->cms_module_path().'/field_city_citizen',$data, TRUE);
    }
    



}