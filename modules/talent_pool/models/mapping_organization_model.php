<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mapping_organization_model extends CMS_Model{


    public $table_diagram_frame        = NULL;
    public $table_diagram_frame_detail = NULL;

    public function do_update_diagram_frame($upload_history_id, $business_area){

        //$this->copy_table_history($upload_history_id);

        // update TJBB Pusat Dan Struktural
        $this->db->update('tp_diagram_frame', array('EmployeeID'=> '', 'EmployeeName' => '', 'GradeID'=> ''), array('APP'=> $business_area));

        $sql = "UPDATE tp_diagram_frame_detail JOIN tp_diagram_frame ON tp_diagram_frame.COID = tp_diagram_frame_detail.ParentID 
        SET tp_diagram_frame_detail.EmployeeID = '',tp_diagram_frame_detail.EmployeeName = '', tp_diagram_frame_detail.GradeID = '' WHERE APP = '".$business_area."'";
        $this->db->query($sql);


        $query = $this->db->select('COID,APP,UnitID,EmployeeID,EmployeeName,Full_Mapping_Code,PositionID,Functional')
               ->from('tp_diagram_frame')
               ->where('Functional', 0)
               ->where('Status', 1)
               ->where('APP', $business_area)
               ->where('Full_Mapping_Code IS NOT NULL')
               ->group_by('COID')
               ->order_by('UnitID','ASC')
               ->get();
        $no=1;

        foreach($query->result() as $data){

            if ($data->Functional == 0){
                $Jenjang_Main_Grp_Text = 'Struktural';
            }
            elseif($data->Functional == 1){
                $Jenjang_Main_Grp_Text = 'Fungsional';
            }
            else{
                $Jenjang_Main_Grp_Text = '';
            }

            $this->count_row_table_profile_struktural($data->COID, $data->APP, $data->UnitID, $data->PositionID, $data->Full_Mapping_Code, $Jenjang_Main_Grp_Text, $upload_history_id);
        }

        // update TJBB Pusat Dan Fungsional

        

        $query = $this->db->select('COID,APP,UnitID,EmployeeID,EmployeeName,Full_Mapping_Code,PositionID,Functional')
               ->from('tp_diagram_frame')
               ->where('Functional', 1)
               ->where('Status', 1)
               ->where('APP', $business_area)
               ->where('Full_Mapping_Code IS NOT NULL')
               ->group_by('COID')
               ->order_by('UnitID','ASC')
               ->get();
        $no=1;

        foreach($query->result() as $data){

            if ($data->Functional == 0){
                $Jenjang_Main_Grp_Text = 'Struktural';
            }
            elseif($data->Functional == 1){
                $Jenjang_Main_Grp_Text = 'Fungsional';
            }
            else{
                $Jenjang_Main_Grp_Text = '';
            }

            $this->count_row_table_profile_fungsional($data->COID, $data->APP, $data->UnitID, $data->PositionID, $data->Full_Mapping_Code, $Jenjang_Main_Grp_Text, $upload_history_id);

        }
        

        //$this->set_mutation_movement_employee_struktural($business_area, $upload_history_id);
        //$this->set_mutation_movement_employee_fungsional($business_area, $upload_history_id);        
    }

    public function count_row_table_profile_struktural($primary_key, $BusA, $Org_unit, $Position_ID, $Position_Name, $Jenjang_Main_Grp_Text, $upload_history_id){

        $SQL = "SELECT Prev_Per_No, Full_Name, Position_Name, PS_group, Skala_Gaji_Dasar, Position_ID 
                FROM tp_profile 
                WHERE Bagan_Status =1 AND BusA ='".$BusA."' AND 
                Org_unit ='".$Org_unit."' AND 
                Jenjang_Main_Grp_Text ='".$Jenjang_Main_Grp_Text."' ";

        $no = 1;
        $keyword_position = $this->cms_parse_keyword($Position_Name);

        if (!empty($keyword_position) && !is_null($keyword_position) && $keyword_position != ''){

          $words= array_diff(explode(',', $keyword_position),array(''));
          $last    = end($words);

          $SQL .= " AND ( ";

          foreach($words as $word){
              if ($last == $word){
                  $SQL .= " UPPER(Position_Name) LIKE '%".strtoupper($word)."%' ";
              }
              else{
                  $SQL .= " UPPER(Position_Name) LIKE '%".strtoupper($word)."%' OR ";
              }                            

              $no++;
          }

          $SQL .= " ) ";
        }       

        $query  = $this->db->query($SQL);
        $num_row = $query->num_rows();
        $data = $query->row(0);

        if ($num_row > 0 && $this->count_row_struktural($data->Prev_Per_No) == 0){

            //$this->set_mutation_movement_employee($data->Prev_Per_No, $data->Full_Name, $Position_Name, $data->Position_Name, $upload_history_id);
            $data_save = array(
                    'EmployeeID'=> $data->Prev_Per_No,
                    'EmployeeName'=> $data->Full_Name,
                    'Description'=> $data->Position_Name,
                    'GradeID'=> $data->PS_group,
                    'PositionID' => $data->Position_ID,                        
            );       
                
            $this->db->update('tp_diagram_frame', $data_save, array('COID'=> $primary_key));
        }    
    }


    public function count_row_table_profile_fungsional($primary_key, $BusA, $Org_unit, $Position_ID, $Position_Name, $Jenjang_Main_Grp_Text, $upload_history_id){
        
        $SQL = "SELECT Prev_Per_No, Full_Name, Position_Name, PS_group, Skala_Gaji_Dasar, Position_ID 
                FROM tp_profile 
                WHERE Bagan_Status ='1' AND 
                BusA ='".$BusA."' AND Org_unit ='".$Org_unit."' AND 
                Jenjang_Main_Grp_Text ='".$Jenjang_Main_Grp_Text."' ";

        $no = 1;

        $keyword_position = $this->cms_parse_keyword($Position_Name);

        if (!empty($keyword_position) && !is_null($keyword_position) && $keyword_position != ''){
          $words = array_diff(explode(',', $keyword_position),array(''));
          $last  = end($words);

          $SQL .= " AND (";

          foreach($words as $word){
              if ($last == $word){
                  $SQL .= " UPPER(Position_Name) LIKE '%".strtoupper($word)."%' ";
              }
              else{
                  $SQL .= " UPPER(Position_Name) LIKE '%".strtoupper($word)."%' OR ";
              }                            

              $no++;
          }

          $SQL .= " )";
        

        $query   = $this->db->query($SQL);
        $num_row = $query->num_rows();

        if ($num_row > 0){

            //$this->db->delete('tp_diagram_frame_detail', array('ParentID'=> $primary_key));

            foreach($query->result() as $data){

            if($this->count_row_fungsional($data->Prev_Per_No) == 0){
                $data_save = array(
                        'ParentID' => $primary_key,
                        'EmployeeID'=> $data->Prev_Per_No,
                        'EmployeeName'=> $data->Full_Name,
                        'GradeID'=> $data->PS_group,                        
                );
                    
                $this->db->insert('tp_diagram_frame_detail', $data_save);
              }
            }
        }
        }    
    }


    public function count_row_table_profile_fungsional_additional($primary_key, $BusA, $Org_unit, $Position_ID, $Position_Name, $Jenjang_Main_Grp_Text, $upload_history_id){

        $query = $this->db->select('Prev_Per_No, Full_Name, Position_Name, PS_group, Skala_Gaji_Dasar, Position_ID')
                          ->from('tp_profile')
                          ->where('Bagan_Status', 1)
                          ->where('BusA', $BusA)
                          ->where('Org_unit', $Org_unit)
                          ->where('Jenjang_Main_Grp_Text', $Jenjang_Main_Grp_Text)
                          ->like('upper(Position_Name)', strtoupper($Position_Name))
                          ->get();
        $no=1;

        $num_row = $query->num_rows();

        if ($num_row > 0){

            //$this->db->delete('tp_diagram_frame_detail', array('ParentID'=> $primary_key));

            foreach($query->result() as $data){

              $data_save = array(
                      'ParentID' => $primary_key,
                      'EmployeeID'=> $data->Prev_Per_No,
                      'EmployeeName'=> $data->Full_Name,
                      'GradeID'=> $data->PS_group,                        
              );
                  
              $this->db->insert('tp_diagram_frame_detail', $data_save);
            }
        }    
    }


    public function set_mutation_movement_employee($EmployeeID, $Full_Name, $old_position, $new_position, $upload_history_id){

        $data = array(
                'EmployeeID'=> $EmployeeID,
                'Full_Name'=> $Full_Name,
                'old_position'=> $old_position,
                'new_position'=> $new_position,                             
        );

        $this->db->insert('tp_mutation_movement', $data);                    
                    
    }

    public function set_mutation_movement_employee_struktural($business_area, $upload_history_id){
        $today = date('Y-m-d H:i:s');

        if ($upload_history_id > 1){
            $old_id = $upload_history_id-1;

            $old_table_diagram_frame = 'tp_diagram_frame_'.$old_id;
            $old_table_diagram_frame_detail = 'tp_diagram_frame_detail_'.$old_id;
        }
        else{
            $old_table_diagram_frame = 'tp_diagram_frame';
            $old_table_diagram_frame_detail = 'tp_diagram_frame_detail';
        }

        $query = $this->db->select('COID, APP, UnitID, EmployeeID, EmployeeName, Description')
                          ->from('tp_diagram_frame')
                          ->where('APP', $business_area)
                          ->where('Functional', 0)         
                          ->where('Status', 1)
                          ->where('EmployeeID !=', '')
                          ->order_by('COID', 'ASC') 
                          ->get();

        foreach($query->result() as $data){

            $this->db->select('COID, EmployeeID, EmployeeName, Description')
                 ->from($old_table_diagram_frame)
                 ->where('APP', $business_area)
                 ->where('Functional', 0)         
                 ->where('Status', 1)
                 ->where('EmployeeID', $data->EmployeeID)
                 ->where('Description !=', $data->Description)
                 ->where('EmployeeID !=', '');

            $db = $this->db->get();
            $num_row = $db->num_rows();
            $row = $db->row(0);

            if ($num_row > 0){

                $data_save = array(
                    'EmployeeID'=> $data->EmployeeID,
                    'Full_Name'=> $data->EmployeeName,
                    'old_position'=> $row->Description,
                    'new_position'=> $data->Description,
                    'bisnis_area'=> $data->APP,
                    'unit_id'=> $data->UnitID,
                    'cStatus' => 3,
                    'CreatedTime' => $today,                              
                );

                $this->db->insert('tp_mutation_movement', $data_save);
            }

        }

    }


    public function set_mutation_movement_employee_fungsional($business_area, $upload_history_id){
        $today = date('Y-m-d H:i:s');

        if ($upload_history_id > 1){
            $old_id = $upload_history_id-1;

            $old_table_diagram_frame = 'tp_diagram_frame_'.$old_id;
            $old_table_diagram_frame_detail = 'tp_diagram_frame_detail_'.$old_id;
        }
        else{
            $old_table_diagram_frame = 'tp_diagram_frame';
            $old_table_diagram_frame_detail = 'tp_diagram_frame_detail';
        }

        //$old_table_diagram_frame = 'tp_diagram_frame_'.$old_id;
        //$old_table_diagram_frame_detail = 'tp_diagram_frame_detail_'.$old_id;


        $query = $this->db->select('COID, APP, UnitID, tp_diagram_frame_detail.EmployeeID AS EmployeeID, tp_diagram_frame_detail.EmployeeName AS EmployeeName, Description')
                          ->from('tp_diagram_frame')
                          ->join('tp_diagram_frame_detail','tp_diagram_frame_detail.ParentID=tp_diagram_frame.COID')
                          ->where('APP', $business_area)
                          ->where('Functional', 1)         
                          ->where('Status', 1)
                          ->where('tp_diagram_frame_detail.EmployeeID !=', '')
                          ->order_by('COID', 'ASC') 
                          ->get();

        foreach($query->result() as $data){

            $this->db->select('COID, '.$old_table_diagram_frame_detail.'.EmployeeID AS EmployeeID, '.$old_table_diagram_frame_detail.'.EmployeeName AS EmployeeName, Description')
                 ->from($old_table_diagram_frame)
                 ->join($old_table_diagram_frame_detail,$old_table_diagram_frame.'.ParentID='.$old_table_diagram_frame.'.COID')
                 ->where('APP', $business_area)
                 ->where('Functional', 1)         
                 ->where('Status', 1)
                 ->where($old_table_diagram_frame_detail.'.EmployeeID', $data->EmployeeID)
                 ->where('Description !=', $data->Description)
                 ->where($old_table_diagram_frame_detail.'.EmployeeID !=', '');

            $db = $this->db->get();
            $num_row = $db->num_rows();
            $row = $db->row(0);

            if ($num_row > 0){

                $data_save = array(
                    'EmployeeID'=> $data->EmployeeID,
                    'Full_Name'=> $data->EmployeeName,
                    'old_position'=> $row->Description,
                    'new_position'=> $data->Description,
                    'bisnis_area'=> $data->APP,
                    'unit_id'=> $data->UnitID,
                    'cStatus' => 3,
                    'CreatedTime' => $today,                              
                );

                $this->db->insert('tp_mutation_movement', $data_save);
            }
        }
    }


    public function set_unmapping_employee_struktural_fungsional(){

        $today = date('Y-m-d H:i:s');        

        $query = $this->db->select('Prev_Per_No, Full_Name, Position_Name, BusA, Org_unit, Bagan_Status')
                          ->from('tp_profile')
                          ->where('Bagan_Status', 1)         
                          ->order_by('Prev_Per_No', 'ASC') 
                          ->get();

        foreach($query->result() as $data){

            $this->db->select('COID, EmployeeID, EmployeeName, Description')
                 ->from('tp_diagram_frame')
                 ->where('Functional', 0)         
                 ->where('Status', 1)
                 ->where('EmployeeID', $data->Prev_Per_No);

            $db = $this->db->get();
            $num_row = $db->num_rows();
            $row = $db->row(0);


            $query_fungsional = $this->db->select('ParentID, EmployeeID, EmployeeName')
                 ->from('tp_diagram_frame_detail')
                 ->where('EmployeeID', $data->Prev_Per_No);

            $db_fungsional = $query_fungsional->get();
            $num_row_fungsional = $db_fungsional->num_rows();
            $row_fungsional = $db_fungsional->row(0);


            if ($num_row <= 0 && $num_row_fungsional <= 0){

                $data_save = array(
                    'EmployeeID'=> $data->Prev_Per_No,
                    'Full_Name'=> $data->Full_Name,
                    'old_position'=> '',
                    'new_position'=> $data->Position_Name,
                    'bisnis_area'=> $data->BusA,
                    'unit_id'=> $data->Org_unit,
                    'cStatus' => 4,
                    'CreatedTime' => $today,                             
                );

                $this->db->insert('tp_mutation_movement', $data_save);
            }
        }
    }


    public function set_unmapping_employee_fungsional(){

        $today = date('Y-m-d H:i:s');

        $query = $this->db->select('Prev_Per_No, Full_Name, Position_Name, BusA, Org_unit, Bagan_Status')
                          ->from('tp_profile')
                          ->where('Bagan_Status', 1)         
                          ->order_by('Prev_Per_No', 'ASC') 
                          ->get();

        foreach($query->result() as $data){

            $this->db->select('ParentID, EmployeeID, EmployeeName')
                 ->from('tp_diagram_frame_detail')
                 ->where('EmployeeID', $data->Prev_Per_No);

            $db = $this->db->get();
            $num_row = $db->num_rows();
            $row = $db->row(0);

            if ($num_row <= 0){

                $data_save = array(
                    'EmployeeID'=> $data->Prev_Per_No,
                    'Full_Name'=> $data->Full_Name,
                    'old_position'=> '',
                    'new_position'=> $data->Position_Name,
                    'bisnis_area'=> $data->BusA,
                    'unit_id'=> $data->Org_unit,
                    'cStatus' => 4,
                    'CreatedTime' => $today,                             
                );

                $this->db->insert('tp_mutation_movement', $data_save);
            }
        }
    }


    public function copy_table_history($upload_history_id){

        if ($upload_history_id > 1){

            $old_id = $upload_history_id-1;

            $old_table_diagram_frame = 'tp_diagram_frame_'.$old_id;
            $old_table_diagram_frame_detail = 'tp_diagram_frame_detail_'.$old_id;

            /*
            $this->dbforge->add_key('COID', TRUE);
            $this->dbforge->create_table($old_table_diagram_frame);

            $this->dbforge->add_key('COIDNo', TRUE);
            $this->dbforge->create_table($old_table_diagram_frame_detail);

            $sql1   = "INSERT INTO ".$old_table_diagram_frame." SELECT * FROM tp_diagram_frame";
            $query1 = $this->db->query($sql1);

            $sql2   = "INSERT INTO ".$old_table_diagram_frame_detail." SELECT * FROM tp_diagram_frame_detail";
            $query2 = $this->db->query($sql2);.
            */

            if (!$this->db->table_exists($old_table_diagram_frame)){
                $this->db->query("CREATE TABLE ".$old_table_diagram_frame." AS (SELECT * FROM tp_diagram_frame)");
            }

            if (!$this->db->table_exists($old_table_diagram_frame_detail)){
                $this->db->query("CREATE TABLE ".$old_table_diagram_frame_detail." AS (SELECT * FROM tp_diagram_frame_detail)");
            }            
            
        }

        return TRUE;
    }

    public function recalculate_unmapping_employee_struktural_fungsional($tahun, $bulan){

       // struktural

        $SQL = "SELECT COID,APP,UnitID,Description,Full_Mapping_Code,EmployeeID FROM tp_diagram_frame WHERE EmployeeID IS NULL AND Status=1";
        $query   = $this->db->query($SQL);
        $num_row = $query->num_rows();
    
        foreach($query->result() as $data){
            
            $keyword_position = $this->cms_parse_keyword($data->Full_Mapping_Code);

            if (!empty($keyword_position) && !is_null($keyword_position) && $keyword_position != ''){

              $words   = array_diff(explode(',', $keyword_position),array(''));
              $last    = end($words);              

              foreach($words as $word){

                  $SQL_src = "SELECT mutation_id,Prev_Per_No,tp_profile.Full_Name,bisnis_area,Position_Name,Org_unit,Position_ID,Position_Name,PS_group FROM `tp_mutation_movement` INNER JOIN 
                              tp_profile ON EmployeeID=Prev_Per_No WHERE 
                              cStatus=4 AND 
                              YEAR(tp_mutation_movement.CreatedTime) ='".$tahun."' AND 
                              MONTH(tp_mutation_movement.CreatedTime) ='".$bulan."' AND 
                              UPPER(Position_Name) LIKE '%".strtoupper($word)."%' AND bisnis_area='".$data->APP."'";

                  $query_src  = $this->db->query($SQL_src);
                  $num_row_src = $query_src->num_rows();
                  $data_src = $query_src->row(0);

                  if ($num_row_src > 0){

                    $data_save = array(
                        'EmployeeID'=> $data_src->Prev_Per_No,
                        'EmployeeName'=> $data_src->Full_Name,
                        'Description'=> $data_src->Position_Name,
                        'GradeID'=> $data_src->PS_group,
                        'PositionID' => $data_src->Position_ID,                        
                    );       
                
                    $this->db->update('tp_diagram_frame', $data_save, array('COID'=> $data->COID));                    
                    $this->db->delete('tp_mutation_movement', array('mutation_id'=> $data_src->mutation_id));
                  }                  
              }
              
            }
        }
        
        // Fungsional
        $SQLF = "SELECT COUNT(tp_diagram_frame_detail.EmployeeID) AS Total,COID,APP,UnitID,Description,Full_Mapping_Code,tp_diagram_frame_detail.EmployeeID FROM 
                 tp_diagram_frame INNER JOIN tp_diagram_frame_detail ON tp_diagram_frame_detail.ParentID=tp_diagram_frame.COID WHERE 
                 tp_diagram_frame.Status=1 GROUP BY 
                 tp_diagram_frame_detail.ParentID HAVING 
                 Total=0";

        $Fquery   = $this->db->query($SQLF);
        $num_rowF = $Fquery->num_rows();
    
        foreach($Fquery->result() as $row){
            
            $Fkeyword_position = $this->cms_parse_keyword($row->Full_Mapping_Code);

            if (!empty($Fkeyword_position) && !is_null($Fkeyword_position) && $Fkeyword_position != ''){

              $words= array_diff(explode(',', $Fkeyword_position),array(''));
              $last    = end($words);              

              foreach($words as $word){

                  $FSQL_src = "SELECT mutation_id,Prev_Per_No,tp_profile.Full_Name,bisnis_area,Position_Name,Org_unit,Position_ID,Position_Name,PS_group FROM `tp_mutation_movement` INNER JOIN 
                              tp_profile ON EmployeeID=Prev_Per_No WHERE 
                              cStatus=4 AND 
                              YEAR(tp_mutation_movement.CreatedTime) ='".$tahun."' AND 
                              MONTH(tp_mutation_movement.CreatedTime) ='".$bulan."' AND 
                              UPPER(Position_Name) LIKE '%".strtoupper($word)."%' AND bisnis_area='".$row->APP."'";

                  $Fquery_src  = $this->db->query($FSQL_src);
                  $Fnum_row_src = $Fquery_src->num_rows();
                  $Fdata_src = $Fquery_src->row(0);

                  if ($Fnum_row_src > 0){
                    $Fdata_save = array(
                        'ParentID' => $row->COID,
                        'EmployeeID'=> $Fdata_src->Prev_Per_No,
                        'EmployeeName'=> $Fdata_src->Full_Name,
                        'GradeID'=> $Fdata_src->PS_group,
                        'PositionID' => $Fdata_src->Position_ID,                        
                    );                
                    
                    $this->db->insert('tp_diagram_frame_detail', $Fdata_save);                    
                    $this->db->delete('tp_mutation_movement', array('mutation_id'=> $Fdata_src->mutation_id));
                  }                  
              }
                            
            }
        }

    }


    public function set_mutation_movement_combine_employee($business_area, $upload_history_id){
        $today = date('Y-m-d H:i:s');

        if ($upload_history_id > 1){
            $old_id = $upload_history_id-1;

            $old_table_diagram_frame = 'tp_diagram_frame_'.$old_id;
            $old_table_diagram_frame_detail = 'tp_diagram_frame_detail_'.$old_id;
        }
        else{
            $old_table_diagram_frame = 'tp_diagram_frame';
            $old_table_diagram_frame_detail = 'tp_diagram_frame_detail';
        }

        $SQL = "SELECT * FROM (
SELECT b.COID AS COID, b.UnitID AS UnitID,b.APP AS Bus, b.Description AS Description, b.EmployeeID AS user_struktural, a.EmployeeID AS user_fungsioanal,COALESCE(a.EmployeeID,b.EmployeeID) AS Gabung 
                FROM `tp_diagram_frame_detail` AS a 
                RIGHT JOIN tp_diagram_frame AS b ON a.ParentID=b.COID 
                WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 GROUP BY COALESCE(a.EmployeeID,b.EmployeeID)
) AS g 
RIGHT JOIN (SELECT b.COID AS COID2, b.UnitID AS UnitID2,b.APP AS Bus2, b.Description AS Description2, b.EmployeeID AS user_struktural2, a.EmployeeID AS user_fungsioanal2,COALESCE(a.EmployeeID,b.EmployeeID) AS Gabung2 
                FROM ".$old_table_diagram_frame_detail." AS a 
                RIGHT JOIN ".$old_table_diagram_frame." AS b ON a.ParentID=b.COID 
                WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 GROUP BY COALESCE(a.EmployeeID,b.EmployeeID)) AS f ON f.Gabung2=g.Gabung AND g.Gabung IS NOT NULL 
ORDER BY g.Gabung ASC";

        $query  = $this->db->query($SQL);
        $num_row = $query->num_rows();        

        foreach($query->result() as $data){            

            if($data->UnitID != $data->UnitID2 || $data->Description != $data->Description2){

              $data_save = array(
                    'EmployeeID'=> $data->Gabung2,
                    'Full_Name'=> $this->data_table_value($table_name='tp_profile', $where_column='Prev_Per_No', $result_column='Full_Name', $data->Gabung2),
                    'old_position'=> $data->Description,
                    'new_position'=> $data->Description2,
                    'bisnis_area'=> $data->Bus2,
                    'unit_id'=> $data->UnitID2,
                    'cStatus' => 3,
                    'CreatedTime' => $today,                              
                );

                $this->db->insert('tp_mutation_movement', $data_save);
            }            

        }
  
    }


    public function ____set_mutation_movement_combine_employee($business_area, $upload_history_id){
        $today = date('Y-m-d H:i:s');

        if ($upload_history_id > 1){
            $old_id = $upload_history_id-1;

            $old_table_diagram_frame = 'tp_diagram_frame_'.$old_id;
            $old_table_diagram_frame_detail = 'tp_diagram_frame_detail_'.$old_id;
        }
        else{
            $old_table_diagram_frame = 'tp_diagram_frame';
            $old_table_diagram_frame_detail = 'tp_diagram_frame_detail';
        }

        $SQL = "SELECT b.COID AS COID, b.UnitID AS UnitID,b.APP AS Bus, b.Description AS Description, b.EmployeeID AS user_struktural, a.EmployeeID AS user_fungsioanal,COALESCE(a.EmployeeID,b.EmployeeID) AS Gabung 
                FROM `tp_diagram_frame_detail` AS a 
                RIGHT JOIN tp_diagram_frame AS b ON a.ParentID=b.COID 
                WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 GROUP BY COALESCE(a.EmployeeID,b.EmployeeID)";

        $query  = $this->db->query($SQL);
        $num_row = $query->num_rows();        

        foreach($query->result() as $data){         

            $total = $this->cari_karyawan_mutasi($data->Gabung, $data->UnitID, $data->Description, $old_table_diagram_frame, $old_table_diagram_frame_detail);

            if($total ==1 ){
              $data_save = array(
                    'EmployeeID'=> $data->Gabung,
                    'Full_Name'=> $this->data_table_value($table_name='tp_profile', $where_column='Prev_Per_No', $result_column='Full_Name', $data->Gabung),
                    'old_position'=> $data->Description,
                    'new_position'=> $data->Description,
                    'bisnis_area'=> $data->Bus,
                    'unit_id'=> $data->UnitID,
                    'cStatus' => 3,
                    'CreatedTime' => $today,                              
                );
                $this->db->insert('tp_mutation_movement', $data_save);
            }
          }        
  
    }


    public function cari_karyawan_mutasi($user_id,$unit_id, $description, $table_diagram_frame, $table_diagram_frame_detail){

      $SQL = "SELECT b.COID AS COID, b.UnitID AS UnitID,b.APP AS Bus, b.Description AS Description, b.EmployeeID AS user_struktural, a.EmployeeID AS user_fungsioanal,COALESCE(a.EmployeeID,b.EmployeeID) AS Gabung 
                FROM ".$table_diagram_frame_detail." AS a 
                RIGHT JOIN ".$table_diagram_frame." AS b ON a.ParentID=b.COID 
                WHERE COALESCE(a.EmployeeID,b.EmployeeID) ='".$user_id."' AND b.Status=1 LIMIT 1";

        $query  = $this->db->query($SQL);
        $num_row = $query->num_rows();
        $data    = $query->row(0);

        if($unit_id != $data->UnitID){
          return 1;
        }
        elseif($description != $data->Description){
          return 1;
        }
        else{
          return 0;
        }

    }


    public function data_table_value($table_name, $where_column, $result_column, $value){

        $this->db->select($result_column)
                 ->from($table_name)
                 ->where($where_column, $value);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            return $data->$result_column;
        }
        else{
            return '';
        }
    }

    public function count_row_struktural($EmployeeID){
        $SQL   = "SELECT count(EmployeeID) AS Total FROM tp_diagram_frame WHERE EmployeeID='".$EmployeeID."' AND EmployeeID IS NOT NULL AND EmployeeID != '' AND Status=1 AND Functional = 0 GROUP BY EmployeeID";
        $query = $this->db->query($SQL);
        $count = $query->num_rows();
        $data   = $query->row();

        if($count ==0){
            return 0;
        }
        else{
            return $data->Total;
        }    

    }


    public function count_row_fungsional($EmployeeID){
        $SQL   = "SELECT count(dtl.EmployeeID) AS Total FROM tp_diagram_frame_detail AS dtl INNER JOIN tp_diagram_frame AS hdr ON hdr.COID=dtl.ParentID WHERE dtl.EmployeeID='".$EmployeeID."' AND hdr.Status=1 AND hdr.Functional = 1 AND dtl.EmployeeID IS NOT NULL AND dtl.EmployeeID !='' GROUP BY dtl.EmployeeID";
        $query = $this->db->query($SQL);
        $count = $query->num_rows();
        $data   = $query->row();

        if($count == 0){
            return 0;
        }
        else{
            return $data->Total;
        }    

    }  


}