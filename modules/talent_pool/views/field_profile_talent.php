<?php
    $record_index = 0;
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/grocery_crud/css/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/grocery_crud/css/jquery_plugins/chosen/chosen.css'); ?>" />
<style type="text/css">
    #md_table_talent input[type="text"]{
        /*max-width:100px;*/
    }
    #md_table_talent .chzn-drop input[type="text"]{
        /*max-width:240px;*/
    }
    #md_table_talent th:last-child, #md_table_talent td:last-child{
        /*width: 60px;*/
    }

    #md_table_talent table, td {
        border-bottom: 1px solid #CCCCCC !important;
    }
    #md_table_talent table, th {
        border-bottom: 3px double #999999 !important;
    }

</style>

<div id="md_table_talent_container">
    <!--<div id="no-datamd_table_talent">No data</div>-->
    <table id="md_table_talent" class="table table-striped table-bordered row-border" style="width:100%;table-layout:fixed;">
        <thead>
            <tr>
                <th width="25%">{{ language:Semester }}</th>
                <th>{{ language:Value }}</th>
                <th width="10%">{{ language:Date }}</th>
                <th width="4%" class="text-center">#</th>
            </tr>
        </thead>
        <tbody>
            <!-- the data presentation be here -->
        </tbody>
    </table>
    <div class="fbutton">
        <span id="md_field_talent_add" class="add btn btn-default btn-flat">
            <i class="glyphicon glyphicon-plus-sign"></i> {{ language:Add Talent }} </span>
    </div>
    <br />
    <!-- This is the real input. If you want to catch the data, please json_decode this input's value -->
    <input id="md_real_field_talent_col" name="md_real_field_talent_col" type="hidden" />
    <!--<textarea id="md_real_field_talent_col" name="md_real_field_talent_col" style="width: 98%; height: 90%; border: 1px solid #999999;"></textarea>-->
</div>


<script type="text/javascript" src="<?php echo base_url('assets/grocery_crud/js/jquery_plugins/jquery.chosen.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/grocery_crud/js/jquery_plugins/jquery.ui.datetime.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/grocery_crud/js/jquery_plugins/jquery-ui-timepicker-addon.js'); ?>"></script>
<script type="text/javascript">

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // DATA INITIALIZATION
    //
    // * Prepare some global variables
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    var DATE_FORMAT = '<?php echo $date_format ?>';
    var OPTIONS_talent = <?php echo json_encode($options); ?>;
    var RECORD_INDEX_talent = <?php echo $record_index; ?>;
    var DATA_talent = {update:new Array(), insert:new Array(), delete:new Array()};
    var old_data = <?php echo json_encode($result); ?>;
    for(var i=0; i<old_data.length; i++){
        var row = old_data[i];
        var record_index = i;
        var primary_key = row['TalentID'];
        var data = row;
        delete data['TalentID'];
        DATA_talent.update.push({
            'record_index' : record_index,
            'primary_key' : primary_key,
            'data' : data,
        });
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ADD ROW FUNCTION
    //
    // * When "Add talent" clicked, this function is called without parameter.
    // * When page loaded for the first time, this function is called with value parameter
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function add_table_row_talent(value){
        // hide no-data div
        $("#no-datamd_table_talent").hide();
        $("#md_table_talent").show();

        var component = '<tr id="md_field_talent_tr_'+RECORD_INDEX_talent+'" class="md_field_talent_tr">';



        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //    FIELD "   SemesterID"
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        var field_value = '';
        if(typeof(value) != 'undefined' && value.hasOwnProperty('SemesterID')){
          field_value = value.SemesterID;
        }
        component += '<td>';
        component += '<select style="width: 250px;" id="md_field_talent_col_SemesterID_'+RECORD_INDEX_talent+'" record_index="'+RECORD_INDEX_talent+'" class="md_field_talent_col numeric chzn-select" column_name="SemesterID" >';
        var options = OPTIONS_talent.SemesterID;
        component += '<option value></option>';
        for(var i=0; i<options.length; i++){
          var option = options[i];
          var selected = '';
          if(option['value'] == field_value){
              selected = 'selected="selected"';
          }
          component += '<option value="'+option['value']+'" '+selected+'>'+option['caption']+'</option>';
        }
        component += '</select>';
        component += '</td>';



        
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //    FIELD "Talent_Value"
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        var field_value = '';
        if(typeof(value) != 'undefined' && value.hasOwnProperty('Talent_Value')){
          field_value = value.Talent_Value;
        }
        component += '<td>';
        component += '<input style="width:95%" id="md_field_talent_col_Talent_Value_'+RECORD_INDEX_talent+'" record_index="'+RECORD_INDEX_talent+'" class="md_field_talent_col form-control" column_name="Talent_Value" type="text" value="'+field_value+'"/>';
        component += '</td>';



        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //    FIELD "Last_Talent_Date"
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        var field_value = '';
        if(typeof(value) != 'undefined' && value.hasOwnProperty('Last_Talent_Date')){
          field_value = php_date_to_js(value.Last_Talent_Date);
        }
        component += '<td>';
        component += '<input style="width:90%" id="md_field_talent_col_Last_Talent_Date_'+RECORD_INDEX_talent+'" record_index="'+RECORD_INDEX_talent+'" class="md_field_talent_col form-control datepicker-input" column_name="Last_Talent_Date" type="text" value="'+field_value+'"/>';
        component += '</td>';

    
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // Delete Button
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        component += '<td class="text-center"><span class="delete-icon btn btn-default btn-flat md_field_talent_delete" record_index="'+RECORD_INDEX_talent+'"><i class="fa fa-trash-o"></i></span></td>';
        component += '</tr>';

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // Add component to table
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        $('#md_table_talent tbody').append(component);
        mutate_input_talent();

    } // end of ADD ROW FUNCTION



    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Main event handling program
    //
    // * Initialization
    // * md_field_talent_add.click (Add row)
    // * md_field_talent_delete.click (Delete row)
    // * md_field_talent_col.change (Edit cell)
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    $(document).ready(function(){

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // INITIALIZATION
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        synchronize_talent();
        for(var i=0; i<DATA_talent.update.length; i++){
            add_table_row_talent(DATA_talent.update[i].data);
            RECORD_INDEX_talent++;
        }
        synchronize_talent_table_width();

        // on resize, adjust the table width
        $(window).resize(function() {
            synchronize_talent_table_width();
        });


        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // md_field_talent_add.click (Add row)
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        $('#md_field_talent_add').click(function(){
            // new data
            var data = new Object();
            
            data.Talent_Value = '';
            data.SemesterID = '';
            data.Last_Talent_Date = '';
         
            // insert data to the DATA_talent
            DATA_talent.insert.push({
                'record_index' : RECORD_INDEX_talent,
                'primary_key' : '',
                'data' : data,
            });

            // add table's row
            add_table_row_talent(data);
            // add  by 1
            RECORD_INDEX_talent++;

            // synchronize to the md_real_field_talent_col
            synchronize_talent();
            synchronize_talent_table_width();
        });


        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // md_field_talent_delete.click (Delete row)
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        $('.md_field_talent_delete').live('click', function(){
            var record_index = $(this).attr('record_index');
            // remove the component
            $('#md_field_talent_tr_'+record_index).remove();

            var record_index_found = false;
            for(var i=0; i<DATA_talent.insert.length; i++){
                if(DATA_talent.insert[i].record_index == record_index){
                    record_index_found = true;
                    // delete element from insert
                    DATA_talent.insert.splice(i,1);
                    break;
                }
            }
            if(!record_index_found){
                for(var i=0; i<DATA_talent.update.length; i++){
                    if(DATA_talent.update[i].record_index == record_index){
                        record_index_found = true;
                        var primary_key = DATA_talent.update[i].primary_key;
                        // delete element from update
                        DATA_talent.update.splice(i,1);
                        // add it to delete
                        DATA_talent.delete.push({
                            'record_index':record_index,
                            'primary_key':primary_key
                        });
                        break;
                    }
                }
            }
            synchronize_talent();
        });


        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // md_field_talent_col.change (Edit cell)
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        $('.md_field_talent_col').live('change', function(){
            var value = $(this).val();
            var column_name = $(this).attr('column_name');
            var record_index = $(this).attr('record_index');
            var record_index_found = false;
            // date picker
            if($(this).hasClass('datepicker-input')){
                value = js_date_to_php(value);
            }
            else if($(this).hasClass('datetime-input')){
                value = js_datetime_to_php(value);
            }
            if(typeof(value)=='undefined'){
                value = '';
            }
            for(var i=0; i<DATA_talent.insert.length; i++){
                if(DATA_talent.insert[i].record_index == record_index){
                    record_index_found = true;
                    // insert value
                    eval('DATA_talent.insert['+i+'].data.'+column_name+' = '+JSON.stringify(value)+';');
                    break;
                }
            }
            if(!record_index_found){
                for(var i=0; i<DATA_talent.update.length; i++){
                    if(DATA_talent.update[i].record_index == record_index){
                        record_index_found = true;
                        // edit value
                        eval('DATA_talent.update['+i+'].data.'+column_name+' = '+JSON.stringify(value)+';');
                        break;
                    }
                }
            }
            synchronize_talent();
        });


    });

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // reset field on save
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    $(document).ajaxSuccess(function(event, xhr, settings) {
        if (settings.url == "{{ module_site_url }}raw_data_management/index/insert") {
            response = $.parseJSON(xhr.responseText);
            if(response.success == true){
                DATA_talent = {update:new Array(), insert:new Array(), delete:new Array()};
                $('#md_table_talent tr').not(':first').remove();
                synchronize_talent();
            }
        }
    });

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // synchronize data to md_real_field_talent_col.
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function synchronize_talent(){
        $('#md_real_field_talent_col').val(JSON.stringify(DATA_talent));
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // synchronize table width (called on resize and add).
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function synchronize_talent_table_width(){
        var parent_width = $("#md_table_talent_container").parent().parent().width();
        if($("#md_table_talent_container table:visible").length > 0){
            $("#md_table_talent_container").width(parent_width);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // function to mutate input
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function mutate_input_talent(){
        // datepicker-input
        $('#md_table_talent .datepicker-input').datepicker({
                dateFormat: js_date_format,
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c+100",
        });
        // date-picker-input-clear
        $('#md_table_talent .datepicker-input-clear').click(function(){
            $(this).parent().find('.datepicker-input').val('');
            return false;
        });
        // datetime-input
        $('#md_table_talent .datetime-input').datetimepicker({
            timeFormat: 'HH:mm:ss',
            dateFormat: js_date_format,
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true
        });
        
        $('#md_table_talent .datetime-input-clear').button();
        
        $('#md_table_talent .datetime-input-clear').click(function(){
            $(this).parent().find('.datetime-input').val("");
            return false;
        });
        // chzn-select
        $("#md_table_talent .chzn-select").chosen({allow_single_deselect: true});
        // numeric
        $('#md_table_talent .numeric').numeric();
        $('#md_table_talent .numeric').keydown(function(e){
            if(e.keyCode == 38)
            {
                if(IsNumeric($(this).val()))
                {
                    var new_number = parseInt($(this).val()) + 1;
                    $(this).val(new_number);
                }else if($(this).val().length == 0)
                {
                    var new_number = 1;
                    $(this).val(new_number);
                }
            }
            else if(e.keyCode == 40)
            {
                if(IsNumeric($(this).val()))
                {
                    var new_number = parseInt($(this).val()) - 1;
                    $(this).val(new_number);
                }else if($(this).val().length == 0)
                {
                    var new_number = -1;
                    $(this).val(new_number);
                }
            }
            $(this).trigger('change');
        });

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // General Functions
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    function js_datetime_to_php(js_datetime){
        if(typeof(js_datetime)=='undefined' || js_datetime == ''){
            return '';
        }
        var datetime_array = js_datetime.split(' ');
        var js_date = datetime_array[0];
        var time = datetime_array[1];
        var php_date = js_date_to_php(js_date);
        return php_date + ' ' + time;
    }
    function php_datetime_to_js(php_datetime){
        if(typeof(php_datetime)=='undefined' || php_datetime == ''){
            return '';
        }
        var datetime_array = php_datetime.split(' ');
        var php_date = datetime_array[0];
        var time = datetime_array[1];
        var js_date = php_date_to_js(php_date);
        return js_date + ' ' + time;
    }

    function js_date_to_php(js_date){
        if(typeof(js_date)=='undefined' || js_date == ''){
            return '';
        }
        var date = '';
        var month = '';
        var year = '';
        var php_date = '';
        if(DATE_FORMAT == 'uk-date'){
            var date_array = js_date.split('/');
            day = date_array[0];
            month = date_array[1];
            year = date_array[2];
            php_date = year+'-'+month+'-'+day;
        }else if(DATE_FORMAT == 'us-date'){
            var date_array = js_date.split('/');
            day = date_array[1];
            month = date_array[0];
            year = date_array[2];
            php_date = year+'-'+month+'-'+day;
        }else if(DATE_FORMAT == 'sql-date'){
            var date_array = js_date.split('-');
            day = date_array[2];
            month = date_array[1];
            year = date_array[0];
            php_date = year+'-'+month+'-'+day;
        }
        return php_date;
    }


    function php_date_to_js(php_date){
        if(typeof(php_date)=='undefined' || php_date == '' || php_date == null){
            return '';
        }
        var date_array = php_date.split('-');
        var year = date_array[0];
        var month = date_array[1];
        var day = date_array[2];
        if(DATE_FORMAT == 'uk-date'){
            return day+'/'+month+'/'+year;
        }else if(DATE_FORMAT == 'us-date'){
            return month+'/'+date+'/'+year;
        }else if(DATE_FORMAT == 'sql-date'){
            return year+'-'+month+'-'+day;
        }else{
            return '';
        }
    }

    function IsNumeric(input){
        return (input - 0) == input && input.length > 0;
    }

</script>