<?php
    $record_index = 0;
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/grocery_crud/css/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/grocery_crud/css/jquery_plugins/chosen/chosen.css'); ?>" />
<style type="text/css">
    #md_table_occupation input[type="text"]{
        /*max-width:100px;*/
    }
    #md_table_occupation .chzn-drop input[type="text"]{
        /*max-width:240px;*/
    }
    #md_table_occupation th:last-child, #md_table_occupation td:last-child{
        /*width: 60px;*/
    }

    #md_table_occupation table, td {
        border-bottom: 1px solid #CCCCCC !important;
    }
    #md_table_occupation table, th {
        border-bottom: 3px double #999999 !important;
    }

</style>

<div id="md_table_occupation_container">
    <table id="md_table_occupation" class="table table-striped table-bordered row-border" style="width:100%;table-layout:fixed;">
        <thead>
            <tr>
                <th width="25%" class="text-center">{{ language:Position }}</th>
                <th width="25%" class="text-center">{{ language:Unit }}</th>                
                <th width="10%" class="text-center">{{ language:Start Date }}</th>
                <th width="10%" class="text-center">{{ language:End Date }}</th>
                <th class="text-center">{{ language:Description }}</th>
                <th width="4%" class="text-center">#</th>
            </tr>
        </thead>
        <tbody>
            <!-- the data presentation be here -->
        </tbody>
    </table>
    <div class="fbutton">
        <span id="md_field_occupation_add" class="add btn btn-default btn-flat">
            <i class="glyphicon glyphicon-plus-sign"></i> {{ language:Add occupation }} </span>
    </div>
    <br />
    <!-- This is the real input. If you want to catch the data, please json_decode this input's value -->
    <input id="md_real_field_occupation_col" name="md_real_field_occupation_col" type="hidden" />
</div>

<script type="text/javascript" src="<?php echo base_url('assets/grocery_crud/js/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/grocery_crud/js/jquery_plugins/jquery.chosen.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/grocery_crud/js/jquery_plugins/jquery.ui.datetime.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/grocery_crud/js/jquery_plugins/jquery-ui-timepicker-addon.js'); ?>"></script>
<script type="text/javascript">

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // DATA INITIALIZATION
    //
    // * Prepare some global variables
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    var DATE_FORMAT = '<?php echo $date_format ?>';
    var OPTIONS_occupation = <?php echo json_encode($options); ?>;
    var RECORD_INDEX_occupation = <?php echo $record_index; ?>;
    var DATA_occupation = {update:new Array(), insert:new Array(), delete:new Array()};
    var old_data = <?php echo json_encode($result); ?>;
    for(var i=0; i<old_data.length; i++){
        var row = old_data[i];
        var record_index = i;
        var primary_key = row['OccupationID'];
        var data = row;
        delete data['OccupationID'];
        DATA_occupation.update.push({
            'record_index' : record_index,
            'primary_key' : primary_key,
            'data' : data,
        });
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ADD ROW FUNCTION
    //
    // * When "Add occupation" clicked, this function is called without parameter.
    // * When page loaded for the first time, this function is called with value parameter
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function add_table_row_occupation(value){
        // hide no-data div
        $("#no-datamd_table_occupation").hide();
        $("#md_table_occupation").show();

        var component = '<tr id="md_field_occupation_tr_'+RECORD_INDEX_occupation+'" class="md_field_occupation_tr">';      

        
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //    FIELD "PositionID"
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        var field_value = '';
        if(typeof(value) != 'undefined' && value.hasOwnProperty('PositionID')){
          field_value = value.PositionID;
        }
        component += '<td>';
        component += '<select style="width: 250px;" id="md_field_occupation_col_PositionID_'+RECORD_INDEX_occupation+'" record_index="'+RECORD_INDEX_occupation+'" class="md_field_occupation_col numeric chzn-select" column_name="PositionID" >';
        var options = OPTIONS_occupation.PositionID;
        component += '<option value></option>';
        for(var i=0; i<options.length; i++){
          var option = options[i];
          var selected = '';
          if(option['value'] == field_value){
              selected = 'selected="selected"';
          }
          component += '<option value="'+option['value']+'" '+selected+'>'+option['caption']+'</option>';
        }
        component += '</select>';
        component += '</td>';


        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //    FIELD "UnitID"
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        var field_value = '';
        if(typeof(value) != 'undefined' && value.hasOwnProperty('UnitID')){
          field_value = value.UnitID;
        }
        component += '<td>';
        component += '<select style="width: 250px;" id="md_field_occupation_col_UnitID_'+RECORD_INDEX_occupation+'" record_index="'+RECORD_INDEX_occupation+'" class="md_field_occupation_col numeric chzn-select" column_name="UnitID" >';
        var options = OPTIONS_occupation.UnitID;
        component += '<option value></option>';
        for(var i=0; i<options.length; i++){
          var option = options[i];
          var selected = '';
          if(option['value'] == field_value){
              selected = 'selected="selected"';
          }
          component += '<option value="'+option['value']+'" '+selected+'>'+option['caption']+'</option>';
        }
        component += '</select>';
        component += '</td>';


        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //    FIELD "StartDate"
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        var field_value = '';
        if(typeof(value) != 'undefined' && value.hasOwnProperty('StartDate')){
          field_value = php_date_to_js(value.StartDate);
        }
        component += '<td>';
        component += '<input style="width:90%" id="md_field_occupation_col_StartDate_'+RECORD_INDEX_occupation+'" record_index="'+RECORD_INDEX_occupation+'" class="md_field_occupation_col form-control datepicker-input" column_name="StartDate" type="text" value="'+field_value+'"/>';
        component += '</td>';


        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //    FIELD "EndDate"
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        var field_value = '';
        if(typeof(value) != 'undefined' && value.hasOwnProperty('EndDate')){
          field_value = php_date_to_js(value.EndDate);
        }
        component += '<td>';
        component += '<input style="width:90%" id="md_field_occupation_col_EndDate_'+RECORD_INDEX_occupation+'" record_index="'+RECORD_INDEX_occupation+'" class="md_field_occupation_col form-control datepicker-input" column_name="EndDate" type="text" value="'+field_value+'"/>';
        component += '</td>';


       

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //    FIELD "Description"
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        var field_value = '';
        if(typeof(value) != 'undefined' && value.hasOwnProperty('Description')){
          field_value = value.Description;
        }
        component += '<td>';
        component += '<textarea style="width: 92% !important; min-width: 92%; max-width: 92%;" id="md_field_occupation_col_Description_'+RECORD_INDEX_occupation+'" record_index="'+RECORD_INDEX_occupation+'" class="md_field_occupation_col form-control" column_name="Description">'+field_value+'</textarea>';
        component += '</td>';        


        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // Delete Button
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        component += '<td class="text-center"><span class="delete-icon btn btn-default btn-flat md_field_occupation_delete" record_index="'+RECORD_INDEX_occupation+'"><i class="fa fa-trash-o"></i></span></td>';
        component += '</tr>';

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // Add component to table
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        $('#md_table_occupation tbody').append(component);
        mutate_input_occupation();

    } // end of ADD ROW FUNCTION



    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Main event handling program
    //
    // * Initialization
    // * md_field_occupation_add.click (Add row)
    // * md_field_occupation_delete.click (Delete row)
    // * md_field_occupation_col.change (Edit cell)
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    $(document).ready(function(){

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // INITIALIZATION
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        synchronize_occupation();
        for(var i=0; i<DATA_occupation.update.length; i++){
            add_table_row_occupation(DATA_occupation.update[i].data);
            RECORD_INDEX_occupation++;
        }
        synchronize_occupation_table_width();

        // on resize, adjust the table width
        $(window).resize(function() {
            synchronize_occupation_table_width();
        });


        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // md_field_occupation_add.click (Add row)
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        $('#md_field_occupation_add').click(function(){
            // new data
            var data = new Object();
            
            data.PositionID = '';
            data.Description = '';
            data.UnitID = '';
            data.StartDate = '';
            data.EndDate = '';
            // insert data to the DATA_occupation
            DATA_occupation.insert.push({
                'record_index' : RECORD_INDEX_occupation,
                'primary_key' : '',
                'data' : data,
            });

            // add table's row
            add_table_row_occupation(data);
            // add  by 1
            RECORD_INDEX_occupation++;

            // synchronize to the md_real_field_occupation_col
            synchronize_occupation();
            synchronize_occupation_table_width();
        });


        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // md_field_occupation_delete.click (Delete row)
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        $('.md_field_occupation_delete').live('click', function(){
            var record_index = $(this).attr('record_index');
            // remove the component
            $('#md_field_occupation_tr_'+record_index).remove();

            var record_index_found = false;
            for(var i=0; i<DATA_occupation.insert.length; i++){
                if(DATA_occupation.insert[i].record_index == record_index){
                    record_index_found = true;
                    // delete element from insert
                    DATA_occupation.insert.splice(i,1);
                    break;
                }
            }
            if(!record_index_found){
                for(var i=0; i<DATA_occupation.update.length; i++){
                    if(DATA_occupation.update[i].record_index == record_index){
                        record_index_found = true;
                        var primary_key = DATA_occupation.update[i].primary_key;
                        // delete element from update
                        DATA_occupation.update.splice(i,1);
                        // add it to delete
                        DATA_occupation.delete.push({
                            'record_index':record_index,
                            'primary_key':primary_key
                        });
                        break;
                    }
                }
            }
            synchronize_occupation();
        });


        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // md_field_occupation_col.change (Edit cell)
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        $('.md_field_occupation_col').live('change', function(){
            var value = $(this).val();
            var column_name = $(this).attr('column_name');
            var record_index = $(this).attr('record_index');
            var record_index_found = false;
            // date picker
            if($(this).hasClass('datepicker-input')){
                value = js_date_to_php(value);
            }
            else if($(this).hasClass('datetime-input')){
                value = js_datetime_to_php(value);
            }
            if(typeof(value)=='undefined'){
                value = '';
            }
            for(var i=0; i<DATA_occupation.insert.length; i++){
                if(DATA_occupation.insert[i].record_index == record_index){
                    record_index_found = true;
                    // insert value
                    eval('DATA_occupation.insert['+i+'].data.'+column_name+' = '+JSON.stringify(value)+';');
                    break;
                }
            }
            if(!record_index_found){
                for(var i=0; i<DATA_occupation.update.length; i++){
                    if(DATA_occupation.update[i].record_index == record_index){
                        record_index_found = true;
                        // edit value
                        eval('DATA_occupation.update['+i+'].data.'+column_name+' = '+JSON.stringify(value)+';');
                        break;
                    }
                }
            }
            synchronize_occupation();
        });


    });

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // reset field on save
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    $(document).ajaxSuccess(function(event, xhr, settings) {
        if (settings.url == "{{ module_site_url }}raw_data_management/index/insert") {
            response = $.parseJSON(xhr.responseText);
            if(response.success == true){
                DATA_occupation = {update:new Array(), insert:new Array(), delete:new Array()};
                $('#md_table_occupation tr').not(':first').remove();
                synchronize_occupation();
            }
        }
    });

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // synchronize data to md_real_field_occupation_col.
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function synchronize_occupation(){
        $('#md_real_field_occupation_col').val(JSON.stringify(DATA_occupation));
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // synchronize table width (called on resize and add).
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function synchronize_occupation_table_width(){
        var parent_width = $("#md_table_occupation_container").parent().parent().width();
        if($("#md_table_occupation_container table:visible").length > 0){
            $("#md_table_occupation_container").width(parent_width);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // function to mutate input
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function mutate_input_occupation(){
        // datepicker-input
        $('#md_table_occupation .datepicker-input').datepicker({
                dateFormat: js_date_format,
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c+100",
        });
        // date-picker-input-clear
        $('#md_table_occupation .datepicker-input-clear').click(function(){
            $(this).parent().find('.datepicker-input').val('');
            return false;
        });
        // datetime-input
        $('#md_table_occupation .datetime-input').datetimepicker({
            timeFormat: 'HH:mm:ss',
            dateFormat: js_date_format,
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true
        });
        
        $('#md_table_occupation .datetime-input-clear').button();
        
        $('#md_table_occupation .datetime-input-clear').click(function(){
            $(this).parent().find('.datetime-input').val("");
            return false;
        });
        // chzn-select
        $("#md_table_occupation .chzn-select").chosen({allow_single_deselect: true});
        // numeric
        $('#md_table_occupation .numeric').numeric();
        $('#md_table_occupation .numeric').keydown(function(e){
            if(e.keyCode == 38)
            {
                if(IsNumeric($(this).val()))
                {
                    var new_number = parseInt($(this).val()) + 1;
                    $(this).val(new_number);
                }else if($(this).val().length == 0)
                {
                    var new_number = 1;
                    $(this).val(new_number);
                }
            }
            else if(e.keyCode == 40)
            {
                if(IsNumeric($(this).val()))
                {
                    var new_number = parseInt($(this).val()) - 1;
                    $(this).val(new_number);
                }else if($(this).val().length == 0)
                {
                    var new_number = -1;
                    $(this).val(new_number);
                }
            }
            $(this).trigger('change');
        });

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // General Functions
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    function js_datetime_to_php(js_datetime){
        if(typeof(js_datetime)=='undefined' || js_datetime == ''){
            return '';
        }
        var datetime_array = js_datetime.split(' ');
        var js_date = datetime_array[0];
        var time = datetime_array[1];
        var php_date = js_date_to_php(js_date);
        return php_date + ' ' + time;
    }
    function php_datetime_to_js(php_datetime){
        if(typeof(php_datetime)=='undefined' || php_datetime == ''){
            return '';
        }
        var datetime_array = php_datetime.split(' ');
        var php_date = datetime_array[0];
        var time = datetime_array[1];
        var js_date = php_date_to_js(php_date);
        return js_date + ' ' + time;
    }

    function js_date_to_php(js_date){
        if(typeof(js_date)=='undefined' || js_date == ''){
            return '';
        }
        var date = '';
        var month = '';
        var year = '';
        var php_date = '';
        if(DATE_FORMAT == 'uk-date'){
            var date_array = js_date.split('/');
            day = date_array[0];
            month = date_array[1];
            year = date_array[2];
            php_date = year+'-'+month+'-'+day;
        }else if(DATE_FORMAT == 'us-date'){
            var date_array = js_date.split('/');
            day = date_array[1];
            month = date_array[0];
            year = date_array[2];
            php_date = year+'-'+month+'-'+day;
        }else if(DATE_FORMAT == 'sql-date'){
            var date_array = js_date.split('-');
            day = date_array[2];
            month = date_array[1];
            year = date_array[0];
            php_date = year+'-'+month+'-'+day;
        }
        return php_date;
    }


    function php_date_to_js(php_date){
        if(typeof(php_date)=='undefined' || php_date == '' || php_date == null){
            return '';
        }

        
        var date_array = php_date.split('-');
        var year = date_array[0];
        var month = date_array[1];
        var day = date_array[2];
        if(DATE_FORMAT == 'uk-date'){
            return day+'/'+month+'/'+year;
        }else if(DATE_FORMAT == 'us-date'){
            return month+'/'+date+'/'+year;
        }else if(DATE_FORMAT == 'sql-date'){
            return year+'-'+month+'-'+day;
        }else{
            return '';
        }
        
    }

    function IsNumeric(input){
        return (input - 0) == input && input.length > 0;
    }

</script>