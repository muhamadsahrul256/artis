<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

 <!-- row -->
 <!--
      <div class="row">
        <div class="col-md-12">
          <ul class="timeline">
           
            <li class="time-label">
                  <span class="bg-red">
                    10 Feb. 2014
                  </span>
            </li>
            
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                <h3 class="timeline-header">Support Team sent you an email</h3>

                <div class="timeline-body">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut efficitur turpis, id tempor odio. Curabitur sed tortor nec massa pulvinar maximus vel eu mi. Maecenas cursus nibh sed arcu sodales feugiat vel eu risus. Praesent odio dolor, tristique aliquet velit sed, consequat dictum purus. Etiam eleifend vehicula sagittis. Duis vel pretium nunc. Fusce euismod turpis sed erat iaculis, nec blandit tortor pulvinar. Curabitur pretium tempus leo. Etiam auctor volutpat maximus. Curabitur semper ac eros eu gravida. Vivamus consectetur fermentum nulla vel ultrices. 
                </div>
                <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs">Read more</a>
                  <a class="btn btn-danger btn-xs">Delete</a>
                </div>
              </div>
            </li>
           
            <li>
              <i class="fa fa-user bg-aqua"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

                <h3 class="timeline-header no-border"><a href="#">MAN</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
              </div>
            </li>
           
            <li>
              <i class="fa fa-comments bg-yellow"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                <h3 class="timeline-header"><a href="#">AS MAN</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>

                <div class="timeline-body">
                  Integer sit amet dapibus massa, sed feugiat diam. Vestibulum in consectetur urna. Nunc lacinia quam felis, sagittis tempor diam semper et. Donec gravida sem non leo gravida tempus. Curabitur lacinia feugiat dapibus. Cras a ligula et nunc faucibus posuere. Pellentesque nec neque in urna consequat feugiat eget convallis eros. Morbi faucibus quis mi nec aliquet.
                </div>
                <div class="timeline-footer">
                  <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                </div>
              </div>
            </li>
           
            <li class="time-label">
                  <span class="bg-green">
                    3 Jan. 2014
                  </span>
            </li>
           
           
        
           
            <li>
              <i class="fa fa-clock-o bg-gray"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

                <h3 class="timeline-header"><a href="#">SPV</a> uploaded new photos</h3>

                
              </div>

            </li>
          </ul>
        </div>
       
      </div>

-->

<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{ language:Position History }}</h3>
            </div>
            <div class="box-body">


    <div class="col-md-12">
        <ul class="timeline">

<?php

function basic_date_format($date){

    if (is_null($date) || $date =='0000-00-00'){
        $tanggal = '';
    }
    else{
        $tanggal = date('d-M-Y', strtotime($date));
    }
    return $tanggal;
}


$SQL    = "SELECT * FROM tp_profile_position_history WHERE EmployeeID='".$primary_key."' ORDER BY Start_Date DESC";
$query  = $this->db->query($SQL);  



foreach($query->result() as $data){ ?>

      <li class="time-label">
          <span class="bg-green">
              <?php echo basic_date_format($data->Start_Date).' s/d '.basic_date_format($data->End_Date); ?>
          </span>
      </li>            
      <li>
            
          <div class="timeline-item">
            <h3 class="timeline-header"><strong><?php echo $data->Position_Name; ?></strong></h3>
                <div class="timeline-body">
                    <?php echo $data->Nama_Panjang_Posisi_SIMKP.' dengan nomor SK '.$data->No_SK_Organizational_Assignmen; ?> 
                </div>
                
          </div>
      </li>

<?php
}



?>

        </ul>
    </div>

    <div class="box-footer">
    
        <a href="{{ base_url }}talent_pool/mutation_movement" class="btn btn-default btn-large btn-flat"><i class="fa fa-reply"></i> {{ language:Back }}</a>
                
    </div>


</div>
</div>