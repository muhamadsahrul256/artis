<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$asset = new CMS_Asset();
foreach($css_files as $file){
    $asset->add_css($file);
}
echo $asset->compile_css();

foreach($js_files as $file){
    $asset->add_js($file);
}

echo $asset->compile_js();

// For every content of option tag, this will replace '&nbsp;' with ' '
function __ommit_nbsp($matches){
    return $matches[1].str_replace('&nbsp;', ' ', $matches[2]).$matches[3];
}

echo preg_replace_callback('/(<option[^<>]*>)(.*?)(<\/option>)/si', '__ommit_nbsp', $output);
?>

<script type="text/javascript">

	function goToPage(id) {
        var node = document.getElementById(id);  
           	if( node &&
            node.tagName == "SELECT" ) {
            window.location.href = node.options[node.selectedIndex].value;    
        }  
    }
    
    $('#select_period').live('change', function(event){
        event.preventDefault();
        var list = new Array();
        $('input[type=checkbox]').each(function() {
            if (this.checked) {
                list.push(this.value);
            }
        });        
    });

    var OPTIONS_period = <?php echo json_encode($filter_period); ?>;
    var dropdown_period = <?php echo json_encode($dropdown_period); ?>;
    var result_data    = <?php echo json_encode($result_data); ?>;

/*
    $(document).ready(function(){
        $('.box-tools').prepend('<div class="form-group">'+             
                  				'<select class="form-control" data-show-subtext="true" onchange=goToPage("select") name="select" id="select">'+
                    			OPTIONS_period+
                  				'</select>'+
                				'</div>');

        $('#result_of_data').prepend('<p class="pull-right">'+result_data+'</p>');
    })

*/

    $(document).ready(function(){
        $('.box-tools').prepend('<div class="btn-group">'+             
                                '<button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button><ul class="dropdown-menu pull-right" role="menu">'+
                                dropdown_period+
                                '</ul>'+
                                '</div>&nbsp;');

        $('#result_of_data').prepend('<p class="pull-right">'+result_data+'</p>');
    })


</script>