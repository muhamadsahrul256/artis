<?php

$lang['Salary Scale'] = 'Salary Scale';
$lang['Business Area'] = 'Business Area';
$lang['Birth Date'] = 'Birth Date';
$lang['Photos'] = 'Photos';
$lang['Training'] = 'Training';
$lang['Description'] = 'Description';
$lang['Date'] = 'Date';
$lang['Address'] = 'Address';
$lang['Begin'] = 'Begin';
$lang['Finish'] = 'Finish';
$lang['Instance'] = 'Instance';
$lang['Level'] = 'Level';