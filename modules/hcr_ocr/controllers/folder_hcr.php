<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class folder_hcr extends CMS_Priv_Strict_Controller {

    protected $URL_MAP     = array();
    protected $folder_name = '';
    protected $today       = '';

    function __construct(){
        parent::__construct();
        $ci = &get_instance();
        $this->folder_name();        
    }


    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    private function make_crud(){

        $this->today = date('Y-m-d H:i:s');

        $crud = $this->new_crud();
        // this is just for code completion
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();

        if ($this->cms_have_privilege('add') != 1) $crud->unset_add();
        if ($this->cms_have_privilege('edit') != 1) $crud->unset_edit();
        if ($this->cms_have_privilege('delete') != 1) $crud->unset_delete();
        if ($this->cms_have_privilege('export') != 1) $crud->unset_export();
        if ($this->cms_have_privilege('download') == 1) $crud->add_action('Download', '', '','ui-icon-image',array($this,'_callback_download_url'));
         
        // $crud->unset_edit();
        // $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();        
        $crud->unset_texteditor('Description');   
       
        $crud->set_language($this->cms_language());
        $crud->set_theme('no-flexigrid-document-upload');

        // table name
        $crud->set_table($this->folder_name);
        $crud->order_by('DocTitle', 'ASC');

        // primary key
        $crud->set_primary_key('DocID');

        // set subject
        $crud->set_subject($this->cms_lang('HCR'));

        // displayed columns on list
        $crud->columns('DocTitle','Description','Visible','CreatedBy');
        // displayed columns on edit operation
        $crud->edit_fields('DocTitle','Description','DocURL','Visible','CreatedBy','UpdatedBy');
        // displayed columns on add operation
        $crud->add_fields('DocTitle','Description','DocURL','Visible','CreatedBy','CreatedTime','UpdatedBy');


        $crud->unset_add_fields('CreatedBy','CreatedTime','UpdatedBy');
        $crud->unset_edit_fields('UpdatedBy');
        $crud->field_type('CreatedBy', 'hidden', $this->cms_user_id());
        $crud->field_type('CreatedTime', 'hidden', $this->today);
        $crud->field_type('UpdatedBy', 'hidden', $this->cms_user_id());

        // caption of each columns
        $crud->display_as('DocTitle', $this->cms_lang('Document Title'));
        $crud->display_as('Description', $this->cms_lang('Description'));
        $crud->display_as('DocSize', $this->cms_lang('Size'));
        $crud->display_as('Downloaded', $this->cms_lang('Downloaded'));
        $crud->display_as('CreatedBy', $this->cms_lang('Created By'));
        $crud->display_as('DocURL', $this->cms_lang('File PDF (max 1MB)'));
        $crud->display_as('Metadata', $this->cms_lang('Document Keyword (Comma Separated)'));
        $crud->display_as('Visible', $this->cms_lang('Visible'));
        

        $crud->required_fields('DocTitle','DocURL');

        $crud->field_type('Visible', 'true_false', array($this->cms_lang('No'), $this->cms_lang('Yes')));
    
        $crud->set_config_upload('pdf', '1MB');

        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        $crud->callback_column('CreatedBy',array($this,'_callback_column_CreatedBy'));

        $crud->set_field_upload('DocURL','modules/'.$this->cms_module_path().'/assets/uploads/'.$this->folder_name);

        

        $this->crud = $crud;
        return $crud;
    }

    public function index(){
        $crud = $this->make_crud();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view('hcr_ocr/'.$this->folder_name.'_view', $output,
            $this->cms_complete_navigation_name($this->folder_name));
    }

    public function delete_selection(){
        $crud = $this->make_crud();
        if(!$crud->unset_delete){
            $id_list = json_decode($this->input->post('data'));
            foreach($id_list as $id){
                if($this->_before_delete($id)){
                    $this->db->delete($this->folder_name,array('DocID'=>$id));
                    $this->_after_delete($id);
                }
            }
        }
    }

    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        // HINT : Put your code here

        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_delete($primary_key){

        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){

        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }

    public function folder_name(){
        $this->folder_name = $this->uri->segment(2);
        return $this;
    }

    public function _callback_column_CreatedBy($value, $row){
        return $this->cms_complete_table_data($table_name='main_user', $where_column='user_id', $result_column='user_name', $value);        
    }

    public function _callback_download_url($value, $primary_key){

        $this->db->select('DocURL')
                 ->from($this->folder_name)
                 ->where('DocID', $value);
        $db = $this->db->get();
        $data = $db->row(0);

        $url = site_url('modules/hcr_ocr/assets/uploads/'.$this->folder_name.'/'.$data->DocURL);
    
        if (!file_exists(FCPATH.'modules/hcr_ocr/assets/uploads/'.$this->folder_name.'/'.$data->DocURL)) {
            return 'javascript:void(0);';
        }
        else{
            return $url;
        }
    }

}