<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class folder_ftk_productivity extends CMS_Priv_Strict_Controller {

    protected $URL_MAP     = array();
    protected $folder_name = '';
    protected $today       = '';

    public $period_id = '';
    public $period_name = '';
    public $start_date = '';
    public $end_date = '';
    public $unit_id = '';

    public $business_area_id = '';

    function __construct(){
        parent::__construct();
        $ci = &get_instance();
        $this->folder_name();        
    }


    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    private function make_crud(){

        $this->today = date('Y-m-d H:i:s');

        $crud = $this->new_crud();
        // this is just for code completion
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        $crud->unset_add();
        // $crud->unset_edit();
        // $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        $crud->unset_export();
        $crud->unset_texteditor('Description');

    
       
        $crud->set_language($this->cms_language());
        $crud->set_theme('no-flexigrid-ftk-productivity');

        // table name
        $crud->set_table('tp_diagram_frame');
        //$crud->order_by('DocTitle', 'ASC');

        // primary key
        $crud->set_primary_key('COID');

        // set subject
        $crud->set_subject($this->cms_lang('FTK Productivity'));

        //$crud->set_subject($this->business_area_id);

        // displayed columns on list
        $crud->columns('DocTitle','Description','Visible','CreatedBy');
        // displayed columns on edit operation
        $crud->edit_fields('DocTitle','Description','DocURL','Visible','CreatedBy','UpdatedBy');
        // displayed columns on add operation
        $crud->add_fields('DocTitle','Description','DocURL','Visible','CreatedBy','CreatedTime','UpdatedBy');


        $crud->unset_add_fields('CreatedBy','CreatedTime','UpdatedBy');
        $crud->unset_edit_fields('UpdatedBy');
        $crud->field_type('CreatedBy', 'hidden', $this->cms_user_id());
        $crud->field_type('CreatedTime', 'hidden', $this->today);
        $crud->field_type('UpdatedBy', 'hidden', $this->cms_user_id());

        // caption of each columns
        $crud->display_as('DocTitle', $this->cms_lang('Document Title'));
        $crud->display_as('Description', $this->cms_lang('Description'));
        $crud->display_as('DocSize', $this->cms_lang('Size'));
        $crud->display_as('Downloaded', $this->cms_lang('Downloaded'));
        $crud->display_as('CreatedBy', $this->cms_lang('Created By'));
        $crud->display_as('DocURL', $this->cms_lang('File PDF (max 1MB)'));
        $crud->display_as('Metadata', $this->cms_lang('Document Keyword (Comma Separated)'));
        $crud->display_as('Visible', $this->cms_lang('Visible'));
        


        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        
        $this->crud = $crud;
        return $crud;
    }

    public function index(){
        $this->set_variable();
        $crud = $this->make_crud();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view($this->cms_module_path().'/'.$this->folder_name.'_view', $output,
            $this->cms_complete_navigation_name($this->folder_name));
    }



    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        // HINT : Put your code here

        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_delete($primary_key){

        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){

        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }

    public function folder_name(){
        $this->folder_name = $this->uri->segment(2);
        return $this;
    }

    public function set_variable(){

        $period_id = $this->input->get('period');
        $business_area_id = $this->input->get('business');

        if (isset($period_id)){

            $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PeriodID', $period_id);
            $db      = $this->db->get();
            $data    = $db->row(0);
            $num_row = $db->num_rows();
            if ($num_row > 0){
                $this->period_id = $data->PeriodID;
                $this->period_name = $data->PeriodName;
                $this->start_date = $data->PStartDate;
                $this->end_date = $data->PEndDate;
            }
        }

        $this->business_area_id = $business_area_id;        
    }

    public function select_option_period(){
        $this->set_variable();  

        $query = $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
               ->from('mst_period')
               ->where('PStatus', 1)
               ->order_by('PStartDate','ASC')
               ->get();

        $empty_select = '<option value="0" SELECTED>{{ language:Current }}</option>';
        foreach($query->result() as $data){
            if($this->period_id == $data->PeriodID){  
                $empty_select .='<option value="'.$data->PeriodID.'" data-subtext="" SELECTED>'.$data->PeriodName.'</option>';
            }
            else {
                $empty_select .='<option value="'.$data->PeriodID.'" data-subtext="">'.$data->PeriodName.'</option>';
            }
        }

        return $empty_select;
    }


    public function select_option_business_unit(){
        $this->set_variable();  

        $query = $this->db->select('business_area_id,description')
               ->from('mst_business_area')
               ->order_by('business_area_id','ASC')
               ->get();

        //$empty_select = '<option value="0" SELECTED>{{ language:Select Business Area }}</option>';
        $empty_select = '';
        foreach($query->result() as $data){
            if($this->business_area_id == $data->business_area_id){  
                $empty_select .='<option value="'.$data->business_area_id.'" data-subtext="" SELECTED>'.$data->description.'</option>';
            }
            else {
                $empty_select .='<option value="'.$data->business_area_id.'" data-subtext="">'.$data->description.'</option>';
            }
        }

        return $empty_select;
    }

    public function generate_folder_ftk_productivity(){

        $this->set_variable();

        $this->db->select('upload_history_id,upload_history_date')
                 ->from('tp_profile_upload_history')
                 ->where('upload_history_created >=', $this->start_date)
                 ->where('upload_history_created <=', $this->end_date)
                 ->order_by('upload_history_created', 'DESC')
                 ->limit(1);
        $db      = $this->db->get();
        $ws      = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){

            if($this->db->table_exists('tp_diagram_frame_'.$ws->upload_history_id) && $this->db->table_exists('tp_diagram_frame_detail_'.$ws->upload_history_id)){
                $tablehdr = 'tp_diagram_frame_'.$ws->upload_history_id;
                $tabledtl = 'tp_diagram_frame_detail_'.$ws->upload_history_id;
            }
            else{
                $tablehdr = 'tp_diagram_frame';
                $tabledtl = 'tp_diagram_frame_detail';
            }            
        }
        else{
            $tablehdr = 'tp_diagram_frame';
            $tabledtl = 'tp_diagram_frame_detail';
        }

        $query = $this->db->select('COID, APP, UnitID, mst_business_area.description AS description')
               ->from($tablehdr)
               ->join('mst_business_area','business_area_id=APP','INNER')
               ->where('APP', $this->business_area_id)
               //->where('FIND_IN_SET (APP,"'.$post_filter_business.'") GROUP BY APP')
               ->where('APP !=','')
               ->where('UnitID !=','')
               ->where('Status', 1)  
               ->group_by('APP')    
               ->order_by('APP','ASC')
               ->get();
        $table = '';

        $table .='<table cellspacing="0" cellpadding="0" border="0" id="flex1">';         
        $table .='<thead>';
        $table .='<tr>';                
        $table .='<th width="15%" class="text-center">'.$this->cms_lang('Business Area').'</th>';
        $table .='<th width="20%" class="text-center">'.$this->cms_lang('Org Unit').'</th>';
        $table .='<th class="text-left">'.$this->cms_lang('Description').'</th>';
        $table .='<th width="10%" class="text-center">'.$this->cms_lang('Realisasi').' ('.$this->cms_lang('People').')</th>';
        $table .='</tr>';
        $table .='</thead>';        
        $table .='<tbody>';

        
        
        $no = 1;
        
        
        foreach($query->result() as $row){
            /*
            $table .='<tr style="border-bottom:1px solid #000000;border-top:2px solid #000000;">';
            $table .= '<td colspan="4"><div class="text-left"><strong>'.$row->description.'</strong></div></td>';
            $table .='</tr>';
            */

            $sql = $this->db->select('COID, APP, UnitID')
               ->from($tablehdr)
               ->where('APP',$row->APP)
               ->where('APP !=','')
               ->where('UnitID !=','')
               ->where('Status', 1)   
               ->group_by('UnitID')              
               ->order_by('UnitID','ASC')
               ->get();            
                
                $sub_total_all = 0;

                foreach($sql->result() as $nama){

                    $ws = $this->db->select('COID, APP, UnitID, Description, Functional')
                           ->from($tablehdr)
                           ->where('UnitID', $nama->UnitID)
                           ->where('APP', $nama->APP)
                           ->where('Status', 1)             
                           ->order_by('COID,Description','ASC')
                           ->get();
                            $sub_total_user = 0;
                            $sub_total_credit_user = 0;
                        $odd = 1;
                        $x = 1;
                        $sub_total_div = 0;
                        $sub_total_user = 0;
                        foreach($ws->result() as $data){
                           
                            if ($odd % 2 == 1){
                                $erow = 'erow';
                            }
                            else{
                                $erow = '';
                            }               

                            $table .='<tr class="'.$erow.'">';
                            $table .='<td><div class="text-left">'.$this->pivot_group_name($this->business_area_id, 'mst_business_area', 'description', 'business_area_id', $no).'</div></td>';
                            $table .='<td><div class="text-left">'.$this->pivot_group_name($data->UnitID, 'mst_organizational_unit', 'description', 'unit_id', $x).'</div></td>';
                            $table .='<td><div class="text-left">'.$data->Description.'</div></td>';
                            $table .='<td width="10%"><div class="text-center">'.$this->_callback_column_Realisasi($data->COID, $data->Functional, $tablehdr, $tabledtl).'</div></td>';
                            $table .='</tr>';

                            $sub_total_user += $this->_column_Realisasi($data->COID, $data->Functional, $tablehdr, $tabledtl);

                            $odd++;
                            $no++;
                            $x++;
                        }
                    $sub_total_div += $sub_total_user;

                    $table .='<tr>';
                    $table .='<td bgcolor="#FFFFCC" colspan="3"><div class="text-right"><strong>Sub Total '.$this->data_table_value('mst_organizational_unit', 'unit_id', 'tNamaPanjangOrg', $nama->UnitID).' ('.$this->data_table_value('mst_organizational_unit', 'unit_id', 'unit_id', $nama->UnitID).')</strong></div></td>';                   
                    $table .='<td width="10%" bgcolor="#FFFFCC"><div class="text-right"><strong>'.$sub_total_div.'</strong></div></td>';          
                    $table .='</tr>';
                    
                    $sub_total_all += $sub_total_div;

                }

            $table .='<tr>';
            $table .= '<td bgcolor="#EAB077" colspan="3"><div class="text-right"><strong>Total '.$row->description.'</strong></div></td>';           
            $table .= '<td width="10%" bgcolor="#EAB077"><div class="text-right"><strong>'.$sub_total_all.'</strong></div></td>';
            $table .='</tr>';           
                       
        }

        $table .= '</tbody>';
        $table .='</table>';
        return $table;
    }


    public function _callback_column_Realisasi($primary_key, $status, $table_hdr, $table_dtl){

        if ($status == 0){

            $SQL   = "SELECT count(EmployeeID) AS Total FROM ".$table_hdr." WHERE COID='".$primary_key."' AND EmployeeID IS NOT NULL GROUP BY COID";
            $query = $this->db->query($SQL);
            $count = $query->num_rows();
            $data   = $query->row();
        }
        else{
            $SQL   = "SELECT count(EmployeeID) AS Total FROM ".$table_dtl." WHERE ParentID='".$primary_key."' AND EmployeeID IS NOT NULL GROUP BY ParentID";
            $query = $this->db->query($SQL);
            $count = $query->num_rows();
            $data   = $query->row();
        }


        if ($count <= 0){
            return '<span class="label label-danger pull-center">0</span>';
        }

        else{
            if ($data->Total <= 0){
                return '<span class="label label-danger pull-center">0</span>';
            }
            else{
                return '<span class="label label-success pull-center" style="margin-left:30px">'.$data->Total.'</span>';
            }            
        }
    }


    public function _column_Realisasi($primary_key, $status, $table_hdr, $table_dtl){

        if ($status == 0){

            $SQL   = "SELECT count(EmployeeID) AS Total FROM ".$table_hdr." WHERE COID='".$primary_key."' AND EmployeeID IS NOT NULL GROUP BY COID";
            $query = $this->db->query($SQL);
            $count = $query->num_rows();
            $data   = $query->row();
        }
        else{
            $SQL   = "SELECT count(EmployeeID) AS Total FROM ".$table_dtl." WHERE ParentID='".$primary_key."' AND EmployeeID IS NOT NULL GROUP BY ParentID";
            $query = $this->db->query($SQL);
            $count = $query->num_rows();
            $data   = $query->row();
        }

        if ($count <= 0){
            return 0;
        }

        else{

            if ($data->Total <= 0){
                return 0;
            }
            else{
                return $data->Total;
            }
            
        }
    }

    public function pivot_group_name($primary_key, $table_name, $column, $where_name, $value){

        $this->db->select($column)
                 ->from($table_name)
                 ->where($where_name, $primary_key);
        $db = $this->db->get();
        $data = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            if ($value ==1){
                return $data->$column;
            }
            else{
                return '';
            }            
        }else{
            return '';
        }
    }

    public function data_table_value($table_name, $where_column, $result_column, $value){

        $this->db->select($result_column)
                 ->from($table_name)
                 ->where($where_column, $value);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            return $data->$result_column;
        }
        else{
            return '';
        }
    }

}