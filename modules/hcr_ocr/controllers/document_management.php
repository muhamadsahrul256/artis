<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class document_management extends CMS_Priv_Strict_Controller {

    protected $URL_MAP     = array();
    protected $folder_name = '';
    protected $today       = '';

    function __construct(){
        parent::__construct();
        $ci = &get_instance();       
    }


    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    private function make_crud(){

        $this->today = date('Y-m-d H:i:s');

        $crud = $this->new_crud();
        // this is just for code completion
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        // $crud->unset_add();
        // $crud->unset_edit();
        // $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        $crud->unset_export();
        $crud->unset_texteditor('DocDescription');


        if ($this->cms_have_privilege('add') != 1) $crud->unset_add();
        if ($this->cms_have_privilege('edit') != 1) $crud->unset_edit();
        if ($this->cms_have_privilege('delete') != 1) $crud->unset_delete();
        if ($this->cms_have_privilege('export') != 1) $crud->unset_export();

        if ($this->cms_have_privilege('download') == 1) {
            $crud->add_action('Download', '', '','',array($this,'_callback_download_url'));
        }       


        if ($state !='edit' AND $state != 'add' AND $state !='read'){
            $crud->set_relation('DocBusA', 'mst_business_area', '{description}');
            $crud->set_relation('CreatedBy', 'main_user', '{user_name}');             
        }
        else{
            $crud->set_relation('DocBusA', 'mst_business_area', '{business_area_id} {description}');
        }    
       
        $crud->set_language($this->cms_language());
        $crud->set_theme('no-flexigrid-document-upload');
        //$crud->set_dialog_forms(false);

        // table name
        $crud->set_table('folder_document_management');
        $crud->order_by('DocTitle', 'ASC');

        // primary key
        $crud->set_primary_key('DocID');

        // set subject
        $crud->set_subject($this->cms_lang('Document Management'));

        // displayed columns on list
        $crud->columns('DocItem','DocPeriod','DocBusA','DocTitle','DocDescription','CreatedBy','CreatedTime');
        // displayed columns on edit operation
        $crud->edit_fields('DocItem','DocPeriod','DocBusA','DocTitle','DocDescription','DocURL','CreatedBy','UpdatedBy');
        // displayed columns on add operation
        $crud->add_fields('DocItem','DocPeriod','DocBusA','DocTitle','DocDescription','DocURL','CreatedBy','CreatedTime','UpdatedBy');


        $crud->unset_add_fields('CreatedBy','CreatedTime','UpdatedBy');
        $crud->unset_edit_fields('UpdatedBy');
        $crud->field_type('CreatedBy', 'hidden', $this->cms_user_id());
        $crud->field_type('CreatedTime', 'hidden', $this->today);
        $crud->field_type('UpdatedBy', 'hidden', $this->cms_user_id());

        // caption of each columns
        $crud->display_as('DocTitle', $this->cms_lang('Title'));
        $crud->display_as('DocDescription', $this->cms_lang('Description'));
        $crud->display_as('DocSize', $this->cms_lang('Size'));
        $crud->display_as('Downloaded', $this->cms_lang('Downloaded'));
        $crud->display_as('CreatedBy', $this->cms_lang('Created By'));
        $crud->display_as('CreatedTime', $this->cms_lang('Created Time'));
        $crud->display_as('DocURL', $this->cms_lang('File <small>pdf,xls,ppt,doc ( max 1MB )</small>'));
        $crud->display_as('Metadata', $this->cms_lang('Document Keyword (Comma Separated)'));
        $crud->display_as('Visible', $this->cms_lang('Visible'));
        $crud->display_as('DocItem', $this->cms_lang('Item'));
        $crud->display_as('DocBusA', $this->cms_lang('Busniness Area'));
        $crud->display_as('DocPeriod', $this->cms_lang('Period'));
        

        $crud->required_fields('DocItem','DocBusA','DocTitle','DocPeriod','DocURL');

        $crud->field_type('Visible', 'true_false', array($this->cms_lang('No'), $this->cms_lang('Yes')));
    
        $crud->set_config_upload('pdf|xls|xlsx|ppt|doc|csv', '1MB');

        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        $crud->set_relation('DocItem', 'mst_document_item', '{Description}');
         
        $crud->set_relation('DocPeriod', 'mst_period', '{PeriodName}'); 

        //$crud->callback_column('CreatedBy',array($this,'_callback_column_CreatedBy'));
        $crud->callback_column('CreatedTime',array($this,'_callback_column_CreatedTime'));

        

        $crud->set_field_upload('DocURL','modules/'.$this->cms_module_path().'/assets/uploads/folder_document_management');

        $this->crud = $crud;
        return $crud;
    }

    public function index(){
        $crud = $this->make_crud();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view($this->cms_module_path().'/document_management_view', $output,
            $this->cms_complete_navigation_name('document_management'));
    }

    public function delete_selection(){
        $crud = $this->make_crud();
        if(!$crud->unset_delete){
            $id_list = json_decode($this->input->post('data'));
            foreach($id_list as $id){
                if($this->_before_delete($id)){
                    $this->db->delete('folder_document_management',array('DocID'=>$id));
                    $this->_after_delete($id);
                }
            }
        }
    }

    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        // HINT : Put your code here

        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_delete($primary_key){

        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){

        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }

    public function _callback_column_CreatedBy($value, $row){
        return $this->cms_complete_table_data($table_name='main_user', $where_column='user_id', $result_column='user_name', $value);        
    }

    public function _callback_column_CreatedTime($value, $row){

        $start_date = $this->cms_complete_table_data($table_name='mst_period', $where_column='PeriodID', $result_column='PStartDate', $row->DocPeriod);
        $end_date   = $this->cms_complete_table_data($table_name='mst_period', $where_column='PeriodID', $result_column='PEndDate', $row->DocPeriod);

        if ($row->CreatedTime >= $start_date && $row->CreatedTime <= $end_date){
            return '<span class="label label-success full-width" style="font-size:11px;">'.date('d-M-Y H:i', strtotime($row->CreatedTime)).'</span>';
        }
        else{
            return '<span class="label label-danger full-width" style="font-size:11px;">'.date('d-M-Y H:i', strtotime($row->CreatedTime)).'</span>';
        }

        //return $row->CreatedTime;

    }

    public function _callback_download_url($value, $primary_key){

        $this->db->select('DocURL')
                 ->from('folder_document_management')
                 ->where('DocID', $value);
        $db = $this->db->get();
        $data = $db->row(0);

        $url = site_url('modules/'.$this->cms_module_path().'/assets/uploads/folder_document_management/'.$data->DocURL);
    
        if (!file_exists(FCPATH.'modules/'.$this->cms_module_path().'/assets/uploads/folder_document_management/'.$data->DocURL)) {
            return 'javascript:void(0);';
        }
        else{
            return $url;
        }
    }

}