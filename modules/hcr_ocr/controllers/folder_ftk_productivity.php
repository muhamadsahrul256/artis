<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class folder_ftk_productivity extends CMS_Priv_Strict_Controller {

    protected $URL_MAP     = array();
    protected $folder_name = '';
    protected $today       = '';

    public $period_id = '';
    public $period_name = '';
    public $start_date = '';
    public $end_date = '';
    public $unit_id = '';

    public $business_area_id = '';


    public $table_diagram_frame        = NULL;
    public $table_diagram_frame_detail = NULL;
    protected $periode_id  = '';
    
    protected $periode_upload_id  = '';
    protected $business_id = '';


    function __construct(){
        parent::__construct();
        $ci = &get_instance();
        $this->folder_name();        
    }


    public function cms_complete_table_name($table_name){
        $this->load->helper($this->cms_module_path().'/function');
        if(function_exists('cms_complete_table_name')){
            return cms_complete_table_name($table_name);
        }else{
            return parent::cms_complete_table_name($table_name);
        }
    }

    public function set_table_frame($period_id){

        if ($this->periode_upload_id <= 0 ){
            $table_diagram_frame        = 'tp_diagram_frame';
            $table_diagram_frame_detail = 'tp_diagram_frame_detail';

            $this->table_diagram_frame        = $table_diagram_frame;
            $this->table_diagram_frame_detail = $table_diagram_frame_detail;
        }
        else{
            $table_diagram_frame        = 'tp_diagram_frame_'.$this->periode_upload_id;
            $table_diagram_frame_detail = 'tp_diagram_frame_detail_'.$this->periode_upload_id;

            if ($this->db->table_exists($table_diagram_frame)){
                $this->table_diagram_frame        = $table_diagram_frame;
                $this->table_diagram_frame_detail = $table_diagram_frame_detail;
            }
            else{
                $this->table_diagram_frame        = 'tp_diagram_frame';
                $this->table_diagram_frame_detail = 'tp_diagram_frame_detail';
            }
        }
    }

    private function make_crud(){

        $this->today = date('Y-m-d H:i:s');

        $crud = $this->new_crud();
        // this is just for code completion
        if (FALSE) $crud = new Extended_Grocery_CRUD();

        // check state & get primary_key
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $primary_key = isset($state_info->primary_key)? $state_info->primary_key : NULL;
        switch($state){
            case 'unknown': break;
            case 'list' : break;
            case 'add' : break;
            case 'edit' : break;
            case 'delete' : break;
            case 'insert' : break;
            case 'update' : break;
            case 'ajax_list' : break;
            case 'ajax_list_info': break;
            case 'insert_validation': break;
            case 'update_validation': break;
            case 'upload_file': break;
            case 'delete_file': break;
            case 'ajax_relation': break;
            case 'ajax_relation_n_n': break;
            case 'success': break;
            case 'export': break;
            case 'print': break;
        }

        // unset things
        $crud->unset_jquery();
        $crud->unset_read();
        $crud->unset_add();
        // $crud->unset_edit();
        // $crud->unset_delete();
        // $crud->unset_list();
        // $crud->unset_back_to_list();
        $crud->unset_print();
        $crud->unset_export();
        $crud->unset_texteditor('Description');

    
       
        $crud->set_language($this->cms_language());
        $crud->set_theme('no-flexigrid-ftk-productivity');

        // table name
        $crud->set_table('tp_diagram_frame');
        //$crud->order_by('DocTitle', 'ASC');

        // primary key
        $crud->set_primary_key('COID');

        // set subject
        $crud->set_subject($this->cms_lang('FTK Productivity'));

        //$crud->set_subject($this->business_area_id);

        // displayed columns on list
        $crud->columns('DocTitle','Description','Visible','CreatedBy');
        // displayed columns on edit operation
        $crud->edit_fields('DocTitle','Description','DocURL','Visible','CreatedBy','UpdatedBy');
        // displayed columns on add operation
        $crud->add_fields('DocTitle','Description','DocURL','Visible','CreatedBy','CreatedTime','UpdatedBy');


        $crud->unset_add_fields('CreatedBy','CreatedTime','UpdatedBy');
        $crud->unset_edit_fields('UpdatedBy');
        $crud->field_type('CreatedBy', 'hidden', $this->cms_user_id());
        $crud->field_type('CreatedTime', 'hidden', $this->today);
        $crud->field_type('UpdatedBy', 'hidden', $this->cms_user_id());

        // caption of each columns
        $crud->display_as('DocTitle', $this->cms_lang('Document Title'));
        $crud->display_as('Description', $this->cms_lang('Description'));
        $crud->display_as('DocSize', $this->cms_lang('Size'));
        $crud->display_as('Downloaded', $this->cms_lang('Downloaded'));
        $crud->display_as('CreatedBy', $this->cms_lang('Created By'));
        $crud->display_as('DocURL', $this->cms_lang('File PDF (max 1MB)'));
        $crud->display_as('Metadata', $this->cms_lang('Document Keyword (Comma Separated)'));
        $crud->display_as('Visible', $this->cms_lang('Visible'));
        


        $crud->callback_before_insert(array($this,'_before_insert'));
        $crud->callback_before_update(array($this,'_before_update'));
        $crud->callback_before_delete(array($this,'_before_delete'));
        $crud->callback_after_insert(array($this,'_after_insert'));
        $crud->callback_after_update(array($this,'_after_update'));
        $crud->callback_after_delete(array($this,'_after_delete'));

        
        $this->crud = $crud;
        return $crud;
    }

    public function index(){
        $this->set_variable();
        $crud = $this->make_crud();
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // render
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $output = $crud->render();
        $this->view($this->cms_module_path().'/'.$this->folder_name.'_view', $output,
            $this->cms_complete_navigation_name($this->folder_name));
    }



    public function _before_insert($post_array){
        $post_array = $this->_before_insert_or_update($post_array);
        // HINT : Put your code here

        return $post_array;
    }

    public function _after_insert($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_update($post_array, $primary_key){
        $post_array = $this->_before_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $post_array;
    }

    public function _after_update($post_array, $primary_key){
        $success = $this->_after_insert_or_update($post_array, $primary_key);
        // HINT : Put your code here
        return $success;
    }

    public function _before_delete($primary_key){

        return TRUE;
    }

    public function _after_delete($primary_key){
        return TRUE;
    }

    public function _after_insert_or_update($post_array, $primary_key){

        return TRUE;
    }

    public function _before_insert_or_update($post_array, $primary_key=NULL){
        return $post_array;
    }

    public function folder_name(){
        $this->folder_name = $this->uri->segment(2);
        return $this;
    }

    public function set_variable(){

        $period_id = $this->input->get('period');
        $business_area_id = $this->input->get('business');

        

        if (isset($period_id)){

            $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
                 ->from('mst_period')
                 ->where('PeriodID', $period_id);
            $db      = $this->db->get();
            $data    = $db->row(0);
            $num_row = $db->num_rows();
            if ($num_row > 0){
                $this->period_id = $data->PeriodID;
                $this->period_name = $data->PeriodName;
                $this->start_date = $data->PStartDate;
                $this->end_date = $data->PEndDate;

                $SQL = "SELECT upload_history_id FROM tp_profile_upload_history 
                    WHERE upload_history_created >='".$data->PStartDate."' AND 
                    upload_history_created <='".$data->PEndDate."' ORDER BY upload_history_created DESC LIMIT 1";

                    $query  = $this->db->query($SQL);
                    $tot_row = $query->num_rows();
                    $row = $query->row(0);

                    if ($tot_row > 0){
                        $this->periode_upload_id   = $row->upload_history_id;
                    }
                    else{
                        $this->periode_upload_id   = 0;
                    }

            }
            else{
                $this->periode_upload_id   = 0;
            }            


        }
        else{
            $this->periode_upload_id   = 0;
        }

        $this->set_table_frame($period_id);


        $this->business_area_id = $business_area_id;        
    }

    public function select_option_period(){
        $this->set_variable();  

        $query = $this->db->select('PeriodID,PeriodName,PStartDate,PEndDate')
               ->from('mst_period')
               ->where('PStatus', 1)
               ->order_by('PStartDate','ASC')
               ->get();

        $empty_select = '<option value="0" SELECTED>{{ language:Current }}</option>';
        foreach($query->result() as $data){
            if($this->period_id == $data->PeriodID){  
                $empty_select .='<option value="'.$data->PeriodID.'" data-subtext="" SELECTED>'.$data->PeriodName.'</option>';
            }
            else {
                $empty_select .='<option value="'.$data->PeriodID.'" data-subtext="">'.$data->PeriodName.'</option>';
            }
        }

        return $empty_select;
    }


    public function select_option_business_unit(){
        $this->set_variable();  

        $query = $this->db->select('business_area_id,description')
               ->from('mst_business_area')
               ->order_by('business_area_id','ASC')
               ->get();

        //$empty_select = '<option value="0" SELECTED>{{ language:Select Business Area }}</option>';
        $empty_select = '';
        foreach($query->result() as $data){
            if($this->business_area_id == $data->business_area_id){  
                $empty_select .='<option value="'.$data->business_area_id.'" data-subtext="" SELECTED>'.$data->description.'</option>';
            }
            else {
                $empty_select .='<option value="'.$data->business_area_id.'" data-subtext="">'.$data->description.'</option>';
            }
        }

        return $empty_select;
    }

    public function generate_folder_ftk_productivity(){

        $this->set_variable();

        $this->db->select('upload_history_id,upload_history_date')
                 ->from('tp_profile_upload_history')
                 ->where('upload_history_created >=', $this->start_date)
                 ->where('upload_history_created <=', $this->end_date)
                 ->order_by('upload_history_created', 'DESC')
                 ->limit(1);
        $db      = $this->db->get();
        $ws      = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){

            if($this->db->table_exists('tp_diagram_frame_'.$ws->upload_history_id) && $this->db->table_exists('tp_diagram_frame_detail_'.$ws->upload_history_id)){
                $tablehdr = 'tp_diagram_frame_'.$ws->upload_history_id;
                $tabledtl = 'tp_diagram_frame_detail_'.$ws->upload_history_id;
            }
            else{
                $tablehdr = 'tp_diagram_frame';
                $tabledtl = 'tp_diagram_frame_detail';
            }            
        }
        else{
            $tablehdr = 'tp_diagram_frame';
            $tabledtl = 'tp_diagram_frame_detail';
        }

        $query = $this->db->select('COID, APP, UnitID, mst_business_area.description AS description')
               ->from($tablehdr)
               ->join('mst_business_area','business_area_id=APP','INNER')
               ->where('APP', $this->business_area_id)
               //->where('FIND_IN_SET (APP,"'.$post_filter_business.'") GROUP BY APP')
               ->where('APP !=','')
               ->where('UnitID !=','')
               ->where('Status', 1)  
               ->group_by('APP')    
               ->order_by('APP','ASC')
               ->get();
        $table = '';

        $table .='<table cellspacing="0" cellpadding="0" border="0" id="flex1">';         
        $table .='<thead>';
        $table .='<tr>';                
        $table .='<th width="15%" class="text-center">'.$this->cms_lang('Business Area').'</th>';
        $table .='<th width="20%" class="text-center">'.$this->cms_lang('Org Unit').'</th>';
        $table .='<th class="text-left">'.$this->cms_lang('Description').'</th>';
        $table .='<th width="10%" class="text-center">'.$this->cms_lang('Realisasi').' ('.$this->cms_lang('People').')</th>';
        $table .='</tr>';
        $table .='</thead>';        
        $table .='<tbody>';

        
        
        $no = 1;
        
        
        foreach($query->result() as $row){
            /*
            $table .='<tr style="border-bottom:1px solid #000000;border-top:2px solid #000000;">';
            $table .= '<td colspan="4"><div class="text-left"><strong>'.$row->description.'</strong></div></td>';
            $table .='</tr>';
            */

            $sql = $this->db->select('COID, APP, UnitID')
               ->from($tablehdr)
               ->where('APP',$row->APP)
               ->where('APP !=','')
               ->where('UnitID !=','')
               ->where('Status', 1)   
               ->group_by('UnitID')              
               ->order_by('UnitID','ASC')
               ->get();            
                
                $sub_total_all = 0;

                foreach($sql->result() as $nama){

                    $ws = $this->db->select('COID, APP, UnitID, Description, Functional')
                           ->from($tablehdr)
                           ->where('UnitID', $nama->UnitID)
                           ->where('APP', $nama->APP)
                           ->where('Status', 1)             
                           ->order_by('COID,Description','ASC')
                           ->get();
                            $sub_total_user = 0;
                            $sub_total_credit_user = 0;
                        $odd = 1;
                        $x = 1;
                        $sub_total_div = 0;
                        $sub_total_user = 0;
                        foreach($ws->result() as $data){
                           
                            if ($odd % 2 == 1){
                                $erow = 'erow';
                            }
                            else{
                                $erow = '';
                            }               

                            $table .='<tr class="'.$erow.'">';
                            $table .='<td class="text-left">'.$this->pivot_group_name($this->business_area_id, 'mst_business_area', 'description', 'business_area_id', $no).'</td>';
                            $table .='<td class="text-left">'.$this->pivot_group_name($data->UnitID, 'mst_organizational_unit', 'description', 'unit_id', $x).'</td>';
                            $table .='<td class="text-left">'.$data->Description.'</td>';
                            $table .='<td class="text-center" width="10%">'.$this->_callback_column_Realisasi($data->COID, $data->Functional, $tablehdr, $tabledtl).'</td>';
                            $table .='</tr>';

                            $sub_total_user += $this->_column_Realisasi($data->COID, $data->Functional, $tablehdr, $tabledtl);

                            $odd++;
                            $no++;
                            $x++;
                        }
                    $sub_total_div += $sub_total_user;

                    $table .='<tr>';
                    $table .='<td bgcolor="#FFFFCC" colspan="3"><div class="text-right"><strong>Sub Total '.$this->data_table_value('mst_organizational_unit', 'unit_id', 'tNamaPanjangOrg', $nama->UnitID).' ('.$this->data_table_value('mst_organizational_unit', 'unit_id', 'unit_id', $nama->UnitID).')</strong></div></td>';                   
                    $table .='<td width="10%" bgcolor="#FFFFCC"><div class="text-right"><strong>'.$sub_total_div.'</strong></div></td>';          
                    $table .='</tr>';
                    
                    $sub_total_all += $sub_total_div;

                }

            if($this->business_area_id == 3401){
                $table .= $this->callback_sub_total_director($primary_key=2);
                $table .= $this->callback_sub_total_director($primary_key=3);
                $table .= $this->callback_sub_total_director($primary_key=5);
                $table .= $this->callback_sub_total_director($primary_key=6);
                $table .= $this->callback_sub_total_director($primary_key=7);
                $table .= $this->callback_sub_total_director($primary_key=8);
            }

            
            $table .='<tr>';
            $table .= '<td bgcolor="#EAB077" colspan="3"><div class="text-right"><strong>Total '.$row->description.'</strong></div></td>';           
            $table .= '<td width="10%" bgcolor="#EAB077"><div class="text-right"><strong>'.$sub_total_all.'</strong></div></td>';
            $table .='</tr>';           
                       
        }

        $table .= '</tbody>';
        $table .='</table>';
        return $table;
    }


    public function _callback_column_Realisasi($primary_key, $status, $table_hdr, $table_dtl){

        if ($status == 0){

            $SQL   = "SELECT count(EmployeeID) AS Total FROM ".$table_hdr." WHERE COID='".$primary_key."' AND EmployeeID IS NOT NULL AND EmployeeID != '' AND Status=1 AND Functional= 0 GROUP BY EmployeeID";
            $query = $this->db->query($SQL);
            $count = $query->num_rows();
            $data   = $query->row();
        }
        else{
            $SQL   = "SELECT count(COALESCE(a.EmployeeID,b.EmployeeID)) AS Total FROM ".$table_dtl." AS a 
                RIGHT JOIN ".$table_hdr." AS b ON a.ParentID=b.COID 
                WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 AND a.ParentID='".$primary_key."' GROUP BY a.ParentID";
            $query = $this->db->query($SQL);
            $count = $query->num_rows();
            $data   = $query->row();
        }


        if ($count <= 0){
            return '<span class="label label-danger pull-center">0</span>';
        }

        else{
            if ($data->Total <= 0){
                return '<span class="label label-danger pull-center">0</span>';
            }
            else{
                return '<span class="label label-success pull-center" style="margin-left:30px">'.$data->Total.'</span>';
            }            
        }
    }


    public function _column_Realisasi($primary_key, $status, $table_hdr, $table_dtl){

        if ($status == 0){

            $SQL   = "SELECT count(EmployeeID) AS Total FROM ".$table_hdr." WHERE COID='".$primary_key."' AND EmployeeID IS NOT NULL AND EmployeeID != '' AND Status=1 AND Functional= 0 GROUP BY EmployeeID";
            $query = $this->db->query($SQL);
            $count = $query->num_rows();
            $data   = $query->row();
        }
        else{
            $SQL   = "SELECT count(COALESCE(a.EmployeeID,b.EmployeeID)) AS Total FROM ".$table_dtl." AS a RIGHT JOIN ".$table_hdr." AS b ON a.ParentID=b.COID WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 AND a.ParentID='".$primary_key."' GROUP BY a.ParentID";
            $query = $this->db->query($SQL);
            $count = $query->num_rows();
            $data   = $query->row();
        }

        if ($count <= 0){
            return 0;
        }

        else{

            if ($data->Total <= 0){
                return 0;
            }
            else{
                return $data->Total;
            }
            
        }
    }

    public function pivot_group_name($primary_key, $table_name, $column, $where_name, $value){

        $this->db->select($column)
                 ->from($table_name)
                 ->where($where_name, $primary_key);
        $db = $this->db->get();
        $data = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            if ($value ==1){
                return $data->$column;
            }
            else{
                return '';
            }            
        }else{
            return '';
        }
    }

    public function data_table_value($table_name, $where_column, $result_column, $value){

        $this->db->select($result_column)
                 ->from($table_name)
                 ->where($where_column, $value);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();

        if ($num_row > 0){
            return $data->$result_column;
        }
        else{
            return '';
        }
    }

    public function callback_sub_total_director($primary_key){

        $total = $this->callback_box_frame($primary_key, $style=NULL, $className=NULL, $rows=NULL);
        $remarks = $this->data_table_value('mst_organizational_unit', 'unit_id', 'tNamaPanjangOrg', $this->data_table_value($table_name='tp_diagram_frame', $where_column='COID', $result_column='UnitID', $primary_key));

        $table  ='<tr>';
        $table .= '<td bgcolor="#FFFF00" colspan="3"><div class="text-right"><strong>Sub Total '.$remarks.'</strong></div></td>';           
        $table .= '<td width="10%" bgcolor="#FFFF00"><div class="text-right"><strong>'.$total.'</strong></div></td>';
        $table .='</tr>';

        return $table;
    }


    public function callback_box_frame($primary_key, $style, $className=NULL, $rows=NULL){

        $totalku = $this->count_struktural($primary_key);           
       
        $output  = $this->callback_box_child_frame($primary_key, $style, $total=0);            

        return $output+$totalku;    
    }

    public function callback_box_child_frame($primary_key, $style, $total){        

        $query = $this->db->select('COID, Functional, EmployeeName, rowspan')
               ->from($this->table_diagram_frame)
               ->where('ParentID', $primary_key)
               ->where('APP', $this->business_area_id)
               ->where('Status', 1)
               ->where('Description IS NOT NULL')
               ->order_by('No_','ASC')
               ->get();
        $no=1;

        $output = 0;
        $num_row = $query->num_rows();

        foreach($query->result() as $data){

            if ($data->Functional == 1){
                $totalku = $this->count_functional($data->COID);
            }
            else{
                $totalku = $this->count_struktural($data->COID);
            }              
                       
            $output += $this->callback_box_child_frame($data->COID, $style=NULL, $totalku);
            
        }

        $total += $output;                

        return $total;    
    }

    public function select_diagram_data($primary_key, $result_column){

        $this->db->select($result_column)
                 ->from($this->table_diagram_frame)
                 //->where('APP', $this->business_id)
                 ->where('COID', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            return $data->$result_column;
        } 
        else{
            return '';
        }
    }


    public function callback_diagram_data($primary_key, $result_column){

        $this->db->select($result_column)
                 ->from($this->table_diagram_frame)
                 ->where('COID', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        
        return $num_row;
    }

    public function callback_diagram_detail($primary_key, $result_column){

        $this->db->select($result_column)
                 ->from($this->table_diagram_frame_detail)
                 ->where('ParentID', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        
        return $num_row;
    }

    public function count_functional($parent){

        $SQL = "SELECT count(COALESCE(a.EmployeeID,b.EmployeeID)) AS Total FROM ".$this->table_diagram_frame_detail." AS a RIGHT JOIN ".$this->table_diagram_frame." AS b ON a.ParentID=b.COID WHERE COALESCE(a.EmployeeID,b.EmployeeID) IS NOT NULL AND COALESCE(a.EmployeeID,b.EmployeeID) !='' AND b.Status=1 AND a.ParentID='".$parent."' GROUP BY a.ParentID";

        $query  = $this->db->query($SQL);
        $tot_row = $query->num_rows();        
        $data    = $query->row(0);

        if($tot_row > 0){
            return $data->Total;
        }
        else{
            return 0;
        }

        
    }

    public function count_struktural($parent){

        $SQL = "SELECT count(EmployeeID) AS Total FROM ".$this->table_diagram_frame." WHERE COID='".$parent."' AND EmployeeID IS NOT NULL AND EmployeeID != '' AND Status=1 AND Functional= 0 GROUP BY EmployeeID";

        $query  = $this->db->query($SQL);
        $tot_row = $query->num_rows();        
        $data    = $query->row(0);

        if($tot_row > 0){
            return $data->Total;
        }
        else{
            return 0;
        }
    }



}