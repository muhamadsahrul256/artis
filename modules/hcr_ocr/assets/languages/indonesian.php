<?php

$lang['OCR Industrial Relation'] = 'OCR Hubungan Industrial';
$lang['OWM Productivity'] = 'Produktifitas OWM';
$lang['FTK Productivity'] = 'Produktifitas FTK';
$lang['Upload Document'] = 'Upload Dokumen';
$lang['File PDF (max 1MB)'] = 'Berkas PDF (maksimal 1MB)';
$lang['Document Title'] = 'Judul Dokumen';
$lang['Document Keyword (Comma Separated)'] = 'Kata kunci dokumen (Dipisahkan koma)';
$lang['Visible'] = 'Terlihat';
$lang['Yes'] = 'Ya';
$lang['No'] = 'Tidak';
$lang['Download file'] = 'Unduh berkas';
$lang['Type'] = 'Tipe';
$lang['Document Management'] = 'Manajemen Dokumen';
$lang['Created By'] = 'Dibuat';
$lang['Created Time'] = 'Waktu';
$lang['Filter'] = 'Penyaringan';
$lang['Saved Period'] = 'Periode Penyimpanan';
$lang['People'] = 'Orang';
$lang['Select Business Area'] = 'Pilih Area Bisnis';
$lang['Current'] = 'Sekarang Ini';
$lang['Show'] = 'Tampilkan';
$lang['Busniness Area'] = 'Bisnis Area';

