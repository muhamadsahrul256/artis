<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$asset = new CMS_Asset();
foreach($css_files as $file){
    $asset->add_css($file);
}
echo $asset->compile_css();

foreach($js_files as $file){
    $asset->add_js($file);
}
echo $asset->compile_js();

// For every content of option tag, this will replace '&nbsp;' with ' '
function __ommit_nbsp($matches){
    return $matches[1].str_replace('&nbsp;', ' ', $matches[2]).$matches[3];
}
echo preg_replace_callback('/(<option[^<>]*>)(.*?)(<\/option>)/si', '__ommit_nbsp', $output);

$today     = date('Y-m-d_H:i:s');

$current_id = $this->uri->segment('4');

$file_name = 'FTK Productivity_'.$today;

?>

<script type="text/javascript">

$(function () {
    // $('#myModal').modal('hide');
});

document.getElementById("btnPrint").onclick = function () {
    printElement(document.getElementById("clonetext"));

    /**var modThis = document.querySelector("#printSection .modifyMe");
    modThis.appendChild(document.createTextNode(" new"));**/

}

function printElement(elem) {
    var domClone = elem.cloneNode(true);

    var $printSection = document.getElementById("printSection");

    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }   
    $printSection.innerHTML = "";

    $printSection.appendChild(domClone);
    window.print();
}

</script>

<script type="text/javascript">
$("#btnExcel").click(function(){
  $("#clonetext").table2excel({
    // exclude CSS class
    exclude: ".noExl",
    name: "Worksheet Name",
    filename: "<?php echo $file_name ?>",
    fileext: ".xls",
    exclude_img: true,
    exclude_links: true,
    exclude_inputs: true
  }); 
});
</script>


<style type="text/css">
@media screen {
    #printSection {
        display: none;
    }
}
@media print {
    body * {
        visibility:hidden;        
        font-family: Arial;
    }
    table{
        font-size: 8px;
    }
    #non-printable { display: none; }
    
    #printSection, #printSection * {
        visibility:visible;
    }
    #printSection {
        position:absolute;
        left:0;
        top:0;
    }
}

</style>

<style type="text/css">
    #flex1 td {
        white-space: pre-wrap !important; /* css-3 */
        white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */
        white-space: -pre-wrap !important; /* Opera 4-6 */
        white-space: -o-pre-wrap !important; /* Opera 7 */
        word-wrap: break-word !important; /* Internet Explorer 5.5+ */
        vertical-align: middle !important;
    }
</style>