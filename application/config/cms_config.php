<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['cms_table_prefix'] = '';
$config['max_menu_depth'] = 10;

$config['site_layout'] = 'default';
$config['__cms_version'] = '0.7.0-stable-1';
$config['site_language'] = 'indonesian';
$config['site_theme'] = 'AdminLTE';
$config['cms_google_analytic_property_id'] = '';
$config['site_favicon'] = '{{ base_url }}assets/nocms/images/custom_favicon/PLN_full_ELO.png.ico';
$config['site_name'] = 'Application Human Resource & Talent Information System (ARTIS)';
$config['site_footer'] = 'Copyright © 2017 Application Human Resource & Talent Information System - Powered By SDM, All Rights Reserved';
$config['site_logo'] = '{{ base_url }}assets/nocms/images/custom_logo/ARTIS-LOGO.png';
$config['site_slogan'] = '-';
$config['cms_signup_activation'] = 'automatic';
$config['cms_email_protocol'] = 'smtp';
$config['cms_email_reply_address'] = 'dompak.sinambela@unias.com';
$config['cms_email_reply_name'] = 'ARTIS';
$config['cms_email_forgot_subject'] = 'Re-activate your account at Application Resource & Talent Information System';
$config['cms_email_forgot_message'] = 'Dear, {{ user_real_name }}<br />Silahkan Klik <a href={{ site_url }}main/forgot/{{ activation_code }}>{{ site_url }}main/forgot/{{ activation_code }}</a> untuk mengganti password akun Application Human Resource & Talent Information System anda.
</br>
</br>
<p><font color=\"#FF0000\" size=\"-1\">Perhatian email ini dikirim secara otomatis dari System. Jangan membalas ke alamat email ini</font></p>';
$config['cms_email_signup_subject'] = 'Activate your account at System';
$config['cms_email_signup_message'] = 'Dear, {{ user_real_name }}<br />Click <a href=\"{{ site_url }}main/activate/{{ activation_code }}\">{{ site_url }}main/activate/{{ activation_code }}</a> to activate your account';
$config['cms_email_useragent'] = 'ARTIS';
$config['cms_email_mailpath'] = '/usr/sbin/sendmail';
$config['cms_email_smtp_host'] = 'astelmail.unias.com';
$config['cms_email_smtp_user'] = 'unias\dompaksi';
$config['cms_email_smtp_pass'] = 'dompak.123';
$config['cms_email_smtp_port'] = '25';
$config['cms_email_smtp_timeout'] = '30';
$config['cms_add_subsite_on_register'] = 'FALSE';
$config['cms_subsite_use_subdomain'] = 'FALSE';
$config['cms_subsite_home_content'] = '{{ widget_name:blog_content }}';
$config['site_version'] = '1.0';
$config['site_logo_login'] = '{{ base_url }}assets/nocms/images/custom_logo/AMOR.png';
$config['cms_email_wordwrap'] = 'TRUE';
$config['cms_email_wrapchars'] = '76';
$config['cms_email_mailtype'] = 'html';
$config['cms_email_charset'] = 'utf-8';
$config['cms_email_validate'] = 'FALSE';
$config['cms_email_priority'] = '3';
$config['cms_email_bcc_batch_mode'] = 'FALSE';
$config['cms_email_bcc_batch_size'] = '200';
$config['sap_upload_time'] = '1,20';