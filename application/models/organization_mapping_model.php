<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class organization_mapping_model extends CMS_Model{
	
    public function get_orgchart($first_level, $second_level){

        $orgchart = '<ul id="tree-data" style="display:none">';
        $orgchart .= '<li id="root" class="orgchart selected">';
        $orgchart .= 'Direktorat Utama';
        $orgchart .= '<ul>';

        for ($x = 1; $x <= $first_level; $x++) {
            $orgchart .= '<li id="node'.$x.'" class="orgchart">';
            $orgchart .= 'Direktorat Operasi';
            $orgchart .= '<ul class="orgchart">';

            for ($i = 1; $i <= $second_level; $i++) {
                $orgchart .= '<li id="node6" class="orgchart">';
                $orgchart .= 'Divisi Layanan';
                $orgchart .= '</li>';
            }

            $orgchart .= '</ul>';
            $orgchart .= '</li>';
        }

        $orgchart .= '</ul>';
        $orgchart .= '</li>';
        $orgchart .= '</ul>';  
        $orgchart .= '<div id="tree-view"></div>';

    	return $orgchart;
    }

    public function dimensional_orgchart($orgs){
        /*
        $orgs = array(
            array("Volvo",2,1),
            array("BMW",1,2),
            array("Saab",3,1),
            array("Land Rover",1,1)
        );
        */

        $orgchart = '<ul id="tree-data" style="display:none">';
        $orgchart .= '<li id="root" class="orgchart selected">';
        $orgchart .= '<span>Direktorat Utama</span>';
        $orgchart .= '<ul>';


        for ($row = 0; $row < 4; $row++) {

            $orgchart .= '<li id="node'.$row.'" class="orgchart">';
            $orgchart .= $orgs[$row][0];
            $orgchart .= '<ul class="orgchart">';

          for ($col = 0; $col < $orgs[$row][1]; $col++) {
                //echo "<li>".$orgs[$row][$col]."</li>";
                $orgchart .= '<li id="node6" class="orgchart">';
                $orgchart .= 'Divisi Layanan';

                $orgchart .= '<ul>';

                for ($axis = 0; $axis < $orgs[$row][2]; $axis++) {
                    $orgchart .= '<li id="node6" class="orgchart">';
                    $orgchart .= 'Divisi Layanan';
                    $orgchart .= '</li>';
                }

                $orgchart .= '</ul>';
                $orgchart .= '</li>';

          }


            $orgchart .= '</ul>';
            $orgchart .= '</li>';
        }

        $orgchart .= '</ul>';
        $orgchart .= '</li>';
        $orgchart .= '</ul>'; 
        $orgchart .= '<div id="tree-view"></div>';

        return $orgchart;
    }


    public function orgchart_data(){

        $orgchart  = '<ul id="tree-data" style="display:none">';
        $orgchart .= '<li id="root" class="orgchart selected">';
        $orgchart .= $this->orgchart_value($primary_key=1, $result_column='Description');
        $orgchart .= '<ul>';

        $query1 = $this->db->select('COID,ParentID')
               ->from('tp_organization')
               ->where('ParentID', 1)
               ->order_by('COID','ASC')
               ->get();
        $no=1;

        foreach($query1->result() as $row){
            $orgchart .= '<li id="node'.$no.'" class="orgchart">';
            $orgchart .= $this->orgchart_value($row->COID, $result_column='Description').'-'.$no;
            $orgchart .= '<ul class="orgchart">';

            $query2 = $this->db->select('COID,ParentID')
               ->from('tp_organization')
               ->where('ParentID', $row->COID)
               ->order_by('COID','ASC')
               ->get();
            foreach($query2->result() as $ws){

                $orgchart .= '<li id="node'.$ws->ParentID.'" class="orgchart">';
                $orgchart .= $this->orgchart_value($ws->COID, $result_column='Description').'-'.$ws->ParentID;
                
                $orgchart .= '<ul class="orgchart">';
                $query3 = $this->db->select('COID,ParentID')
                        ->from('tp_organization')
                        ->where('ParentID', $ws->COID)
                        ->order_by('COID','ASC')
                        ->get();
                foreach($query3->result() as $col){

                    $orgchart .= '<li id="node'.$row->ParentID.'" class="orgchart">';
                    $orgchart .= $this->orgchart_value($col->COID, $result_column='Description');

                    $orgchart .= '<ul class="orgchart">';
                    $query4 = $this->db->select('COID,ParentID')
                        ->from('tp_organization')
                        ->where('ParentID', $col->COID)
                        ->order_by('COID','ASC')
                        ->get();
                    foreach($query4->result() as $run){

                        $orgchart .= '<li id="node'.$row->ParentID.'" class="orgchart">';
                        $orgchart .= $this->orgchart_value($run->COID, $result_column='Description');
                        $orgchart .= '</li>';

                    }

                    $orgchart .= '</ul>';
                    $orgchart .= '</li>';


                }

                $orgchart .= '</ul>';
                $orgchart .= '</li>';

            }

            $orgchart .= '</ul>';
            $orgchart .= '</li>';

            $no++;
        }

       

        $orgchart .= '</ul>';
        $orgchart .= '</li>';
        $orgchart .= '</ul>'; 

       

        $orgchart .= '<div id="tree-view"></div>';

        return $orgchart;
    }

    public function piramida_orgchart_data(){

        $orgchart  = '<ul id="tree-data" style="display:none">';
        $orgchart .= '<li id="root" class="orgchart selected">';
        $orgchart .= $this->orgchart_value($primary_key=1, $result_column='Description');
        $orgchart .= '<ul>';

      
        $query = $this->db->select('*')
               ->from('tp_organization')
               ->where('ParentID', 1)
               ->order_by('COID','ASC')
               ->get();
        $no=1;

        foreach($query->result() as $row){

            $orgchart .= '<li id="node'.$no.'" class="orgchart">';
            $orgchart .= $row->COID.'. '.$this->orgchart_value($row->COID, $result_column='Description');

            $orgchart .= $this->orgchart_child($row->COID);
            $orgchart .= '</li>';

            $no++;
        }

       

        $orgchart .= '</ul>';
        $orgchart .= '</li>';
        $orgchart .= '</ul>';        

       

        $orgchart .= '<div id="tree-view"></div>';

        return $orgchart;
    }

    public function piramida_orgchart_data_css(){

        $orgchart  = '<div class="tree">';
        //$orgchart .= '<h1 class="text-center">Bagan</h1>';
        $orgchart .= '<ul>';
        $orgchart .= '<li>';
        $orgchart .= '<a href="javascript:void(0);" class="launch-modal" data-id="1">';

        $orgchart .= '<span class="garis-atas judul">';
        $orgchart .= $this->orgchart_value(1, $result_column='Description');
        $orgchart .= '</span>';

        $orgchart .= '<span class="garis-atas">';
        $orgchart .= $this->profile_user($this->orgchart_value($primary_key=1, $result_column='EmployeeID'), $result_column='Full_Name');
        $orgchart .= '</span>';

        $orgchart .= '<span class="pull-left">';
        $orgchart .= $this->orgchart_value(1, $result_column='EmployeeID');
        $orgchart .= '</span>';

        $orgchart .= '<span class="pull-center">&nbsp;|&nbsp;</span>';

        $orgchart .= '<span class="pull-right">';
        $orgchart .= $this->orgchart_value(1, $result_column='GradeID');
        $orgchart .= '</span>';


        //$orgchart .= $this->orgchart_value($primary_key=1, $result_column='EmployeeID');
        $orgchart .= '</a>';


        
        $orgchart .= '<ul class="parent">';

      
        $query = $this->db->select('*')
               ->from($this->cms_complete_table_name('tp_organization'))
               ->where('ParentID', 1)
               ->order_by('COID','ASC')
               ->get();
        $no=1;

        foreach($query->result() as $data){

            $EmployeeID = $this->orgchart_value($data->COID, $result_column='EmployeeID');

            if(empty($EmployeeID) || is_null($EmployeeID)){
                $empty = 'jabatan-kosong';
            }
            else{
                $empty = '';
            }

           
            
                    
            $orgchart .= '<li class="'.$data->class_li.'">';
            $orgchart .= '<a href="javascript:void(0);" class="launch-modal '.$data->class_href.'" data-id="'.$data->COID.'">';
            //$orgchart .= $row->COID.'. '.$this->orgchart_value($row->COID, $result_column='Description').'<br/>';


            $orgchart .= '<span class="garis-atas judul '.$empty.'">';
            $orgchart .= $this->orgchart_value($data->COID, $result_column='Description');
            $orgchart .= '</span>';

            $orgchart .= '<span class="garis-atas">';
            $orgchart .= $this->profile_user($data->EmployeeID, $result_column='Full_Name');
            $orgchart .= '</span>';

            $orgchart .= '<span class="pull-left">';
            $orgchart .= $this->orgchart_value($data->COID, $result_column='EmployeeID');
            $orgchart .= '</span>';

            $orgchart .= '<span class="pull-center">&nbsp;|&nbsp;</span>';

            $orgchart .= '<span class="pull-right">';
            $orgchart .= $this->orgchart_value($data->COID, $result_column='GradeID');
            $orgchart .= '</span>';

            $orgchart .= '</a>';
            $orgchart .= $this->orgchart_child_css($data->COID);
            $orgchart .= '</li>';

            $no++;
        }

       

        $orgchart .= '</ul>';
        $orgchart .= '</li>';
        $orgchart .= '</ul>';        

       

        $orgchart .= '</div>';

        return $orgchart;
    }


    public function orgchart_value($primary_key, $result_column){

        $this->db->select($result_column)
                 ->from($this->cms_complete_table_name('tp_organization'))
                 ->where('COID', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            return $data->$result_column;
        } 
        else{
            return '';
        }
    }

    public function orgchart_child($parent_id){

        $orgchart = '';

        $query = $this->db->select('COID,ParentID')
               ->from($this->cms_complete_table_name('tp_organization'))
               ->where('ParentID', $parent_id)
               ->order_by('COID','ASC')
               ->get();
        $num_row = $query->num_rows();
        $orgchart .= '<ul class="orgchart">';
        foreach($query->result() as $data){            

            $orgchart .= '<li id="node'.$data->ParentID.'" class="orgchart">';
            $orgchart .= $data->COID.'. '.$this->orgchart_value($data->COID, $result_column='Description');

            $orgchart .= $this->orgchart_child($data->COID);

            $orgchart .= '</li>';

        }
        $orgchart .= '</ul>';

        if ($num_row > 0){
            return $orgchart;
        }
        else{
            return '';
        }       


    }

    public function orgchart_child_css($parent_id){

        $orgchart = '';

        $query = $this->db->select('*')
               ->from($this->cms_complete_table_name('tp_organization'))
               ->where('ParentID', $parent_id)
               ->order_by('COID','ASC')
               ->get();
        $num_row = $query->num_rows();
        $orgchart .= '<ul>';
        foreach($query->result() as $data){

            $EmployeeID = $this->orgchart_value($data->COID, $result_column='EmployeeID');

            $sql = $this->db->select('*')
                       ->from($this->cms_complete_table_name('tp_organization_fungsional'))
                       ->where('ParentID', $data->COID)
                       ->order_by('COIDNo','ASC')
                       ->get();
            $sql_row = $sql->num_rows();

            if ($data->Fungsional == 0){
                if(empty($EmployeeID) || is_null($EmployeeID)){
                    $empty = 'jabatan-kosong';
                }
                else{
                    $empty = '';
                }
            }
            else{
                if($sql_row <= 0){
                    $empty = 'jabatan-kosong';
                }
                else{
                    $empty = '';
                }
            }

            $orgchart .= '<li class="'.$data->class_li.'">';
            $orgchart .= '<a href="javascript:void(0);" class="launch-modal '.$data->class_href.'" data-id="'.$data->COID.'">';
            $orgchart .= '<span class="garis-atas judul '.$empty.'">';
            $orgchart .= $this->orgchart_value($data->COID, $result_column='Description');
            $orgchart .= '</span>';


            if ($data->Fungsional == 0){

                $orgchart .= '<span class="garis-atas">';
                $orgchart .= $this->profile_user($data->EmployeeID, $result_column='Full_Name');
                $orgchart .= '</span>';

                $orgchart .= '<span class="pull-left">';
                $orgchart .= $this->orgchart_value($data->COID, $result_column='EmployeeID');
                $orgchart .= '</span>';

                $orgchart .= '<span class="pull-center">&nbsp;|&nbsp;</span>';

                $orgchart .= '<span class="pull-right">';
                $orgchart .= $this->orgchart_value($data->COID, $result_column='GradeID');
                $orgchart .= '</span>';
            }
            else{                
            
                foreach($sql->result() as $detail){
                    $orgchart .= '<span class="pull-left">';
                    $orgchart .= '- ';
                    $orgchart .= $this->profile_user($detail->EmployeeID, $result_column='Full_Name');
                    $orgchart .= '</span>';                    
                    $orgchart .= '<span class="pull-right">';
                    $orgchart .= $this->orgchart_value($detail->COIDNo, $result_column='EmployeeID');
                    $orgchart .= '&nbsp;|&nbsp;';
                    $orgchart .= $this->orgchart_value($detail->COIDNo, $result_column='GradeID');
                    $orgchart .= '</span>';
                    $orgchart .= '</br>';
                }

            }

                        

            $orgchart .= '</a>';



            $orgchart .= $this->orgchart_child_css($data->COID);

            $orgchart .= '</li>';

        }
        $orgchart .= '</ul>';

        if ($num_row > 0){
            return $orgchart;
        }
        else{
            return '';
        }
    }

    public function profile_user($primary_key, $result_column){

        $this->db->select($result_column)
                 ->from($this->cms_complete_table_name('tp_raw_data'))
                 ->where('Prev_Per_No', $primary_key);
        $db      = $this->db->get();
        $data    = $db->row(0);
        $num_row = $db->num_rows();
        if ($num_row > 0){
            return $data->$result_column;
        } 
        else{
            return '--';
        }
    }

}
