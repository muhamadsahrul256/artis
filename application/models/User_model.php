<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class User_model extends CI_Model {
    function User_model()
    {
	parent::Model();
        $this->load->database();
    }

    function tambahuser($dataarray)
    {
        for($i=0;$i<count($dataarray);$i++){
            $data = array(
                'nama'=>$dataarray[$i]['nama'],
                'alamat'=>$dataarray[$i]['alamat']
            );
            $this->db->insert('eimport', $data);
        }
    }

    function getuser()
    {
        $query = $this->db->get('eimport');
        return $query->result();
    }
}