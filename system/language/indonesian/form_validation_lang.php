<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Open Software License version 3.0
 *
 * This source file is subject to the Open Software License (OSL 3.0) that is
 * bundled with this package in the files license.txt / license.rst.  It is
 * also available through the world wide web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2013, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= '{field} harus diisi.';
$lang['form_validation_isset']			= '{field} harus memiliki nilai.';
$lang['form_validation_valid_email']	= '{field} harus memiliki alamat email yang benar.';
$lang['form_validation_valid_emails']	= '{field} harus memiliki alamat email yang benar.';
$lang['form_validation_valid_url']		= '{field} harus memiliki nilai URL yang benar.';
$lang['form_validation_valid_ip']		= '{field} harus memiliki nilai IP yang benar.';
$lang['form_validation_min_length']		= '{field} field must be at least {param} characters in length.';
$lang['form_validation_max_length']		= '{field} field cannot exceed {param} characters in length.';
$lang['form_validation_exact_length']	= '{field} field must be exactly {param} characters in length.';
$lang['form_validation_alpha']			= '{field} field may only contain alphabetical characters.';
$lang['form_validation_alpha_numeric']	= '{field} field may only contain alpha-numeric characters.';
$lang['form_validation_alpha_numeric_spaces']	= '{field} field may only contain alpha-numeric characters and spaces.';
$lang['form_validation_alpha_dash']		= '{field} field may only contain alpha-numeric characters, underscores, and dashes.';
$lang['form_validation_numeric']		= '{field} field must contain only numbers.';
$lang['form_validation_is_numeric']		= '{field} field must contain only numeric characters.';
$lang['form_validation_integer']		= '{field} field must contain an integer.';
$lang['form_validation_regex_match']	= '{field} field is not in the correct format.';
$lang['form_validation_matches']		= '{field} field does not match the {param} field.';
$lang['form_validation_differs']		= '{field} field must differ from the {param} field.';
$lang['form_validation_is_unique'] 		= '{field} field must contain a unique value.';
$lang['form_validation_is_natural']		= '{field} field must only contain digits.';
$lang['form_validation_is_natural_no_zero']	= '{field} field must only contain digits and must be greater than zero.';
$lang['form_validation_decimal']		= '{field} field must contain a decimal number.';
$lang['form_validation_less_than']		= '{field} field must contain a number less than {param}.';
$lang['form_validation_less_than_equal_to']	= '{field} field must contain a number less than or equal to {param}.';
$lang['form_validation_greater_than']		= '{field} field must contain a number greater than {param}.';
$lang['form_validation_greater_than_equal_to']	= '{field} field must contain a number greater than or equal to {param}.';

/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */